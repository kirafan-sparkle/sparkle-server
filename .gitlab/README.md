## .gitlab

This directory contains files that are used by GitLab CI/CD.

- ci
  - It contains CI/CD configuration files.
- issue_templates
  - It contains issue templates.
- merge_request_templates
  - It contains merge request templates.

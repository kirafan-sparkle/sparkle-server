## Dockerfile

These files are used to build the docker image for the container. The docker image is built using the following command:

```
# Go to top level directory
cd ../../
# Build the docker image
docker build -t sparkle-server-test-amd64 -f ./build/container/Dockerfile_amd64 .
docker build -t sparkle-server-test-arm64 -f ./build/container/Dockerfile_arm64 .
```

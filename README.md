## Sparkle-server

[![Golang](https://img.shields.io/badge/Go-00ADD8?style=flat&logo=go&logoColor=white)](https://go.dev)
[![VSCode](https://img.shields.io/badge/-Visual%20Studio%20Code-007ACC.svg?logo=visual-studio-code&style=flat)](https://code.visualstudio.com/)
[![AGPL](https://img.shields.io/github/license/ad-aures/castopod?color=blue)](https://www.gnu.org/licenses/agpl-3.0.en.html)
[![total coding time](https://wakatime.com/badge/user/4893740b-9141-4a49-aec6-eb82b6367166/project/a7859e86-9b9c-4c8b-afd8-cf6e3b2886e1.svg)](https://wakatime.com)

![image](./docs/icon.png)

**Let’s draw the continuation of the story with you**

Star-Api emulation server

## Progress

- [ ] Account
  - [x] Create
  - [x] Login
  - [x] Login bonus
  - [x] Transfer
  - [x] Stamina
  - [x] Profile
  - [x] Achievement
  - [x] Support party
  - [x] Battle party
  - [ ] Town party
  - [ ] Import data
    - [ ] Offline (broken)
    - [ ] Online
- [x] Weapon
  - [x] Make
  - [x] Upgrade
  - [x] Evolution
  - [x] Receive
  - [x] Sale
  - [x] SkillUpPowder
- [ ] Character
  - [x] Upgrade
  - [x] Limitbreak
  - [x] Evolution
  - [x] SetView
  - [x] ResetView
  - [x] SetShown
  - [ ] Ability board
- [ ] Room
  - [x] Buy/Sale/Set room objects
  - [x] Schedule (static)
  - [ ] Content room
- [x] Town
- [ ] Friend
  - [x] Search
  - [x] Request
  - [x] Accept
  - [x] Reject
  - [x] Delete
  - [x] SupportList (dynamic)
  - [ ] SupportList (static)
- [x] Training
  - [x] Init
  - [x] Start
  - [x] Complete
  - [x] Cancel
- [ ] Mission
  - [ ] Refresh
  - [ ] Progress
  - [ ] Complete
- [ ] Present
  - [ ] Insert
  - [x] Receive (initial)
  - [ ] Receive (basic)
  - [ ] Received list
- [ ] Trade
  - [x] Monthly trade
  - [x] Event trade
  - [x] Gem trade
  - [ ] Paid gem trade
  - [ ] Item sale
- [ ] Gacha
  - [x] Tutorial gacha
  - [x] Selectable gacha
  - [x] Normal gacha
    - [ ] Gacha gifts
    - [ ] Draw points
- [ ] Quest
  - [x] Core
    - [x] Start
    - [x] Save
    - [x] Complete
      - [x] Player Exp leveling
      - [x] Weapon Exp leveling
      - [x] Friendship Exp leveling
      - [x] Character Exp leveling
  - [x] Option
    - [x] Resume
    - [x] Retry
    - [x] Re-challenge
    - [x] Give-up
  - [ ] List
    - [x] Part1 quests
      - [x] Chapters
    - [x] Part2 quests
      - [x] Chapters
    - [x] Author quests
    - [x] Daily quests
    - [x] Weekend quests (challenge quests)
    - [x] Event quests
    - [ ] Memorial quests
    - [ ] Crea quests
- [ ] Event
  - [ ] Trade event
  - [ ] Box event
  - [ ] Point event
  - [ ] Map event
- [x] System
  - [x] App version
  - [x] Information
  - [x] Event banner
  - [x] Notifications

## Project goal

- Implement all endpoints of the game
  - All endpoints must respond in 3 seconds.

## Architecture

- Generator: openapi-generator-cli
- API framework: Golang net/http + mux.Router
- Architecture: Domain Driven Development
- Database: gorm(MariaDB / MySQL / SQLite)
- Logger: zap
- Observability: OpenTelemetry(Traces and logs) (Jaeger or Uptrace)

## Requirements (Development)

- VSCode
- Golang 1.21.6
- Docker (Optional)
- Air (Optional)

## Requirements (Production)

- MariaDB (or MySQL)
- Docker (Optional)

### Running the server (Development)

To run the server, follow these simple steps:

```
cp .env_example cmd/main/.env
(Write your .env)
go run cmd/main/main.go
```

If you need hot-reload, follow these steps:

```
Install Docker-Desktop
Open repository folder in VSCode
Run "Remote-Containers: Reopen in Container"
Run "air"
```

### Running the server (Production)

See [docker-compose.yml](./docker-compose.yml), or download binary from release page.

## Contributing

To contribute to the server, please follow these simple steps:

- Fork a code and make your repository.
  - This project doesn't allow direct push currently.
- Read the docs at some folders, and read issues about what we needs next.
  - This project uses DDD architecture, so you need to understand the how it works.
- Find the issue you want to do, and assign yourself.
  - Please contact to Dosugamea first if you have time.
  - It will helps to preventing file conflicts.
- Push your modified code to your private repository.
- Make a pull request to the original repository.
- Wait for the review and merge.
- After a approve, the code will be merged to the original repository.

## Contributors

- Server development
  - [Dosugamea](https://gitlab.com/Dosugamea)
  - [Nagisa Togakushi](https://gitlab.com/NagisaTogakushi)
  - [lihe](https://gitlab.com/lihe07)
  - Hiyori
  - yakisoba
- Data mining / Decryption
  - [Y52en](https://gitlab.com/Y52en)
  - [Youko](https://gitlab.com/Y2theK)
  - [CircleLin](https://gitlab.com/circlelin)
  - [Ayaya](https://gitlab.com/kirafan_autodec)
- Seed data provider
  - 【krfnRΤΑ 走者】全手動 ANDR○ID 最高 B○T (左テンキー)
- Launcher development
  - 250king
  - misaka10843
- Tester & suggestion
  - 炼铜魔导师
  - fibachocolate
  - AyayaRize
  - smallwu
  - ababababa_teaya
  - BAW の 772
  - HimahariNeru
  - aditya putra
- Thanks for all contributors. It couldn't done without their a lot of hard works.
  - (Of course, big thanks to original game developers!)

## FAQ

- Q. So, how to run the game?
  - A. You need the official assets before the service was shut down.
- Q. Where is database?
  - A. Please make it yourself.
- Q. Can I donate to this project?
  - A. ABSOLUTELY NO, NEVER. Please buy books or another game by the publishers. (Send me the screenshot if you like it)

## References

- Analysis
  - [Wiki](https://wiki.kirafan.moe)
  - [DB](https://gitlab.com/kirafan/database)
- Fan wiki
  - https://kirarafantasia.miraheze.org/wiki/Main_Page
  - https://wikiwiki.jp/kirarafan/
  - https://w.atwiki.jp/miottia/pages/59.html
- Other wiki
  - https://kirarafantasia.boom-app.wiki
- Big thanks to these websites. It couldn't done without these infos.

## Contact (DMCA)

- If you need to contact, please send to here.
  - [dsgamer777+sparkle@gmail.com](mailto:dsgamer777+sparkle@gmail.com)

## License

AGPL-3.0 (See [LICENSE](./LICENSE.md))

Basically you can use for your own purpose, but you need to publish code if you serve with change.
This license is for preventing commercial use.
The "commercial" includes ads, donations, youtube monetization, etc.

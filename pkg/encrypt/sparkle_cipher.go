package encrypt

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"hash/crc32"
)

type SparkleCipher struct {
	BLOCK_SIZE int
	key        []byte
	padding    string
}

type SparkleCipherRepository interface {
	GetStarCC(body interface{}, serverTime string) string
	getIV(path string) []byte
	formatDictAsSparkleStr(body interface{}) string
	DecryptAsRequest(path string, body string, res any) error
	EncryptAsResponse(path string, body interface{}) string
}

type Key string
type Pad string

func NewSparkleCipher(key Key, padding Pad) *SparkleCipher {
	keyBytes, err := hex.DecodeString(string(key))
	if (err) != nil {
		panic(err)
	}
	// エンコーダー設定
	return &SparkleCipher{
		BLOCK_SIZE: 16,
		key:        keyBytes,
		padding:    string(padding),
	}
}

func (k *SparkleCipher) getIV(path string) []byte {
	iv := []byte(path + k.padding)[:k.BLOCK_SIZE]
	return iv
}

func (k *SparkleCipher) GetStarCC(body interface{}, serverTime string) string {
	bodyForHash := k.formatDictAsSparkleBytes(body)
	serverTimeBytes := []byte(serverTime)
	hashBytes := append(bodyForHash, serverTimeBytes...)
	crc32Hash := crc32.ChecksumIEEE(hashBytes)
	return fmt.Sprintf("%x", crc32Hash)
}

func (k *SparkleCipher) GetStarCCByBytes(body []byte, serverTime string) string {
	serverTimeBytes := []byte(serverTime)
	hashBytes := append(body, serverTimeBytes...)
	crc32Hash := crc32.ChecksumIEEE(hashBytes)
	return fmt.Sprintf("%x", crc32Hash)
}

func (k *SparkleCipher) formatDictAsSparkleBytes(body interface{}) []byte {
	b, err := json.Marshal(body)
	if err != nil {
		panic(err)
	}
	return b
}

func (k *SparkleCipher) EncryptAsResponse(path string, body interface{}) []byte {
	iv := k.getIV(path)
	cipherBlock, err := aes.NewCipher(k.key)
	if err != nil {
		panic(err)
	}
	decompressedBytes := k.formatDictAsSparkleBytes(body)
	// NOTE: This zlib compressed bytes doesn't same as python zlib
	// https://stackoverflow.com/questions/74098712/zlib-difference-between-golang-and-python
	compressedBytes := compress(decompressedBytes)
	paddedBytes := pad(compressedBytes, k.BLOCK_SIZE)
	encryptedBytes := make([]byte, len(paddedBytes))
	mode := cipher.NewCBCEncrypter(cipherBlock, iv)
	mode.CryptBlocks(encryptedBytes, paddedBytes)
	return encryptedBytes
}

func (k *SparkleCipher) EncryptAsResponseByBytes(path string, data []byte) []byte {
	iv := k.getIV(path)
	cipherBlock, err := aes.NewCipher(k.key)
	if err != nil {
		panic(err)
	}
	// NOTE: This zlib compressed bytes doesn't same as python zlib
	// https://stackoverflow.com/questions/74098712/zlib-difference-between-golang-and-python
	compressedBytes := compress(data)
	paddedBytes := pad(compressedBytes, k.BLOCK_SIZE)
	encryptedBytes := make([]byte, len(paddedBytes))
	mode := cipher.NewCBCEncrypter(cipherBlock, iv)
	mode.CryptBlocks(encryptedBytes, paddedBytes)
	return encryptedBytes
}

func (k *SparkleCipher) DecryptAsRequest(path string, body string, result any) error {
	iv := k.getIV(path)
	cipherBlock, err := aes.NewCipher(k.key)
	if err != nil {
		return err
	}
	mode := cipher.NewCBCDecrypter(cipherBlock, iv)
	encryptedBytes, err := base64.StdEncoding.DecodeString(body)
	if err != nil {
		return err
	}
	decryptedBytes := make([]byte, len(encryptedBytes))
	mode.CryptBlocks(decryptedBytes, encryptedBytes)
	unPaddedBytes, err := unpad(decryptedBytes, k.BLOCK_SIZE)
	if err != nil {
		return err
	}
	decompressedBytes, err := decompress(unPaddedBytes)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(decompressedBytes, &result); err != nil {
		return err
	}
	return nil
}

func (k *SparkleCipher) DecryptAsRequestBytes(path string, body string) ([]byte, error) {
	iv := k.getIV(path)
	cipherBlock, err := aes.NewCipher(k.key)
	if err != nil {
		return nil, err
	}
	mode := cipher.NewCBCDecrypter(cipherBlock, iv)
	encryptedBytes, err := base64.StdEncoding.DecodeString(body)
	if err != nil {
		return nil, err
	}
	decryptedBytes := make([]byte, len(encryptedBytes))
	mode.CryptBlocks(decryptedBytes, encryptedBytes)
	unPaddedBytes, err := unpad(decryptedBytes, k.BLOCK_SIZE)
	if err != nil {
		return nil, err
	}
	decompressedBytes, err := decompress(unPaddedBytes)
	if err != nil {
		return nil, err
	}
	return decompressedBytes, nil
}

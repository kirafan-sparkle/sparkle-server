# pkg/encrypt

Encryption and decryption utilities for handle the app requests.
Probably they can be used at another repositories.

## compress.go

It has gzip utility functions.
The request and responses must use gzip.

## padding.go

It has the padding and un-padding utility functions.
The game encryption algorithm uses blocks of 16 bytes.

## sparkle_cipher.go

it has the core functions of encryption and decryptions for use at the app requests.
It uses compress and padding functions automatically.

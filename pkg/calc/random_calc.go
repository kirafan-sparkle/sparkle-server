package calc

import (
	"math/rand"
	"strings"
	"time"

	"github.com/lithammer/shortuuid/v3"
)

// GetSparkleRandomString returns a random string of length 10
func GetSparkleRandomString() string {
	return strings.ToUpper(shortuuid.New())[:10]
}

func GetSparkleRandomNumber() int64 {
	src := rand.NewSource(time.Now().UnixNano())
	r := rand.New(src)
	return r.Int63()
}

func CumulativeRandomChoice(weights []int64, seed *int64) int64 {
	if seed != nil {
		rand.NewSource(*seed)
	} else {
		rand.NewSource(time.Now().UnixNano())
	}
	var cumulativeWeights []int64
	var sum int64 = 0
	for _, w := range weights {
		sum += w
		cumulativeWeights = append(cumulativeWeights, sum)
	}
	r := rand.Int63n(sum) + 1
	var i int
	for i = 0; i < len(cumulativeWeights); i++ {
		if r <= cumulativeWeights[i] {
			break
		}
	}
	return int64(i)
}

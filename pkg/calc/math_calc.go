package calc

import (
	"github.com/jinzhu/copier"
	"golang.org/x/exp/constraints"
)

func Unique[T comparable](elems []T) []T {
	// Source: https://ema-hiro.hatenablog.com/entry/20170712/1499785693
	m := make(map[T]struct{})
	for _, ele := range elems {
		m[ele] = struct{}{}
	}

	uniq := []T{}
	for i := range m {
		uniq = append(uniq, i)
	}
	return uniq
}

func Filter[T any](elms []T, fn func(T) bool) []T {
	// Source: https://qiita.com/koh789/items/d787a00685693f80ec04#filter
	outputs := make([]T, 0)
	for _, elm := range elms {
		if fn(elm) {
			outputs = append(outputs, elm)
		}
	}
	return outputs
}

func Map[T, V any](elms []T, fn func(T) V) []V {
	// Source: https://qiita.com/koh789/items/d787a00685693f80ec04#map
	outputs := make([]V, len(elms), cap(elms))
	for i, elm := range elms {
		outputs[i] = fn(elm)
	}
	return outputs
}

func Contains[T comparable](elems []T, v T) bool {
	for _, s := range elems {
		if v == s {
			return true
		}
	}
	return false
}

func Max[T constraints.Ordered](a, b T) T {
	if a > b {
		return a
	}
	return b
}

func Min[T constraints.Ordered](a, b T) T {
	if a < b {
		return a
	}
	return b
}

func ToPtr[T constraints.Ordered](v T) *T {
	return &v
}

func Copy(dst interface{}, src interface{}) error {
	err := copier.Copy(dst, src)
	return err
}

// BoolAsInt converts a boolean value to an integer (1 for true, 0 for false).
func BoolAsInt(b bool) int64 {
	if b {
		return 1
	}
	return 0
}

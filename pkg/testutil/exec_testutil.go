package testutil

import (
	"errors"
	"fmt"

	"gorm.io/gorm"
)

type ExecutableQueryParam struct {
	sqlStr string
	args   []interface{}
}

func execQuery(db *gorm.DB, param ExecutableQueryParam) error {
	if tx := db.Exec(param.sqlStr, param.args...); tx.Error != nil {
		return tx.Error
	}
	return nil
}

func ToBeExist(db *gorm.DB, param ExecutableQueryParam) error {
	if err := execQuery(db, param); err != nil {
		return errors.New("record is not exist / expected=exist, actual=not exist")
	}
	return nil
}

func ToBeNotExist(db *gorm.DB, param ExecutableQueryParam) error {
	if err := execQuery(db, param); errors.Is(err, gorm.ErrRecordNotFound) {
		return fmt.Errorf("record is exist / expected=not exist, actual=exist")
	}
	return nil
}

func ToHaveLength(db *gorm.DB, param ExecutableQueryParam, expectedLength int64) error {
	if err := execQuery(db, param); err != nil {
		return err
	}
	if db.RowsAffected != expectedLength {
		return fmt.Errorf("expected length does not match / expected=%v, actual=%v", expectedLength, db.RowsAffected)
	}
	return nil
}

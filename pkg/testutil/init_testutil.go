package testutil

import (
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/migrate"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/seed"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func GetInMemoryDatabase() *gorm.DB {
	gormConfig := &gorm.Config{
		CreateBatchSize: 150,
	}
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), gormConfig)
	if err != nil {
		panic(err)
	}
	return db
}

func InitInMemoryDatabase(db *gorm.DB, options seed.SeedOption) {
	globalConf := env.LoadServerGlobalConfig()
	seedConf := globalConf.Seed

	migrate.AutoMigrate(db)
	seed.AutoSeed(db, &seedConf, options)
}

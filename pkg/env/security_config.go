package env

import (
	"fmt"
	"net"
	"os"
	"strings"

	"github.com/kelseyhightower/envconfig"
)

type EncryptKey string
type EncryptPad string
type CorsAllowOrigins string
type IpWhitelistRanges []*net.IPNet

type SecurityConfig struct {
	EncryptKey        EncryptKey        `envconfig:"ENCRYPT_KEY" default:""`
	EncryptPad        EncryptPad        `envconfig:"ENCRYPT_PAD" default:""`
	CorsAllowOrigins  CorsAllowOrigins  `envconfig:"CORS_ALLOW_ORIGINS" default:"*"`
	IpWhitelistRanges IpWhitelistRanges `envconfig:"IP_WHITELIST_RANGES"`
}

var defaultRanges = []*net.IPNet{
	{IP: net.IPv4(127, 0, 0, 1), Mask: net.IPv4Mask(255, 255, 255, 255)},
	{IP: net.IPv6loopback, Mask: net.CIDRMask(128, 128)},
}

func loadIpWhitelist() IpWhitelistRanges {
	whitelistEnv := os.Getenv("SECURITY_IP_WHITELIST_RANGES")
	if whitelistEnv == "" {
		return []*net.IPNet{}
	}
	whitelistRanges := strings.Split(whitelistEnv, ",")
	allowedRanges := make([]*net.IPNet, len(whitelistRanges)+len(defaultRanges))
	copy(allowedRanges, defaultRanges)

	for i, cidr := range whitelistRanges {
		_, ipNet, err := net.ParseCIDR(cidr)
		if err != nil {
			panic(err)
		}
		allowedRanges[i+len(defaultRanges)] = ipNet
	}
	return allowedRanges
}

func LoadSecurityConfig() SecurityConfig {
	var env SecurityConfig
	if err := envconfig.Process("SECURITY", &env); err != nil {
		panic(fmt.Sprintf("Failed to load security config: %v", err))
	}
	env.IpWhitelistRanges = loadIpWhitelist()
	return env
}

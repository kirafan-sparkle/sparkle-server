# pkg/env

This package provides a simple way to load environment variables from a `.env` file.
It build config instance from `os.Environ()` and `.env` file.

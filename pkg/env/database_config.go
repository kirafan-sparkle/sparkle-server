package env

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
)

const (
	DatabaseTypeSqlite   DatabaseType = "sqlite"
	DatabaseTypeMysql    DatabaseType = "mysql"
	DatabaseTypeInMemory DatabaseType = "in-memory"
)

type DatabaseType string
type DatabaseName string
type DatabaseHost string
type DatabasePort int64
type DatabaseUser string
type DatabasePass string

type DatabaseConfig struct {
	Type DatabaseType `envconfig:"TYPE" default:"sqlite"`
	Name DatabaseName `envconfig:"NAME" default:"sparkle-server.db"`
	Host DatabaseHost `envconfig:"HOST" default:"localhost"`
	Port DatabasePort `envconfig:"PORT" default:"3333"`
	User DatabaseUser `envconfig:"USER" default:"root"`
	Pass DatabasePass `envconfig:"PASS" default:"root"`
}

func LoadDatabaseConfig() DatabaseConfig {
	var env DatabaseConfig
	if err := envconfig.Process("DB", &env); err != nil {
		panic(fmt.Sprintf("Failed to load database config: %v", err))
	}
	return env
}

package env

import (
	"fmt"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

type ScheduleEndpoint string

type ServerGlobalConfig struct {
	Database         DatabaseConfig      `ignored:"true"`
	Observability    ObservabilityConfig `ignored:"true"`
	Security         SecurityConfig      `ignored:"true"`
	Flag             FlagConfig          `ignored:"true"`
	Seed             SeedConfig          `ignored:"true"`
	ScheduleEndpoint ScheduleEndpoint    `envconfig:"SCHEDULE_ENDPOINT" default:"local"`
}

func LoadServerGlobalConfig() ServerGlobalConfig {
	_ = godotenv.Overload()

	var config ServerGlobalConfig
	if err := envconfig.Process("", &config); err != nil {
		panic(fmt.Sprintf("Failed to load general config: %v", err))
	}

	config.Database = LoadDatabaseConfig()
	config.Observability = LoadObservabilityConfig()
	config.Flag = LoadFlagConfig()
	config.Security = LoadSecurityConfig()
	config.Seed = LoadSeedConfig()
	return config
}

func GetScheduleEndpoint(globalConfig *ServerGlobalConfig) ScheduleEndpoint {
	return globalConfig.ScheduleEndpoint
}

func GetEncryptKey(globalConfig *ServerGlobalConfig) *EncryptKey {
	return &globalConfig.Security.EncryptKey
}

func GetEncryptPad(globalConfig *ServerGlobalConfig) *EncryptPad {
	return &globalConfig.Security.EncryptPad
}

func GetCorsAllowedOrigins(globalConfig *ServerGlobalConfig) CorsAllowOrigins {
	return globalConfig.Security.CorsAllowOrigins
}

func GetIpWhitelistRanges(globalConfig *ServerGlobalConfig) *IpWhitelistRanges {
	return &globalConfig.Security.IpWhitelistRanges
}

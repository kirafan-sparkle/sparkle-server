package env

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
)

type SeedEndpoint string
type SeedSource string

type SeedConfig struct {
	Enabled  bool         `envconfig:"ENABLED" default:"true"`
	Source   SeedSource   `envconfig:"SOURCE" default:""`
	Endpoint SeedEndpoint `envconfig:"ENDPOINT" default:""`
}

func LoadSeedConfig() SeedConfig {
	var env SeedConfig
	if err := envconfig.Process("SEED", &env); err != nil {
		panic(fmt.Sprintf("Failed to load seed config: %v", err))
	}
	return env
}

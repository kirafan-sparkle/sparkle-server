package env

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
)

type FlagsmithProvider struct {
	EnvKey   string `envconfig:"ENV_KEY" default:""`
	Endpoint string `envconfig:"ENDPOINT" default:""`
}

type FlagProvider struct {
	Flagsmith FlagsmithProvider `ignored:"true"`
}

type FlagConfig struct {
	Enabled  bool         `envconfig:"ENABLED" default:"false"`
	EnvName  string       `envconfig:"ENV_NAME" default:"production"`
	Provider FlagProvider `ignored:"true"`
}

func LoadFlagConfig() FlagConfig {
	var env FlagConfig
	if err := envconfig.Process("FLAG", &env); err != nil {
		panic(fmt.Sprintf("Failed to load flag config: %v", err))
	}

	var flagsmithProvider FlagsmithProvider
	if err := envconfig.Process("FLAG_PROVIDER_FLAGSMITH", &flagsmithProvider); err != nil {
		panic(fmt.Sprintf("Failed to load flag config: %v", err))
	}

	env.Provider = FlagProvider{
		Flagsmith: flagsmithProvider,
	}
	return env
}

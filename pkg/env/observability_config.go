package env

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
)

type JaegerDsn string
type SentryDsn string
type UptraceDsn string
type CollectorDsn string

type ObservabilityTracerDsn struct {
	Jaeger    JaegerDsn    `envconfig:"JAEGER" default:""`
	Sentry    SentryDsn    `envconfig:"SENTRY" default:""`
	Uptrace   UptraceDsn   `envconfig:"UPTRACE" default:""`
	Collector CollectorDsn `envconfig:"COLLECTOR" default:""`
}

type ObservabilityMeterDsn struct {
	Uptrace   UptraceDsn   `envconfig:"UPTRACE" default:""`
	Collector CollectorDsn `envconfig:"COLLECTOR" default:""`
}

type LogLevel string

type ObservabilityConfig struct {
	Enabled   bool                   `envconfig:"ENABLED" default:"true"`
	EnvName   string                 `envconfig:"ENV_NAME" default:"production"`
	LogLevel  uint8                  `envconfig:"LOG_LEVEL" default:"1"`
	LogStdout bool                   `envconfig:"LOG_STDOUT" default:"false"`
	TracerDsn ObservabilityTracerDsn `ignored:"true"`
	MeterDsn  ObservabilityMeterDsn  `ignored:"true"`
}

func LoadObservabilityConfig() ObservabilityConfig {
	var tracerDsns ObservabilityTracerDsn
	if err := envconfig.Process("OTEL_TRACER_DSN", &tracerDsns); err != nil {
		panic(fmt.Sprintf("Failed to load observability tracer dsn config: %v", err))
	}
	var meterDsns ObservabilityMeterDsn
	if err := envconfig.Process("OTEL_METER_DSN", &meterDsns); err != nil {
		panic(fmt.Sprintf("Failed to load observability meter dsn config: %v", err))
	}
	var config ObservabilityConfig
	if err := envconfig.Process("OTEL", &config); err != nil {
		panic(fmt.Sprintf("Failed to load observability config: %v", err))
	}
	config.TracerDsn = tracerDsns
	config.MeterDsn = meterDsns
	return config
}

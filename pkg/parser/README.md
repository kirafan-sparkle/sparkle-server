# pkg/parser

## file_parser.go

It has ExportToFile function for use at tests.

## quest_request_parser.go

It has ParseCharacterExpString and ParseWeaponExpString functions for use at quest requests.

## shared_parser.go

It has ParseCommaStringAsInt64Array function for use at item consume requests.

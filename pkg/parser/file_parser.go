package parser

import (
	"bytes"
	"encoding/json"
	"os"
)

func ExportToFile(filename string, data interface{}) error {
	// Convert interface to []byte
	out, err := json.Marshal(data)
	if err != nil {
		return err
	}
	var indentOut bytes.Buffer
	err = json.Indent(&indentOut, out, "", "    ")
	if err != nil {
		return err
	}
	err = os.WriteFile(filename+".json", indentOut.Bytes(), 0644)
	if err != nil {
		return err
	}
	return nil
}

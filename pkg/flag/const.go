package flag

import "time"

// Flag name

type FlagName string

const (
	IsMaintenance FlagName = "is_maintenance"
)

// Cache duration

const (
	CacheDurationDefault = 3 * time.Minute
)

var CacheDurationDict = map[FlagName]time.Duration{
	IsMaintenance: 5 * time.Minute,
}

package flag

import (
	"context"
	"time"

	of "github.com/open-feature/go-sdk/openfeature"
)

type SparkleFlagClient struct {
	cl                    *of.Client
	lastRequestedTimeDict map[FlagName]time.Time
	cacheDurationDefault  time.Duration
	cacheDurationDict     map[FlagName]time.Duration
	cacheBoolDict         map[FlagName]bool
	cacheStringDict       map[FlagName]string
	cacheIntDict          map[FlagName]int64
}

func NewSparkleFlagClient(cl *of.Client) *SparkleFlagClient {
	return &SparkleFlagClient{
		cl:                    cl,
		lastRequestedTimeDict: make(map[FlagName]time.Time, len(CacheDurationDict)),
		cacheDurationDefault:  CacheDurationDefault,
		cacheDurationDict:     CacheDurationDict,
		cacheBoolDict:         make(map[FlagName]bool, len(CacheDurationDict)),
		cacheStringDict:       make(map[FlagName]string, len(CacheDurationDict)),
		cacheIntDict:          make(map[FlagName]int64, len(CacheDurationDict)),
	}
}

func (c *SparkleFlagClient) shouldRefreshCache(flagName FlagName) bool {
	lastRequestedTime, ok := c.lastRequestedTimeDict[flagName]
	if !ok {
		return true
	}
	if val, ok := c.cacheDurationDict[flagName]; ok {
		return time.Since(lastRequestedTime) > val
	}
	return time.Since(lastRequestedTime) > c.cacheDurationDefault
}

func (c *SparkleFlagClient) GetStringFlagValue(flagName FlagName, defaultValue string) (string, error) {
	if val, ok := c.cacheStringDict[flagName]; ok && !c.shouldRefreshCache(flagName) {
		return val, nil
	}
	newValue, err := c.cl.StringValue(context.Background(), string(flagName), defaultValue, of.EvaluationContext{})
	if err != nil {
		return "", err
	}
	c.cacheStringDict[flagName] = newValue
	c.lastRequestedTimeDict[flagName] = time.Now()
	return newValue, nil
}

func (c *SparkleFlagClient) GetBooleanFlagValue(flagName FlagName, defaultValue bool) (bool, error) {
	if val, ok := c.cacheBoolDict[flagName]; ok && !c.shouldRefreshCache(flagName) {
		return val, nil
	}
	newValue, err := c.cl.BooleanValue(context.Background(), string(flagName), defaultValue, of.EvaluationContext{})
	if err != nil {
		return false, err
	}
	c.cacheBoolDict[flagName] = newValue
	c.lastRequestedTimeDict[flagName] = time.Now()
	return newValue, nil
}

func (c *SparkleFlagClient) GetIntFlagValue(flagName FlagName, defaultValue int64) (int64, error) {
	if val, ok := c.cacheIntDict[flagName]; ok && !c.shouldRefreshCache(flagName) {
		return val, nil
	}
	newValue, err := c.cl.IntValue(context.Background(), string(flagName), defaultValue, of.EvaluationContext{})
	if err != nil {
		return 0, err
	}
	c.cacheIntDict[flagName] = newValue
	c.lastRequestedTimeDict[flagName] = time.Now()
	return newValue, nil
}

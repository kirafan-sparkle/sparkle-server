package response

import (
	_ "embed"
	"strings"
)

//go:embed resp_not_allowed_ip.txt
var NOT_ALLOWED_IP_MESSAGE string

func NewNotAllowedIpResponse() BaseResponse {
	return NewMaintenanceResponseWithMessage(NOT_ALLOWED_IP_MESSAGE)
}

//go:embed resp_account_not_found.txt
var ACCOUNT_NOT_FOUND_MESSAGE string

func NewAccountNotFoundResponse() BaseResponse {
	return NewErrorResponseWithMessage("Error", ACCOUNT_NOT_FOUND_MESSAGE)
}

//go:embed resp_in_maintenance.txt
var MAINTENANCE_MESSAGE string

func NewMaintenanceResponse() BaseResponse {
	return NewMaintenanceResponseWithMessage(MAINTENANCE_MESSAGE)
}

//go:embed resp_not_implemented.txt
var NOT_IMPLEMENTED_MESSAGE string

func NewNotImplementedResponse() BaseResponse {
	return NewMaintenanceResponseWithMessage(NOT_IMPLEMENTED_MESSAGE)
}

//go:embed resp_server_exploded.txt
var SERVER_EXPLODED_MESSAGE string

func NewServerExplodedResponse() BaseResponse {
	return NewMaintenanceResponseWithMessage(SERVER_EXPLODED_MESSAGE)
}

//go:embed resp_timeout.txt
var SERVER_TIMEOUT_MESSAGE string

func NewServerTimeoutResponse() BaseResponse {
	return NewMaintenanceResponseWithMessage(SERVER_TIMEOUT_MESSAGE)
}

//go:embed resp_invalid_request.txt
var INVALID_REQUEST_MESSAGE string

func NewInvalidRequestResponse(parameterName string) BaseResponse {
	message := strings.Replace(INVALID_REQUEST_MESSAGE, "[PARAMETER_NAME]", parameterName, 1)
	return NewMaintenanceResponseWithMessage(message)
}

//go:embed resp_payment_complete.txt
var PAYMENT_COMPLETE_MESSAGE string

func NewPaymentCompleteResponse() BaseResponse {
	return NewMaintenanceResponseWithMessage(PAYMENT_COMPLETE_MESSAGE)
}

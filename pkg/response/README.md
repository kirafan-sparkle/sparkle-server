# pkg/response

## response_message.go

It has various `ResponseMessage` constants and functions for make `Response` with `ResponseMessage`.

## response.go

It has `Response` type and `NewResponse` functions.

## result_code.go

It has `ResultCode` type and `ResultCode` constants.

## server_time.go

It has `NewSparkleTime` and `ToSparkleTime` functions.
They are needed for returning server time to client.

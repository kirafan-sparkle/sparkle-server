package response

const SERVER_VERSION = 2211181

type ResultCode int

const (
	RESULT_ERROR                             ResultCode = -1
	RESULT_SUCCESS                           ResultCode = 0   // (Nothing happens)
	RESULT_OUTDATED                          ResultCode = 1   // 最新のバージョンがあります。アプリを更新してください。 (Jump to store page)
	RESULT_APPLYING                          ResultCode = 2   // 申請中アプリバージョン
	RESULT_ACCESS_LIMITATION                 ResultCode = 3   // お使いの通信環境ではご利用頂けません。[エラーコード: 3] タイトルに戻ります。
	RESULT_TERMS_UPDATED                     ResultCode = 4   // 利用規約が更新されています。再度確認してください。
	RESULT_MAINTENANCE                       ResultCode = 10  // 現在メンテナンス注です。メンテナンス完了までお待ちください。
	RESULT_UNAVAILABLE                       ResultCode = 20  // この機能は現在使用できません。タイトルに戻ります。
	RESULT_OUTDATED_AB_VERSION               ResultCode = 30  // データの更新が必要です。タイトルに戻ります。
	RESULT_INVALID_REQUEST_HASH              ResultCode = 101 // 通信エラーが発生しました。[エラーコード: 101] タイトルに戻ります。
	RESULT_INVALID_PARAMETERS                ResultCode = 102 // 通信エラーが発生しました。[エラーコード: 102] タイトルに戻ります。
	RESULT_INSUFFICIENT_PARAMETERS           ResultCode = 103 // 通信エラーが発生しました。[エラーコード: 103] タイトルに戻ります。
	RESULT_INVALID_JSON_SCHEMA               ResultCode = 104 // 通信エラーが発生しました。[エラーコード: 104] タイトルに戻ります。
	RESULT_DB_ERROR                          ResultCode = 105 // サーバーが混雑しております。リトライします。
	RESULT_NG_WORD                           ResultCode = 106 // このテキストは使用できません。
	RESULT_ALREADY_PROCESSED                 ResultCode = 107 // 通信は処理済みです。データ更新のためタイトルに戻ります。
	RESULT_PLAYER_ALREADY_EXISTS             ResultCode = 201 // 通信エラーが発生しました。[エラーコード: 201]
	RESULT_PLAYER_NOT_FOUND                  ResultCode = 202 // (フレンド検索) 該当するユーザーは見つかりませんでした。
	RESULT_PLAYER_SESSION_EXPIRED            ResultCode = 203 // セッションの有効期限が切れています。再度ログインを行ってください。タイトルに戻ります。
	RESULT_PLAYER_SUSPENDED                  ResultCode = 204 // チュートリアル改修に伴い、現在のチュートリアル中断データはご利用いただけません。タイトル画面より「ユーザーデータ初期化」ボタンをタップし、ユーザーデータの初期化を行ってください。タイトルに戻ります。
	RESULT_PLAYER_STOPPED                    ResultCode = 205 // 停止中のユーザーです。タイトルに戻ります。
	RESULT_PLAYER_DELETED                    ResultCode = 206 // 削除済のユーザーです。タイトルに戻ります。(It deletes client user data)
	RESULT_INVALID_MOVE_PARAMETERS           ResultCode = 210 // 引き継ぎIDまたはパスワードが間違っています。
	RESULT_INVALID_LINK_PARAMETERS           ResultCode = 211 // ご指定のアカウントで連携したユーザーが見つかりません。
	RESULT_UUID_NOT_FOUND                    ResultCode = 212 // 引き継ぎが行われました。
	RESULT_INVALID_AUTH_VOIDED               ResultCode = 213 // 購入キャンセルを繰り返し行っていたことを確認致しました。
	RESULT_WEAPON_UPGRADE_LIMIT              ResultCode = 293 // ぶきの強化上限です。
	RESULT_CHARACTER_UPGRADE_LIMIT           ResultCode = 294 // キャラクターの強化上限です。
	RESULT_FACILITY_EXT_LIMIT                ResultCode = 295 // タウン施設の所持枠の拡張上限に達しています。
	RESULT_SUPPORT_CHAR_LIMIT                ResultCode = 296 // サポートキャラクター数の上限に達しています。
	RESULT_SUPPORT_LIMIT                     ResultCode = 297 // サポート編成数の上限に達しています。
	RESULT_ROOM_OBJECT_EXT_LIMIT             ResultCode = 298 // ルームオブジェクトの所持枠の拡張上限に達しています。
	RESULT_WEAPON_EXT_LIMIT                  ResultCode = 299 // ぶきの所持枠の拡張上限に達しています。
	RESULT_WEAPON_LIMIT                      ResultCode = 301 // ぶきの所持上限に達しています。
	RESULT_FACILITY_LIMIT                    ResultCode = 302 // タウン施設の所持上限に達しています。
	RESULT_ROOM_OBJECT_LIMIT                 ResultCode = 303 // ルームオブジェクトの所持上限に達しています。
	RESULT_CHARACTER_LIMIT                   ResultCode = 304 // キャラクターの所持上限に達しています。
	RESULT_ITEM_LIMIT                        ResultCode = 305 // アイテムの所持上限に達しています。
	RESULT_TOWN_LIMIT                        ResultCode = 306 // タウンの所持上限に達しています。
	RESULT_ROOM_LIMIT                        ResultCode = 307 // ルームの所持上限に達しています。
	RESULT_LIMIT_BREAK_LIMIT                 ResultCode = 308 // 限界突破の上限に達しています。
	RESULT_GACHA_DRAW_LIMIT                  ResultCode = 309 // 1日の上限に達しています。
	RESULT_FRIEND_LIMIT                      ResultCode = 310 // フレンド数の上限です。
	RESULT_FAVORITE_ITEM                     ResultCode = 311 // お気に入り登録されているアイテムです。
	RESULT_FAVORITE_WEAPON                   ResultCode = 312 // お気に入り登録されているぶきです。
	RESULT_FAVORITE_CHARACTER                ResultCode = 313 // お気に入り登録されているキャラクターです。
	RESULT_GACHA_STEP_DRAW_LIMIT             ResultCode = 314 // 召喚の上限に達しています。
	RESULT_GACHA_NOT_FREE_DRAW               ResultCode = 315 // 召喚データが更新されました。ホーム画面に戻ります。(It doesn't return to home)
	RESULT_GACHA_POSSIBLE_FREE_DRAW          ResultCode = 316 // 召喚データが更新されました。ホーム画面に戻ります。(It doesn't return to home)
	RESULT_WEAPON_SKILL_UP_LIMIT             ResultCode = 317 // ぶきのスキルアップ上限です。
	RESULT_ALREADY_OWN_CHARACTER             ResultCode = 331 // すでに所持しているキャラクターです。
	RESULT_ALREADY_LOAD_WEAPON               ResultCode = 332 // すでに装備しているぶきです。
	RESULT_ALREADY_OWN_ORB                   ResultCode = 333 // すでに所持しているオーブです。
	RESULT_FRIEND_PROPOSE_LIMIT              ResultCode = 334 // フレンド申請数の上限です
	RESULT_CHEST_OUT_OF_PERIOD               ResultCode = 351 // 宝箱の交換期間の範囲外です。
	RESULT_CHEST_CAN_NOT_RESET               ResultCode = 352 // 宝箱をリセットできません。
	RESULT_CHEST_EMPTY_REWARD                ResultCode = 353 // 宝箱の報酬が存在しません。
	RESULT_FRIEND_LIMIT_TARGET               ResultCode = 354 // 相手のフレンド申請数が上限です。
	RESULT_GOLD_IS_SHORT                     ResultCode = 441 // コインが不足しています。
	RESULT_ITEM_IS_SHORT                     ResultCode = 442 // 所持アイテムが不足しています。
	RESULT_UNLIMITED_GEM_IS_SHORT            ResultCode = 443 // 有償の星彩石が不足しています。
	RESULT_LIMITED_GEM_IS_SHORT              ResultCode = 444 // 星彩石が不足しています。
	RESULT_GEM_IS_SHORT                      ResultCode = 445 // 星彩石が不足しています。
	RESULT_KIRARA_IS_SHORT                   ResultCode = 446 // きららポイントが不足しています。
	RESULT_KIRARA_LIMIT                      ResultCode = 447 // きららポイントの上限です。
	RESULT_ITEM_IS_EXPIRED                   ResultCode = 448 // アイテムの使用期限が過ぎています。
	RESULT_STAMINA_IS_SHORT                  ResultCode = 451 // スタミナが不足しています。
	RESULT_COST_IS_SHORT                     ResultCode = 452 // コストが不足しています。
	RESULT_QUEST_OUT_OF_PERIOD               ResultCode = 461 // クエスト開催期間の範囲外です。
	RESULT_GACHA_OUT_OF_PERIOD               ResultCode = 462 // 召喚の期間が過ぎています。
	RESULT_TRADE_OUT_OF_PERIOD               ResultCode = 463 // アイテム購入期間の範囲外です。
	RESULT_BUY_OUT_OF_PERIOD                 ResultCode = 464 // 購入期間の範囲外です。
	RESULT_QUEST_RETRY_LIMIT                 ResultCode = 465 // リトライ制限のあるクエストです。
	RESULT_QUEST_ORDER_LIMIT                 ResultCode = 466 // 1日の上限に達しています。
	RESULT_BARGAIN_OUT_OF_PERIOD             ResultCode = 467 // バーゲン期間外です。
	RESULT_PRESENT_OUT_OF_PERIOD             ResultCode = 468 // プレゼントの受け取り期間外です。
	RESULT_MORE_STAMINA_NEEDED_THAN_EXPECTED ResultCode = 469 // 指定されたスタミナよりも多くのスタミナ消費が必要です。
	RESULT_NOT_YET_FRIEND                    ResultCode = 501 // フレンドではありません。
	RESULT_ALREADY_FRIEND                    ResultCode = 502 // すでにフレンドです。
	RESULT_FRIEND_NOT_REQUEST                ResultCode = 503 // フレンド申請がされていません。
	RESULT_ALREADY_COMPLETE_MISSION          ResultCode = 510 // すでに完了しています。
	RESULT_ALREADY_FRIEND_REQUEST            ResultCode = 511 // すでに申請しています。
	RESULT_ALREADY_FRIEND_REQUEST_RECEIVED   ResultCode = 512 // すでに相手から申請されています。
	RESULT_CURL_CONNECTION_TIMEOUT           ResultCode = 601 // cURL 通信エラー
	RESULT_CURL_CONNECTION_ERROR             ResultCode = 602 // cURL タイムアウト
	RESULT_APP_STORE_ERROR                   ResultCode = 603 // 課金エラー(App Store)
	RESULT_GOOGLE_PLAY_ERROR                 ResultCode = 604 // 課金エラー(GooglePlay)
	RESULT_APP_STORE_INVALID_RECEIPT         ResultCode = 605 // レシートの検証に失敗しました。
	RESULT_GOOGLE_PLAY_INVALID_STATUS        ResultCode = 606 // 購入の検証に失敗しました。
	RESULT_STORE_ITEM_NOT_FOUND              ResultCode = 607 // 商品が存在しません。
	RESULT_STORE_ALREADY_PURCHASED           ResultCode = 608 // すでに購入済みの商品です。
	RESULT_PURCHASE_GEM_OVERFLOW             ResultCode = 609 // 商品の購入上限です。
	RESULT_EXCHANGE_SHOP_OUT_OF_PERIOD       ResultCode = 651 // 星彩石(有償)交換の提供期間外です。
	RESULT_EXCHANGE_SHOP_LIMIT               ResultCode = 652 // 星彩石(有償)交換の交換上限数を超えています。
	RESULT_TRAINING_COMPLETE_TIME_ERROR      ResultCode = 701 // トレーニングの終了時刻ではありません。
	RESULT_TRAINING_ALREADY_COMPLETED        ResultCode = 702 // すでにこのトレーニングは完了しています。
	RESULT_TRAINING_OUT_OF_PERIOD            ResultCode = 703 // トレーニング開催期間の範囲外です。
	RESULT_PROMOTION_CODE_NOT_FOUND          ResultCode = 801 // コードが存在しません。
	RESULT_PROMOTION_CODE_ALREADY_USED       ResultCode = 802 // コードは使用済みです。
	RESULT_PROMOTION_CODE_OF_SAME_CAMPAIGN   ResultCode = 803 // 同じキャンペーンのコードはこれ以上使用できません。
	RESULT_PROMOTION_CAMPAIGN_NOT_OPEN       ResultCode = 804 // キャンペーン期間外です。
	RESULT_PROMOTION_CAMPAIGN_ALREADY_OPEN   ResultCode = 805 // テスト用のコードです。
	RESULT_UNKNOWN_ERROR                     ResultCode = 999 // エラーが発生しました。タイトルに戻ります。 [コード: 999]
	// Not in list: 通信エラーが発生しました。 (Number)
)

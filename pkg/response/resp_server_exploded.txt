<size=40>サーバーが爆発しました / Server exploded / 服务器爆炸了</size>

想定外のエラーが発生しました。お手数おかけしますが、バグ報告をお願いします。
Unexpected error occurred at server. Sorry for inconvenience, but please report this bug.
服务器发生了意料之外的错误。很抱歉给您带来不便，请报告此错误。

https://gitlab.com/kirafan/sparkle/server/-/issues/new
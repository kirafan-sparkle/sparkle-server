package errwrap

import (
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type SparkleError struct {
	/* response result code */
	code response.ResultCode
	/* response message */
	respMessage string
	/* internal error */
	err error
}

func NewSparkleError(code response.ResultCode, errMessage string) *SparkleError {
	err := errors.New(errMessage)
	return &SparkleError{code: code, err: err}
}

func NewSparkleErrorWithCustomError(code response.ResultCode, err error) *SparkleError {
	return &SparkleError{code: code, err: err}
}

func (e *SparkleError) WrapError(message string) {
	e.err = errors.Wrap(e.err, message)
}

func (e *SparkleError) Error() string {
	return e.err.Error()
}

func (e *SparkleError) GetRawError() error {
	return e.err
}

func (e *SparkleError) SetResponseMessage(respMessage string) {
	e.respMessage = respMessage
}

func (e *SparkleError) AsResponse() response.BaseResponse {
	if e.respMessage != "" {
		messages := []string{
			"<size=30>エラーが発生しました / Something went wrong / 出现了错误</size>",
			"",
			"Reason: " + e.respMessage,
			"",
			"エラーが発生しました。",
			"再度試してみても問題が解決しない場合は、報告をお願いします。",
			"An unexpected error occurred on our server.",
			"If the problem persists, please report it.",
			"服务器发生了意料之外的错误。",
			"如果问题仍然存在，请报告。",
			"https://gitlab.com/kirafan/sparkle/server/-/issues/new",
		}
		return response.NewMaintenanceResponseWithMessage(strings.Join(messages, "\n"))
	}
	return response.NewErrorResponse(e.code)
}

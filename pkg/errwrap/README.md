# pkg/errwrap

This package has a simple function to wrap errors with a error status code.
Sometimes the application/service needs to return an error with a status code for the interface layer, so this is needed.

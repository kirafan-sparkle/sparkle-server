# pkg/gacha

## const.go

Probability of each rarity is defined here.

## roll_rarity.go

The roll function for rarity is defined here.

## roll_character.go

The roll function for character is defined here.

## gacha.go

The main function for gacha is defined here.
It uses the roll functions defined in `roll_rarity.go` and `roll_character.go` with uses constants in `const.go`.

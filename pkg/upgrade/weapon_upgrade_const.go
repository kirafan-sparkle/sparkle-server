package upgrade

type WeaponUpgradeProbability uint8

const (
	WeaponUpgradeGoodProb    WeaponUpgradeProbability = 80
	WeaponUpgradeGreatProb   WeaponUpgradeProbability = 15
	WeaponUpgradePerfectProb WeaponUpgradeProbability = 5
)

type WeaponUpgradeResult uint8

const (
	WeaponUpgradeResultGood WeaponUpgradeResult = iota
	WeaponUpgradeResultGreat
	WeaponUpgradeResultPerfect
)

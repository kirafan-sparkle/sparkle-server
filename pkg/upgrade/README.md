# pkg/upgrade

## character_upgrade.go (character_upgrade_const.go)

The file is for calculate character upgrade exp amount at `UpgradePlayerCharacter` endpoint.

## weapon_upgrade.go (weapon_upgrade_const.go)

The file is for calculate weapon upgrade exp amount at `UpgradeWeapon` endpoint.

package upgrade

type CharacterUpgradeProbability uint8

const (
	CharacterUpgradeGoodProb    CharacterUpgradeProbability = 80
	CharacterUpgradeGreatProb   CharacterUpgradeProbability = 15
	CharacterUpgradePerfectProb CharacterUpgradeProbability = 5
)

type CharacterUpgradeResult uint8

const (
	CharacterUpgradeResultGood CharacterUpgradeResult = iota
	CharacterUpgradeResultGreat
	CharacterUpgradeResultPerfect
)

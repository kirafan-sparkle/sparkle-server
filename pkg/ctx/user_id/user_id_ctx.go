package user_id

import (
	"context"

	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

// contextKey is context key for getting userId
type contextKey struct{}

// WithContext sets a requested user's id to context
func WithContext(ctx context.Context, userId value_user.UserId) context.Context {
	return context.WithValue(ctx, contextKey{}, userId)
}

// FromContext gets a requested user's id from context
func FromContext(ctx context.Context) (value_user.UserId, bool) {
	userId, valid := ctx.Value(contextKey{}).(value_user.UserId)
	return userId, valid
}

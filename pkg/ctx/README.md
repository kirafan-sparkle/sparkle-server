# pkg/ctx

This package contains context utilities.
Sub packages can inject value to context and get it from context.

## References

- なぜ Go ではロガーをコンストラクタ DI してはならないのか
  - https://zenn.dev/mpyw/articles/go-dont-inject-logger#%E3%82%B3%E3%83%B3%E3%83%86%E3%82%AD%E3%82%B9%E3%83%88%E3%81%AF%E3%82%A4%E3%83%9F%E3%83%A5%E3%83%BC%E3%82%BF%E3%83%96%E3%83%AB%E3%81%A7%E3%81%82%E3%82%8B
- Go の context.Context に入れる値をリクエストスコープに限る理由
  - https://moneyforward-dev.jp/entry/2020/07/28/go-context/
- golang context の使い方とか概念(context とは)的な話
  - https://qiita.com/marnie_ms4/items/985d67c4c1b29e11fffc

package logger

import (
	"context"

	"github.com/uptrace/opentelemetry-go-extra/otelzap"
)

// contextKey is context key for getting logger
type contextKey struct{}

// WithContext sets a logger to context
func WithContext(ctx context.Context, logger *otelzap.Logger) context.Context {
	return context.WithValue(ctx, contextKey{}, logger)
}

// FromContext gets a logger from context
func FromContext(ctx context.Context) *otelzap.Logger {
	logger, ok := ctx.Value(contextKey{}).(*otelzap.Logger)
	if !ok {
		return nil
	}
	return logger
}

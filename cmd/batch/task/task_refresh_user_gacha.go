package task

import (
	"context"

	batch_repository "gitlab.com/kirafan/sparkle/server/cmd/batch/repository"
	"gitlab.com/kirafan/sparkle/server/internal/application/service"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	domain_repository "gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gorm.io/gorm"
)

type TaskRefreshUserGacha struct {
	name string
	db   *gorm.DB
	uu   usecase.UserUsecase
	ugs  service.PlayerGachaService
}

func NewTaskRefreshUserGacha(db *gorm.DB, uu usecase.UserUsecase, ugs service.PlayerGachaService) batch_repository.TaskRepository {
	name := "refresh_user_gacha"
	return &TaskRefreshUserGacha{name, db, uu, ugs}
}

func (s *TaskRefreshUserGacha) GetName() string {
	return s.name
}

func (s *TaskRefreshUserGacha) Run() error {
	userIds, err := s.GetUserIds()
	if err != nil {
		return err
	}
	for _, userId := range userIds {
		ctx := context.Background()
		user, err := s.ugs.RefreshPlayerGacha(ctx, userId)
		if err != nil {
			return err
		}
		_, err = s.uu.UpdateUser(ctx, user, domain_repository.UserRepositoryParam{
			Gachas: true,
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *TaskRefreshUserGacha) GetUserIds() ([]value_user.UserId, error) {
	var userIds []value_user.UserId
	if err := s.db.Model(&model_user.User{}).Pluck("id", &userIds).Error; err != nil {
		return nil, err
	}
	return userIds, nil
}

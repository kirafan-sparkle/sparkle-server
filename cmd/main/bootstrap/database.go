package bootstrap

import (
	"context"

	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
)

func GetDatabase(ctx context.Context, logger *otelzap.Logger, conf *env.DatabaseConfig) *gorm.DB {
	if logger == nil {
		panic("logger is nil")
	}
	if conf == nil {
		panic("database config is nil")
	}

	db := database.GetDatabaseWithObservability(logger, conf)
	return db
}

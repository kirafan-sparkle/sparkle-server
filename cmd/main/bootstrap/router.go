package bootstrap

import (
	"context"
	"net/http"

	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/internal/registry"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
)

func CreateRouter(ctx context.Context, logger *otelzap.Logger, db *gorm.DB, serverGlobalConf *env.ServerGlobalConfig) http.Handler {
	if logger == nil {
		panic("logger is nil")
	}
	if db == nil {
		panic("db is nil")
	}
	if serverGlobalConf == nil {
		panic("serverGlobalConf is nil")
	}

	// Create gorm and logger repo instance
	router := registry.InitializeRouter(logger, db, serverGlobalConf)

	// Serve Index
	res := []byte("Sparkle API Server is working")
	router.Path("/").Handler(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(200)
			w.Write(res)
		}),
	)

	wrappedRouter := observability.InjectObservabilityToRouter(router)
	return wrappedRouter
}

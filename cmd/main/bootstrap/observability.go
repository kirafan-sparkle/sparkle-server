package bootstrap

import (
	"context"

	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability/meter_provider"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability/tracer_provider"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
)

type ObservabilityResults struct {
	Logger                *otelzap.Logger
	TracerShutdownHandler func(context.Context) error
	MeterShutdownHandler  func(context.Context) error
}

// Initialize openTelemetry observability
func GetObservability(ctx context.Context, conf *env.ObservabilityConfig) ObservabilityResults {
	if conf == nil {
		panic("observability config is nil")
	}

	// Init logger
	resp := ObservabilityResults{
		Logger: observability.InitLogger(conf.LogLevel),
	}

	if !conf.Enabled {
		return resp
	}

	// Init tracer
	otelResource := observability.GetOtelResource(ctx, conf.EnvName)
	if conf.TracerDsn.Uptrace != "" {
		resp.TracerShutdownHandler = tracer_provider.InitUpTraceTracer(ctx, string(conf.TracerDsn.Uptrace), otelResource)
	} else if conf.TracerDsn.Jaeger != "" {
		resp.TracerShutdownHandler = tracer_provider.InitJaegerTracer(ctx, string(conf.TracerDsn.Jaeger), otelResource, conf.EnvName == "local")
	} else if conf.TracerDsn.Sentry != "" {
		tracer_provider.InitSentryTracer(string(conf.TracerDsn.Sentry), conf.EnvName == "local")
	} else if conf.TracerDsn.Collector != "" {
		// tracerShutdownHandler = tracer_provider.InitCollectorTracer(ctx, string(conf.TracerDsn.Collector), otelResource)
		panic("tracerDsn collector is not implemented")
	} else {
		tracer_provider.InitStdoutTracer(ctx, otelResource)
	}

	// Init meter
	if conf.MeterDsn.Uptrace != "" {
		meter_provider.InitUpTraceMeter(ctx, string(conf.MeterDsn.Uptrace), otelResource)
	} else if conf.MeterDsn.Collector != "" {
		// meterShutdownHandler = tracer_provider.InitCollectorMeter(ctx, string(conf.MeterDsn.Collector), otelResource)
		panic("meterDsn collector is not implemented")
	}

	return resp
}

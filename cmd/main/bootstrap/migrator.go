package bootstrap

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/migrate"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/seed"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
)

func RunMigration(ctx context.Context, db *gorm.DB, conf *env.SeedConfig) {
	if db == nil {
		panic("db is nil")
	}
	if conf == nil {
		panic("seed config is nil")
	}

	if !conf.Enabled {
		return
	}
	migrate.AutoMigrate(db)
	if conf.Source != "" {
		options := seed.NewFullSeedOption()
		seed.AutoSeed(db, conf, options)
	}
}

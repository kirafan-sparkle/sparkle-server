package bootstrap

import (
	"context"

	of "github.com/open-feature/go-sdk/openfeature"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/flag"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/flag/flag_provider"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
)

func InitSparkleFlagClient(ctx context.Context, flagConfig *env.FlagConfig) {
	if flagConfig == nil {
		panic("flag config is nil")
	}

	var provider of.FeatureProvider
	if !flagConfig.Enabled || flagConfig.Provider.Flagsmith.EnvKey == "" {
		provider = flag_provider.GetNopProvider()
	} else {
		provider = flag_provider.GetFlagsmithClient(
			flagConfig.Provider.Flagsmith.EnvKey,
			flagConfig.Provider.Flagsmith.Endpoint,
		)
	}
	of.SetProvider(provider)
	ofClient := of.NewClient("api-server")

	flag.InitSparkleFlagClient(ofClient)
}

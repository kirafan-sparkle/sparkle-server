/*
 * SparkleAPI
 * "Our tomorrow is always a prologue"
 *
 * Contact: contact@sparklefantasia.com
 */

package main

import (
	"context"
	"net/http"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/kirafan/sparkle/server/cmd/main/bootstrap"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
)

func main() {
	// Create cli app
	app := cli.NewApp()
	app.Name = "sparkle-api"
	app.Usage = "This executable starts sparkle-api server at port 8080"
	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:  "migrate",
			Usage: "run migration and seed (default false)",
		},
	}

	// Run cli app
	app.Action = func(c *cli.Context) error {
		ctx := context.Background()
		conf := env.LoadServerGlobalConfig()

		observability := bootstrap.GetObservability(ctx, &conf.Observability)
		logger := observability.Logger

		db := bootstrap.GetDatabase(ctx, logger, &conf.Database)
		router := bootstrap.CreateRouter(ctx, logger, db, &conf)
		bootstrap.RunMigration(ctx, db, &conf.Seed)

		bootstrap.InitSparkleFlagClient(ctx, &conf.Flag)

		if observability.MeterShutdownHandler != nil {
			defer observability.MeterShutdownHandler(ctx)
		}
		if observability.TracerShutdownHandler != nil {
			defer observability.TracerShutdownHandler(ctx)
		}

		logger.Info("=======================================================")
		logger.Info("Sparkle API Server")
		logger.Info("Our tomorrow is always a prologue")
		logger.Info("")
		logger.Info("Brought to you by Sparkle team")
		logger.Info("Contact: contact@sparklefantasia.com")
		logger.Info("")
		logger.Info("THIS IS FREE SOFTWARE. BEWARE OF SCAMMERS.")
		logger.Info("If you bought this software, request refund immediately.")
		logger.Info("=======================================================")
		logger.Info("")
		logger.Info("Server is running on port 8080")
		err := http.ListenAndServe("0.0.0.0:8080", router)
		logger.Fatal(err.Error())
		return nil
	}
	app.Run(os.Args)
}

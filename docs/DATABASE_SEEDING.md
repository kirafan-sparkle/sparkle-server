# Database seeding
This api supports database seeding.
We heavily recommend to use this feature before initial api startup.
(It is almost impossible to play with write them by hands.)

## How to use
- 1 Make a `assets` folder at correct place.
  - For build from source (`go run cmd/main/main.go`)
    - `cmd/main/assets`
      - same folder as `main.go`
  - For prebuilt binaries
    - `assets`
      - same folder as binary
- 2 Put seed files into `assets` folder.
- 3 Change environment values (`.env` file) as below.
  - `SEED_ENABLED: true`
  - `SEED_SOURCE: local`
- 4 Startup this api, it automatically runs database seeding at launch.

## Supported asset files
Currently, below seed files are supported.

- chapters.json
- character_quests.json
- characters.json
- event_quest_periods.json
- event_quests.json
- evo_table_evolution.json
- evo_table_limit_break.json
- evo_table_weapon.json
- exp_table_characters.json
- exp_table_friendships.json
- exp_table_ranks.json
- exp_table_skills.json
- exp_table_weapons.json
- gacha_tables.json
- gachas.json
- item_table_town_facilities.json
- items.json
- level_table_town_facilities.json
- login_bonuses.json
- missions.json
- named_types.json
- offers.json
- package_item_contents.json
- package_item_list.json
- presents.json
- quest_stamina_reductions.json
- quests.json
- room_objects.json
- town_facilities.json
- trade_recipes.json
- wave_drop_list.json
- wave_list.json
- wave_random_list.json
- weapon_character_tables.json
- weapon_recipes.json
- weapons.json

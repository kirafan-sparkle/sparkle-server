## Wire

DDD project has much files and lazy to inject every dependencies by hands.
So we uses google/wire to fulfill the dependencies.

### How to use

- Make a terminal at the root of this project
- Run `go install github.com/google/wire/cmd/wire@latest` (If you haven't ever installed)
- Run `wire ./internal/registry`
- Run `wire ./internal/application/service`
- Now you'll get below files:
  - registry/wire_gen.go
  - application/service/wire_gen.go

### References

- [google/wire を使って Go で DI(dependency injection)してみる](https://www.asobou.co.jp/blog/web/google-wire)
- [google/wire Tutorial (Official)](https://github.com/google/wire/blob/main/_tutorial/README.md)

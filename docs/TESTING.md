## Testing

This doc describes how to run the test codes.

### Tested folders

There are some test codes at below folders.

- application/service
- application/usecase
- domain/model
- infrastructure/middleware
- interfaces
- util
- util/encrypt

### Configuration of test environment

In case you want to run the test codes, you need to put `.env` file to some folders since most of tests requires database seedings.
The .env required folders and .env configuration are below.

#### .env body

```env
SEED_SOURCE=(remote or local)
SEED_ENDPOINT=(S3 storage endpoint)
DB_TYPE=('in-memory' for CI, or'sqlite3' for debugging)
DB_NAME=develop.db
DB_VERBOSE=1
```

#### Paths

- application/service/.env
- application/usecase/.env
- interfaces/.env

#### To make faster test

We uses remote seeding for CI but it takes time to download the seed data.
If you want to make faster test, you can use local file seeding.

1. Download the seed data from S3 storage.
2. Put the seed data to `/assets` folder.
3. Change the `SEED_SOURCE` to `local` in `.env` file.
4. Make symbolic link to the seed data per a testing folder to `/assets` folder.

- Windows (Requires Administrator permission)
  - `mklink /d assets <target folder>`
- Linux / Mac
  - `ln -s <target folder> assets`
- Example
  - Repository/application/usecase/assets -> Repository/assets
  - Repository/application/service/assets -> Repository/assets
  - Repository/application/interfaces/assets -> Repository/assets

package repository

import (
	"context"

	model_trade "gitlab.com/kirafan/sparkle/server/internal/domain/model/trade"
)

type TradeRecipeRepository interface {
	GetTradeRecipes(ctx context.Context) ([]model_trade.TradeRecipe, error)
}

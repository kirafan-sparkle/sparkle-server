package repository

import (
	"context"

	model_character "gitlab.com/kirafan/sparkle/server/internal/domain/model/character"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
)

type CharacterRepository interface {
	FindCharacter(ctx context.Context, query *model_character.Character, criteria map[string]interface{}, associations *[]string) (*model_character.Character, error)
	FindCharacters(ctx context.Context, query *model_character.Character, criteria map[string]interface{}, associations *[]string) ([]*model_character.Character, error)
	GetNamedTypesByIds(ctx context.Context, characterIds []value_character.CharacterId) ([]uint16, error)
}

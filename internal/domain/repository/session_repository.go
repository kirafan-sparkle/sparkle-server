package repository

import (
	"context"

	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type SessionRepository interface {
	Validate(ctx context.Context, session string) (value_user.UserId, error)
}

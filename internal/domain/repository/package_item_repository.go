package repository

import (
	"context"

	model_trade "gitlab.com/kirafan/sparkle/server/internal/domain/model/trade"
)

type PackageItemRepository interface {
	FindByPK(ctx context.Context, packageId uint) (*model_trade.PackageItem, error)
	GetAll(ctx context.Context) ([]*model_trade.PackageItem, error)
}

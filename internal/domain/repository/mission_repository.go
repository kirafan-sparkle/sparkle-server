package repository

import (
	"context"

	model_mission "gitlab.com/kirafan/sparkle/server/internal/domain/model/mission"
)

type MissionRepository interface {
	FindMission(ctx context.Context, query *model_mission.Mission, criteria map[string]interface{}, associations []string) (*model_mission.Mission, error)
	FindMissions(ctx context.Context, query *model_mission.Mission, criteria map[string]interface{}, associations []string) ([]*model_mission.Mission, error)
}

package repository

import (
	"context"

	model_room_object "gitlab.com/kirafan/sparkle/server/internal/domain/model/room_object"
)

type RoomObjectRepository interface {
	FindRoomObject(ctx context.Context, roomObjectId uint) (*model_room_object.RoomObject, error)
	FindRoomObjects(ctx context.Context, query *model_room_object.RoomObject, criteria map[string]interface{}, associations *[]string) ([]*model_room_object.RoomObject, error)
}

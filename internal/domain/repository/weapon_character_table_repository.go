package repository

import (
	"context"

	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
)

type WeaponCharacterTableRepository interface {
	FindByCharacterId(ctx context.Context, characterId value_character.CharacterId) (*model_weapon.WeaponCharacterTable, error)
}

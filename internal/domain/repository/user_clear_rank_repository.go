package repository

import (
	"context"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type UserClearRankRepository interface {
	UpsertClearRank(ctx context.Context, userId value_user.UserId, clearRank model_user.UserQuestClearRank) error
	GetClearRanks(ctx context.Context, userId value_user.UserId) (model_user.UserQuestClearRanks, error)
}

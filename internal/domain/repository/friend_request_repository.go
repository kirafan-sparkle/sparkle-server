package repository

import (
	"context"

	model_friend "gitlab.com/kirafan/sparkle/server/internal/domain/model/friend"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
)

type FriendRequestRepository interface {
	/* Check if a friend request exists between two players */
	IsExistFriendRequest(ctx context.Context, srcUserId value_friend.PlayerId, dstUserId value_friend.PlayerId) (bool, error)
	/* Add a friend request */
	AddFriendRequest(ctx context.Context, friendRequest model_friend.FriendRequest) (*value_friend.ManagedFriendId, error)
	/* Delete a friend request */
	DeleteFriendRequest(ctx context.Context, managedFriendId value_friend.ManagedFriendId) error
	/* Get a friend request by managedFriendId */
	GetFriendRequest(ctx context.Context, managedFriendId value_friend.ManagedFriendId) (*model_friend.FriendRequest, error)
	/* Get outgoing friend requests */
	GetOutGoingFriendRequests(ctx context.Context, userId value_friend.PlayerId) ([]*model_friend.FriendRequest, error)
	/* Get incoming friend requests */
	GetInComingFriendRequests(ctx context.Context, userId value_friend.PlayerId) ([]*model_friend.FriendRequest, error)
}

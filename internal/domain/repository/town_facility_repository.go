package repository

import (
	"context"

	model_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/model/town_facility"
)

type TownFacilityRepository interface {
	FindTownFacility(ctx context.Context, facilityId uint32) (*model_town_facility.TownFacility, error)
}

package repository

import (
	"context"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_ability_board "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_board"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type ManagedAbilityBoardRepository interface {
	IsAbilityBoardExist(ctx context.Context, userId value_user.UserId, abilityBoardId value_ability_board.AbilityBoardId) (bool, error)
	FindManagedAbilityBoard(ctx context.Context, userId value_user.UserId, managedAbilityBoardId value_user.ManagedAbilityBoardId) (*model_user.ManagedAbilityBoard, error)
	FindManagedAbilityBoards(ctx context.Context, userId value_user.UserId) ([]model_user.ManagedAbilityBoard, error)
	InsertManagedAbilityBoard(ctx context.Context, userId value_user.UserId, board *model_user.ManagedAbilityBoard) (*model_user.ManagedAbilityBoard, error)
	UpdateManagedAbilityBoard(ctx context.Context, userId value_user.UserId, board *model_user.ManagedAbilityBoard) (*model_user.ManagedAbilityBoard, error)
}

package repository

import (
	"context"

	model_ability_sphere "gitlab.com/kirafan/sparkle/server/internal/domain/model/ability_sphere"
	value_ability_sphere "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_sphere"
)

type AbilitySphereRepository interface {
	FindAbilitySphereBySphereItemId(ctx context.Context, itemId value_ability_sphere.AbilitySphereItemId) (*model_ability_sphere.AbilitySphere, error)
	FindAbilitySphereRecipeBySphereItemId(ctx context.Context, itemId value_ability_sphere.AbilitySphereItemId) (*model_ability_sphere.AbilitySphereRecipe, error)
}

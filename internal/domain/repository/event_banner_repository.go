package repository

import (
	"context"

	model_event_banner "gitlab.com/kirafan/sparkle/server/internal/domain/model/event_banner"
)

type EventBannerRepository interface {
	GetEventBanners(ctx context.Context) ([]*model_event_banner.EventBanner, error)
}

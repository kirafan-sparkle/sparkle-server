package repository

import (
	"context"

	model_named_type "gitlab.com/kirafan/sparkle/server/internal/domain/model/named_type"
)

type NamedTypeRepository interface {
	FindNamedType(ctx context.Context, query *model_named_type.NamedType, criteria map[string]interface{}, associations *[]string) (*model_named_type.NamedType, error)
	FindNamedTypes(ctx context.Context, query *model_named_type.NamedType, criteria map[string]interface{}, associations *[]string) ([]*model_named_type.NamedType, error)
}

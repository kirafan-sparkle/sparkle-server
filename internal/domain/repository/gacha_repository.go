package repository

import (
	"context"

	model_gacha "gitlab.com/kirafan/sparkle/server/internal/domain/model/gacha"
)

type GachaRepository interface {
	FindGacha(ctx context.Context, query *model_gacha.Gacha, criteria map[string]interface{}, associations *[]string) (*model_gacha.Gacha, error)
	FindGachas(ctx context.Context, query *model_gacha.Gacha, criteria map[string]interface{}, associations *[]string) ([]*model_gacha.Gacha, error)
}

package repository

import (
	"context"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type CharacterQuestRepository interface {
	GetCharacterQuest(ctx context.Context, internalUserId value_user.UserId, characterQuestId uint) (*model_quest.CharacterQuest, error)
	GetCharacterQuests(ctx context.Context, internalUserId value_user.UserId) ([]model_quest.CharacterQuest, error)
}

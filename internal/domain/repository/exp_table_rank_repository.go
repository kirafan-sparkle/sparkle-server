package repository

import (
	"context"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
)

type ExpTableRankRepository interface {
	FindExpTableRank(ctx context.Context, query *model_exp_table.ExpTableRank, criteria map[string]interface{}) (*model_exp_table.ExpTableRank, error)
	FindExpTableRanks(ctx context.Context, query *model_exp_table.ExpTableRank, criteria map[string]interface{}) ([]*model_exp_table.ExpTableRank, error)
}

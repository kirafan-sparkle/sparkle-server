package repository

import (
	"context"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type UserRepositoryParam struct {
	Entire                   bool
	ItemSummary              bool
	SupportCharacters        bool
	ManagedBattleParties     bool
	ManagedCharacters        bool
	ManagedNamedTypes        bool
	ManagedFieldPartyMembers bool
	ManagedTowns             bool
	ManagedFacilities        bool
	ManagedRoomObjects       bool
	ManagedWeapons           bool
	ManagedRooms             bool
	ManagedMasterOrbs        bool
	FavoriteMembers          bool
	OfferTitleTypes          bool
	AdvIds                   bool
	TipIds                   bool
	Missions                 bool
	Presents                 bool
	Gachas                   bool
	Trades                   bool
	LoginBonuses             bool
	Achievements             bool
	Trainings                bool
	TrainingSlots            bool
}

type UserRepository interface {
	CreateUser(ctx context.Context, user *model_user.User) (*model_user.User, error)
	FindUserById(ctx context.Context, id value_user.UserId, param UserRepositoryParam) (*model_user.User, error)
	FindUserByUUID(ctx context.Context, UUId string, param UserRepositoryParam) (*model_user.User, error)
	FindUserBySession(ctx context.Context, deviceUUId string, sessionUUId string) (*model_user.User, error)
	FindUserByFriendCode(ctx context.Context, friendCode value_friend.FriendOrCheatCode) (*model_user.User, error)
	FindUserByMoveCode(ctx context.Context, moveCode string) (*model_user.User, error)
	UpdateUser(ctx context.Context, user *model_user.User, param UserRepositoryParam) (*model_user.User, error)
}

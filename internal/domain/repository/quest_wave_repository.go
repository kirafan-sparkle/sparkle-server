package repository

import (
	"context"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
)

type QuestWaveRepository interface {
	FindQuestWave(ctx context.Context, query *model_quest.QuestWave, criteria map[string]interface{}, associations *[]string) (*model_quest.QuestWave, error)
	FindQuestWaves(ctx context.Context, query *model_quest.QuestWave, criteria map[string]interface{}, associations *[]string) ([]*model_quest.QuestWave, error)
	FindQuestWaveRandoms(ctx context.Context, query *model_quest.QuestWaveRandom, criteria map[string]interface{}, associations *[]string) ([]*model_quest.QuestWaveRandom, error)
	FindQuestWaveDrops(ctx context.Context, query *model_quest.QuestWaveDrop, criteria map[string]interface{}, associations *[]string) ([]*model_quest.QuestWaveDrop, error)
}

package repository

import (
	"context"

	model_present "gitlab.com/kirafan/sparkle/server/internal/domain/model/present"
)

type PresentRepository interface {
	FindPresent(ctx context.Context, query *model_present.Present, criteria map[string]interface{}, associations *[]string) (*model_present.Present, error)
	FindPresents(ctx context.Context, query *model_present.Present, criteria map[string]interface{}, associations *[]string) ([]*model_present.Present, error)
}

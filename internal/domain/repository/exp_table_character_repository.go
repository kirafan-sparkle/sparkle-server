package repository

import (
	"context"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
)

type ExpTableCharacterRepository interface {
	FindExpTableCharacter(ctx context.Context, query *model_exp_table.ExpTableCharacter, criteria map[string]interface{}) (*model_exp_table.ExpTableCharacter, error)
	FindExpTableCharacters(ctx context.Context, query *model_exp_table.ExpTableCharacter, criteria map[string]interface{}) ([]*model_exp_table.ExpTableCharacter, error)
	GetRequiredCoinsForUpgrade(ctx context.Context, currentLevel value_character.CharacterLevel) (uint16, error)
}

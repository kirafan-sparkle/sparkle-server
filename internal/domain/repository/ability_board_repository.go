package repository

import (
	"context"

	model_ability_board "gitlab.com/kirafan/sparkle/server/internal/domain/model/ability_board"
	value_ability_board "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_board"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

type AbilityBoardRepository interface {
	FindAbilityBoardByItemId(ctx context.Context, itemId value_item.ItemId) (*model_ability_board.AbilityBoard, error)
	FindAbilityBoard(ctx context.Context, abilityBoardId value_ability_board.AbilityBoardId) (*model_ability_board.AbilityBoard, error)
	FindAbilityBoardSlot(ctx context.Context, abilityBoardId value_ability_board.AbilityBoardId, abilityBoardSlotIndex value_ability_board.AbilityBoardSlotIndex) (*model_ability_board.AbilityBoardSlot, error)
	FindAbilityBoardSlotRecipe(ctx context.Context, abilityBoardSlotRecipeId value_ability_board.AbilityBoardSlotRecipeId) (*model_ability_board.AbilityBoardSlotRecipe, error)
}

package repository

import (
	"context"

	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
)

type EvoTableLimitBreakRepository interface {
	FindByRecipeId(ctx context.Context, recipeId uint) (*model_evo_table.EvoTableLimitBreak, error)
}

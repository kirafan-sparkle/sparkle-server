package repository

import (
	"context"

	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
)

type WeaponRecipeRepository interface {
	FindByRecipeId(ctx context.Context, recipeId uint32) (*model_weapon.WeaponRecipe, error)
}

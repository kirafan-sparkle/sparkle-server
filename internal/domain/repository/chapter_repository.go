package repository

import (
	"context"

	model_chapter "gitlab.com/kirafan/sparkle/server/internal/domain/model/chapter"
)

type ChapterRepository interface {
	GetAll(ctx context.Context) ([]*model_chapter.Chapter, error)
}

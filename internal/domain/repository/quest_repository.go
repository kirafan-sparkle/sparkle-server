package repository

import (
	"context"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
)

type QuestRepository interface {
	FindQuest(ctx context.Context, query *model_quest.Quest, criteria map[string]interface{}, associations []string) (*model_quest.Quest, error)
	FindQuests(ctx context.Context, query *model_quest.Quest, criteria map[string]interface{}, associations []string) ([]*model_quest.Quest, error)
	GetQuestCategory(ctx context.Context, questId uint) (*value_quest.QuestCategoryType, error)
}

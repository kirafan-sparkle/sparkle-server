package repository

import (
	"context"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
)

type ExpTableFriendshipRepository interface {
	FindExpTableFriendship(ctx context.Context, query *model_exp_table.ExpTableFriendship, criteria map[string]interface{}) (*model_exp_table.ExpTableFriendship, error)
	FindExpTableFriendships(ctx context.Context, query *model_exp_table.ExpTableFriendship, criteria map[string]interface{}) ([]*model_exp_table.ExpTableFriendship, error)
}

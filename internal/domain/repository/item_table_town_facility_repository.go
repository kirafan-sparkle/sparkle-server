package repository

import (
	"context"

	model_item_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/item_table"
)

type ItemTableTownFacilityRepository interface {
	FindItemTableTownFacility(ctx context.Context, itemNo uint32) ([]*model_item_table.ItemTableTownFacility, error)
}

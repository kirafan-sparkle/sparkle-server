package repository

import (
	"context"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type UserPresentRepository interface {
	GetReceivableUserPresents(ctx context.Context, userId value_user.UserId) ([]*model_user.UserPresent, error)
	GetReceivedUserPresents(ctx context.Context, userId value_user.UserId) ([]*model_user.UserPresent, error)
	ReceiveUserPresents(ctx context.Context, userId value_user.UserId, managedPresentIds []int64) error
	DeleteExpiredUserPresents(ctx context.Context, userId value_user.UserId) error
}

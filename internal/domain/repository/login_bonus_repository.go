package repository

import (
	"context"

	model_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/model/login_bonus"
)

type LoginBonusRepository interface {
	FindAvailableLoginBonuses(ctx context.Context) ([]*model_login_bonus.LoginBonus, error)
}

package repository

import (
	"context"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
)

type ExpTableSkillRepository interface {
	FindExpTableSkill(ctx context.Context, query *model_exp_table.ExpTableSkill, criteria map[string]interface{}) (*model_exp_table.ExpTableSkill, error)
	FindExpTableSkills(ctx context.Context, query *model_exp_table.ExpTableSkill, criteria map[string]interface{}) ([]*model_exp_table.ExpTableSkill, error)
}

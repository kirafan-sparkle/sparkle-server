package repository

import (
	"context"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type EventQuestRepository interface {
	GetEventQuest(ctx context.Context, internalUserId value_user.UserId, eventQuestId uint) (*model_quest.EventQuest, error)
	GetEventQuests(ctx context.Context, internalUserId value_user.UserId) ([]*model_quest.EventQuest, error)
	GetEventQuestPeriods(ctx context.Context, internalUserId value_user.UserId) ([]*model_quest.EventQuestPeriod, error)
}

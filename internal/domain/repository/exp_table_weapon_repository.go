package repository

import (
	"context"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
)

type ExpTableWeaponRepository interface {
	FindExpTableWeapon(ctx context.Context, query *model_exp_table.ExpTableWeapon, criteria map[string]interface{}) (*model_exp_table.ExpTableWeapon, error)
	FindExpTableWeapons(ctx context.Context, query *model_exp_table.ExpTableWeapon, criteria map[string]interface{}) ([]*model_exp_table.ExpTableWeapon, error)
	GetRequiredCoinsForUpgrade(ctx context.Context, currentLevel uint8) (uint16, error)
}

package repository

import (
	"context"

	model_training "gitlab.com/kirafan/sparkle/server/internal/domain/model/training"
)

type TrainingRepository interface {
	GetTraining(ctx context.Context, trainingId uint) (*model_training.Training, error)
	GetTrainings(ctx context.Context, categoryId uint8) ([]*model_training.Training, error)
}

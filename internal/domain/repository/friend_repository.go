package repository

import (
	"context"

	model_friend "gitlab.com/kirafan/sparkle/server/internal/domain/model/friend"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
)

type FriendRepository interface {
	/* Check if two players are already friends */
	IsAlreadyFriend(ctx context.Context, userId1 value_friend.PlayerId, userId2 value_friend.PlayerId) (bool, error)
	/* Add a friend relation */
	AddFriend(ctx context.Context, friend model_friend.Friend) (*value_friend.ManagedFriendId, error)
	/* Delete a friend relation */
	DeleteFriend(ctx context.Context, managedFriendId value_friend.ManagedFriendId) error
	/* Get the number of friends a player has */
	GetFriendCount(ctx context.Context, userId value_friend.PlayerId) (int64, error)
	/* Get a player's friends */
	GetFriends(ctx context.Context, userId value_friend.PlayerId) ([]*model_friend.FriendFull, error)
	/* Get a friend by managedFriendId */
	GetFriend(ctx context.Context, managedFriendId value_friend.ManagedFriendId) (*model_friend.Friend, error)
}

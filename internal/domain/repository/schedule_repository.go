package repository

import (
	"context"

	model_schedule "gitlab.com/kirafan/sparkle/server/internal/domain/model/schedule"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
)

type ScheduleRepository interface {
	Create(ctx context.Context, characterIds []value_character.CharacterId, gridData string) ([]*model_schedule.Schedule, error)
	Refresh(ctx context.Context, characterSchedules []string) ([]string, error)
}

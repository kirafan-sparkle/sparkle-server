package repository

import (
	"context"

	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
)

type EvoTableWeaponRepository interface {
	FindByWeaponId(ctx context.Context, weaponId value_weapon.WeaponId) (*model_evo_table.EvoTableWeapon, error)
}

package repository

import (
	"context"

	model_information "gitlab.com/kirafan/sparkle/server/internal/domain/model/information"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
)

type InformationRepository interface {
	GetInformations(ctx context.Context, platform value_version.Platform) ([]*model_information.Information, error)
}

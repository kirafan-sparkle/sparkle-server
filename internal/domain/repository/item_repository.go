package repository

import (
	"context"

	model_item "gitlab.com/kirafan/sparkle/server/internal/domain/model/item"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

type ItemRepository interface {
	FindItem(ctx context.Context, query *model_item.Item, criteria map[string]interface{}, associations *[]string) (*model_item.Item, error)
	FindItems(ctx context.Context, query *model_item.Item, criteria map[string]interface{}, associations *[]string) ([]*model_item.Item, error)
	GetWeaponUpgradeAmount(ctx context.Context, itemId value_item.ItemId) (int32, error)
	GetCharacterUpgradeAmount(ctx context.Context, itemId value_item.ItemId) (int32, int32, error)
}

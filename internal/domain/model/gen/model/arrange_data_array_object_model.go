package model

type ArrangeDataArrayObject struct {
	Dir int64

	ManagedRoomObjectId int64

	RoomNo int64

	RoomObjectId int64

	X int64

	Y int64
}

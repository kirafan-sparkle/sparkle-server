package model

type EventQuestPeriodGroupsArrayObject struct {
	EndAt string

	EventQuestPeriodGroupQuests []EventQuestPeriodGroupQuestsArrayObject

	GroupId int64

	StartAt string

	Unlocked bool
}

package model

type GetAllPlayerQuestResponse struct {
	CharacterQuests []CharacterQuestsArrayObject

	EventQuestPeriods []EventQuestPeriodsArrayObject

	EventQuests []EventQuestsArrayObject

	LastPlayedChapterQuestIds []int64

	PlayedOpenChapterIds []int64

	PlayerOfferQuests []PlayerOfferQuestsArrayObject

	QuestPart2s []Questpart2sArrayObject

	QuestStaminaReductions []QuestStaminaReductionsArrayObject

	Quests []QuestsArrayObject

	ReadGroups []interface{}
}

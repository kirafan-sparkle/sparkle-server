package model

type TrainingCompleteRewardsArrayObject struct {
	FirstGem int64

	RewardCharaExp int64

	RewardFriendship int64

	RewardGold int64

	RewardKRRPoint int64

	SlotId int64

	TrainingRewardItems []TrainingRewardItemsArrayObject
}

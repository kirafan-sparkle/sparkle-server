package model

type ReleaseSlotPlayerAbilityResponse struct {
	ItemSummary []ItemSummaryArrayObject

	Player BasePlayer

	PlayerAbilityBoards []PlayerAbilityBoardsArrayObject
}

package model

type UpgradePlayerWeaponResponse struct {
	Gold int64

	ItemSummary []ItemSummaryArrayObject

	ManagedWeapon UpgradePlayerWeaponResponseManagedWeapon

	SuccessType int64
}

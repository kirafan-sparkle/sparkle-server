package model

type ResetAllViewPlayerCharacterResponse struct {
	ManagedCharacters []ManagedCharactersArrayObject
}

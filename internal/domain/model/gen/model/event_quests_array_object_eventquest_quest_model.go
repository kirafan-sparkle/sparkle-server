package model

type EventQuestsArrayObjectEventquestQuest struct {
	AdvId1 int64

	AdvId2 int64

	AdvId3 int64

	AdvOnly int64

	Amount1 int64

	Amount2 int64

	Amount3 int64

	BgId int64

	BgmCue *string

	Category int64

	Ex2Amount int64

	ExId2 int64

	Gem1 int64

	Gold1 int64

	Id int64

	IsHideElement int64

	IsNpcOnly bool

	ItemId1 int64

	ItemId2 int64

	ItemId3 int64

	LastBgmCue *string

	LoserBattle int64

	MainFlg int64

	Name string

	QuestFirstClearReward EventQuestsArrayObjectEventquestQuestQuestFirstClearReward

	QuestNpcs []interface{}

	RetryLimit int64

	RewardCharacterExp int64

	RewardFriendshipExp int64

	RewardMoney int64

	RewardUserExp int64

	RoomObjectAmount int64

	RoomObjectId int64

	Section int64

	Stamina int64

	StoreReview int64

	TypeIconId int64

	Warn int64

	WaveId1 int64

	WaveId2 int64

	WaveId3 int64

	WaveId4 int64

	WaveId5 int64

	WaveIds []int64

	WeaponAmount int64

	WeaponId int64
}

package model

type ProductsArrayObjectDirectSale struct {
	BadgeType int64 `json:"badgeType,omitempty"`

	DetailText string `json:"detailText,omitempty"`

	Id int64 `json:"id,omitempty"`

	RestockAt *int64 `json:"restockAt,omitempty"`

	RestockType int64 `json:"restockType,omitempty"`

	RewardAmount int64 `json:"rewardAmount,omitempty"`

	RewardId int64 `json:"rewardId,omitempty"`

	RewardType int64 `json:"rewardType,omitempty"`

	Stock int64 `json:"stock,omitempty"`
}

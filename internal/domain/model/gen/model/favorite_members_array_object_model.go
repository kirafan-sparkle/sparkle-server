package model

type FavoriteMembersArrayObject struct {
	ArousalLevel int64

	CharacterId int64

	FavoriteIndex int64

	ManagedCharacterId int64
}

package model

type SetPlayerMoveResponse struct {
	AccessToken string

	PlayerId int64
}

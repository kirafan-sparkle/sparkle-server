package model

type SalePlayerItemResponse struct {
	ItemSummary []ItemSummaryArrayObject

	Player BasePlayer
}

package model

type GetAllPlayerResponse struct {
	AdvIds                   []int64
	DropItem                 []DropItemArrayObject `json:"dropItem,omitempty"`
	FavoriteMembers          []FavoriteMembersArrayObject
	FeaturedInformations     *string
	FlagPush                 int64
	FlagStamina              int64
	FlagUi                   int64
	FriendProposedCount      int64
	IsCloseInfo              int64
	IsNewProduct             int64
	ItemSummary              []ItemSummaryArrayObject
	LastMemberAdded          string
	ManagedAbilityBoards     []PlayerAbilityBoardsArrayObject
	ManagedBattleParties     []ManagedBattlePartiesArrayObject
	ManagedCharacters        []ManagedCharactersArrayObject
	ManagedFieldPartyMembers []ManagedFieldPartyMembersArrayObject
	ManagedMasterOrbs        []ManagedMasterOrbsArrayObject
	ManagedNamedTypes        []ManagedNamedTypesArrayObject
	ManagedRoomObjects       []ManagedRoomObjectsArrayObject
	ManagedRooms             []ManagedRoomsArrayObject
	ManagedTownFacilities    []ManagedTownFacilitiesArrayObject
	ManagedTowns             []ManagedTownsArrayObject
	ManagedWeapons           []ManagedWeaponsArrayObject

	OccurEnemy      OrderPlayerQuestLogResponseOccurEnemy `json:"occurEnemy,omitempty"`
	OfferTitleTypes []OfferTitleTypesArrayObject
	OrderReceiveId  int64 `json:"orderReceiveId,omitempty"`

	Player       BasePlayer
	PresentCount int64
	QuestData    *string

	StepCode int64

	SupportCharacter  GetAllPlayerResponseSupportCharacter `json:"supportCharacter,omitempty"`
	SupportCharacters []SupportCharactersArrayObject

	SupportFriend GetAllPlayerResponseSupportFriend `json:"supportFriend,omitempty"`
	TipIds        []int64
	TrainingCount int64
}

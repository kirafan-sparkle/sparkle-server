package model

type GetAllPlayerFriendResponse struct {
	Friends []FriendsArrayObject

	Guests []GuestsArrayObject
}

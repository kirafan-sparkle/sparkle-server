package model

type RoomObjectsArrayObject struct {
	BargainBuyAmount int64

	BargainEndAt string

	BargainFlag int64

	BargainStartAt string

	BuyAmount int64

	DispEndAt string

	DispStartAt string

	Id int64

	IsSaleable bool

	Name string

	ObjectLimit int64

	SaleAmount int64

	ShopFlag int64

	Type int64
}

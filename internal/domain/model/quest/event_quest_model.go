package model_quest

import (
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

type EventQuest struct {
	Id        uint `gorm:"primaryKey"`
	EventType int64
	StartAt   time.Time
	EndAt     time.Time
	// Foreign key
	QuestId      uint
	Quest        Quest
	MapFlg       int64
	ChestId      int
	TradeGroupId int
	// Maybe required ids to unlock this event quest
	CondEventQuestIds []EventQuestCondId
	// Job limitation??
	ExAlchemist value.BoolLikeUInt8
	ExFighter   value.BoolLikeUInt8
	ExKnight    value.BoolLikeUInt8
	ExMagician  value.BoolLikeUInt8
	ExPriest    value.BoolLikeUInt8
	// Element limitation??
	ExFire  value.BoolLikeUInt8
	ExWater value.BoolLikeUInt8
	ExWind  value.BoolLikeUInt8
	ExEarth value.BoolLikeUInt8
	ExSun   value.BoolLikeUInt8
	ExMoon  value.BoolLikeUInt8
	// Maybe character limitation??
	ExName int8
	// Maybe Cost limitation??
	ExCost int8
	// Maybe Rarity limitation??
	ExRarity int8
	// ?????
	ExTitle int8
	// Event group things
	GroupId   int64
	GroupName string
	GroupSkip int64
	// Maybe available days
	Mon  value.BoolLikeUInt8
	Tue  value.BoolLikeUInt8
	Wed  value.BoolLikeUInt8
	Thu  value.BoolLikeUInt8
	Fri  value.BoolLikeUInt8
	Sat  value.BoolLikeUInt8
	Sun  value.BoolLikeUInt8
	Freq int64
	// Unknown default 1
	Interval int64
	Day      int64
	Month    int64
	// ??
	OrderLimit int64
	// Box event value (moved from wrapper model)
	ChestCostItemId     value_item.ItemId
	ChestCostItemAmount uint8
}

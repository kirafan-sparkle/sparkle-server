package model_quest

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
)

type QuestFirstClearReward struct {
	Id uint `gorm:"primary_key"`
	// Foreign key
	QuestId          uint
	ItemId1          value_item.ItemId
	Amount1          int8
	ItemId2          value_item.ItemId
	Amount2          int8
	ItemId3          value_item.ItemId
	Amount3          int8
	Gem              int64
	Gold             int64
	RoomObjectId     int64
	RoomObjectAmount int8
	WeaponId         int64
	WeaponAmount     int8
}

type QuestNpc struct {
	Id uint `gorm:"primary_key"`
	// Foreign key
	QuestId             uint
	CharacterId         uint64
	CharacterLevel      uint8
	CharacterLimitBreak uint8
	CharacterSkillLevel uint8
	WeaponId            uint64
	WeaponLevel         uint8
	WeaponSkillLevel    uint8
}

type Quest struct {
	// Quest Id
	Id uint `gorm:"primary_key"`
	// Quest Name
	Name string
	// Quest Icon
	TypeIconId value_quest.QuestIconType
	// Story Id before battle
	AdvId1 int64
	// Story Id after battle
	AdvId2 int64
	// Story Id after lose (For Part2-1-8 Daemon Shadow Mistress)
	AdvId3 int64
	// Story only quest if 1 specified (default 0)
	AdvOnly value.BoolLikeUInt8
	// display category of quest menu (maybe)
	Category value_quest.QuestCategoryType
	// Required item to start quest
	ExId2 value_item.ItemId
	// Required item amount to start quest
	Ex2Amount uint8
	// Required stamina to start quest
	Stamina int64
	// Waves of battles
	WaveId1 int64
	WaveId2 int64
	WaveId3 int64
	WaveId4 int64
	WaveId5 int64
	// First clear reward fields
	QuestFirstClearReward QuestFirstClearReward
	// Every clear reward fields
	RewardCharacterExp  uint64
	RewardFriendshipExp uint64
	RewardMoney         uint64
	RewardUserExp       uint64
	// Background Id
	BgId int64
	// BGM Id
	BgmCue *string
	// LastWave BGM Id
	LastBgmCue *string
	// Hide friend characters at support selection if true (default false)
	IsNpcOnly bool
	// Recommended support characters for this quest
	QuestNpcs []QuestNpc
	// Hide enemy element type on quest list if 1 specified (default 0)
	IsHideElement value.BoolLikeUInt8
	// Disallow retry feature if 1 specified (default 0)
	RetryLimit value.BoolLikeUInt8
	// Always clear as gold even lose if 1 specified (default 0)
	LoserBattle value.BoolLikeUInt8
	// Maybe player level warn (which range of level makes warn?) (Default 0)
	Warn value.BoolLikeUInt8
	// Maybe shows store review link (Looks like not working) (Default 0)
	StoreReview value.BoolLikeUInt8
	// Mystery value, maybe not used in main quests. (Default -1)
	Section int32
	// Mystery value, last event was set 1 but other quests were -1. (Default -1)
	MainFlg int32
}

type AllQuestInfo struct {
	Quests          []*Quest
	QuestPart2s     []*Quest
	EventQuests     []*EventQuest
	CharacterQuests []*CharacterQuest
	// TODO: Implement craft quest model
	CraftQuests []*interface{}
	// TODO: Implement player offer quest model
	PlayerOfferQuests []*interface{}
}

package model_quest

import (
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
)

type QuestWave struct {
	ManagedWaveId int64 `gorm:"primary_key"`
	// primary key to filter ( one wave has multiple enemies)
	WaveId            int64
	EnemyPlacement    value_quest.EnemyPlacement
	EnemyId           int64
	EnemyLv           int16
	EnemyDisplayScale float64
	// QuestWaveDrop foreign key
	EnemyDropId int64
	// QuestWaveRandom foreign key
	EnemyRandomId int64
	// Event point (maybe)
	Point int16
}

type QuestWaveDrop struct {
	ManagedWaveDropId int64 `gorm:"primary_key"`
	// primary key to filter ( one enemy has multiple drop patterns)
	EnemyDropId         int64
	DropItemProbability uint8
	DropItemId          value_item.ItemId
	DropItemAmount      uint32
}

type QuestWaveRandom struct {
	ManagedWaveRandomId uint `gorm:"primary_key"`
	// primary key to filter ( one random has multiple random patterns)
	EnemyRandomId          int64
	EnemyRandomProbability uint8
	EnemyId                int64
	EnemyLv                int16
	EnemyDisplayScale      float64
	// QuestWaveDrop foreign key
	EnemyDropId int
}

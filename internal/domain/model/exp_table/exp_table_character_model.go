package model_exp_table

import value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"

type ExpTableCharacter struct {
	Level               value_character.CharacterLevel `gorm:"primaryKey"`
	NextExp             uint32
	TotalExp            uint64
	RequiredCoinPerItem uint16
}

package model_chapter

import (
	"time"

	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
)

type Chapter struct {
	ChapterId uint `gorm:"primary_key"`
	// Display order Id (can not use as primary key because of normal and hard has same Id)
	Id           uint8
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    *time.Time `sql:"index"`
	Name         string
	Part         value_quest.ChapterPart
	Difficulty   value_quest.ChapterDifficulty
	NewDispEndAt *time.Time
	// Required quest to unlock this chapter (default -1)
	CondQuestId int32
	// Quests in this chapter
	QuestIds []ChapterQuestId
	// Part2 chapters quests
	CollectQuestId int32
	TrialQuestId   int32
}

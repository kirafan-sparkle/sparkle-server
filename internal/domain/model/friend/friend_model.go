package model_friend

import (
	"time"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
)

/** both-approved friend model */
type Friend struct {
	ManagedFriendId value_friend.ManagedFriendId `gorm:"primaryKey"`
	CreatedAt       time.Time                    `gorm:"autoCreateTime"`
	UpdatedAt       time.Time                    `gorm:"autoUpdateTime"`
	PlayerId1       value_friend.PlayerId        `gorm:"index"`
	Player1         model_user.User              `gorm:"PRELOAD:false;foreignKey:PlayerId1"`
	PlayerId2       value_friend.PlayerId        `gorm:"index"`
	Player2         model_user.User              `gorm:"PRELOAD:false;foreignKey:PlayerId2"`
}

func (f *Friend) IsChangeable(actorId value_friend.PlayerId) bool {
	return f.PlayerId1 == actorId || f.PlayerId2 == actorId
}

func NewFriend(playerId1 value_friend.PlayerId, playerId2 value_friend.PlayerId) *Friend {
	now := time.Now()
	friend := Friend{
		CreatedAt: now,
		UpdatedAt: now,
		PlayerId1: playerId1,
		PlayerId2: playerId2,
	}
	return &friend
}

package model_friend

import (
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

type FriendSupportCharacter struct {
	ManagedCharacterId int
	CharacterId        value_character.CharacterId
	Level              uint8
	// maybe unneeded...
	Exp int64
	// 限界突破回数 (0~4)
	LevelBreak uint8
	// Totteoki
	SkillLevel1 uint8
	// Character skill 1
	SkillLevel2 uint8
	// Character skill 2
	SkillLevel3      uint8
	WeaponId         int32
	WeaponLevel      uint8
	WeaponSkillLevel uint8
	// maybe unneeded...
	WeaponSkillExp uint64
	NamedLevel     uint8
	// maybe unneeded...
	NamedExp uint64
	// maybe unneeded...
	DuplicatedCount int8
	ArousalLevel    int8
	AbilityBoardId  int32
	EquipItemIds    []value_item.ItemId
}

package model_friend

import (
	"time"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

/** This is not database model, it is only for make response */
type FriendFull struct {
	ManagedFriendId value_friend.ManagedFriendId
	PlayerId        value_friend.PlayerId
	State           value_friend.FriendState
	Direction       value_friend.FriendDirection
	Name            string
	MyCode          value_friend.FriendOrCheatCode
	Comment         *string
	// FIXME: Change to value object
	CurrentAchievementId int64
	// FIXME: Change to value object
	Level       uint16
	LastLoginAt time.Time
	// Dead parameter (always 0)
	TotalExp uint8
	// Is this value needed...?
	SupportLimit uint8
	// Dead parameter (always nil)
	NamedTypes        *string
	SupportName       string
	SupportCharacters []FriendSupportCharacter
	// NOTE: FirstFavoriteMember has fewer fields than FriendSupportCharacter but is otherwise identical
	// So we can use the same struct for both
	FirstFavoriteMember FriendSupportCharacter
}

func NewDummyFriendFull(playerId value_friend.PlayerId, name string, comment string, myCode value_friend.FriendOrCheatCode, icon value_character.CharacterId) FriendFull {
	return FriendFull{
		ManagedFriendId:      1,
		PlayerId:             playerId,
		State:                value_friend.FriendStateGuest,
		Direction:            value_friend.FriendDirectionNone,
		Name:                 name,
		MyCode:               myCode,
		Comment:              &comment,
		CurrentAchievementId: -1,
		Level:                1,
		LastLoginAt:          time.Now(),
		SupportName:          "None",
		FirstFavoriteMember: FriendSupportCharacter{
			ManagedCharacterId: 1,
			CharacterId:        icon,
			Level:              100,
			LevelBreak:         4,
			WeaponId:           21200,
			WeaponLevel:        20,
			WeaponSkillLevel:   30,
			AbilityBoardId:     -1,
			EquipItemIds:       make([]value_item.ItemId, 0),
		},
	}
}

func NewSearchResultFriendFull(user model_user.User) (*FriendFull, error) {
	favoriteCharacter, err := user.GetManagedCharacter(user.FavoriteMembers[0].ManagedCharacterId)
	if err != nil {
		return nil, err
	}
	playerId, err := value_friend.NewPlayerId(user.Id, value_friend.PlayerSpecialTypeDefault)
	if err != nil {
		return nil, err
	}

	return &FriendFull{
		ManagedFriendId:      0,
		PlayerId:             *playerId,
		State:                value_friend.FriendStateGuest,
		Direction:            value_friend.FriendDirectionNone,
		Name:                 user.Name,
		MyCode:               user.MyCode,
		Comment:              user.Comment,
		CurrentAchievementId: int64(user.CurrentAchievementId),
		Level:                user.Level,
		LastLoginAt:          user.LastLoginAt,
		TotalExp:             uint8(user.TotalExp),
		FirstFavoriteMember: FriendSupportCharacter{
			ManagedCharacterId: int(favoriteCharacter.ManagedCharacterId),
			CharacterId:        favoriteCharacter.CharacterId,
			Level:              uint8(favoriteCharacter.Level),
			Exp:                int64(favoriteCharacter.Exp),
			LevelBreak:         favoriteCharacter.LevelBreak,
			DuplicatedCount:    int8(favoriteCharacter.DuplicatedCount),
			ArousalLevel:       int8(favoriteCharacter.ArousalLevel),
			SkillLevel1:        favoriteCharacter.SkillLevel1,
			SkillLevel2:        favoriteCharacter.SkillLevel2,
			SkillLevel3:        favoriteCharacter.SkillLevel3,
			// Probably below values are unneeded (why they were in the model?)
			WeaponId:         -1,
			WeaponLevel:      0,
			WeaponSkillLevel: 0,
			WeaponSkillExp:   0,
			NamedLevel:       0,
			NamedExp:         0,
			AbilityBoardId:   0,
			EquipItemIds:     nil,
		},
		// Below values are unneeded (why they were in the model?)
		SupportLimit:      8,
		NamedTypes:        nil,
		SupportName:       "dummy",
		SupportCharacters: nil,
	}, nil
}

func NewOutGoingFriendRequestFull(req FriendRequest) *FriendFull {
	return &FriendFull{
		ManagedFriendId:      req.ManagedFriendId,
		PlayerId:             req.PlayerId2,
		State:                value_friend.FriendStateWaitingApprove,
		Direction:            value_friend.FriendDirectionSelfToOther,
		Name:                 req.Player2.Name,
		MyCode:               req.Player2.MyCode,
		Comment:              req.Player2.Comment,
		CurrentAchievementId: int64(req.Player2.CurrentAchievementId),
		Level:                req.Player2.Level,
		LastLoginAt:          req.Player2.LastLoginAt,
		TotalExp:             uint8(req.Player2.TotalExp),
		// STUB
		SupportCharacters: make([]FriendSupportCharacter, 0),
		FirstFavoriteMember: FriendSupportCharacter{
			ManagedCharacterId: 1,
			CharacterId:        value_character.CharacterId(32022011),
			Level:              100,
			Exp:                0,
			LevelBreak:         4,
			SkillLevel1:        35,
			SkillLevel2:        25,
			SkillLevel3:        25,
			WeaponId:           21200,
			WeaponLevel:        20,
			WeaponSkillLevel:   30,
			WeaponSkillExp:     0,
			NamedLevel:         5,
			NamedExp:           0,
			DuplicatedCount:    5,
			ArousalLevel:       5,
			AbilityBoardId:     -1,
			EquipItemIds:       make([]value_item.ItemId, 0),
		},
		// Unneeded values...
		SupportLimit: 8,
		NamedTypes:   nil,
		SupportName:  "",
	}
}

func NewInComingFriendRequestFull(req FriendRequest) *FriendFull {
	return &FriendFull{
		ManagedFriendId:      req.ManagedFriendId,
		PlayerId:             req.PlayerId1,
		State:                value_friend.FriendStateWaitingApprove,
		Direction:            value_friend.FriendDirectionOtherToSelf,
		Name:                 req.Player1.Name,
		MyCode:               req.Player1.MyCode,
		Comment:              req.Player1.Comment,
		CurrentAchievementId: int64(req.Player1.CurrentAchievementId),
		Level:                req.Player1.Level,
		LastLoginAt:          req.Player1.LastLoginAt,
		TotalExp:             uint8(req.Player1.TotalExp),
		// STUB
		SupportCharacters: make([]FriendSupportCharacter, 0),
		FirstFavoriteMember: FriendSupportCharacter{
			ManagedCharacterId: 1,
			CharacterId:        value_character.CharacterId(32022011),
			Level:              100,
			Exp:                0,
			LevelBreak:         4,
			SkillLevel1:        35,
			SkillLevel2:        25,
			SkillLevel3:        25,
			WeaponId:           21200,
			WeaponLevel:        20,
			WeaponSkillLevel:   30,
			WeaponSkillExp:     0,
			NamedLevel:         5,
			NamedExp:           0,
			DuplicatedCount:    5,
			ArousalLevel:       5,
			AbilityBoardId:     -1,
			EquipItemIds:       make([]value_item.ItemId, 0),
		},
		// Unneeded values...
		SupportLimit: 8,
		NamedTypes:   new(string),
		SupportName:  "",
	}
}

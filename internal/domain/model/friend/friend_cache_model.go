package model_friend

import (
	"time"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
)

/** Showing all friends per a request is too heavy, so we cache it for a day with this database */
type FriendCache struct {
	ManagedFriendId uint `gorm:"primaryKey"`
	/** requester's player id */
	PlayerId  value_friend.PlayerId `gorm:"index"`
	CreatedAt time.Time             `gorm:"autoCreateTime"`
	ExpiresAt time.Time
	Direction value_friend.FriendDirection
	/** another user's player id */
	TargetPlayerId value_friend.PlayerId
	TargetPlayer   model_user.User `gorm:"PRELOAD:false;foreignKey:TargetPlayerId"`
}

package model_friend

import (
	"errors"
	"time"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
)

/** on-going friend model */
type FriendRequest struct {
	ManagedFriendId value_friend.ManagedFriendId `gorm:"primaryKey"`
	CreatedAt       time.Time                    `gorm:"autoCreateTime"`
	UpdatedAt       time.Time                    `gorm:"autoUpdateTime"`
	PlayerId1       value_friend.PlayerId        `gorm:"index"`
	Player1         model_user.User              `gorm:"PRELOAD:false;foreignKey:PlayerId1"`
	PlayerId2       value_friend.PlayerId        `gorm:"index"`
	Player2         model_user.User              `gorm:"PRELOAD:false;foreignKey:PlayerId2"`
	State           value_friend.FriendState     `gorm:"index"`
}

func (f *FriendRequest) IsChangeable(actorUserId value_friend.PlayerId) bool {
	return f.PlayerId1 == actorUserId || f.PlayerId2 == actorUserId
}

func (f *FriendRequest) IsSender(actorUserId value_friend.PlayerId) bool {
	return f.PlayerId1 == actorUserId
}

func (f *FriendRequest) IsReceiver(actorUserId value_friend.PlayerId) bool {
	return f.PlayerId2 == actorUserId
}

func (f *FriendRequest) GetOtherPlayerId(actorUserId value_friend.PlayerId) value_friend.PlayerId {
	if f.PlayerId1 == actorUserId {
		return f.PlayerId2
	}
	return f.PlayerId1
}

func NewFriendRequest(senderId value_friend.PlayerId, receiverId value_friend.PlayerId) (*FriendRequest, error) {
	if senderId == receiverId {
		return nil, errors.New("senderId and receiverId are the same")
	}
	return &FriendRequest{
		PlayerId1: senderId,
		PlayerId2: receiverId,
		State:     value_friend.FriendStateWaitingApprove,
	}, nil
}

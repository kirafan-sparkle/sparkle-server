package model_gacha

import (
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_gacha "gitlab.com/kirafan/sparkle/server/internal/domain/value/gacha"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

type Gacha struct {
	GachaId  uint `gorm:"primary_key"`
	Name     string
	BannerId string
	// Maybe displayed MV name at login?
	MvName *string // default: nil
	// Gacha info page url
	WebViewUrl string
	// Maybe display index at client?
	Sort int64
	Type value_gacha.GachaType

	// Internal request accept time range
	StartAt time.Time
	EndAt   time.Time
	// Display time range
	DispStartAt time.Time
	DispEndAt   time.Time
	// False if this gacha is available every time
	HasPeriod bool
	// Maybe display day of week?
	Mon value.BoolLikeUInt8
	Tue value.BoolLikeUInt8
	Wed value.BoolLikeUInt8
	Thu value.BoolLikeUInt8
	Fri value.BoolLikeUInt8
	Sat value.BoolLikeUInt8
	Sun value.BoolLikeUInt8

	// Maybe gift item per a draw
	GachaSteps []GachaStep

	// Maybe required gem count per a draw
	Gem1 int16
	// Maybe required gem count per 10 draw
	Gem10 int16
	// Maybe required gem count at first 10 draw
	First10 int16
	// Required gem count with unlimited gem
	UnlimitedGem int16 // default: -1
	// Required required gem count with unlimited gem
	UnlimitedGemDrawCount int64 // default 1
	// 1 if the 1 time gacha need unlimited gem
	UnlimitedGem1Flag value.BoolLikeUInt8
	// 1 if the 10 times gacha need unlimited gem
	UnlimitedGem10Flag value.BoolLikeUInt8

	// Mystery value (-1, 0, or 1?)
	DrawLimit int8 // default -1
	// Mystery value (-1, 0, or 1?)
	AllLimit int8 // default -1

	// Draw ticket item id and amount
	ItemId     value_item.ItemId // default: 10000
	ItemAmount int64             // default: 1
	// Chance up key-holder id
	RateUpTicketId value_item.ItemId // default: 9006
	// Retry key-holder id
	ReDrawItemId value_item.ItemId // default: 9008

	// Gift item-box at draw?
	Box1Id           int64 // box step info db primary id? (default: same as id)
	BonusItemBoxId1  int64 // default: -1 or 1000
	BonusItemBoxId10 int64 // default: -1 or 1001

	// Unknown (default false)
	HighRarity bool
	// Maybe expected occurrence rate (but unused)
	Pick1 uint8 // static 45
	Pick2 uint8 // static 45
	Pick3 uint8 // static 45
	Pick4 uint8 // static 45
	Pick5 uint8 // static 45

	GachaTables []GachaTable
	// Moved from wrapper model
	// Selectable character Id list
	SelectionCharacterIds []GachaSelectionCharacter
	// Tradable character Id list
	DrawPoints []GachaDrawPointTradable
}

func (g *Gacha) GetNormalCharacters() []value_character.CharacterId {
	var result []value_character.CharacterId
	for _, table := range g.GachaTables {
		if !table.Pickup {
			result = append(result, table.CharacterId)
		}
	}
	return result
}

func (g *Gacha) GetPickupCharacters() []value_character.CharacterId {
	var result []value_character.CharacterId
	for _, table := range g.GachaTables {
		if table.Pickup {
			result = append(result, table.CharacterId)
		}
	}
	return result
}

func (g *Gacha) IsUnconditional() bool {
	return g.ItemId == 10000
}

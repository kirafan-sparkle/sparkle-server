package model_gacha

import (
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
)

type GachaTable struct {
	GachaTableId uint `gorm:"primary_key"`
	CharacterId  value_character.CharacterId
	Rarity       value_character.CharacterRarity
	Pickup       bool
	// Foreign key
	GachaId uint
}

package model_character

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

type Character struct {
	CharacterId           uint64 `gorm:"primaryKey"`
	CharacterName         string
	NamedType             uint16
	Rarity                uint8
	Class                 value_character.ClassType
	Element               value_character.ElementType
	Cost                  uint8
	GrowthTableId         uint8
	InitLv                uint8
	InitLimitLv           uint8
	InitHp                uint16
	InitAtk               uint16
	InitMgc               uint16
	InitDef               uint16
	InitMDef              uint16
	InitSpd               uint16
	InitLuck              uint16
	SkillLimitLv          uint8
	CharaSkillID          uint32
	CharaSkillExpTableID  uint8
	ClassSkillID1         uint32
	ClassSkillID2         uint32
	ClassSkillExpTableID1 uint8
	ClassSkillExpTableID2 uint8
	StunCoef              float64
	AltItemID             value_item.ItemId
	AltItemAmount         uint8
	FullOpen              value.BoolLikeUInt8
	LimitBreakRecipeID    uint16
	IsWeaken              bool
	IsDistributed         bool
	IsPeriodLimited       bool
	Year                  uint16
}

package model_schedule

import value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"

type ScheduleDropItem struct {
	DropItemId value_item.ItemId `json:"drop_item_id"`
	Period     [2]uint8          `json:"period"`
}

type Schedule struct {
	ScheduleTable string             `json:"schedule_table"`
	DropItems     []ScheduleDropItem `json:"drop_items"`
}

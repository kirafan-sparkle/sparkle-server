package model_chest

import (
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

type Chest struct {
	ChestId int64 `gorm:"primary_key"`

	Name       string
	EventType  uint8
	BannerName string
	BgName     string

	StartAt     time.Time
	EndAt       time.Time
	DispStartAt time.Time
	DispEndAt   time.Time

	CostItemId     value_item.ItemId
	CostItemAmount uint8

	CurrentStep  uint16
	MaxStep      uint16
	CurrentStock uint16
	TotalStock   uint16
	EnableReset  value.BoolLikeUInt8

	TutorialTipsId int64
	TradeGroupId   int64

	ResetChestPrizes []ResetChestPrize
}

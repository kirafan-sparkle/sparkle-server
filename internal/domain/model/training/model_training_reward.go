package model_training

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

type TrainingReward struct {
	Id         uint `gorm:"primary_key"`
	TrainingId uint
	ItemId     value_item.ItemId
	// maybe this one will show special effect on item icon at clear screen
	RareDisp value.BoolLikeUInt8
	// server side only
	DropRate []TrainingRewardDrop `gorm:"foreignkey:RewardId"`
}

package model_training

import (
	"time"

	value_training "gitlab.com/kirafan/sparkle/server/internal/domain/value/training"
)

type TrainingSlot struct {
	TrainingSlotPk uint8                 `gorm:"primary_key"`
	TrainingSlotId value_training.SlotId `gorm:"index;unique;not null;autoIncrement:false"`
	CreatedAt      time.Time
	UpdatedAt      time.Time
	DeletedAt      *time.Time
	NeedRank       uint16
}

package model_training

type TrainingRewardDrop struct {
	// Primary key
	Id uint `gorm:"primary_key"`
	// foreignkey
	RewardId uint
	// Drop amount (only for server side not exists in client)
	Amount uint32
	// Drop probability (only for server side not exists in client)
	Probability uint8
}

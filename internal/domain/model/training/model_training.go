package model_training

import (
	"time"

	value_training "gitlab.com/kirafan/sparkle/server/internal/domain/value/training"
)

type Training struct {
	TrainingId uint `gorm:"primary_key"`
	CreatedAt  time.Time
	UpdatedAt  time.Time
	DeletedAt  *time.Time
	// Training menu name
	Name string
	// Needed cost type (Coin or KRRPoint)
	CostType value_training.TrainingCostType
	// Needed amount of cost type
	Cost uint64
	// Unused, Seems not working at all or worked at server side  (default: 1)
	OpenRank uint16
	// Unused, Seems not working at all  (default: 1)
	Category int16
	// Unknown, maybe user's total order count? or worked at server side (default: 0)
	Num uint16
	// Unused, maybe limit of total order count? or worked at server side (default: -1)
	LimitNum int16
	// Required character level to go this training (default: -1)
	CondCharaLv int16
	// Required party member count to go this training (default: -1)
	CondPartyNum int16
	// Required time to clear in minutes
	ClearTime uint16
	// Required gems to skip
	SkipGem uint32
	// First clear reward gem (default 5)
	FirstGem uint32
	// Reward character exp per training clear
	RewardCharaExp uint64
	// Reward namedType exp per training clear
	RewardFriendship uint64
	// Reward currency type (Coin or KRRPoint)
	RewardCurrencyType value_training.TrainingRewardCurrencyType
	// Reward currency amount
	RewardCurrencyAmount uint64
	// Item rewards (display)
	Rewards []TrainingReward `gorm:"foreignkey:TrainingId"`
}

package model_evo_table

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
)

type EvoTableWeapon struct {
	RecipeId        uint `gorm:"primaryKey"`
	SrcWeaponId     value_weapon.WeaponId
	DestWeaponId    value_weapon.WeaponId
	RequiredCoin    uint32
	IsFinalEvo      value.BoolLikeUInt8
	RecipeMaterials []EvoTableWeaponRecipeMaterial `gorm:"foreignKey:RecipeId"`
}

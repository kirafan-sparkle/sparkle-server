package model_evo_table

import value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"

type EvoTableEvolutionRequiredItem struct {
	RecipeRequiredItemId uint `gorm:"primaryKey"`
	// Foreign key
	RecipeId uint
	ItemId   value_item.ItemId
	Amount   int16
}

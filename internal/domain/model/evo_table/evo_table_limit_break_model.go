package model_evo_table

import (
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

type EvoTableLimitBreak struct {
	RecipeId            uint `gorm:"primaryKey"`
	Rarity              value_character.CharacterRarity
	Class               value_character.ClassType
	RequiredCoinPerItem uint32
	ClassItemId         value_item.ItemId
	ClassItemAmount     int16
	AllClassItemId      value_item.ItemId
	AllClassItemAmount  int16
	TitleItemAmount     int16
}

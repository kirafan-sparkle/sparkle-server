package model_evo_table

import value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"

type EvoTableWeaponRecipeMaterial struct {
	Id       uint `gorm:"primaryKey"`
	RecipeId uint
	ItemId   value_item.ItemId
	Amount   uint32
}

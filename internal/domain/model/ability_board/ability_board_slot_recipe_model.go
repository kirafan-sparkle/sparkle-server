package model_ability_board

import (
	value_ability_board "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_board"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

type AbilityBoardSlotRecipe struct {
	AbilityBoardSlotRecipeId value_ability_board.AbilityBoardSlotRecipeId `gorm:"primary_key"`
	ItemId                   value_item.ItemId
	ItemAmount               uint32
	RequiredCoins            uint32
}

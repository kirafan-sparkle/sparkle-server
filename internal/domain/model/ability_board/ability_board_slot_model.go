package model_ability_board

import value_ability_board "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_board"

type AbilityBoardSlot struct {
	AbilityBoardSlotId   value_ability_board.AbilityBoardSlotId `gorm:"primary_key"`
	AbilityBoardId       value_ability_board.AbilityBoardId
	SlotIndex            value_ability_board.AbilityBoardSlotIndex
	IsSpecialSlot        bool
	IsInitialSlot        bool
	AbilityBoardRecipeId value_ability_board.AbilityBoardSlotRecipeId
}

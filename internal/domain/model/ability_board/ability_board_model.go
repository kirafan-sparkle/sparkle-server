package model_ability_board

import (
	value_ability_board "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_board"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

type AbilityBoard struct {
	AbilityBoardId value_ability_board.AbilityBoardId `gorm:"primary_key"`
	ItemId         value_item.ItemId
}

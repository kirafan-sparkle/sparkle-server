package model_ability_sphere

import (
	value_ability_sphere "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_sphere"
)

type AbilitySphereRecipe struct {
	AbilitySphereRecipeId value_ability_sphere.AbilitySphereRecipeId `gorm:"primary_key"`
	BaseItemId            value_ability_sphere.AbilitySphereItemId
	SrcItemId             value_ability_sphere.AbilitySphereItemId
	SrcItemAmount         uint32
	DstItemId             value_ability_sphere.AbilitySphereItemId
	GoldAmount            uint32
}

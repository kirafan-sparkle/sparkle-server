package model_ability_sphere

import (
	value_ability_sphere "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_sphere"
)

type AbilitySphere struct {
	AbilitySphereId value_ability_sphere.AbilitySphereId `gorm:"primary_key"`
	SphereItemId    value_ability_sphere.AbilitySphereItemId
	SphereType      value_ability_sphere.AbilitySphereType
	/** effect amount or special effect id */
	ItemValue uint32
}

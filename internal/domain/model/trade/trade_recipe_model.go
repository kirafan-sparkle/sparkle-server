package model_trade

import (
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_present "gitlab.com/kirafan/sparkle/server/internal/domain/value/present"
)

type TradeRecipe struct {
	Id      uint `gorm:"primaryKey"`
	StartAt *time.Time
	EndAt   *time.Time
	// -1 if not event, otherwise eventId
	EventType int32
	// 0 if not event, otherwise eventGroupId?
	GroupId uint32
	// Unknown 0 or 1?
	LabelId uint8
	// Sort priority?
	Sort int32
	// Source info
	SrcType1   value_present.PresentType
	SrcId1     int32
	SrcAmount1 int32
	SrcType2   value_present.PresentType
	SrcId2     int32
	SrcAmount2 int32
	// Destination info
	DstType   value_present.PresentType
	DstId     int32
	DstAmount int32
	// Alternative item if the dest is character and max level
	AltType   value_present.PresentType
	AltId     value_item.ItemId
	AltAmount int32
	// 10003 if altId isn't -1
	AltMessageMakeId int32
	// 10004 if altId isn't -1
	MessageMakeId int32
	// Maybe show limited if this is true
	LimitedFlag value.BoolLikeUInt8
	// Maybe show monthly reset if this is true
	ResetFlag value.BoolLikeUInt8
	// Maximum trade amount per a trade, 999 for unlimited (default display value)
	TradeMax uint16
	// Maximum trade amounts for this recipe, -1 for unlimited
	LimitCount int16
}

func (t *TradeRecipe) IsAvailable(now time.Time) bool {
	if t.StartAt == nil || t.EndAt == nil {
		return true
	}
	return t.StartAt.Before(now) && t.EndAt.After(now)
}

func (t *TradeRecipe) IsMonthlyReset() bool {
	return t.ResetFlag.IsTrue()
}

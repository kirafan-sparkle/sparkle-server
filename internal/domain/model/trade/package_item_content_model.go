package model_trade

import (
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_present "gitlab.com/kirafan/sparkle/server/internal/domain/value/present"
)

type PackageItemContent struct {
	Id        uint `gorm:"primaryKey"`
	PackageId uint
	// Seems only PresentTypeItem(2) or PresentTypeGold(8)?
	ItemType   value_present.PresentType
	ItemId     value_item.ItemId
	ItemAmount uint32
}

package model_trade

type PackageItem struct {
	Id        uint `gorm:"primaryKey"`
	PackageId uint
	IconId    uint32
	Name      string
	Detail    string
	Contents  []PackageItemContent `gorm:"foreignKey:PackageId"`
}

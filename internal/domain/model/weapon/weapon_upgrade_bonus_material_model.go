package model_weapon

import (
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
)

type WeaponUpgradeBonusMaterial struct {
	Id       uint32 `gorm:"primaryKey"`
	WeaponId value_weapon.WeaponId
	ItemId   value_item.ItemId
}

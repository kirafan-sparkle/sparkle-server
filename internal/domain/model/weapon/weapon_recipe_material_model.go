package model_weapon

import value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"

type WeaponRecipeMaterial struct {
	Id       uint32 `gorm:"primaryKey"`
	RecipeId uint32
	ItemId   value_item.ItemId
	Amount   uint32
}

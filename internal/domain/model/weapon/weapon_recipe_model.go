package model_weapon

import (
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
)

type WeaponRecipe struct {
	RecipeId        uint32 `gorm:"primaryKey"`
	WeaponId        value_weapon.WeaponId
	BuyAmount       uint32
	RecipeMaterials []WeaponRecipeMaterial `gorm:"foreignKey:RecipeId;references:RecipeId"`
}

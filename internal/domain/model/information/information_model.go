package model_information

import (
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
)

type Information struct {
	Id       int64                  `gorm:"primaryKey" json:"id"`
	Sort     int64                  `json:"sort"`
	Platform value_version.Platform `json:"platform"`

	StartAt     time.Time `json:"startAt"`
	EndAt       time.Time `json:"endAt"`
	DispStartAt time.Time `json:"dispStartAt"`
	DispEndAt   time.Time `json:"dispEndAt"`

	IsFeatured value.BoolLikeUInt8 `json:"isFeatured"`
	ImgId      string              `json:"imgId"`
	Url        string              `json:"url"`
}

package model_user

import (
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
)

func newManagedWeapon(userId value_user.UserId, weaponId value_weapon.WeaponId) ManagedWeapon {
	return ManagedWeapon{
		UserId:     userId,
		WeaponId:   weaponId,
		Level:      1,
		Exp:        0,
		SkillLevel: 1,
		SkillExp:   0,
		State:      1,
		SoldAt:     nil,
	}
}

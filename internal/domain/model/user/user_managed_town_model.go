package model_user

import value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"

type ManagedTown struct {
	ManagedTownId uint `gorm:"primarykey"`
	// Foreign key
	UserId   value_user.UserId
	GridData string
}

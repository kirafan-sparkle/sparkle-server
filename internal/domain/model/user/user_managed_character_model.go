package model_user

import (
	"time"

	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

type ManagedCharacter struct {
	ManagedCharacterId value_user.ManagedCharacterId `gorm:"primarykey"`
	CreatedAt          time.Time                     `gorm:"autoCreateTime"`
	UpdatedAt          time.Time                     `gorm:"autoUpdateTime"`
	// ForeignKey
	PlayerId value_user.UserId
	// レベル現在値 (1~100)
	Level value_character.CharacterLevel
	// レベル最大値 (30~100)
	LevelLimit value_character.CharacterLevel
	// 経験値
	Exp uint64
	// 限界突破回数 (0~4)
	LevelBreak uint8
	// 重複回数
	DuplicatedCount uint8
	// 覚醒レベル (0~5)
	ArousalLevel uint8
	// とっておき 現在レベル (1~35)
	SkillLevel1 uint8
	// とっておき 最大レベル (5~35)
	SkillLevelLimit1 uint8
	// とっておき 使用回数 (0~)
	SkillExp1 uint32
	// スキル1 現在レベル (1~25)
	SkillLevel2 uint8
	// スキル1 最大レベル (5~25)
	SkillLevelLimit2 uint8
	// スキル1 使用回数 (0~)
	SkillExp2 uint32
	// スキル2 現在レベル (1~)
	SkillLevel3 uint8
	// スキル2 最大レベル (5~25)
	SkillLevelLimit3 uint8
	// スキル2 使用回数 (0~)
	SkillExp3 uint32
	// isNew (0: un-shown / 1: shown)
	Shown uint8
	// CardId
	CharacterId value_character.CharacterId
	// SD displayId
	ViewCharacterId value_character.CharacterId
}

type SkillType uint8

const (
	SkillType1 SkillType = iota
	SkillType2
	SkillType3
)

func (m *ManagedCharacter) IsMaxLevel() bool {
	return m.Level == m.LevelLimit
}

func (m *ManagedCharacter) UpdateLevel(level value_character.CharacterLevel) {
	newLevel := calc.Max(m.Level, level)
	m.Level = calc.Min(newLevel, m.LevelLimit)
	m.UpdatedAt = time.Now()
}

func (m *ManagedCharacter) AddExp(exp uint64) {
	m.Exp += exp
	m.UpdatedAt = time.Now()
}

func (m *ManagedCharacter) AdjustExp(maxExp uint64) {
	m.Exp = calc.Min(m.Exp, maxExp)
	m.UpdatedAt = time.Now()
}

func (m *ManagedCharacter) IsMaxSkillLevel(skillType SkillType) bool {
	switch skillType {
	case SkillType1:
		return m.SkillLevel1 == m.SkillLevelLimit1
	case SkillType2:
		return m.SkillLevel2 == m.SkillLevelLimit2
	case SkillType3:
		return m.SkillLevel3 == m.SkillLevelLimit3
	default:
		return false
	}
}

func (m *ManagedCharacter) UpdateSkillLevel(level uint8, skillType SkillType) {
	switch skillType {
	case SkillType1:
		newLevel := calc.Max(m.SkillLevel1, level)
		m.SkillLevel1 = calc.Min(newLevel, m.SkillLevelLimit1)
	case SkillType2:
		newLevel := calc.Max(m.SkillLevel2, level)
		m.SkillLevel2 = calc.Min(newLevel, m.SkillLevelLimit2)
	case SkillType3:
		newLevel := calc.Max(m.SkillLevel3, level)
		m.SkillLevel3 = calc.Min(newLevel, m.SkillLevelLimit3)
	default:
	}
	m.UpdatedAt = time.Now()
}

func (m *ManagedCharacter) AddSkillExp(exp uint32, skillType SkillType) {
	switch skillType {
	case SkillType1:
		m.SkillExp1 += exp
	case SkillType2:
		m.SkillExp2 += exp
	case SkillType3:
		m.SkillExp3 += exp
	}
	m.UpdatedAt = time.Now()
}

func (m *ManagedCharacter) AdjustSkillExp(skillType SkillType, maxExp uint32) {
	switch skillType {
	case SkillType1:
		m.SkillExp1 = calc.Min(m.SkillExp1, maxExp)
	case SkillType2:
		m.SkillExp2 = calc.Min(m.SkillExp2, maxExp)
	case SkillType3:
		m.SkillExp3 = calc.Min(m.SkillExp3, maxExp)
	}
	m.UpdatedAt = time.Now()
}

// Add duplicated count and increase ArousalLevel if needed
func (m *ManagedCharacter) IncreaseDuplicatedCount() bool {
	m.DuplicatedCount++
	m.UpdatedAt = time.Now()
	var levelUpCounts = map[value_character.CharacterIdRarity][]uint8{
		value_character.CharacterIdRarityStar3: {1, 6, 17},
		value_character.CharacterIdRarityStar4: {1, 4, 7, 11},
		value_character.CharacterIdRarityStar5: {1, 2, 3, 4, 5},
	}
	var skillLevel1UpCounts = map[value_character.CharacterIdRarity][]uint8{
		value_character.CharacterIdRarityStar3: {2, 2, 2},
		value_character.CharacterIdRarityStar4: {2, 1, 1, 1},
		value_character.CharacterIdRarityStar5: {2, 2, 2, 2, 2},
	}
	if len(levelUpCounts) != len(skillLevel1UpCounts) {
		panic("levelUpCounts and skillLevel1UpCounts must have the same length")
	}
	rarity := m.CharacterId.GetRarity()
	if m.ArousalLevel < uint8(len(levelUpCounts[rarity])) && m.DuplicatedCount >= levelUpCounts[rarity][m.ArousalLevel] {
		m.ArousalLevel++
		m.SkillLevelLimit1 += skillLevel1UpCounts[rarity][m.ArousalLevel-1]
	}
	return false
}

func (m *ManagedCharacter) IncreaseLevelBreakCount() {
	m.LevelBreak++
	m.LevelLimit += 5
	if m.LevelBreak%2 == 0 {
		m.SkillLevelLimit1 += 3
		m.SkillLevelLimit2 += 3
		m.SkillLevelLimit3 += 3
	} else {
		m.SkillLevelLimit1 += 2
		m.SkillLevelLimit2 += 2
		m.SkillLevelLimit3 += 2
	}
	m.UpdatedAt = time.Now()
}

func (m *ManagedCharacter) IsLimitBreakable(increaseCount uint8) bool {
	return m.LevelBreak+increaseCount <= 4
}

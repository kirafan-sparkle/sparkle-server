package model_user

import (
	"time"

	model_training "gitlab.com/kirafan/sparkle/server/internal/domain/model/training"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_training "gitlab.com/kirafan/sparkle/server/internal/domain/value/training"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type UserTraining struct {
	UserTrainingId uint `gorm:"primary_key"`
	CreatedAt      time.Time
	UpdatedAt      time.Time
	Cleared        value.BoolLikeUInt8
	UserId         value_user.UserId
	TrainingId     uint
	Training       model_training.Training `gorm:"PRELOAD:false"`
}

func NewUserTraining(userId value_user.UserId, trainingId uint) *UserTraining {
	currentTime := time.Now()
	return &UserTraining{
		CreatedAt:  currentTime,
		UpdatedAt:  currentTime,
		Cleared:    value.BoolLikeUIntFalse,
		UserId:     userId,
		TrainingId: trainingId,
	}
}

func (t UserTraining) GetCostInfo() (value_training.TrainingCostType, uint64) {
	return t.Training.CostType, t.Training.Cost
}

func (t *UserTraining) SetClearedState() {
	t.Cleared = value.BoolLikeUIntTrue
}

func (t UserTraining) IsFirstClear() bool {
	return t.Cleared.IsFalse()
}

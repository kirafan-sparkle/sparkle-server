package model_user

import (
	"time"

	model_mission "gitlab.com/kirafan/sparkle/server/internal/domain/model/mission"
	value_mission "gitlab.com/kirafan/sparkle/server/internal/domain/value/mission"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type UserMission struct {
	ManagedMissionId uint `gorm:"primary_key"`
	CreatedAt        time.Time
	UpdatedAt        time.Time
	DeletedAt        *time.Time `sql:"index"`
	LimitTime        time.Time
	Rate             uint
	State            value_mission.MissionState
	// foreignKey
	MissionId uint
	Mission   model_mission.Mission
	// foreignKey
	UserId value_user.UserId
	User   User `gorm:"PRELOAD:false"`
}

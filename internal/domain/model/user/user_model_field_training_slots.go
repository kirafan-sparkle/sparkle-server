package model_user

import (
	"errors"

	value_training "gitlab.com/kirafan/sparkle/server/internal/domain/value/training"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type trainingSlotsField []*UserTrainingSlot

func (f *trainingSlotsField) GetEndedSlotAmount() uint8 {
	amount := uint8(0)
	for _, v := range *f {
		if v.IsEnded() {
			amount++
		}
	}
	return amount
}

func (f *trainingSlotsField) GetSlotById(slotId value_training.SlotId) (*UserTrainingSlot, error) {
	for _, v := range *f {
		if v.TrainingSlotId == slotId {
			return v, nil
		}
	}
	return nil, errors.New("slot not found")
}

func (f *trainingSlotsField) GetSlotByOrderId(orderId uint) (*UserTrainingSlot, error) {
	for _, v := range *f {
		// If the training slot is empty, this orderId can be nil
		if v.Order == nil {
			continue
		}
		if *v.OrderId == orderId {
			return v, nil
		}
	}
	return nil, errors.New("slot not found")
}

func (f trainingSlotsField) GetOutingManagedCharacterIds() []value_user.ManagedCharacterId {
	ids := make([]value_user.ManagedCharacterId, 0, 5*len(f))
	for _, v := range f {
		ids = append(ids, v.GetOutingManagedCharacterIds()...)
	}
	return ids
}

func (f trainingSlotsField) GetOutingUserTrainingIds() []uint {
	ids := make([]uint, 0, len(f))
	for _, v := range f {
		if v.Order == nil {
			continue
		}
		ids = append(ids, v.Order.UserTrainingId)
	}
	return ids
}

package model_user

import (
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	value_shared "gitlab.com/kirafan/sparkle/server/internal/domain/value/shared"
)

type UserOnGoingQuest struct {
	OrderReceiveId uint
	QuestId        uint
	QuestData      string
	WaveData       [][]*model_quest.QuestWave
	DropData       [][][]model_quest.QuestWaveDrop
}

func NewUserOnGoingQuest(
	orderReceiveId uint,
	questId uint,
	questData string,
	waveData [][]*model_quest.QuestWave,
	dropData [][][]model_quest.QuestWaveDrop,
) UserOnGoingQuest {
	return UserOnGoingQuest{
		OrderReceiveId: orderReceiveId,
		QuestId:        questId,
		QuestData:      questData,
		WaveData:       waveData,
		DropData:       dropData,
	}
}

func (uog *UserOnGoingQuest) AsJsonString() *value_shared.JsonString {
	res, err := value_shared.NewJsonString(uog)
	if err != nil {
		panic(err)
	}
	return &res
}

package model_user

import (
	"time"

	model_gacha "gitlab.com/kirafan/sparkle/server/internal/domain/model/gacha"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

// Moved from gacha model, these values depends per a user
type UserGacha struct {
	UserGachaId uint `gorm:"primary_key"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   *time.Time `sql:"index"`

	Gem10CurrentStep uint8  // default: 1
	PlayerDrawPoint  uint16 // default: 0

	// Maybe this is a count of drew  10 times gacha this day?
	Gem10Daily uint16
	// Maybe this is a total count of drew 10 times gacha?
	Gem10Total uint32
	// Maybe this is a count of drew gacha this day?
	Gem1Daily uint16
	// Maybe this is a total count of drew gacha?
	Gem1Total uint32
	// Maybe this is a count of drew gacha with unlimited gem in this day?
	UGem1Daily uint16
	// Maybe this is a total count of drew gacha with unlimited gem?
	UGem1Total uint32
	// Maybe this is a count of drew gacha with item in this day?
	ItemDaily uint16
	// Maybe this is a total count of drew gacha with item?
	ItemTotal uint32

	// Free drew count? (maybe reset per a day?)
	Gem1FreeDrawCount  uint16 // default 0
	Gem10FreeDrawCount uint16 // default 0

	// foreignKey
	GachaId uint
	Gacha   model_gacha.Gacha `gorm:"PRELOAD:false"`
	// foreignKey
	UserId value_user.UserId
	User   User `gorm:"PRELOAD:false"`
}

package model_user

import (
	"time"

	model_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/model/login_bonus"
	value_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/value/login_bonus"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

// Moved from login_bonus model, dayIndex depends per a user
type UserLoginBonus struct {
	UserLoginBonusId uint `gorm:"primary_key"`
	CreatedAt        time.Time
	UpdatedAt        time.Time
	DeletedAt        *time.Time `sql:"index"`

	LoginBonusDayIndex value_login_bonus.LoginBonusDayIndex

	// foreignKey
	LoginBonusId uint
	LoginBonus   model_login_bonus.LoginBonus
	// foreignKey
	UserId value_user.UserId
	User   User `gorm:"PRELOAD:false"`
}

package model_user

import (
	"errors"
	"time"

	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

func newManagedNamedType(userId value_user.UserId, namedType uint16, titleType value_character.TitleType) (ManagedNamedType, error) {
	if namedType > 248 {
		return ManagedNamedType{}, errors.New("namedType must be between 0 and 248")
	}
	if titleType > 37 {
		return ManagedNamedType{}, errors.New("titleType must be between 0 and 37")
	}
	currentTime := time.Now()
	managedNamedType := ManagedNamedType{
		CreatedAt:            currentTime,
		UpdatedAt:            currentTime,
		UserId:               userId,
		NamedType:            namedType,
		Level:                1,
		Exp:                  0,
		TitleType:            titleType,
		FriendshipExpTableId: 0,
	}
	return managedNamedType, nil
}

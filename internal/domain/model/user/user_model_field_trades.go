package model_user

import (
	"errors"
	"time"

	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type tradesField []UserTradeRecipe

func (s *tradesField) GetUserTradeByRecipeId(recipeId uint) *UserTradeRecipe {
	for i := range *s {
		if (*s)[i].RecipeId == recipeId {
			return &(*s)[i]
		}
	}
	return nil
}

func (s *tradesField) IncreaseUserTradeCounts(recipeId uint, increaseAmount uint16) error {
	for i := range *s {
		if (*s)[i].RecipeId == recipeId {
			(*s)[i].UpdatedAt = time.Now()
			(*s)[i].TotalTradeCount += increaseAmount
			(*s)[i].MonthlyTradeCount += increaseAmount
			return nil
		}
	}
	return errors.New("specified recipeId not found")
}

func (s *tradesField) HasTradeRecipe(recipeId uint) bool {
	exist := s.GetUserTradeByRecipeId(recipeId)
	return exist != nil
}

func (s *tradesField) AddTradeRecipe(userId value_user.UserId, recipeId uint) error {
	if s.HasTradeRecipe(recipeId) {
		return errors.New("specified recipeId already exists")
	}
	now := time.Now()
	newTradeRecipe := UserTradeRecipe{
		CreatedAt:         now,
		UpdatedAt:         now,
		TotalTradeCount:   0,
		MonthlyTradeCount: 0,
		RecipeId:          recipeId,
		UserId:            userId,
	}
	(*s) = append((*s), newTradeRecipe)
	return nil
}

func (s *tradesField) RefreshTradeRecipes(currentTime time.Time) {
	for index := 0; index < len((*s)); index++ {
		(*s)[index].RefreshMonthlyCount(currentTime)
		if !(*s)[index].IsAvailable(currentTime) {
			(*s)[index].DeletedAt = &currentTime
		}
	}
}

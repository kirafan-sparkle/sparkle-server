package model_user

import (
	"time"

	value_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/value/town_facility"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type managedFacilitiesField []ManagedTownFacility

func (s *managedFacilitiesField) GetManagedFacility(managedTownFacilityId uint32) (*ManagedTownFacility, error) {
	for _, facility := range *s {
		if uint32(facility.ManagedTownFacilityId) == managedTownFacilityId {
			return &facility, nil
		}
	}
	return nil, ErrUserManagedFacilityNotFound
}

func (s *managedFacilitiesField) AddManagedFacility(userId value_user.UserId, facilityId uint32, buildPointIndex int32, level uint8, actionTime uint64, buildTime uint64, openState uint8) error {
	newManagedFacility, err := newManagedTownFacility(
		userId,
		facilityId,
		buildPointIndex,
		level,
		actionTime,
		buildTime,
		openState,
	)
	if err != nil {
		return err
	}
	*s = append(*s, *newManagedFacility)
	return nil
}

func (s *managedFacilitiesField) RemoveManagedFacility(managedTownFacilityId uint32) error {
	for i := range *s {
		if uint32((*s)[i].ManagedTownFacilityId) != managedTownFacilityId {
			continue
		}
		currentTime := time.Now()
		(*s)[i].DeletedAt = &currentTime
		return nil
	}
	return ErrUserManagedFacilityNotFound
}

func (s *managedFacilitiesField) LevelUpManagedFacility(
	managedTownFacilityId uint32,
	newLevel uint8,
	actionTime uint64,
	openState value_town_facility.TownFacilityOpenState,
) error {
	for i := range *s {
		if uint32((*s)[i].ManagedTownFacilityId) == managedTownFacilityId {
			(*s)[i].Level = newLevel
			(*s)[i].ActionTime = actionTime
			(*s)[i].OpenState = openState
			return nil
		}
	}
	return ErrUserManagedFacilityNotFound
}

func (s *managedFacilitiesField) OpenUpManagedFacility(
	managedTownFacilityId uint32,
	newLevel uint8,
	buildTime uint64,
	openState value_town_facility.TownFacilityOpenState,
) error {
	for i := range *s {
		if uint32((*s)[i].ManagedTownFacilityId) == managedTownFacilityId {
			(*s)[i].Level = newLevel
			(*s)[i].BuildTime = buildTime
			(*s)[i].OpenState = openState
			return nil
		}
	}
	return ErrUserManagedFacilityNotFound
}

func (s *managedFacilitiesField) UpdateManagedFacilityActionTime(managedTownFacilityId uint32, actionTime uint64) error {
	for i := range *s {
		if uint32((*s)[i].ManagedTownFacilityId) == managedTownFacilityId {
			(*s)[i].ActionTime = actionTime
			return nil
		}
	}
	return ErrUserManagedFacilityNotFound
}

func (s *managedFacilitiesField) MoveManagedTownFacility(
	managedTownFacilityId uint32,
	buildPointIndex value_town_facility.BuildPointIndex,
	buildTime uint64,
	openState value_town_facility.TownFacilityOpenState,
) error {
	for i := range *s {
		if uint32((*s)[i].ManagedTownFacilityId) == managedTownFacilityId {
			(*s)[i].BuildPointIndex = buildPointIndex
			(*s)[i].BuildTime = buildTime
			(*s)[i].OpenState = openState
			return nil
		}
	}
	return ErrUserManagedFacilityNotFound
}

package model_user

import (
	value_room "gitlab.com/kirafan/sparkle/server/internal/domain/value/room"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

// User main room and sub room
type ManagedRoom struct {
	ManagedRoomId uint `gorm:"primarykey"`
	// Foreign key
	UserId value_user.UserId
	// RoomNo 1 or 2
	FloorId value_room.FloorId
	// Mystery (0: default)
	GroupId uint8
	// Room object data
	ArrangeData []ManagedRoomArrangeData
	// Mystery (0: default)
	Active uint8
}

// User room objects
type ManagedRoomArrangeData struct {
	// This is only for server side not in response
	ManagedRoomArrangeDataId uint `gorm:"primarykey"`
	// Foreign key
	ManagedRoomObjectId int `gorm:"uniqueIndex:idx_managed_room_object_id"`
	// Foreign key
	ManagedRoomId uint
	// RoomObjectId of this object (ref: https://wiki.kirafan.moe/#/roomobjects)
	RoomObjectId uint32
	// Target of this object be placed (0: main room / 1: bed room / -1: background or wall or floor)
	RoomNo value_room.RoomNo
	// PositionX of room object
	X value_room.RoomObjectPosition
	// PositionY of room object
	Y value_room.RoomObjectPosition
	// Direction of room object
	Dir value_room.RoomObjectDirection
}

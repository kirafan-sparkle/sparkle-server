package model_user

import (
	"time"

	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

// Cleared quests ( not adv Id since it conflicts to type)
type ClearedAdvId struct {
	Id        uint `gorm:"primarykey"`
	CreatedAt time.Time
	UserId    value_user.UserId
	User      User
	AdvId     uint64
}

package model_user

import (
	"time"

	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type ItemSummary struct {
	ItemSummaryId uint `gorm:"primarykey"`
	CreatedAt     time.Time
	UpdatedAt     time.Time
	Amount        uint32
	// ItemId
	Id value_item.ItemId
	// Foreign key
	UserId value_user.UserId
}

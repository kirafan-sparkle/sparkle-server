package model_user

import (
	"time"

	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

type ManagedNamedType struct {
	ManagedNamedTypeId uint      `gorm:"primarykey"`
	CreatedAt          time.Time `gorm:"autoCreateTime"`
	UpdatedAt          time.Time `gorm:"autoUpdateTime"`
	// Foreign Key TODO: add relation later
	UserId    value_user.UserId
	NamedType uint16
	Level     uint8
	Exp       uint32
	TitleType value_character.TitleType
	// Unknown (default 0)
	FriendshipExpTableId uint8
}

func (m *ManagedNamedType) AddExp(amount uint32) {
	m.Exp += amount
	m.UpdatedAt = time.Now()
}

func (m *ManagedNamedType) UpdateLevel(level uint8) {
	m.Level = calc.Max(m.Level, level)
	m.UpdatedAt = time.Now()
}

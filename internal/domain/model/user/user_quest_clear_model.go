package model_user

import (
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type UserQuestClearRank struct {
	QuestClearRankId uint `gorm:"primarykey"`
	UserId           value_user.UserId
	QuestId          uint
	ClearRank        value_quest.ClearRank
}

func NewUserQuestClearRank(
	userId value_user.UserId,
	questId uint,
	clearRank value_quest.ClearRank,
) UserQuestClearRank {
	return UserQuestClearRank{
		UserId:    userId,
		QuestId:   questId,
		ClearRank: clearRank,
	}
}

func NewUserAdvQuestClearRank(
	userId value_user.UserId,
	questId uint,
) UserQuestClearRank {
	return UserQuestClearRank{
		UserId:    userId,
		QuestId:   questId,
		ClearRank: value_quest.ClearRankGold,
	}
}

type UserQuestClearRanks []*UserQuestClearRank

func (qcr *UserQuestClearRanks) GetClearRank(questId uint) value_quest.ClearRank {
	for _, qcr := range *qcr {
		if qcr.QuestId == questId {
			return qcr.ClearRank
		}
	}
	return value_quest.ClearRankNone
}

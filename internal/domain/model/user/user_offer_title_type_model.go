package model_user

import (
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gorm.io/gorm"
)

// Crea craft
type OfferTitleType struct {
	gorm.Model
	TitleType      value_character.TitleType
	ContentRoomFlg uint8
	Category       int8
	OfferIndex     int8
	OfferPoint     int32
	OfferMaxPoint  int32
	State          uint8
	Shown          uint8
	SubState       uint8
	SubState1      uint8
	SubState2      uint8
	SubState3      uint8
	// Foreign Key
	UserId value_user.UserId
}

func (o *OfferTitleType) SetShown() {
	o.Shown = 1
}

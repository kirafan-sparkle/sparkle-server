package model_user

import value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"

type ManagedBattleParty struct {
	ManagedBattlePartyId int `gorm:"primarykey"`
	// Foreign key
	UserId              value_user.UserId
	Name                string
	CostLimit           uint8
	ManagedCharacterId1 int64
	ManagedCharacterId2 int64
	ManagedCharacterId3 int64
	ManagedCharacterId4 int64
	ManagedCharacterId5 int64
	ManagedWeaponId1    int32
	ManagedWeaponId2    int32
	ManagedWeaponId3    int32
	ManagedWeaponId4    int32
	ManagedWeaponId5    int32
	MasterOrbId         uint8
}

package model_user

import (
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type itemSummaryField []ItemSummary

func (s *itemSummaryField) Length() int {
	return len(*s)
}

func (s *itemSummaryField) HasItem(itemId value_item.ItemId) bool {
	for _, item := range *s {
		if item.Id == itemId {
			return true
		}
	}
	return false
}

func (s *itemSummaryField) HasItemMoreThan(itemId value_item.ItemId, count uint32) bool {
	for _, item := range *s {
		if item.Id == itemId {
			return item.Amount >= count
		}
	}
	return false
}

func (s *itemSummaryField) GetItemAmount(itemId value_item.ItemId) uint32 {
	for _, item := range *s {
		if item.Id == itemId {
			return item.Amount
		}
	}
	return 0
}

func (s *itemSummaryField) AddItem(userId value_user.UserId, itemId value_item.ItemId, count uint32) {
	for i, item := range *s {
		if item.Id == itemId {
			(*s)[i].Amount += count
			return
		}
	}
	newItem := ItemSummary{
		UserId: userId,
		Id:     itemId,
		Amount: count,
	}
	*s = append(*s, newItem)
}

func (s *itemSummaryField) ConsumeItem(itemId value_item.ItemId, count uint32) bool {
	for index := 0; index < len(*s); index++ {
		if (*s)[index].Id != itemId {
			continue
		}
		if (*s)[index].Amount < count {
			return false
		}
		(*s)[index].Amount -= count
		return true
	}
	return false
}

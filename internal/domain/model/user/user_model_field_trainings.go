package model_user

import (
	"errors"
	"slices"
)

type trainingsField []*UserTraining

func (f *trainingsField) GetUserTrainingsExceptOuting(outingUserTrainingIds []uint) []*UserTraining {
	result := make([]*UserTraining, 0, len(outingUserTrainingIds))
	for _, userTraining := range *f {
		if slices.Contains(outingUserTrainingIds, userTraining.UserTrainingId) {
			continue
		}
		result = append(result, userTraining)
	}
	return result
}

func (f *trainingsField) GetUserTrainingById(trainingId uint) (*UserTraining, error) {
	for _, training := range *f {
		if training.TrainingId == trainingId {
			return training, nil
		}
	}
	return nil, errors.New("not found")
}

func (f *trainingsField) GetUserTrainingByUserTrainingId(userTrainingId uint) (*UserTraining, error) {
	for _, training := range *f {
		if training.UserTrainingId == userTrainingId {
			return training, nil
		}
	}
	return nil, errors.New("not found")
}

package model_user

import (
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gorm.io/datatypes"
)

type ManagedFieldPartyMember struct {
	ManagedPartyMemberId uint `gorm:"primaryKey"`
	// Foreign key
	UserId value_user.UserId
	// Foreign key TODO: add the key at migration
	ManagedCharacterId value_user.ManagedCharacterId
	// Foreign key TODO: add the key at migration
	ManagedFacilityId uint
	// Same as card id
	CharacterId uint64
	// 1: main / 2: sub
	RoomId uint8
	// Unknown (0~9 maybe slot no?)
	LiveIdx uint8
	// big base64 json
	ScheduleTable *string
	// Unknown (default 0)
	ScheduleId int8
	// Unknown (default 0)
	ScheduleTag int8
	// Unknown (default -1)
	TouchItemResultNo int64
	// Unknown (default 0)
	Flag uint8
	// Unknown (default nil)
	PartyDropPresents *datatypes.JSON
}

package model_user

import (
	"time"

	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

func NewUserGachaByUserGacha(oldUserGacha UserGacha) UserGacha {
	now := time.Now()
	return UserGacha{
		CreatedAt:          oldUserGacha.CreatedAt,
		UpdatedAt:          now,
		DeletedAt:          nil,
		Gem10CurrentStep:   oldUserGacha.Gem10CurrentStep,
		PlayerDrawPoint:    oldUserGacha.PlayerDrawPoint,
		Gem10Daily:         oldUserGacha.Gem10Daily,
		Gem10Total:         oldUserGacha.Gem10Total,
		Gem1Daily:          oldUserGacha.Gem1Daily,
		Gem1Total:          oldUserGacha.Gem1Total,
		UGem1Daily:         oldUserGacha.UGem1Daily,
		UGem1Total:         oldUserGacha.UGem1Total,
		ItemDaily:          oldUserGacha.ItemDaily,
		ItemTotal:          oldUserGacha.ItemTotal,
		Gem1FreeDrawCount:  oldUserGacha.Gem1FreeDrawCount,
		Gem10FreeDrawCount: oldUserGacha.Gem10FreeDrawCount,
		GachaId:            oldUserGacha.GachaId,
		UserId:             oldUserGacha.UserId,
	}
}

func NewUserGacha(userId value_user.UserId, gachaId uint) UserGacha {
	now := time.Now()
	return UserGacha{
		CreatedAt:          now,
		UpdatedAt:          now,
		DeletedAt:          nil,
		Gem10CurrentStep:   1,
		PlayerDrawPoint:    0,
		Gem10Daily:         0,
		Gem10Total:         0,
		Gem1Daily:          0,
		Gem1Total:          0,
		UGem1Daily:         0,
		UGem1Total:         0,
		ItemDaily:          0,
		ItemTotal:          0,
		Gem1FreeDrawCount:  0,
		Gem10FreeDrawCount: 0,
		GachaId:            gachaId,
		UserId:             userId,
	}
}

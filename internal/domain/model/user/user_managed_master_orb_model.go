package model_user

import value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"

type ManagedMasterOrb struct {
	ManagedMasterOrbId uint `gorm:"primarykey"`
	// Foreign key
	UserId value_user.UserId
	// Sometimes it can be -1 (why)
	Level int8
	// Default value is 0
	Exp         uint32
	MasterOrbId uint8
}

package model_user

import (
	"time"

	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type ManagedRoomObject struct {
	ManagedRoomObjectId int `gorm:"primarykey"`
	DeletedAt           *time.Time
	// Foreign key
	UserId       value_user.UserId
	RoomObjectId uint32
}

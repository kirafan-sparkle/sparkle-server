package model_user

import (
	"time"

	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type managedRoomObjectsField []ManagedRoomObject

func (s *managedRoomObjectsField) AddRoomObject(userId value_user.UserId, roomObjectId uint32, roomObjectLimit uint16) error {
	roomObjectCount := len(*s)
	if uint16(roomObjectCount) >= roomObjectLimit {
		return ErrUserRoomObjectLimitExceed
	}
	newRoomObject := ManagedRoomObject{
		UserId:       userId,
		RoomObjectId: roomObjectId,
	}
	*s = append(*s, newRoomObject)
	return nil
}

func (s *managedRoomObjectsField) HasRoomObject(managedRoomObjectId int, roomObjectId uint32) bool {
	for i := range *s {
		if (*s)[i].ManagedRoomObjectId == managedRoomObjectId &&
			(*s)[i].RoomObjectId == roomObjectId {
			return true
		}
	}
	return false
}

func (s *managedRoomObjectsField) GetRoomObject(managedRoomObjectId uint) (*ManagedRoomObject, error) {
	for i := range *s {
		if (*s)[i].ManagedRoomObjectId == int(managedRoomObjectId) {
			return &(*s)[i], nil
		}
	}
	return nil, ErrUserRoomNotFound
}

func (s *managedRoomObjectsField) RemoveRoomObject(managedRoomObjectId int, roomObjectId uint32) error {
	for i := range *s {
		if (*s)[i].ManagedRoomObjectId != managedRoomObjectId {
			continue
		}
		if (*s)[i].RoomObjectId != roomObjectId {
			continue
		}
		currentTime := time.Now()
		(*s)[i].DeletedAt = &currentTime
		return nil
	}
	return ErrUserRoomObjectNotFound
}

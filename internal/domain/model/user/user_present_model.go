package model_user

import (
	"time"

	model_present "gitlab.com/kirafan/sparkle/server/internal/domain/model/present"
	value_present "gitlab.com/kirafan/sparkle/server/internal/domain/value/present"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type UserPresent struct {
	ManagedPresentId uint `gorm:"primary_key"`
	// Foreign Key
	UserId value_user.UserId `gorm:"index:idx_user_presents_user_id_and_received_at,priority:1"`
	User   User              `gorm:"PRELOAD:false"`
	// foreignKey
	PresentId  uint `gorm:"ForeignKey:PresentId"`
	Present    model_present.Present
	CreatedAt  time.Time
	ReceivedAt *time.Time `gorm:"index:idx_user_presents_user_id_and_received_at,priority:2"`
	DeadlineAt time.Time
	// Since tutorial items needs template but making all item combinations as template takes too much space.
	Type value_present.PresentType
	// ItemId or CharacterId
	ObjectId int64
	Amount   int64
}

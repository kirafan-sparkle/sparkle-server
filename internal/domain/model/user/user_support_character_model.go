package model_user

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gorm.io/gorm"
)

// Means ManagedSupportParty
type SupportCharacter struct {
	ManagedSupportId    value_user.ManagedSupportId          `gorm:"primarykey"`
	ManagedCharacterIds []SupportCharacterManagedCharacterId `gorm:"foreignKey:ManagedSupportId"`
	ManagedWeaponIds    []SupportCharacterManagedWeaponId    `gorm:"foreignKey:ManagedSupportId"`
	// Support party name
	Name string
	// 1: Active, 0: Inactive
	Active value.BoolLikeUInt8
	// Foreign Key
	UserId value_user.UserId
}

type SupportCharacterManagedCharacterId struct {
	gorm.Model
	// Foreign Key (TODO: Add the key to migration)
	ManagedCharacterId value_user.ManagedCharacterId
	// Foreign Key
	ManagedSupportId value_user.ManagedSupportId
}

type SupportCharacterManagedWeaponId struct {
	gorm.Model
	// Foreign Key (TODO: Add the key to migration)
	ManagedWeaponId value_user.ManagedWeaponId
	// Foreign Key
	ManagedSupportId value_user.ManagedSupportId
}

package model_user

import (
	"fmt"
	"time"

	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type achievementsField []UserAchievement

func (s *achievementsField) GetAchievement(achievementId uint) *UserAchievement {
	for i := range *s {
		if (*s)[i].AchievementId == achievementId {
			return &(*s)[i]
		}
	}
	return nil
}

func (s *achievementsField) HasAchievement(achievementId uint) bool {
	for i := range *s {
		if (*s)[i].AchievementId == achievementId {
			return true
		}
	}
	return false
}

func (s *achievementsField) GetAchievementsByTitleType(titleType value_character.TitleType) []*UserAchievement {
	achievements := make([]*UserAchievement, 0, len(*s))
	for i := range *s {
		if (*s)[i].Achievement.TitleType == titleType {
			achievements = append(achievements, &(*s)[i])
		}
	}
	return achievements
}

func (s *achievementsField) SetAchievementsShownByTitleTypes(titleTypes []value_character.TitleType) {
	for _, titleType := range titleTypes {
		achievements := s.GetAchievementsByTitleType(titleType)
		for _, achievement := range achievements {
			achievement.IsNew = false
		}
	}
}

func (s *achievementsField) AddAchievement(userId value_user.UserId, achievementId uint) error {
	if s.HasAchievement(achievementId) {
		return fmt.Errorf("achievementId %d already exists", achievementId)
	}
	now := time.Now()
	newAchievement := UserAchievement{
		CreatedAt:     now,
		UpdatedAt:     now,
		UserId:        userId,
		Enable:        true,
		IsNew:         true,
		AchievementId: achievementId,
	}
	(*s) = append((*s), newAchievement)
	return nil
}

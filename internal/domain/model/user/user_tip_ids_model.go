package model_user

import (
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gorm.io/gorm"
)

// Showed tip ids (not tipId since it conflicts to type)
type ShownTipId struct {
	gorm.Model
	// Foreign key
	UserId value_user.UserId
	TipId  uint32
}

package model_user

import (
	"time"

	model_training "gitlab.com/kirafan/sparkle/server/internal/domain/model/training"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_training "gitlab.com/kirafan/sparkle/server/internal/domain/value/training"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type UserTrainingSlot struct {
	UserTrainingSlotId uint `gorm:"primary_key"`
	UserId             value_user.UserId
	TrainingSlotId     value_training.SlotId
	TrainingSlot       model_training.TrainingSlot `gorm:"references:TrainingSlotId"`
	OpenSlot           value.BoolLikeUInt8
	OrderId            *uint
	Order              *UserTrainingOrder                 `gorm:"PRELOAD:false"`
	ManagedCharacters  []UserTrainingSlotManagedCharacter `gorm:"PRELOAD:false"`
}

func NewUserTrainingSlot(userId value_user.UserId, slotId value_training.SlotId, isOpen value.BoolLikeUInt8) *UserTrainingSlot {
	newTrainingSlotManagedCharacters := make([]UserTrainingSlotManagedCharacter, 5)
	for i := range newTrainingSlotManagedCharacters {
		newTrainingSlotManagedCharacters[i] = NewUserTrainingSlotManagedCharacter()
	}
	return &UserTrainingSlot{
		UserId:            userId,
		TrainingSlotId:    slotId,
		OpenSlot:          isOpen,
		OrderId:           nil,
		ManagedCharacters: newTrainingSlotManagedCharacters,
	}
}

func (s *UserTrainingSlot) SetNewOrder(userTraining *UserTraining, managedCharacterIds []value_user.ManagedCharacterId) {
	now := time.Now()
	requiredTimeMinutes := userTraining.Training.ClearTime
	newOrder := UserTrainingOrder{
		StartAt:        now,
		EndAt:          now.Add(time.Duration(requiredTimeMinutes) * time.Minute),
		UserTrainingId: userTraining.UserTrainingId,
	}
	s.Order = &newOrder
	for i, v := range managedCharacterIds {
		s.ManagedCharacters[i].ManagedCharacterId = v
	}
	s.OpenSlot = value.BoolLikeUIntFalse
}

func (s *UserTrainingSlot) SetOrderCleared() {
	s.OrderId = nil
	s.Order = nil
	s.OpenSlot = value.BoolLikeUIntTrue
}

func (s UserTrainingSlot) IsOpened() bool {
	return s.OpenSlot.IsTrue()
}

func (s UserTrainingSlot) IsEnded() bool {
	if s.Order == nil {
		return false
	}
	return s.Order.IsEnded()
}

func (s UserTrainingSlot) GetOutingManagedCharacterIds() []value_user.ManagedCharacterId {
	ids := make([]value_user.ManagedCharacterId, len(s.ManagedCharacters))
	for i, mc := range s.ManagedCharacters {
		ids[i] = mc.ManagedCharacterId
	}
	return ids
}

type UserTrainingSlotManagedCharacter struct {
	Id                 uint `gorm:"primary_key"`
	CreatedAt          time.Time
	UpdatedAt          time.Time
	DeletedAt          *time.Time
	UserTrainingSlotId uint // foreignKey
	ManagedCharacterId value_user.ManagedCharacterId
}

func NewUserTrainingSlotManagedCharacter() UserTrainingSlotManagedCharacter {
	currentTime := time.Now()
	return UserTrainingSlotManagedCharacter{
		CreatedAt:          currentTime,
		UpdatedAt:          currentTime,
		DeletedAt:          nil,
		ManagedCharacterId: -1,
	}
}

package model_user

import (
	"time"

	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
	"gitlab.com/kirafan/sparkle/server/pkg/parser"
)

type managedWeaponsField []ManagedWeapon

func (s *managedWeaponsField) GetManagedWeaponByWeaponId(weaponId value_weapon.WeaponId) (*ManagedWeapon, error) {
	for i := range *s {
		if (*s)[i].WeaponId == weaponId {
			return &(*s)[i], nil
		}
	}
	return nil, ErrUserManagedWeaponNotFound
}

func (s *managedWeaponsField) GetManagedWeapon(managedWeaponId value_user.ManagedWeaponId) (*ManagedWeapon, error) {
	for i := range *s {
		if (*s)[i].ManagedWeaponId == managedWeaponId {
			return &(*s)[i], nil
		}
	}
	return nil, ErrUserManagedWeaponNotFound
}

func (s *managedWeaponsField) HasWeapon(weaponId value_weapon.WeaponId) bool {
	for _, weapon := range *s {
		if weapon.WeaponId == weaponId {
			return true
		}
	}
	return false
}

func (s *managedWeaponsField) AddWeapon(
	weaponId value_weapon.WeaponId,
	isSpecialWeapon bool,
	weaponLimit uint16,
	userId value_user.UserId,
) error {
	weaponCount := len(*s)
	if uint16(weaponCount)+1 > weaponLimit && !isSpecialWeapon {
		return ErrUserWeaponLimitExceed
	}
	// NOTE: Special weapon can not be duplicated
	if s.HasWeapon(weaponId) && isSpecialWeapon {
		return ErrUserWeaponSpecialWeaponAlreadyExists
	}
	newWeapon := newManagedWeapon(userId, weaponId)
	*s = append(*s, newWeapon)
	return nil
}

func (s *managedWeaponsField) RemoveWeapon(managedWeaponId value_user.ManagedWeaponId) bool {
	for index := range *s {
		if (*s)[index].ManagedWeaponId != managedWeaponId {
			continue
		}
		currentTime := time.Now()
		(*s)[index].DeletedAt = &currentTime
		return true
	}
	return false
}

func (s *managedWeaponsField) AddWeaponSkillExps(weaponExps map[uint64]parser.SkillResult) {
	for i := range *s {
		for j := range weaponExps {
			managedWeaponId := value_user.NewManagedWeaponId(j)
			if (*s)[i].ManagedWeaponId == managedWeaponId {
				(*s)[i].AddWeaponSkillExp(uint32(weaponExps[j].Skill1))
			}
		}
	}
}

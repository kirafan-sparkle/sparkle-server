package model_user

import (
	"time"

	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

func newManagedCharacter(userId value_user.UserId, characterId value_character.CharacterId) ManagedCharacter {
	currentTime := time.Now()
	managedCharacter := ManagedCharacter{
		CreatedAt:        currentTime,
		UpdatedAt:        currentTime,
		PlayerId:         userId,
		CharacterId:      characterId,
		ViewCharacterId:  characterId,
		Level:            1,
		Exp:              0,
		LevelBreak:       0,
		DuplicatedCount:  0,
		ArousalLevel:     0,
		SkillLevel1:      1,
		SkillLevel2:      1,
		SkillLevel3:      1,
		SkillExp1:        0,
		SkillExp2:        0,
		SkillExp3:        0,
		SkillLevelLimit1: 5,
		SkillLevelLimit2: 5,
		SkillLevelLimit3: 5,
		Shown:            0,
	}
	characterRarity := characterId.GetRarity()
	switch characterRarity {
	case value_character.CharacterIdRarityStar3:
		managedCharacter.LevelLimit = 30
	case value_character.CharacterIdRarityStar4:
		managedCharacter.LevelLimit = 40
	case value_character.CharacterIdRarityStar5:
		managedCharacter.LevelLimit = 50
	}
	return managedCharacter
}

package model_user

import (
	"time"

	model_trade "gitlab.com/kirafan/sparkle/server/internal/domain/model/trade"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

// Moved from gacha model, these values depends per a user
type UserTradeRecipe struct {
	Id        uint `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`

	TotalTradeCount   uint16
	MonthlyTradeCount uint16

	// foreignKey
	RecipeId uint
	Recipe   model_trade.TradeRecipe `gorm:"PRELOAD:false"`
	// foreignKey
	UserId value_user.UserId
	User   User `gorm:"PRELOAD:false"`
}

func (t *UserTradeRecipe) IsAvailable(now time.Time) bool {
	return t.Recipe.IsAvailable(now)
}

func (t *UserTradeRecipe) ShouldResetMonthlyCount(currentTime time.Time) bool {
	if !t.Recipe.IsMonthlyReset() {
		return false
	}
	if currentTime.Year() > t.UpdatedAt.Year() {
		return true
	}
	if currentTime.Year() == t.UpdatedAt.Year() && currentTime.Month() > t.UpdatedAt.Month() {
		return true
	}
	return false
}

// Ignores refresh if it's not monthly reset trade
func (u *UserTradeRecipe) RefreshMonthlyCount(currentTime time.Time) {
	if u.ShouldResetMonthlyCount(currentTime) {
		u.MonthlyTradeCount = 0
	}
}

package model_user

import (
	"errors"

	value_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/value/town_facility"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

func newManagedTownFacility(
	userId value_user.UserId,
	facilityId uint32,
	buildPointIndex int32,
	level uint8,
	actionTime uint64,
	buildTime uint64,
	openState uint8,
) (*ManagedTownFacility, error) {
	newOpenState, err := value_town_facility.NewTownFacilityOpenState(openState)
	if err != nil {
		return nil, err
	}
	newBuildPointIndex, err := value_town_facility.NewBuildPointIndex(buildPointIndex)
	if err != nil {
		return nil, err
	}
	if level < 1 || level > 10 {
		return nil, errors.New("town facility level must be between 1 and 10")
	}

	managedTownFacility := ManagedTownFacility{
		UserId:          userId,
		FacilityId:      facilityId,
		BuildPointIndex: newBuildPointIndex,
		Level:           level,
		OpenState:       newOpenState,
		ActionTime:      actionTime,
		BuildTime:       buildTime,
	}
	return &managedTownFacility, nil
}

package model_user

import (
	"time"
)

type UserTrainingOrder struct {
	OrderId        uint `gorm:"primary_key"`
	StartAt        time.Time
	EndAt          time.Time
	UserTrainingId uint
	UserTraining   UserTraining `gorm:"PRELOAD:false"`
}

func (o UserTrainingOrder) IsEnded() bool {
	return time.Now().After(o.EndAt)
}

package model_user

import (
	"errors"

	value_ability_board "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_board"
	value_ability_sphere "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_sphere"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gorm.io/gorm"
)

const (
	equipItemSlotsDefault = 10
	equipItemSlotsMax     = 16
)

type ManagedAbilityBoard struct {
	ManagedAbilityBoardId uint `gorm:"primarykey"`
	// Foreign Key
	UserId             value_user.UserId
	AbilityBoardId     value_ability_board.AbilityBoardId
	ManagedCharacterId value_user.ManagedCharacterId
	// TODO: make value object for this field
	EquipItemIds []ManagedAbilityBoardEquipItemId
}

func (b *ManagedAbilityBoard) GetEquipItemSlotCount() uint8 {
	return uint8(len(b.EquipItemIds))
}

// init equipItemIds slots if needed
func (b *ManagedAbilityBoard) Initialize() {
	if b.EquipItemIds != nil {
		return
	}
	equipItems := make([]ManagedAbilityBoardEquipItemId, equipItemSlotsDefault)
	for i := 0; i < equipItemSlotsDefault; i++ {
		index := value_ability_board.AbilityBoardSlotIndex(uint8(i))
		equipItems[i] = NewManagedAbilityBoardEquipItem(b.ManagedAbilityBoardId, index)
	}
	b.EquipItemIds = equipItems
}

func (b *ManagedAbilityBoard) AddEquipItemSlot() error {
	currentSlotCount := b.GetEquipItemSlotCount()
	if currentSlotCount >= equipItemSlotsMax {
		return errors.New("the equip item slots is full")
	}
	newSlotIndex, err := value_ability_board.NewAbilityBoardSlotIndex(currentSlotCount + 1)
	if err != nil {
		return err
	}
	newEquipItemIdSlot := NewManagedAbilityBoardEquipItem(b.ManagedAbilityBoardId, newSlotIndex)
	b.EquipItemIds = append(b.EquipItemIds, newEquipItemIdSlot)
	return nil
}

func (b *ManagedAbilityBoard) SetEquipItem(index value_ability_board.AbilityBoardSlotIndex, sphereItemId value_ability_sphere.AbilitySphereItemId) error {
	if uint8(index) >= b.GetEquipItemSlotCount() {
		return errors.New("the slot index is out of range")
	}
	if sphereItemId == value_ability_sphere.AbilitySphereItemIdInvalid {
		return errors.New("the item id is invalid")
	}
	b.EquipItemIds[index].ItemId = sphereItemId
	return nil
}

func (b *ManagedAbilityBoard) GetEquipItem(index value_ability_board.AbilityBoardSlotIndex) (value_ability_sphere.AbilitySphereItemId, error) {
	if uint8(index) >= b.GetEquipItemSlotCount() {
		return value_ability_sphere.AbilitySphereItemIdInvalid, errors.New("the slot index is out of range")
	}
	if b.EquipItemIds[index].ItemId == value_ability_sphere.AbilitySphereItemIdUnspecified {
		return value_ability_sphere.AbilitySphereItemIdInvalid, errors.New("the specified slot is empty")
	}
	return b.EquipItemIds[index].ItemId, nil
}

func (b *ManagedAbilityBoard) GetEquipItemIfExist(index value_ability_board.AbilityBoardSlotIndex) (value_ability_sphere.AbilitySphereItemId, bool) {
	if uint8(index) >= b.GetEquipItemSlotCount() {
		return value_ability_sphere.AbilitySphereItemIdInvalid, false
	}
	if b.EquipItemIds[index].ItemId == value_ability_sphere.AbilitySphereItemIdUnspecified {
		return value_ability_sphere.AbilitySphereItemIdInvalid, false
	}
	return b.EquipItemIds[index].ItemId, true
}

func NewManagedAbilityBoard(userId value_user.UserId, abilityBoardId value_ability_board.AbilityBoardId, managedCharacterId value_user.ManagedCharacterId) ManagedAbilityBoard {
	return ManagedAbilityBoard{
		UserId:             userId,
		AbilityBoardId:     abilityBoardId,
		ManagedCharacterId: managedCharacterId,
	}
}

type ManagedAbilityBoardEquipItemId struct {
	gorm.Model
	Index value_ability_board.AbilityBoardSlotIndex
	// Foreign Key (TODO: Add the key to migration)
	ManagedAbilityBoardId uint
	ItemId                value_ability_sphere.AbilitySphereItemId
}

func NewManagedAbilityBoardEquipItem(managedAbilityboardId uint, index value_ability_board.AbilityBoardSlotIndex) ManagedAbilityBoardEquipItemId {
	return ManagedAbilityBoardEquipItemId{
		ManagedAbilityBoardId: managedAbilityboardId,
		ItemId:                value_ability_sphere.AbilitySphereItemIdUnspecified,
		Index:                 index,
	}
}

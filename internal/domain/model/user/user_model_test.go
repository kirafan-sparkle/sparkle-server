package model_user

import (
	"errors"
	"reflect"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"github.com/google/uuid"
	model_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/model/login_bonus"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/value/login_bonus"
	value_mission "gitlab.com/kirafan/sparkle/server/internal/domain/value/mission"
	value_present "gitlab.com/kirafan/sparkle/server/internal/domain/value/present"
)

func TestUser_IncreaseLoginDays(t *testing.T) {
	tests := []struct {
		name string
		u    *User
		want uint16
	}{
		{
			name: "IncreaseLoginDays Success",
			u:    &User{Name: "test", LoginDays: 1},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.u.IncreaseLoginDays()
			if got := tt.u; got.LoginDays != tt.want {
				t.Errorf("IncreaseLoginDays() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewUser(t *testing.T) {
	type args struct {
		uuid string
		name string
	}
	uuid := uuid.New().String()
	tests := []struct {
		name string
		args args
		want User
	}{
		{
			name: "Create new user success",
			args: args{uuid: uuid, name: "test"},
			want: User{Name: "test"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewUser(tt.args.uuid, tt.args.name)
			t.Logf("NewUser() = %+v", got)
		})
	}
}

func TestUser_AddKirara(t *testing.T) {
	type args struct {
		amount uint32
	}
	tests := []struct {
		name    string
		u       *User
		args    args
		want    *User
		wantErr error
	}{
		{
			name:    "AddKirara success when the result is lower than limit",
			u:       &User{Name: "test", Kirara: 1, KiraraLimit: 1000},
			args:    args{amount: 1},
			want:    &User{Name: "test", Kirara: 2, KiraraLimit: 1000},
			wantErr: nil,
		},
		{
			name:    "AddKirara failed when the result is higher than limit",
			u:       &User{Name: "test", Kirara: 1000, KiraraLimit: 1000},
			args:    args{amount: 1},
			want:    &User{Name: "test", Kirara: 1000, KiraraLimit: 1000},
			wantErr: ErrUserKiraraLimitExceed,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := tt.u.AddKirara(tt.args.amount)
			if !errors.Is(err, tt.wantErr) {
				t.Errorf("User.AddKirara() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(tt.u, tt.want) {
				t.Errorf("User.Kirara = %v, want %v", tt.u.Kirara, tt.want.Kirara)
			}
		})
	}
}

// ReceivePresent tests

var createUserInstance = func(presents []UserPresent) *User {
	return &User{
		CharacterLimit:       0,
		CharacterWeaponCount: 0,
		FacilityLimit:        0,
		FacilityLimitCount:   0,
		Gold:                 0,
		Kirara:               0,
		KiraraLimit:          100000,
		UnlimitedGem:         0,
		LimitedGem:           0,
		RoomObjectLimit:      0,
		RoomObjectLimitCount: 0,
		Stamina:              0,
		StaminaMax:           0,
		WeaponLimit:          20,
		WeaponLimitCount:     0,
		ItemSummary:          []ItemSummary{},
		ManagedCharacters:    []ManagedCharacter{},
		ManagedFacilities:    []ManagedTownFacility{},
		ManagedRoomObjects:   []ManagedRoomObject{},
		ManagedWeapons:       []ManagedWeapon{},
		ManagedMasterOrbs:    []ManagedMasterOrb{},
		PresentCount:         uint8(len(presents)),
		Presents:             presents,
	}
}

var createUserPresent = func(managedPresentId uint, presentType value_present.PresentType, objectId int64, amount int64) UserPresent {
	return UserPresent{
		ManagedPresentId: managedPresentId,
		CreatedAt:        time.Now(),
		DeadlineAt:       time.Now().AddDate(0, 0, int(value_mission.EXPIRE_TUTORIAL_PRESENT)),
		ReceivedAt:       nil,
		Type:             presentType,
		ObjectId:         objectId,
		Amount:           amount,
	}
}

func TestUser_ReceivePresentTypeGold(t *testing.T) {
	user := createUserInstance([]UserPresent{
		createUserPresent(1, value_present.PresentTypeGold, 0, 100000),
		createUserPresent(2, value_present.PresentTypeGold, 0, 100000),
	})
	// Test receiving a first gold present successfully
	_, err := user.ReceivePresent(1)
	if !errors.Is(err, nil) {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeGold) error = %v, wantErr %v", err, nil)
		return
	}
	if user.Gold != 100000 {
		t.Errorf("User.Gold = %v, want %v", user.Gold, 100000)
	}
	// Test receiving a second gold present successfully
	_, err = user.ReceivePresent(2)
	if !errors.Is(err, nil) {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeGold) error = %v, wantErr %v", err, nil)
		return
	}
	if user.Gold != 200000 {
		t.Errorf("User.Gold = %v, want %v", user.Gold, 200000)
	}
	// Test receiving the same gold present again, which should fail
	_, err = user.ReceivePresent(1)
	if !errors.Is(err, ErrUserPresentNotFound) {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeGold) error = %v, wantErr %v", err, ErrUserPresentNotFound)
		return
	}
}

func TestUser_ReceivePresentTypeKiraraPoint(t *testing.T) {
	user := createUserInstance([]UserPresent{
		createUserPresent(1, value_present.PresentTypeKiraraPoint, 0, 100000),
		createUserPresent(2, value_present.PresentTypeKiraraPoint, 0, 100000),
	})
	// Test receiving a Kirara present successfully
	_, err := user.ReceivePresent(1)
	if !errors.Is(err, nil) {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeKiraraPoint) error = %v, wantErr %v", err, nil)
		return
	}
	if user.Kirara != 100000 {
		t.Errorf("User.Kirara = %v, want %v", user.Kirara, 100000)
	}
	// Test receiving the same Kirara present again, which should fail
	_, err = user.ReceivePresent(1)
	if !errors.Is(err, ErrUserPresentNotFound) {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeKiraraPoint) error = %v, wantErr %v", err, ErrUserPresentNotFound)
		return
	}
	// Test receiving a Kirara present that exceeds the Kirara limit, which should fail
	_, err = user.ReceivePresent(2)
	if !errors.Is(err, ErrUserKiraraLimitExceed) {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeKiraraPoint) error = %v, wantErr %v", err, ErrUserKiraraLimitExceed)
		return
	}
	if user.Kirara != 100000 {
		t.Errorf("User.KiraraPoint = %v, want %v", user.Kirara, 100000)
	}
	// Test receiving the same Kirara present again after the Kirara limit has been exceeded.
	// This should fail with ErrUserKiraraLimitExceed because the present is not received yet.
	_, err = user.ReceivePresent(2)
	if !errors.Is(err, ErrUserKiraraLimitExceed) {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeKiraraPoint) error = %v, wantErr %v", err, ErrUserKiraraLimitExceed)
		return
	}
	if user.Kirara != 100000 {
		t.Errorf("User.KiraraPoint = %v, want %v", user.Kirara, 100000)
	}
}

func TestUser_ReceivePresentTypeLimitedGem(t *testing.T) {
	user := createUserInstance([]UserPresent{
		createUserPresent(1, value_present.PresentTypeLimitedGem, 0, 100),
		createUserPresent(2, value_present.PresentTypeLimitedGem, 0, 1000),
	})
	// Test receiving the first LimitedGem present successfully
	_, err := user.ReceivePresent(1)
	if !errors.Is(err, nil) {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeLimitedGem) error = %v, wantErr %v", err, nil)
		return
	}
	if user.LimitedGem != 100 {
		t.Errorf("User.LimitedGem = %v, want %v", user.LimitedGem, 100)
	}
	// Test receiving the second LimitedGem present successfully
	_, err = user.ReceivePresent(2)
	if !errors.Is(err, nil) {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeLimitedGem) error = %v, wantErr %v", err, nil)
		return
	}
	if user.LimitedGem != 1100 {
		t.Errorf("User.LimitedGem = %v, want %v", user.LimitedGem, 1100)
	}
	// Test receiving the same LimitedGem present again, which should fail
	_, err = user.ReceivePresent(1)
	if !errors.Is(err, ErrUserPresentNotFound) {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeLimitedGem) error = %v, wantErr %v", err, ErrUserPresentNotFound)
		return
	}
}

func TestUser_ReceivePresentTypeUnlimitedGem(t *testing.T) {
	user := createUserInstance([]UserPresent{
		createUserPresent(1, value_present.PresentTypeUnlimitedGem, 0, 100),
		createUserPresent(2, value_present.PresentTypeUnlimitedGem, 0, 1000),
	})
	// Test receiving the first UnlimitedGem present successfully
	_, err := user.ReceivePresent(1)
	if !errors.Is(err, nil) {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeUnlimitedGem) error = %v, wantErr %v", err, nil)
		return
	}
	if user.UnlimitedGem != 100 {
		t.Errorf("User.UnlimitedGem = %v, want %v", user.UnlimitedGem, 100)
	}
	// Test receiving the second UnlimitedGem present successfully
	_, err = user.ReceivePresent(2)
	if !errors.Is(err, nil) {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeUnlimitedGem) error = %v, wantErr %v", err, nil)
		return
	}
	if user.UnlimitedGem != 1100 {
		t.Errorf("User.UnlimitedGem = %v, want %v", user.UnlimitedGem, 1100)
	}
	// Test receiving the same UnlimitedGem present again, which should fail
	_, err = user.ReceivePresent(1)
	if !errors.Is(err, ErrUserPresentNotFound) {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeUnlimitedGem) error = %v, wantErr %v", err, ErrUserPresentNotFound)
		return
	}
}

func TestUser_ReceivePresentTypeItem(t *testing.T) {
	// Create a new user instance with four Item presents
	user := createUserInstance([]UserPresent{
		createUserPresent(1, value_present.PresentTypeItem, 1, 10),
		createUserPresent(2, value_present.PresentTypeItem, 1, 100),
		createUserPresent(3, value_present.PresentTypeItem, 2, 10),
		createUserPresent(4, value_present.PresentTypeItem, 2, 100),
	})

	// Test receiving the first Item present successfully
	_, err := user.ReceivePresent(1)
	if err != nil {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeItem) returned an unexpected error: %v", err)
	}
	if len(user.ItemSummary) != 1 {
		t.Errorf(
			"User.ItemSummary has an incorrect length after receiving the first Item present; got %v, want %v",
			len(user.ItemSummary),
			1,
		)
	}
	if user.ItemSummary[0].Amount != 10 {
		t.Errorf(
			"User.ItemSummary[0].Amount has an incorrect value after receiving the first Item present; got %v, want %v",
			user.ItemSummary[0].Amount,
			10,
		)
	}

	// Test receiving the second Item present successfully
	_, err = user.ReceivePresent(2)
	if err != nil {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeItem) returned an unexpected error: %v", err)
	}
	if len(user.ItemSummary) != 1 {
		t.Errorf(
			"User.ItemSummary has an incorrect length after receiving the second Item present; got %v, want %v",
			len(user.ItemSummary),
			1,
		)
	}
	if user.ItemSummary[0].Amount != 110 {
		t.Errorf(
			"User.ItemSummary[0].Amount has an incorrect value after receiving the second Item present; got %v, want %v",
			user.ItemSummary[0].Amount,
			110,
		)
	}

	// Test receiving the third Item present successfully
	_, err = user.ReceivePresent(3)
	if err != nil {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeItem) returned an unexpected error: %v", err)
	}
	if len(user.ItemSummary) != 2 {
		t.Errorf(
			"User.ItemSummary has an incorrect length after receiving the third Item present; got %v, want %v",
			len(user.ItemSummary),
			2,
		)
	}
	if user.ItemSummary[1].Amount != 10 {
		t.Errorf(
			"User.ItemSummary[1].Amount has an incorrect value after receiving the third Item present; got %v, want %v",
			user.ItemSummary[1].Amount,
			10,
		)
	}

	// Test receiving the fourth Item present successfully
	_, err = user.ReceivePresent(4)
	if err != nil {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeItem) returned an unexpected error: %v", err)
	}
	if len(user.ItemSummary) != 2 {
		t.Errorf(
			"User.ItemSummary has an incorrect length after receiving the fourth Item present; got %v, want %v",
			len(user.ItemSummary),
			2,
		)
	}
	if user.ItemSummary[1].Amount != 110 {
		t.Errorf(
			"User.ItemSummary[1].Amount has an incorrect value after receiving the fourth Item present; got %v, want %v",
			user.ItemSummary[1].Amount,
			110,
		)
	}
	// t.Logf("User.ItemSummary = %+v", user.ItemSummary)
}

func TestUser_ReceivePresentTypeWeapon(t *testing.T) {
	user := createUserInstance([]UserPresent{
		createUserPresent(1, value_present.PresentTypeWeapon, 1, 1),
		createUserPresent(2, value_present.PresentTypeWeapon, 1, 1),
	})

	// Test receiving the first Weapon present successfully
	_, err := user.ReceivePresent(1)
	if err != nil {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeWeapon) returned an unexpected error: %v", err)
	}
	if len(user.ManagedWeapons) != 1 {
		t.Errorf(
			"User.ManagedWeapon has an incorrect length after receiving the first Weapon present; got %v, want %v",
			len(user.ManagedWeapons),
			1,
		)
	}
	if user.ManagedWeapons[0].WeaponId != 1 {
		t.Errorf(
			"User.ManagedWeapons[0].WeaponId has an incorrect value after receiving the first Weapon present; got %v, want %v",
			user.ManagedWeapons[0].WeaponId,
			1,
		)
	}
	// Test receiving the second Weapon present successfully
	_, err = user.ReceivePresent(2)
	if err != nil {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeWeapon) returned an unexpected error: %v", err)
	}
	if len(user.ManagedWeapons) != 2 {
		t.Errorf(
			"User.ManagedWeapon has an incorrect length after receiving the second Weapon present; got %v, want %v",
			len(user.ManagedWeapons),
			1,
		)
	}
	if user.ManagedWeapons[1].WeaponId != 1 {
		t.Errorf(
			"User.ManagedWeapons[1].WeaponId has an incorrect value after receiving the second Weapon present; got %v, want %v",
			user.ManagedWeapons[1].WeaponId,
			1,
		)
	}
	// t.Logf("User.ManagedWeapons = %+v", user.ManagedWeapons)
}

func TestUser_ReceivePresentTypeCharacter(t *testing.T) {
	KfcnStar5Id, _ := value_character.NewCharacterId(30012000)
	KfcnStar4Id, _ := value_character.NewCharacterId(30011000)
	user := createUserInstance([]UserPresent{
		createUserPresent(1, value_present.PresentTypeCharacter, int64(KfcnStar5Id), 1),
		createUserPresent(2, value_present.PresentTypeCharacter, int64(KfcnStar5Id), 1),
		createUserPresent(3, value_present.PresentTypeCharacter, int64(KfcnStar4Id), 1),
		createUserPresent(4, value_present.PresentTypeCharacter, 12345, 1),
	})

	// Test receiving the first Character present successfully
	_, err := user.ReceivePresent(1)
	if err != nil {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeCharacter) returned an unexpected error: %v", err)
	}
	if len(user.ManagedCharacters) != 1 {
		t.Errorf(
			"User.ManagedCharacter has an incorrect length after receiving the first Character present; got %v, want %v",
			len(user.ManagedCharacters),
			1,
		)
	}
	if user.ManagedCharacters[0].CharacterId != KfcnStar5Id {
		t.Errorf(
			"User.ManagedCharacters[0].CharacterId has an incorrect value after receiving the first Character present; got %v, want %v",
			user.ManagedCharacters[0].CharacterId,
			KfcnStar5Id,
		)
	}

	// Test receiving the second Character present successfully
	_, err = user.ReceivePresent(2)
	if err != nil {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeCharacter) returned an unexpected error: %v", err)
	}
	if len(user.ManagedCharacters) != 1 {
		t.Errorf(
			"User.ManagedCharacter has an incorrect length after receiving the second Character present; got %v, want %v",
			len(user.ManagedCharacters),
			1,
		)
	}
	if user.ManagedCharacters[0].CharacterId != KfcnStar5Id {
		t.Errorf(
			"User.ManagedCharacters[0].CharacterId has an incorrect value after receiving the second Character present; got %v, want %v",
			user.ManagedCharacters[0].CharacterId,
			1,
		)
	}
	if user.ManagedCharacters[0].DuplicatedCount != 1 {
		t.Errorf(
			"User.ManagedCharacters[0].DuplicatedCount has an incorrect value after receiving the second Character present; got %v, want %v",
			user.ManagedCharacters[0].DuplicatedCount,
			1,
		)
	}

	// Test receiving the third Character present successfully
	_, err = user.ReceivePresent(3)
	if err != nil {
		t.Errorf("User.ReceivePresent(value_present.PresentTypeCharacter) returned an unexpected error: %v", err)
	}
	if len(user.ManagedCharacters) != 2 {
		t.Errorf(
			"User.ManagedCharacter has an incorrect length after receiving the third Character present; got %v, want %v",
			len(user.ManagedCharacters),
			2,
		)
	}
	if user.ManagedCharacters[1].CharacterId != KfcnStar4Id {
		t.Errorf(
			"User.ManagedCharacters[1].CharacterId has an incorrect value after receiving the third Character present; got %v, want %v",
			user.ManagedCharacters[1].CharacterId,
			KfcnStar4Id,
		)
	}
	// t.Logf("User.ManagedCharacters = %+v", user.ManagedCharacters)
}

func TestUser_ReceiveLoginBonuses(t *testing.T) {
	tests := []struct {
		name      string
		u         User
		wantGems  uint32
		wantItems []ItemSummary
		want1     *[]UserLoginBonus
	}{
		{
			name: "ReceiveLoginBonuses without UserLoginBonus success",
			u: User{
				UnlimitedGem: 0,
				LimitedGem:   0,
				ItemSummary:  []ItemSummary{},
				LoginBonuses: []UserLoginBonus{},
			},
			wantGems:  0,
			wantItems: []ItemSummary{},
			want1:     nil,
		},
		{
			name: "ReceiveLoginBonuses normalLoginBonus day1 items and gems success",
			u: User{
				UnlimitedGem: 0,
				LimitedGem:   0,
				ItemSummary:  []ItemSummary{},
				LoginBonuses: []UserLoginBonus{
					{
						CreatedAt:          time.Now(),
						UpdatedAt:          time.Now().AddDate(0, 0, -1),
						LoginBonusDayIndex: value_login_bonus.LoginBonusDayIndexDay1,
						LoginBonus: model_login_bonus.LoginBonus{
							BonusDays: []model_login_bonus.LoginBonusDay{
								{
									BonusDayId: value_login_bonus.LoginBonusDayIndexDay1,
									BonusItems: []model_login_bonus.LoginBonusDayBonusItem{
										{
											Type:   value_login_bonus.LoginBonusDayPresentTypeItem,
											ObjId:  1,
											Amount: 10,
										},
										{
											Type:   value_login_bonus.LoginBonusDayPresentTypeItem,
											ObjId:  2,
											Amount: 5,
										},
										{
											Type:   value_login_bonus.LoginBonusDayPresentTypeItem,
											ObjId:  3,
											Amount: 0,
										},
										{
											Type:   value_login_bonus.LoginBonusDayPresentTypeLimitedGem,
											Amount: 1000,
										},
									},
								},
							},
							ImageId: value_login_bonus.LoginBonusImageNormal,
							Type:    value_login_bonus.LoginBonusTypeNormal,
						},
					},
				},
			},
			wantGems: 1000,
			wantItems: []ItemSummary{
				{
					Id:     1,
					Amount: 10,
				},
				{
					Id:     2,
					Amount: 5,
				},
			},
			want1: &[]UserLoginBonus{
				{
					LoginBonusDayIndex: value_login_bonus.LoginBonusDayIndexDay1,
					LoginBonus: model_login_bonus.LoginBonus{
						BonusDays: []model_login_bonus.LoginBonusDay{
							{
								BonusDayId: value_login_bonus.LoginBonusDayIndexDay1,
								BonusItems: []model_login_bonus.LoginBonusDayBonusItem{
									{
										Type:   value_login_bonus.LoginBonusDayPresentTypeItem,
										ObjId:  1,
										Amount: 10,
									},
									{
										Type:   value_login_bonus.LoginBonusDayPresentTypeItem,
										ObjId:  2,
										Amount: 5,
									},
									{
										Type:   value_login_bonus.LoginBonusDayPresentTypeItem,
										ObjId:  3,
										Amount: 0,
									},
									{
										Type:   value_login_bonus.LoginBonusDayPresentTypeLimitedGem,
										Amount: 1000,
									},
								},
							},
						},
						ImageId: value_login_bonus.LoginBonusImageNormal,
						Type:    value_login_bonus.LoginBonusTypeNormal,
					},
				},
			},
		},
		{
			name: "ReceiveLoginBonuses normalLoginBonus day2 item only success",
			u: User{
				UnlimitedGem: 0,
				LimitedGem:   0,
				ItemSummary:  []ItemSummary{},
				LoginBonuses: []UserLoginBonus{
					{
						CreatedAt:          time.Now(),
						UpdatedAt:          time.Now().AddDate(0, 0, -1),
						LoginBonusDayIndex: value_login_bonus.LoginBonusDayIndexDay2,
						LoginBonus: model_login_bonus.LoginBonus{
							BonusDays: []model_login_bonus.LoginBonusDay{
								{
									BonusDayId: value_login_bonus.LoginBonusDayIndexDay1,
									BonusItems: []model_login_bonus.LoginBonusDayBonusItem{
										{
											Type:   value_login_bonus.LoginBonusDayPresentTypeItem,
											ObjId:  1,
											Amount: 10,
										},
									},
								},
								{
									BonusDayId: value_login_bonus.LoginBonusDayIndexDay2,
									BonusItems: []model_login_bonus.LoginBonusDayBonusItem{
										{
											Type:   value_login_bonus.LoginBonusDayPresentTypeItem,
											ObjId:  2,
											Amount: 2,
										},
									},
								},
							},
							ImageId: value_login_bonus.LoginBonusImageNormal,
							Type:    value_login_bonus.LoginBonusTypeNormal,
						},
					},
				},
			},
			wantGems: 0,
			wantItems: []ItemSummary{{
				Id:     2,
				Amount: 2,
			}},
			want1: &[]UserLoginBonus{
				{
					LoginBonusDayIndex: value_login_bonus.LoginBonusDayIndexDay2,
					LoginBonus: model_login_bonus.LoginBonus{
						BonusDays: []model_login_bonus.LoginBonusDay{
							{
								BonusDayId: value_login_bonus.LoginBonusDayIndexDay1,
								BonusItems: []model_login_bonus.LoginBonusDayBonusItem{
									{
										Type:   value_login_bonus.LoginBonusDayPresentTypeItem,
										ObjId:  1,
										Amount: 10,
									},
								},
							},
							{
								BonusDayId: value_login_bonus.LoginBonusDayIndexDay2,
								BonusItems: []model_login_bonus.LoginBonusDayBonusItem{
									{
										Type:   value_login_bonus.LoginBonusDayPresentTypeItem,
										ObjId:  2,
										Amount: 2,
									},
								},
							},
						},
						ImageId: value_login_bonus.LoginBonusImageNormal,
						Type:    value_login_bonus.LoginBonusTypeNormal,
					},
				},
			},
		},
		{
			name: "ReceiveLoginBonuses normalLoginBonus day3 gem only success",
			u: User{
				UnlimitedGem: 0,
				LimitedGem:   0,
				ItemSummary:  []ItemSummary{},
				LoginBonuses: []UserLoginBonus{
					{
						CreatedAt:          time.Now(),
						UpdatedAt:          time.Now().AddDate(0, 0, -1),
						LoginBonusDayIndex: value_login_bonus.LoginBonusDayIndexDay3,
						LoginBonus: model_login_bonus.LoginBonus{
							BonusDays: []model_login_bonus.LoginBonusDay{
								{
									BonusDayId: value_login_bonus.LoginBonusDayIndexDay1,
									BonusItems: []model_login_bonus.LoginBonusDayBonusItem{
										{
											Type:   value_login_bonus.LoginBonusDayPresentTypeItem,
											ObjId:  1,
											Amount: 10,
										},
									},
								},
								{
									BonusDayId: value_login_bonus.LoginBonusDayIndexDay2,
									BonusItems: []model_login_bonus.LoginBonusDayBonusItem{
										{
											Type:   value_login_bonus.LoginBonusDayPresentTypeItem,
											ObjId:  2,
											Amount: 2,
										},
									},
								},
								{
									BonusDayId: value_login_bonus.LoginBonusDayIndexDay3,
									BonusItems: []model_login_bonus.LoginBonusDayBonusItem{
										{
											Type:   value_login_bonus.LoginBonusDayPresentTypeLimitedGem,
											Amount: 100,
										},
									},
								},
							},
							ImageId: value_login_bonus.LoginBonusImageNormal,
							Type:    value_login_bonus.LoginBonusTypeNormal,
						},
					},
				},
			},
			wantGems:  100,
			wantItems: []ItemSummary{},
			want1: &[]UserLoginBonus{
				{
					LoginBonusDayIndex: value_login_bonus.LoginBonusDayIndexDay3,
					LoginBonus: model_login_bonus.LoginBonus{
						BonusDays: []model_login_bonus.LoginBonusDay{
							{
								BonusDayId: value_login_bonus.LoginBonusDayIndexDay1,
								BonusItems: []model_login_bonus.LoginBonusDayBonusItem{
									{
										Type:   value_login_bonus.LoginBonusDayPresentTypeItem,
										ObjId:  1,
										Amount: 10,
									},
								},
							},
							{
								BonusDayId: value_login_bonus.LoginBonusDayIndexDay2,
								BonusItems: []model_login_bonus.LoginBonusDayBonusItem{
									{
										Type:   value_login_bonus.LoginBonusDayPresentTypeItem,
										ObjId:  2,
										Amount: 2,
									},
								},
							},
							{
								BonusDayId: value_login_bonus.LoginBonusDayIndexDay3,
								BonusItems: []model_login_bonus.LoginBonusDayBonusItem{
									{
										Type:   value_login_bonus.LoginBonusDayPresentTypeLimitedGem,
										Amount: 100,
									},
								},
							},
						},
						ImageId: value_login_bonus.LoginBonusImageNormal,
						Type:    value_login_bonus.LoginBonusTypeNormal,
					},
				},
			},
		},
		{
			name: "ReceiveLoginBonuses eventLoginBonus day1 gems success",
			u: User{
				UnlimitedGem: 0,
				LimitedGem:   0,
				ItemSummary:  []ItemSummary{},
				LoginBonuses: []UserLoginBonus{
					{
						CreatedAt:          time.Now(),
						UpdatedAt:          time.Now().AddDate(0, 0, -1),
						LoginBonusDayIndex: value_login_bonus.LoginBonusDayIndexDay1,
						LoginBonus: model_login_bonus.LoginBonus{
							BonusDays: []model_login_bonus.LoginBonusDay{
								{
									BonusDayId: value_login_bonus.LoginBonusDayIndexDay1,
									BonusItems: []model_login_bonus.LoginBonusDayBonusItem{
										{
											Type:   value_login_bonus.LoginBonusDayPresentTypeLimitedGem,
											Amount: 300,
										},
									},
								},
							},
							ImageId: value_login_bonus.LoginBonusImageKirafanRadio,
							Type:    value_login_bonus.LoginBonusTypeEvent,
						},
					},
				},
			},
			wantGems:  300,
			wantItems: []ItemSummary{},
			want1: &[]UserLoginBonus{
				{
					LoginBonusDayIndex: value_login_bonus.LoginBonusDayIndexDay1,
					LoginBonus: model_login_bonus.LoginBonus{
						BonusDays: []model_login_bonus.LoginBonusDay{
							{
								BonusDayId: value_login_bonus.LoginBonusDayIndexDay1,
								BonusItems: []model_login_bonus.LoginBonusDayBonusItem{
									{
										Type:   value_login_bonus.LoginBonusDayPresentTypeLimitedGem,
										Amount: 300,
									},
								},
							},
						},
						ImageId: value_login_bonus.LoginBonusImageKirafanRadio,
						Type:    value_login_bonus.LoginBonusTypeEvent,
					},
				},
			},
		},
	}

	opts := []cmp.Option{
		cmpopts.IgnoreFields(UserLoginBonus{}, "CreatedAt", "UpdatedAt"),
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got1 := tt.u.ReceiveLoginBonuses()
			if !reflect.DeepEqual(tt.u.LimitedGem, tt.wantGems) {
				t.Errorf("User.ReceiveLoginBonuses() got.LimitedGem = %+v, want %+v", tt.u.LimitedGem, tt.wantGems)
			}
			if !reflect.DeepEqual(tt.u.ItemSummary, tt.wantItems) {
				t.Errorf("User.ReceiveLoginBonuses() got.ItemSummary = %v, want %v", tt.u.ItemSummary, tt.wantItems)
			}
			if cmp.Equal(got1, tt.want1, opts...) != true {
				t.Errorf("User.ReceiveLoginBonuses() UserLoginBonusDiff = %+v", cmp.Diff(*got1, *tt.want1, opts...))
			}
		})
	}
}

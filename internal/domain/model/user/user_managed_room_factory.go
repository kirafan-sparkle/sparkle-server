package model_user

import (
	value_room "gitlab.com/kirafan/sparkle/server/internal/domain/value/room"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type InitialManagedRoomArrange struct {
	RoomObjectId uint32
	RoomNo       value_room.RoomNo
	X            value_room.RoomObjectPosition
	Y            value_room.RoomObjectPosition
	Dir          value_room.RoomObjectDirection
}

func GetInitialManagedRoomArranges() []InitialManagedRoomArrange {
	initialManagedRoomArranges := []InitialManagedRoomArrange{
		// Fantasy background
		{
			RoomObjectId: 1200001,
			RoomNo:       -1,
			X:            0,
			Y:            0,
			Dir:          0,
		},
		// Default wall
		{
			RoomObjectId: 1100001,
			RoomNo:       -1,
			X:            0,
			Y:            0,
			Dir:          0,
		},
		// Default floor
		{
			RoomObjectId: 1000001,
			RoomNo:       -1,
			X:            0,
			Y:            0,
			Dir:          0,
		},
		// Wood chair
		{
			RoomObjectId: 101007,
			RoomNo:       0,
			X:            7,
			Y:            4,
			Dir:          1,
		},
		// Magazine rack
		{
			RoomObjectId: 201004,
			RoomNo:       0,
			X:            4,
			Y:            8,
			Dir:          0,
		},
		// House plants
		{
			RoomObjectId: 501002,
			RoomNo:       0,
			X:            0,
			Y:            6,
			Dir:          1,
		},
		// Planter
		{
			RoomObjectId: 501001,
			RoomNo:       0,
			X:            8,
			Y:            8,
			Dir:          0,
		},
		// Bed
		{
			RoomObjectId: 300002,
			RoomNo:       1,
			X:            1,
			Y:            2,
			Dir:          1,
		},
		{
			RoomObjectId: 300002,
			RoomNo:       1,
			X:            1,
			Y:            5,
			Dir:          1,
		},
		{
			RoomObjectId: 300002,
			RoomNo:       1,
			X:            5,
			Y:            1,
			Dir:          1,
		},
		{
			RoomObjectId: 300002,
			RoomNo:       1,
			X:            5,
			Y:            4,
			Dir:          1,
		},
		{
			RoomObjectId: 300002,
			RoomNo:       1,
			X:            5,
			Y:            7,
			Dir:          1,
		},
	}
	return initialManagedRoomArranges
}

func NewInitialManagedRooms(userId value_user.UserId, managedRoomObjects []ManagedRoomObject) []ManagedRoom {
	initialManagedRoomArranges := GetInitialManagedRoomArranges()
	if len(managedRoomObjects) != len(initialManagedRoomArranges) {
		panic("The length of managedRoomObjects is not equal to the length of initialManagedRoomArranges")
	}

	newManagedRoomArrangeData := make([]ManagedRoomArrangeData, len(initialManagedRoomArranges))
	for i := range newManagedRoomArrangeData {
		newManagedRoomArrangeData[i] = ManagedRoomArrangeData{
			ManagedRoomObjectId: managedRoomObjects[i].ManagedRoomObjectId,
			RoomObjectId:        initialManagedRoomArranges[i].RoomObjectId,
			RoomNo:              initialManagedRoomArranges[i].RoomNo,
			X:                   initialManagedRoomArranges[i].X,
			Y:                   initialManagedRoomArranges[i].Y,
			Dir:                 initialManagedRoomArranges[i].Dir,
		}
	}

	newManagedRoom := ManagedRoom{
		FloorId:     1,
		GroupId:     0,
		ArrangeData: newManagedRoomArrangeData,
		Active:      0,
		UserId:      userId,
	}

	newManagedRooms := []ManagedRoom{newManagedRoom}
	return newManagedRooms
}

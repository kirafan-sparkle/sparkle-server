package model_user

import (
	"time"

	value_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/value/town_facility"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type ManagedTownFacility struct {
	ManagedTownFacilityId uint      `gorm:"primary_key"`
	CreatedAt             time.Time `gorm:"autoCreateTime"`
	UpdatedAt             time.Time `gorm:"autoUpdateTime"`
	DeletedAt             *time.Time
	// Foreign key
	UserId     value_user.UserId
	FacilityId uint32
	// Placement at town (0~4: main menu / other)
	BuildPointIndex value_town_facility.BuildPointIndex
	// Facility level (1~10)
	Level uint8
	// 0: not opened / 1: opened
	OpenState value_town_facility.TownFacilityOpenState
	// Unix time
	ActionTime uint64
	BuildTime  uint64
}

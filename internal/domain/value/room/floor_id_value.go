package value_room

import "errors"

type FloorId int8

const (
	FloorIdUnspecified FloorId = -1
	FloorIdMain        FloorId = 1
	FloorIdSub         FloorId = 2
)

var ErrInvalidFloorId = errors.New("invalid floor id")

func NewFloorId(value int64) (FloorId, error) {
	if value < int64(FloorIdUnspecified) || value > int64(FloorIdSub) {
		return 0, ErrInvalidFloorId
	}
	return FloorId(value), nil
}

package value_present

import "errors"

// Mysterious. Almost all of the values are 0 except free distribute and kirara point limit
// Maybe unused?
type PresentSource uint8

const (
	PresentSourceDefault        PresentSource = iota
	PresentSourceFreeDistribute PresentSource = 1
	PresentSourceExceedLimit    PresentSource = 13
)

var ErrInvalidPresentSource = errors.New("invalid present source")

func NewPresentSource(value uint8) (PresentSource, error) {
	if value > uint8(PresentSourceExceedLimit) {
		return PresentSource(0), ErrInvalidPresentSource
	}
	return PresentSource(value), nil
}

package value_training

import "errors"

type TrainingCostType uint8

const (
	TrainingCostTypeCoin TrainingCostType = iota
	TrainingCostTypeKirara
	TrainingCostTypeNone
)

func NewTrainingCostType(t uint8) (TrainingCostType, error) {
	if t > 1 {
		return TrainingCostTypeNone, errors.New("invalid training reward currency type")
	}
	return TrainingCostType(t), nil
}

func (t TrainingCostType) String() string {
	switch t {
	case TrainingCostTypeNone:
		return "None"
	case TrainingCostTypeKirara:
		return "Kirara"
	case TrainingCostTypeCoin:
		return "Coin"
	}
	return "Unknown"
}

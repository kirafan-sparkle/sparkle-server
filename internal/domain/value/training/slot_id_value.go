package value_training

import "errors"

type SlotId uint8

const (
	SlotId0 SlotId = iota
	SlotId1
	SlotId2
	SlotId3
)

func NewSlotId(t uint8) (SlotId, error) {
	if t > uint8(SlotId3) {
		return SlotId0, errors.New("invalid slot id")
	}
	return SlotId(t), nil
}

package value_training

import "errors"

type TrainingRewardCurrencyType uint8

const (
	TrainingRewardCurrencyTypeCoin TrainingRewardCurrencyType = iota
	TrainingRewardCurrencyTypeKirara
	TrainingRewardCurrencyTypeNone
)

func NewTrainingRewardCurrencyType(t uint8) (TrainingRewardCurrencyType, error) {
	if t > 1 {
		return TrainingRewardCurrencyTypeNone, errors.New("invalid training reward currency type")
	}
	return TrainingRewardCurrencyType(t), nil
}

func (t TrainingRewardCurrencyType) String() string {
	switch t {
	case TrainingRewardCurrencyTypeNone:
		return "None"
	case TrainingRewardCurrencyTypeKirara:
		return "Kirara"
	case TrainingRewardCurrencyTypeCoin:
		return "Coin"
	}
	return "Unknown"
}

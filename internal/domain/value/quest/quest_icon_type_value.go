package value_quest

import "errors"

type QuestIconType int8

const (
	// N章
	QuestIconTypeChapter QuestIconType = -1
	// 収集
	QuestIconTypeCollect QuestIconType = iota - 1
	// 強敵
	QuestIconLowLevelBoss
	// 超強敵
	QuestIconMiddleLevelBoss
	// 極
	QuestIconHighLevelBoss
	// 乱戦
	QuestIconBossRush
	// 強化
	QuestIconLevelUp
	// 進化
	QuestIconEvolute
	// コイン
	QuestIconCoin
	// 挑戦
	QuestIconChallenge
	// 外伝
	QuestIconSideStory
	// 討伐
	QuestIconDefeat
	// 後日談
	QuestIconAfterStory
	// 限定
	QuestIconLimited
	// クラフト
	QuestIconCraft
	// 試練
	QuestIconOrdeal
	// 探窟
	QuestIconExploration
	// 超難関
	QuestIconSuperHard
)

var ErrInvalidQuestIconType = errors.New("invalid quest icon type")

func NewQuestIconType(value int8) (QuestIconType, error) {
	if value < int8(QuestIconTypeChapter) || value > int8(QuestIconSuperHard) {
		return QuestIconType(0), ErrInvalidQuestIconType
	}
	return QuestIconType(value), nil
}

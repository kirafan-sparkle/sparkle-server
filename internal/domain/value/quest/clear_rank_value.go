package value_quest

import "errors"

type ClearRank uint8

const (
	ClearRankNone ClearRank = iota
	ClearRankBronze
	ClearRankSilver
	ClearRankGold
)

var ErrClearRankInvalid = errors.New("invalid clear rank")

func NewClearRank(value uint8) (ClearRank, error) {
	if value < uint8(ClearRankNone) {
		return 0, ErrClearRankInvalid
	}
	if value > uint8(ClearRankGold) {
		return 0, ErrClearRankInvalid
	}
	return ClearRank(value), nil
}

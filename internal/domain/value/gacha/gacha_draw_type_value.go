package value_gacha

import "errors"

type GachaDrawType uint8

const (
	GachaDrawTypeUnlimitedGem1 GachaDrawType = iota + 1
	GachaDrawTypeGem1
	GachaDrawTypeGem10
	GachaDrawTypeItem
)

var ErrInvalidGachaDrawType = errors.New("invalid GachaDrawType")

func NewGachaDrawType(v uint8) (GachaDrawType, error) {
	if v > uint8(GachaDrawTypeItem) {
		return GachaDrawTypeUnlimitedGem1, ErrInvalidGachaDrawType
	}
	return GachaDrawType(v), nil
}

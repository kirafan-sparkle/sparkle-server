package value_user

import (
	"golang.org/x/exp/constraints"
)

type UserId uint

func NewUserId[T constraints.Integer](id T) UserId {
	return UserId(uint(id))
}

package value_user

type Gold uint64

func (g Gold) ToValue() uint64 {
	return uint64(g)
}

func NewGold(g uint64) Gold {
	return Gold(g)
}

package value_user

import (
	"golang.org/x/exp/constraints"
)

type ManagedAbilityBoardId int64

func (id *ManagedAbilityBoardId) ToValue() int64 {
	return int64(*id)
}

func NewManagedAbilityBoardId[T constraints.Integer](id T) ManagedAbilityBoardId {
	return ManagedAbilityBoardId(int64(id))
}

package value_login_bonus

import "errors"

// texture -> login_bonus_background -> id
type LoginBonusImageId uint32

const (
	// Total login bonus
	LoginBonusImageNone LoginBonusImageId = 0
	// Normal login bonus
	LoginBonusImageNormal       LoginBonusImageId = 2
	LoginBonusImageKirafanRadio LoginBonusImageId = 1000
	// Other images are excluded since depends on event resources
)

var ErrInvalidLoginBonusImageId = errors.New("invalid LoginBonusImageId")

func NewLoginBonusImageId(v uint32) (LoginBonusImageId, error) {
	if v > uint32(LoginBonusImageKirafanRadio) {
		return LoginBonusImageNone, ErrInvalidLoginBonusImageId
	}
	return LoginBonusImageId(v), nil
}

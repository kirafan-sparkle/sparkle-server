package value_login_bonus

import "errors"

// texture -> login_bonus_icon -> id
type LoginBonusDayIconId uint16

const (
	// Total login
	BonusDayIconIdNone LoginBonusDayIconId = 0
	// Normal daily login
	BonusDayIconIdTradingMedal10             LoginBonusDayIconId = 10
	BonusDayIconIdGem10TradingMedal5         LoginBonusDayIconId = 11
	BonusDayIconIdGem20TradingMedal5         LoginBonusDayIconId = 12
	BonusDayIconIdLotteryTicket              LoginBonusDayIconId = 13
	BonusDayIconIdTradingMedal20             LoginBonusDayIconId = 14
	BonusDayIconIdGem20TradingMedal10        LoginBonusDayIconId = 15
	BonusDayIconIdLotteryTicketEtoiliumShard LoginBonusDayIconId = 16
	// Event daily login
	BonusDayIconIdGem20              LoginBonusDayIconId = 1020
	BonusDayIconIdGem30              LoginBonusDayIconId = 1030
	BonusDayIconIdGem40              LoginBonusDayIconId = 1040
	BonusDayIconIdGem100             LoginBonusDayIconId = 1100
	BonusDayIconIdGem300             LoginBonusDayIconId = 1300
	BonusDayIconIdLotteryTicketSmall LoginBonusDayIconId = 2000
	BonusDayIconIdStaminaGold5       LoginBonusDayIconId = 2100
	BonusDayIconIdTradingMedal50     LoginBonusDayIconId = 2200
	BonusDayIconIdGoldKey5           LoginBonusDayIconId = 2300
	// Other icons are excluded since depends on event resources
)

var ErrInvalidLoginBonusDayIconId = errors.New("invalid LoginBonusDayIconId")

func NewLoginBonusDayIconId(v uint16) (LoginBonusDayIconId, error) {
	if v > uint16(BonusDayIconIdGoldKey5) {
		return BonusDayIconIdNone, ErrInvalidLoginBonusDayIconId
	}
	return LoginBonusDayIconId(v), nil
}

package value_login_bonus

import "errors"

type LoginBonusType uint8

const (
	LoginBonusTypeNormal LoginBonusType = iota
	LoginBonusTypeEvent
	LoginBonusTypeTotalLogin
)

var ErrInvalidLoginBonusType = errors.New("invalid LoginBonusType")

func NewLoginBonusType(v uint8) (LoginBonusType, error) {
	if v > uint8(LoginBonusTypeTotalLogin) {
		return LoginBonusTypeNormal, ErrInvalidLoginBonusType
	}
	return LoginBonusType(v), nil
}

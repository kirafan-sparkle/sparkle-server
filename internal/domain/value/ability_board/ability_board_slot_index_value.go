package value_ability_board

import "fmt"

type AbilityBoardSlotIndex uint8

const (
	AbilityBoardSlotIndexMin AbilityBoardSlotIndex = 0
	AbilityBoardSlotIndexMax AbilityBoardSlotIndex = 16
)

func NewAbilityBoardSlotIndex(v uint8) (AbilityBoardSlotIndex, error) {
	if v > uint8(AbilityBoardSlotIndexMax) || v < uint8(AbilityBoardSlotIndexMin) {
		return 0, fmt.Errorf("specified abilityBoardSlotIndex %d is out of range [%d, %d]", v, AbilityBoardSlotIndexMin, AbilityBoardSlotIndexMax)
	}
	return AbilityBoardSlotIndex(v), nil
}

package value_ability_board

import (
	"fmt"
)

type AbilityBoardSlotId uint

const (
	AbilityBoardSlotIdMin AbilityBoardSlotId = 3000000
	AbilityBoardSlotIdMax AbilityBoardSlotId = 3999999
)

func NewAbilityBoardSlotId(v uint) (AbilityBoardSlotId, error) {
	if v > uint(AbilityBoardSlotIdMax) || v < uint(AbilityBoardSlotIdMin) {
		return 0, fmt.Errorf("specified abilityBoardSlotId %d is out of range [%d, %d]", v, AbilityBoardSlotIdMin, AbilityBoardSlotIdMax)
	}
	return AbilityBoardSlotId(v), nil
}

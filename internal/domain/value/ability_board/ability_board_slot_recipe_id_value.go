package value_ability_board

import "fmt"

type AbilityBoardSlotRecipeId int32

const (
	AbilityBoardSlotRecipeIdUnspecified AbilityBoardSlotRecipeId = -1
	AbilityBoardSlotRecipeIdMin         AbilityBoardSlotRecipeId = 30000000
	AbilityBoardSlotRecipeIdMax         AbilityBoardSlotRecipeId = 39999999
)

func NewAbilityBoardSlotRecipeId(v int32) (AbilityBoardSlotRecipeId, error) {
	if v > int32(AbilityBoardSlotRecipeIdMax) || (v < int32(AbilityBoardSlotRecipeIdMin) && v != int32(AbilityBoardSlotRecipeIdUnspecified)) {
		return 0, fmt.Errorf("specified abilityBoardSlotRecipeId %d is out of range [%d, %d]", v, AbilityBoardSlotRecipeIdMin, AbilityBoardSlotRecipeIdMax)
	}
	return AbilityBoardSlotRecipeId(v), nil
}

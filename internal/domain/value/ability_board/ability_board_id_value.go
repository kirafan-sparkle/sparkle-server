package value_ability_board

import (
	"fmt"
)

type AbilityBoardId uint

const (
	AbilityBoardIdMin AbilityBoardId = 3000000
	AbilityBoardIdMax AbilityBoardId = 3999999
)

func NewAbilityBoardId(v uint) (AbilityBoardId, error) {
	if v > uint(AbilityBoardIdMax) || v < uint(AbilityBoardIdMin) {
		return 0, fmt.Errorf("specified abilityBoardId %d is out of range [%d, %d]", v, AbilityBoardIdMin, AbilityBoardIdMax)
	}
	return AbilityBoardId(v), nil
}

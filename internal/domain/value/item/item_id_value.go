package value_item

import (
	"errors"

	"golang.org/x/exp/constraints"
)

type ItemId int64

const (
	ItemIdUnspecified ItemId = -1
	ItemIdInvalid     ItemId = 0
)

var ErrInvalidItemId = errors.New("invalid item id")

func NewItemId[T constraints.Integer](id T) (ItemId, error) {
	if int64(id) < int64(ItemIdUnspecified) {
		return ItemIdInvalid, ErrInvalidItemId
	}
	return ItemId(int64(id)), nil
}

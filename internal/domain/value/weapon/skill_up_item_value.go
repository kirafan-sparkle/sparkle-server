package value_weapon

import (
	"errors"

	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

type SkillUpItemType uint32

const (
	SkillUpItemTypeInvalid       SkillUpItemType = 0
	SkillUpItemTypeSkillUpPowder SkillUpItemType = 1
)

const (
	SkillUpItemIdSkillUpPowder value_item.ItemId = 9005
)

func NewSkillUpItemType(v value_item.ItemId) (SkillUpItemType, error) {
	if v != SkillUpItemIdSkillUpPowder {
		return SkillUpItemTypeInvalid, errors.New("invalid item id")
	}
	return SkillUpItemTypeSkillUpPowder, nil
}

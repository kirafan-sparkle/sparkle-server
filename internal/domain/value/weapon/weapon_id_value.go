package value_weapon

import "golang.org/x/exp/constraints"

type WeaponId uint32

func NewWeaponId[T constraints.Integer](id T) WeaponId {
	return WeaponId(uint(id))
}

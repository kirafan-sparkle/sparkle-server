package value_event_quest

import "errors"

type EventType uint32

func (e EventType) GetCategory() (EventTypeCategory, error) {
	category, err := NewEventTypeCategory(uint8(e / 1000))
	return category, err
}

const (
	EventTypeEventRerunRealistTachiNoMirai                             EventType = 1139
	EventTypeEventRealistTachiNoMirai                                  EventType = 1138
	EventTypeEventBocchiZaRojouLiveTour                                EventType = 1137
	EventTypeEventDaiDasshutsuOnsenPrison                              EventType = 1136
	EventTypeEventOtakaraZakuzakuCampaign                              EventType = 1135
	EventTypeEventKaisaiEtoiliaGakuenBunkasai                          EventType = 1134
	EventTypeEventRerunHibariToRiiSanToKoufukuNoHana                   EventType = 1133
	EventTypeEventRerunKurumiInWonderland                              EventType = 1132
	EventTypeEventIkuseiOuenCampaign2                                  EventType = 1131
	EventTypeEventBeachSharkPanic                                      EventType = 1129
	EventTypeEventManatsuNoEtoiliaFestival                             EventType = 1128
	EventTypeEventTanabataNoWakareboshi                                EventType = 1127
	EventTypeEventOshigotoSagashiToOuchiSagashi                        EventType = 1126
	EventTypeEventHauntedBridal                                        EventType = 1125
	EventTypeEventBlendCallAccident                                    EventType = 1124
	EventTypeEventChouNankanHenoMichiTaitanHen                         EventType = 1123
	EventTypeEventMegamiNoEasterEgg                                    EventType = 1122
	EventTypeEventIshuzokuKonyokuOnsengai                              EventType = 1121
	EventTypeEventFlyFishMemory                                        EventType = 1120
	EventTypeEventHowaitodeeDaibakuhatsu                               EventType = 1119
	EventTypeEventIkuseiOuenCampaign1                                  EventType = 1118
	EventTypeEventBarentainToKoiNoShikakuKankei                        EventType = 1117
	EventTypeEventSnsBuToFukkatsuNoMaou                                EventType = 1116
	EventTypeEventShinshunIkuseiOuenCampaign                           EventType = 1115
	EventTypeEventKurisumasuIkuseiOuenCampaign                         EventType = 1114
	EventTypeEventOShougatsuchuuShiWoSoshiSeyoJuunishiDaishuuKetsu     EventType = 1112
	EventTypeEventSuupaakurisumasukyaroruEtoiliaKouen                  EventType = 1111
	EventTypeEventJabonWoKiru                                          EventType = 1110
	EventTypeEventChouNankanHenoMichiOrubaHen                          EventType = 1109
	EventTypeEventHarouinMajoMaShiro                                   EventType = 1108
	EventTypeEventArukiTsuzukeruKimiNotameni                           EventType = 1107
	EventTypeEventGenzaiNewGameSeisakuchuu                             EventType = 1106
	EventTypeEventMujintouAidoruSeikatsu                               EventType = 1105
	EventTypeEventKiniroYuubinkyokuToYagiSanNoTegami                   EventType = 1104
	EventTypeEventSummerNightResort                                    EventType = 1103
	EventTypeEventSummerNightRadio                                     EventType = 1102
	EventTypeEventEtoiliaNatsuNoHitomaku2021                           EventType = 1101
	EventTypeEventDokidokiEtoiliaGakuenShinninKyoushiHen               EventType = 1100
	EventTypeEventSodateTeIdomoUAbiriteitsuriiJissouKinenKuesuto       EventType = 1099
	EventTypeEventChikakuTeTooiSekaiNoShoujoTachi                      EventType = 1098
	EventTypeEventBuraidaruoaheru                                      EventType = 1097
	EventTypeEventJaiantokuromonVsMekamatchi                           EventType = 1096
	EventTypeEventMeidoKanDehaWaraeNai                                 EventType = 1095
	EventTypeEventWatashitachiDatteYakyuuShitai                        EventType = 1094
	EventTypeEventEtoiliaYukemuriJikenRokuBakenekoRyokanToMatatabiNoYu EventType = 1093
	EventTypeEventRanpuNoMajoMajikarutoorun                            EventType = 1092
	EventTypeEventShinshunIkuseiCampaign                               EventType = 1091
	EventTypeEventFukushuuNoHowaitodeekoroshiamu                       EventType = 1090
	EventTypeEventAiNoBarentainchokobatoru                             EventType = 1089
	EventTypeEventIsekaiManjuuTachitoEtoiliaNiSemaruYami               EventType = 1088
	EventTypeEventSupeesueiriangeemu                                   EventType = 1087
	EventTypeEventHonjitsuNoShuyakuHaKunDaShinnenkaiDaYoZeninshuugou   EventType = 1086
	EventTypeEventKuromonToKyodaiNaChristmasTree                       EventType = 1085
	EventTypeEventBoukyakuNoMumaTachi                                  EventType = 1083
	EventTypeEventIsekaiShoukanSaretaraKyuuseishuDatta                 EventType = 1082
	EventTypeEventHalloweenNoKaitouDensetsu                            EventType = 1081
	EventTypeEventDropoutIdolsAnotherWorldTv                           EventType = 1080
	EventTypeEventAMiracleEncounterAndTheDreamParty                    EventType = 1079
	EventTypeEventRaburabutoraberuDaisakusen                           EventType = 1078
	EventTypeEventBattleOfPiratesTorawareNoChiaToMizugiKaizoku         EventType = 1076
	EventTypeEventSummerHolidayUltimateQuiz                            EventType = 1075
	EventTypeEventNatsuyasumiShukudaiDaisakusen                        EventType = 1074
	EventTypeEventEtoiliaNatsuNoHitomaku2020                           EventType = 1073
	EventTypeEventOosoudouEtoiliaDoujinshiSokubaikai                   EventType = 1072
	EventTypeEventTanabataNoYakusoku                                   EventType = 1071
	EventTypeEventEtoiliaBaseballShowDown                              EventType = 1070
	EventTypeEventTheGreatSportsFestivalTheGoddessHolyCup              EventType = 1069
	EventTypeEventOneechans11                                          EventType = 1068
	EventTypeEventTheMaidWitnessedFlakyIncidentInTheBlondemanor        EventType = 1067
	EventTypeEventRerunSanshaSanyousCasualTrip                         EventType = 1066
	EventTypeEventDokidokiEtoiliaCampus                                EventType = 1065
	EventTypeEventTheStarWhereMiraclesAreBorn                          EventType = 1063
	EventTypeEventDarkCloudsHinamatsuriFestival                        EventType = 1062
	EventTypeEventHerosGourmetEtoiliasFoodJournal                      EventType = 1061
	EventTypeEventPursueTheDarknessChocolateSyndicate                  EventType = 1059
	EventTypeEventNewYearNurturingSupportCampaign                      EventType = 1058
	EventTypeEventNewYearsSpecialProgrammeItsANewYearEtoilia           EventType = 1057
	EventTypeEventHungryChristmas                                      EventType = 1056
	EventTypeEventLampsLimitChallenge                                  EventType = 1055
	EventTypeEventEtoiliaExplorateur                                   EventType = 1054
	EventTypeEventYutakasDiscipleAspiration                            EventType = 1053
	EventTypeEventOperationTrickOrTreat                                EventType = 1052
	EventTypeEventChimameCorpsAtWork                                   EventType = 1051
	EventTypeEventTheDemonGirlInAnotherWorldWithTheAncestue            EventType = 1049
	EventTypeEventPiratesOfEtoilia                                     EventType = 1047
	EventTypeEventRyuuguuAdventure                                     EventType = 1045
	EventTypeEventEtoiliaSummerOneShots2019                            EventType = 1044
	EventTypeEventRerunMysteriousIslandAndTrialOfBonds                 EventType = 1042
	EventTypeEventRerunFirstIssueComicEtoilia                          EventType = 1041
	EventTypeEventWanderingBountyHuntersRinKaren                       EventType = 1040
	EventTypeEventOpeningEtoiliaSportsFestival                         EventType = 1039
	EventTypeEventAmausaanSambition                                    EventType = 1037
	EventTypeEventHitsugiKatsugiGaAndTheGoldenKuromonStatue            EventType = 1036
	EventTypeEventSanshaSanyousCasualTrip                              EventType = 1035
	EventTypeEventRerunOperationAFriendsFriendIsAlsoAFriend            EventType = 1034
	EventTypeEventEtoiliaAdventureLogAwakeningOfTheSteelGiant          EventType = 1033
	EventTypeEventEtoiliaAdventureLogInstructorrizeSecretTraining      EventType = 1032
	EventTypeEventChocolatePanic                                       EventType = 1031
	EventTypeEventRerunOutclubTheNorthernMountainAndTheFirstCamp       EventType = 1030
	EventTypeEventCreamatesBigMeetingNewyearSugorokuTournament2        EventType = 1029
	EventTypeEventCreamatesBigMeetingNewyearSugorokuTournament         EventType = 1028
	EventTypeEventEtoiliasBlackChristmas                               EventType = 1027
	EventTypeEventYellForAll                                           EventType = 1026
	EventTypeEventIsTheOrderARabbitInADifferentWorld                   EventType = 1025
	EventTypeEventHibariRiiSanAndTheFlowerOfHappiness                  EventType = 1024
	EventTypeEventRerunStileSecondShopOpen                             EventType = 1023
	EventTypeEventMiyakoAndthecatssong                                 EventType = 1022
	EventTypeEventGreatDetectiveCoronetTheTragedyOfYuu                 EventType = 1021
	EventTypeEventMysteriousIslandAndTrialOfBonds                      EventType = 1020
	EventTypeEventTreasureChestAtTheBottomOfTheSea                     EventType = 1018
	EventTypeEventBeachHutOfTheYear                                    EventType = 1017
	EventTypeEventKeionEtoiliaLive                                     EventType = 1016
	EventTypeEventKurumiInWonderland                                   EventType = 1015
	EventTypeEventFirstIssueComicEtoilia                               EventType = 1014
	EventTypeEventRememberYosakoi                                      EventType = 1013
	EventTypeEventEtoiliasDiva                                         EventType = 1012
	EventTypeEventOutclubTheNorthernMountainAndTheFirstCamp            EventType = 1011
	EventTypeEventOperationAFriendsFriendIsAlsoAFriend                 EventType = 1010
	EventTypeEventFairiesTrick                                         EventType = 1009
	EventTypeEventNenecchiQuest                                        EventType = 1008
	EventTypeEventHinamatsuriFestival                                  EventType = 1007
	EventTypeEventTheEaterOfTheBlacksmithsDream                        EventType = 1006
	EventTypeEventChocolateQuest                                       EventType = 1005
	EventTypeEventStileSecondShopOpen                                  EventType = 1003
	EventTypeEventKiniroNewYear                                        EventType = 1002
	EventTypeEventSantaclausOfEtoilia                                  EventType = 1001
	EventTypeEventMushroomBaby                                         EventType = 1000
)

const (
	EventTypeAuthorPecoATrialVersion                 EventType = 2001
	EventTypeAuthorKiniroDarkness                    EventType = 2002
	EventTypeAuthorSpicimination                     EventType = 2003
	EventTypeAuthorYuyushikiCostumeInTheForestsLodge EventType = 2004
	EventTypeAuthorNoKuruToKoutetsuNoHoukou          EventType = 2005
	EventTypeAuthorSlowtamers                        EventType = 2006
)

const (
	EventTypeDailyNichiKyoukaTaneShuukakuBaYou      EventType = 3100
	EventTypeDailyGatsuKyoukaTaneShuukakuBaGatsu    EventType = 3101
	EventTypeDailyHiKyoukaTaneShuukakuBaHonoo       EventType = 3102
	EventTypeDailyMizuKyoukaTaneShuukakuBaMizu      EventType = 3103
	EventTypeDailyKiKyoukaTaneShuukakuBaKaze        EventType = 3104
	EventTypeDailyTsuchiKyoukaTaneShuukakuBaTsuchi  EventType = 3105
	EventTypeDailyNichiShinkaShuSaikutsuBaYou       EventType = 3200
	EventTypeDailyGatsuShinkaShuSaikutsuBaGatsu     EventType = 3201
	EventTypeDailyHiShinkaShuSaikutsuBaHonoo        EventType = 3202
	EventTypeDailyMizuShinkaShuSaikutsuBaMizu       EventType = 3203
	EventTypeDailyKiShinkaShuSaikutsuBaKaze         EventType = 3204
	EventTypeDailyTsuchiShinkaShuSaikutsuBaTsuchi   EventType = 3205
	EventTypeDailyJousetsuShinkaNoNinteiBa          EventType = 3300
	EventTypeDailyGessuikinOugonHeigen              EventType = 3400
	EventTypeDailyGetsumokuTsuchiSenshinoShuurenBa  EventType = 3500
	EventTypeDailyKadoNaitoNoShuurenBa              EventType = 3501
	EventTypeDailySuikinNichiMahoutsukaiNoShuurenBa EventType = 3502
	EventTypeDailyMokunichiArukemisutoNoShuurenBa   EventType = 3503
	EventTypeDailyTueFriSunSouryoNoShuurenBa        EventType = 3504
)

const (
	EventTypeMemorialHidamari     EventType = 5000
	EventTypeMemorialYuyushiki    EventType = 5001
	EventTypeMemorialGakkou       EventType = 5002
	EventTypeMemorialAchannel     EventType = 5003
	EventTypeMemorialKinmoza      EventType = 5004
	EventTypeMemorialNewGame      EventType = 5005
	EventTypeMemorialStella       EventType = 5006
	EventTypeMemorialUrara        EventType = 5007
	EventTypeMemorialKillme       EventType = 5008
	EventTypeMemorialSakura       EventType = 5009
	EventTypeMemorialBlends       EventType = 5010
	EventTypeMemorialSlowstart    EventType = 5011
	EventTypeMemorialKon          EventType = 5012
	EventTypeMemorialYurukyan     EventType = 5013
	EventTypeMemorialKomiga       EventType = 5014
	EventTypeMemorialGA           EventType = 5015
	EventTypeMemorialYumekui      EventType = 5016
	EventTypeMemorialHanayamata   EventType = 5017
	EventTypeMemorialAnhapi       EventType = 5018
	EventTypeMemorialHarukana     EventType = 5019
	EventTypeMemorialGochiusa     EventType = 5020
	EventTypeMemorialAnima        EventType = 5021
	EventTypeMemorialKirara       EventType = 5022
	EventTypeMemorialSansha       EventType = 5023
	EventTypeMemorialHitugi       EventType = 5024
	EventTypeMemorialMatikado     EventType = 5025
	EventTypeMemorialHarumination EventType = 5026
	EventTypeMemorialKoufuku      EventType = 5027
	EventTypeMemorialKoiasu       EventType = 5028
	EventTypeMemorialAcchikocchi  EventType = 5030
	EventTypeMemorialOchifuru     EventType = 5031
	EventTypeMemorialPawasuma     EventType = 5032 // Maybe (not sure)
	EventTypeMemorialKoharu       EventType = 5033 // Maybe (not sure)
	EventTypeMemorialSlowLoop     EventType = 5035 // Maybe (not sure)
	EventTypeMemorialRpg          EventType = 5036 // Maybe (not sure)
	EventTypeMemorialBotti        EventType = 5037 // Maybe (not sure)
)

const (
	EventTypeCraftHidamari     EventType = 6000
	EventTypeCraftYuyushiki    EventType = 6001
	EventTypeCraftGakkou       EventType = 6002
	EventTypeCraftAchannel     EventType = 6003
	EventTypeCraftKinmoza      EventType = 6004
	EventTypeCraftNewGame      EventType = 6005
	EventTypeCraftStella       EventType = 6006
	EventTypeCraftUrara        EventType = 6007
	EventTypeCraftKillme       EventType = 6008
	EventTypeCraftSakura       EventType = 6009
	EventTypeCraftBlends       EventType = 6010
	EventTypeCraftSlowstart    EventType = 6011
	EventTypeCraftKeion        EventType = 6012
	EventTypeCraftYurukyan     EventType = 6013
	EventTypeCraftKomiga       EventType = 6014
	EventTypeCraftGA           EventType = 6015
	EventTypeCraftYumekui      EventType = 6016
	EventTypeCraftHanayamata   EventType = 6017
	EventTypeCraftAnhapi       EventType = 6018
	EventTypeCraftHarukana     EventType = 6019
	EventTypeCraftGochiusa     EventType = 6020
	EventTypeCraftAnima        EventType = 6021
	EventTypeCraftKirara       EventType = 6022
	EventTypeCraftSansha       EventType = 6023
	EventTypeCraftHitugi       EventType = 6024
	EventTypeCraftMatikado     EventType = 6025
	EventTypeCraftHarumination EventType = 6026
	EventTypeCraftKoufuku      EventType = 6027
	EventTypeCraftKoiasu       EventType = 6028
	EventTypeCraftTamayomi     EventType = 6029
	EventTypeCraftAcchikocchi  EventType = 6030
	EventTypeCraftOchifuru     EventType = 6031
	EventTypeCraftPawasuma     EventType = 6032
	EventTypeCraftKoharu       EventType = 6033
	EventTypeCraftSlowLoop     EventType = 6035
	EventTypeCraftRpg          EventType = 6036
	EventTypeCraftBotti        EventType = 6037
)

const (
	EventTypeChallenge_2024_07_PowerfulEnemyChallengeQuest  EventType = 204941
	EventTypeChallenge_2024_06_PowerfulEnemyChallengeQuest  EventType = 204940
	EventTypeChallenge_2024_05_PowerfulEnemyChallengeQuest  EventType = 204939
	EventTypeChallenge_2024_04_PowerfulEnemyChallengeQuest  EventType = 204938
	EventTypeChallenge_2024_03_PowerfulEnemyChallengeQuest  EventType = 204937
	EventTypeChallenge_2024_02_PowerfulEnemyChallengeQuest  EventType = 204936
	EventTypeChallenge_2024_01_PowerfulEnemyChallengeQuest  EventType = 204935
	EventTypeChallenge_2023_12_PowerfulEnemyChallengeQuest  EventType = 204934
	EventTypeChallenge_2023_11_PowerfulEnemyChallengeQuest  EventType = 204933
	EventTypeChallenge_2023_10_PowerfulEnemyChallengeQuest  EventType = 204932
	EventTypeChallenge_2023_09_PowerfulEnemyChallengeQuest  EventType = 204931
	EventTypeChallenge_2023_08_PowerfulEnemyChallengeQuest  EventType = 204930
	EventTypeChallenge_2023_07_PowerfulEnemyChallengeQuest  EventType = 204929
	EventTypeChallenge_2023_06_PowerfulEnemyChallengeQuest  EventType = 204928
	EventTypeChallenge_2023_05_PowerfulEnemyChallengeQuest  EventType = 204927
	EventTypeChallenge_2023_04_PowerfulEnemyChallengeQuest  EventType = 204926
	EventTypeChallenge_2023_03_PowerfulEnemyChallengeQuest  EventType = 204925
	EventTypeChallenge_2023_02_PowerfulEnemyChallengeQuest  EventType = 204924
	EventTypeChallenge_2023_01_PowerfulEnemyChallengeQuest  EventType = 204923
	EventTypeChallenge_2022_12_PowerfulEnemyChallengeQuest  EventType = 204922
	EventTypeChallenge_2022_11_PowerfulEnemyChallengeQuest  EventType = 204921
	EventTypeChallenge_2022_10_PowerfulEnemyChallengeQuest  EventType = 204920
	EventTypeChallenge_2022_09_PowerfulEnemyChallengeQuest  EventType = 204919
	EventTypeChallenge_2022_08_PowerfulEnemyChallengeQuest  EventType = 204918
	EventTypeChallenge_2022_07_PowerfulEnemyChallengeQuest  EventType = 204917
	EventTypeChallenge_2022_06_PowerfulEnemyChallengeQuest  EventType = 204916
	EventTypeChallenge_2022_05_PowerfulEnemyChallengeQuest  EventType = 204915
	EventTypeChallenge_2022_04_PowerfulEnemyChallengeQuest  EventType = 204914
	EventTypeChallenge_2022_03_PowerfulEnemyChallengeQuest  EventType = 204913
	EventTypeChallenge_2022_02_PowerfulEnemyChallengeQuest  EventType = 204912
	EventTypeChallenge_2022_01_PowerfulEnemyChallengeQuest  EventType = 204911
	EventTypeChallenge_2021_12_PowerfulEnemyChallengeQuest8 EventType = 204910
	EventTypeChallenge_2021_12_PowerfulEnemyChallengeQuest7 EventType = 204909
	EventTypeChallenge_2021_12_PowerfulEnemyChallengeQuest6 EventType = 204908
	EventTypeChallenge_2021_12_PowerfulEnemyChallengeQuest5 EventType = 204907
	EventTypeChallenge_2021_12_PowerfulEnemyChallengeQuest4 EventType = 204906
	EventTypeChallenge_2021_12_PowerfulEnemyChallengeQuest3 EventType = 204905
	EventTypeChallenge_2021_12_PowerfulEnemyChallengeQuest2 EventType = 204904
	EventTypeChallenge_2021_12_PowerfulEnemyChallengeQuest1 EventType = 204903
	EventTypeChallenge_2021_11_PowerfulEnemyChallengeQuest  EventType = 204902
	EventTypeChallenge_2021_10_PowerfulEnemyChallengeQuest  EventType = 204901
	EventTypeChallenge_2021_09_PowerfulEnemyChallengeQuest  EventType = 204900
	EventTypeChallenge_2021_08_PowerfulEnemyChallengeQuest  EventType = 204099
	EventTypeChallenge_2021_07_PowerfulEnemyChallengeQuest  EventType = 204098
	EventTypeChallenge_2021_06_PowerfulEnemyChallengeQuest  EventType = 204097
	EventTypeChallenge_2021_05_PowerfulEnemyChallengeQuest  EventType = 204096
	EventTypeChallenge_2021_04_PowerfulEnemyChallengeQuest  EventType = 204095
	EventTypeChallenge_2021_03_PowerfulEnemyChallengeQuest  EventType = 204094
	EventTypeChallenge_2021_02_PowerfulEnemyChallengeQuest  EventType = 204093
	EventTypeChallenge_2021_01_PowerfulEnemyChallengeQuest  EventType = 204092
	EventTypeChallenge_2020_12_PowerfulEnemyChallengeQuest  EventType = 204091
	EventTypeChallenge_2020_11_PowerfulEnemyChallengeQuest  EventType = 204033
	EventTypeChallenge_2020_10_PowerfulEnemyChallengeQuest  EventType = 204032
	EventTypeChallenge_2020_09_PowerfulEnemyChallengeQuest  EventType = 204031
	EventTypeChallenge_2020_08_PowerfulEnemyChallengeQuest  EventType = 204030
	EventTypeChallenge_2020_07_PowerfulEnemyChallengeQuest  EventType = 204029
	EventTypeChallenge_2020_06_PowerfulEnemyChallengeQuest  EventType = 204028
	EventTypeChallenge_2020_05_PowerfulEnemyChallengeQuest  EventType = 204027
	EventTypeChallenge_2020_04_PowerfulEnemyChallengeQuest  EventType = 204026
	EventTypeChallenge_2020_03_PowerfulEnemyChallengeQuest  EventType = 204025
	EventTypeChallenge_2020_02_PowerfulEnemyChallengeQuest  EventType = 204024
	EventTypeChallenge_2020_01_PowerfulEnemyChallengeQuest  EventType = 204023
	EventTypeChallenge_2019_12_PowerfulEnemyChallengeQuest  EventType = 204022
	EventTypeChallenge_2019_11_PowerfulEnemyChallengeQuest  EventType = 204021
	EventTypeChallenge_2019_10_PowerfulEnemyChallengeQuest  EventType = 204020
	EventTypeChallenge_2019_09_PowerfulEnemyChallengeQuest  EventType = 204019
	EventTypeChallenge_2019_08_PowerfulEnemyChallengeQuest  EventType = 204018
	EventTypeChallenge_2019_07_PowerfulEnemyChallengeQuest  EventType = 204017
	EventTypeChallenge_2019_06_PowerfulEnemyChallengeQuest  EventType = 204016
	EventTypeChallenge_2019_05_PowerfulEnemyChallengeQuest  EventType = 204015
	EventTypeChallenge_2019_04_PowerfulEnemyChallengeQuest  EventType = 204014
	EventTypeChallenge_2019_03_PowerfulEnemyChallengeQuest  EventType = 204013
	EventTypeChallenge_2019_02_PowerfulEnemyChallengeQuest  EventType = 204012
	EventTypeChallenge_2019_01_PowerfulEnemyChallengeQuest  EventType = 204011
	EventTypeChallenge_2018_12_PowerfulEnemyChallengeQuest  EventType = 204010
	EventTypeChallenge_2018_11_PowerfulEnemyChallengeQuest  EventType = 204009
	EventTypeChallenge_2018_10_PowerfulEnemyChallengeQuest  EventType = 204008
	EventTypeChallenge_2018_09_PowerfulEnemyChallengeQuest  EventType = 204007
	EventTypeChallenge_2018_08_PowerfulEnemyChallengeQuest  EventType = 204006
	EventTypeChallenge_2018_07_PowerfulEnemyChallengeQuest  EventType = 204005
	EventTypeChallenge_2018_06_PowerfulEnemyChallengeQuest  EventType = 204004
	EventTypeChallenge_2018_05_PowerfulEnemyChallengeQuest  EventType = 204003
)

var ErrInvalidEventType = errors.New("invalid event quest type")

func NewEventType(value uint32) (EventType, error) {
	if value < uint32(EventTypeEventMushroomBaby) || value > uint32(EventTypeChallenge_2024_07_PowerfulEnemyChallengeQuest) {
		return EventType(0), ErrInvalidEventType
	}
	return EventType(value), nil
}

package value_event_quest

type EventTypeCategory uint8

const (
	EventTypeCategoryEvent     EventTypeCategory = 1
	EventTypeCategoryAuthor    EventTypeCategory = 2
	EventTypeCategoryDaily     EventTypeCategory = 3
	EventTypeCategoryMemorial  EventTypeCategory = 5
	EventTypeCategoryCraft     EventTypeCategory = 6
	EventTypeCategoryChallenge EventTypeCategory = 200
)

func NewEventTypeCategory(v uint8) (EventTypeCategory, error) {
	if v > uint8(EventTypeCategoryChallenge) {
		return EventTypeCategory(0), ErrInvalidEventType
	}
	return EventTypeCategory(v), nil
}

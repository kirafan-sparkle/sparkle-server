package value_ability_sphere

import (
	"fmt"
)

type AbilitySphereRecipeId uint

const (
	AbilitySphereRecipeIdMin AbilitySphereId = 401000
	AbilitySphereRecipeIdMax AbilitySphereId = 402240
)

func NewAbilitySphereRecipeId(v uint) (AbilitySphereRecipeId, error) {
	if v > uint(AbilitySphereRecipeIdMax) || v < uint(AbilitySphereRecipeIdMin) {
		return 0, fmt.Errorf("specified AbilitySphereRecipeId %d is out of range [%d, %d]", v, AbilitySphereRecipeIdMax, AbilitySphereRecipeIdMin)
	}
	return AbilitySphereRecipeId(v), nil
}

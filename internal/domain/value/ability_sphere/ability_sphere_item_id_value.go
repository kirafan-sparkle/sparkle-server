package value_ability_sphere

import (
	"fmt"

	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

type AbilitySphereItemId int32

const (
	AbilitySphereItemIdUnspecified AbilitySphereItemId = -1
	AbilitySphereItemIdInvalid     AbilitySphereItemId = 0
)

const (
	AbilitySphereItemIdMin AbilitySphereItemId = 40000
	AbilitySphereItemIdMax AbilitySphereItemId = 40224
)

const (
	AbilitySphereItemIdGradeMin = 0
	AbilitySphereItemIdGradeMax = 2
)
const (
	AbilitySphereItemIdGrade1 = iota
	AbilitySphereItemIdGrade2
	AbilitySphereItemIdGrade3
)

const (
	AbilitySphereItemIdSphereTypeMin = 0
	AbilitySphereItemIdSphereTypeMax = 24
)
const (
	AbilitySphereItemIdSphereTypeHealth             AbilitySphereType = iota // 体力
	AbilitySphereItemIdSphereTypePhysicalPower                               // 物理攻撃
	AbilitySphereItemIdSphereTypeMagicPower                                  // 魔法攻撃
	AbilitySphereItemIdSphereTypePhysicalDefense                             // 物理防御
	AbilitySphereItemIdSphereTypeMagicDefense                                // 魔法防御
	AbilitySphereItemIdSphereTypeFireResistance                              // 炎耐性
	AbilitySphereItemIdSphereTypeWindResistance                              // 風耐性
	AbilitySphereItemIdSphereTypeDirtResistance                              // 土耐性
	AbilitySphereItemIdSphereTypeWaterResistance                             // 水耐性
	AbilitySphereItemIdSphereTypeMoonResistance                              // 月耐性
	AbilitySphereItemIdSphereTypeSunResistance                               // 陽耐性
	AbilitySphereItemIdSphereTypeConfuseResistance                           // 混乱耐性
	AbilitySphereItemIdSphereTypeFreezeChanceUp                              // 混乱チャンスアップ
	AbilitySphereItemIdSphereTypeHungerChanceUp                              // はらぺこチャンスアップ
	AbilitySphereItemIdSphereTypeWeakChanceUp                                // よわきチャンスアップ
	AbilitySphereItemIdSphereTypeSleepChanceUp                               // 眠りチャンスアップ
	AbilitySphereItemIdSphereTypeStatusUpKeep                                // ステータスアップ持続
	AbilitySphereItemIdSphereTypeStatusDownKeep                              // ステータスダウン持続
	AbilitySphereItemIdSphereTypeCriticalDamageKeep                          // クリティカルダメージ持続
	AbilitySphereItemIdSphereTypeBaitStatusKeep                              // 狙われ状態持続
	AbilitySphereItemIdSphereTypeQuickDrawKeep                               // クイックドロー状態持続
	AbilitySphereItemIdSphereTypeRecoveryKeep                                // リカバリー状態持続
	AbilitySphereItemIdSphereTypeHealthHoldKeep                              // がまん持続
	AbilitySphereItemIdSphereTypeEffectDenyKeep                              // 状態異常無効持続
	AbilitySphereItemIdSphereTypeSpecialGaugeUp                              // とっておき上昇
)

func NewAbilitySphereItemId(itemId value_item.ItemId) (AbilitySphereItemId, error) {
	if itemId == value_item.ItemIdUnspecified {
		return AbilitySphereItemIdUnspecified, nil
	}

	if uint(itemId) > uint(AbilitySphereItemIdMax) || uint(itemId) < uint(AbilitySphereItemIdMin) {
		return 0, fmt.Errorf("specified AbilitySphereItemId %d is out of range [%d, %d]", itemId, AbilitySphereItemIdMin, AbilitySphereItemIdMax)
	}
	// 1st and 2nd digit must be 40
	// 3rd digit is the grade
	grade := int(itemId/100) % 10
	if grade > AbilitySphereItemIdGradeMax {
		return 0, fmt.Errorf(
			"specified AbilitySphereItemId %d's grade %d is out of range [%d, %d]",
			itemId, grade, AbilitySphereItemIdGradeMin, AbilitySphereItemIdGradeMax,
		)
	}
	// 4th and 5th digit is the sphere type
	sphereType := int(itemId) % 100
	if sphereType > AbilitySphereItemIdSphereTypeMax {
		return 0, fmt.Errorf(
			"specified AbilitySphereItemId %d's sphere type %d is out of range [%d, %d]",
			itemId, sphereType, AbilitySphereItemIdSphereTypeMin, AbilitySphereItemIdSphereTypeMax,
		)
	}
	return AbilitySphereItemId(itemId), nil
}

func (i AbilitySphereItemId) AsItem() value_item.ItemId {
	return value_item.ItemId(i)
}

package value_ability_sphere

import (
	"fmt"
)

type AbilitySphereType uint8

const (
	AbilitySphereTypeMin AbilitySphereType = 0
	AbilitySphereTypeMax AbilitySphereType = 5
)

const (
	AbilitySphereTypeHealth AbilitySphereType = iota
	AbilitySphereTypePhysicalPower
	AbilitySphereTypeMagicPower
	AbilitySphereTypePhysicalDefense
	AbilitySphereTypeMagicDefense
	AbilitySphereTypeSpecial
)

func NewAbilitySphereType(v uint8) (AbilitySphereType, error) {
	if v > uint8(AbilitySphereTypeMax) || v < uint8(AbilitySphereTypeMin) {
		return 0, fmt.Errorf("specified AbilitySphereType %d is out of range [%d, %d]", v, AbilitySphereTypeMin, AbilitySphereTypeMax)
	}
	return AbilitySphereType(v), nil
}

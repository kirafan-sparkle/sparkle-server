package value_ability_sphere

import (
	"fmt"
)

type AbilitySphereId uint

const (
	AbilitySphereIdMin AbilitySphereId = 400000
	AbilitySphereIdMax AbilitySphereId = 402240
)

func NewAbilitySphereId(v uint) (AbilitySphereId, error) {
	if v > uint(AbilitySphereIdMax) || v < uint(AbilitySphereIdMin) {
		return 0, fmt.Errorf("specified AbilitySphereId %d is out of range [%d, %d]", v, AbilitySphereIdMin, AbilitySphereIdMax)
	}
	return AbilitySphereId(v), nil
}

package value_town_facility

import "errors"

type ItemTableTownFacilityCategory uint8

const (
	ItemTableTownFacilityCategoryNone ItemTableTownFacilityCategory = iota
	ItemTableTownFacilityCategoryCoin
	ItemTableTownFacilityCategoryItem
	ItemTableTownFacilityCategoryKirara
	ItemTableTownFacilityCategoryStamina
	ItemTableTownFacilityCategoryFriendship
	ItemTableTownFacilityCategoryLimitedGem
)

var ErrInvalidItemTableTownFacilityCategory = errors.New("invalid item table town facility category")

func NewItemTableTownFacilityCategory(v uint8) (ItemTableTownFacilityCategory, error) {
	if v > uint8(ItemTableTownFacilityCategoryItem) {
		return ItemTableTownFacilityCategoryNone, ErrInvalidItemTableTownFacilityCategory
	}
	return ItemTableTownFacilityCategory(v), nil
}

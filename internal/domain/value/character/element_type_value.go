package value_character

import "errors"

type ElementType int8

const (
	ElementTypeNone ElementType = iota - 1
	ElementTypeFire
	ElementTypeWater
	ElementTypeEarth
	ElementTypeWind
	ElementTypeMoon
	ElementTypeSun
)

var ErrInvalidElementType = errors.New("invalid ElementType")

func NewElementType(v uint8) (ElementType, error) {
	if v > uint8(ElementTypeSun) {
		return ElementTypeNone, ErrInvalidElementType
	}
	return ElementType(v), nil
}

package value_character

import "fmt"

type CharacterLevel uint8

func (c *CharacterLevel) IsMaximum() bool {
	return *c == CharacterLevelMax
}

const (
	CharacterLevelMin CharacterLevel = 1
	CharacterLevelMax CharacterLevel = 100
)

func NewCharacterLevel(value uint8) (*CharacterLevel, error) {
	if value < uint8(CharacterLevelMin) {
		return nil, fmt.Errorf("specified character level %d is less than the minimum allowed level %d", value, CharacterLevelMin)
	}
	if value > uint8(CharacterLevelMax) {
		return nil, fmt.Errorf("specified character level %d is greater than the maximum allowed level %d", value, CharacterLevelMax)
	}
	lvl := CharacterLevel(value)
	return &lvl, nil
}

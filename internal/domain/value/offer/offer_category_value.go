package value_offer

import "errors"

type OfferCategory uint8

const (
	OfferCategoryCreaCraft OfferCategory = iota + 1
	OfferCategoryCreaCraftBonus
	OfferCategoryCreamateCommunity
)

var ErrInvalidOfferCategory = errors.New("invalid OfferCategory")

func NewOfferCategory(v uint8) (OfferCategory, error) {
	if v < uint8(OfferCategoryCreaCraft) || v > uint8(OfferCategoryCreamateCommunity) {
		return OfferCategoryCreaCraft, ErrInvalidOfferCategory
	}
	return OfferCategory(v), nil
}

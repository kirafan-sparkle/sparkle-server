package value_mission

import "errors"

type MissionRewardType uint8

const (
	MissionRewardTypeNone MissionRewardType = iota
	MissionRewardTypeGold
	MissionRewardTypeItem
	MissionRewardTypeKiraraPoint
	MissionRewardTypeStamina
	MissionRewardTypeFriendship
	MissionRewardTypeGem
)

var ErrInvalidMissionRewardType = errors.New("invalid MissionRewardType")

func NewMissionRewardType(v uint8) (MissionRewardType, error) {
	if v > uint8(MissionRewardTypeGem) {
		return MissionRewardTypeNone, ErrInvalidMissionRewardType
	}
	return MissionRewardType(v), nil
}

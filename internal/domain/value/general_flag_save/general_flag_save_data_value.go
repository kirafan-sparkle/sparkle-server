package value_general_flag_save

import "errors"

type GeneralFlagSaveData uint8

const (
	GeneralFlagSaveDataPart1Invalid GeneralFlagSaveData = iota
	GeneralFlagSaveDataPart1Chapter0
	GeneralFlagSaveDataPart1Chapter1
	GeneralFlagSaveDataPart1Chapter2
	GeneralFlagSaveDataPart1Chapter3
	GeneralFlagSaveDataPart1Chapter4
	GeneralFlagSaveDataPart1Chapter5
	GeneralFlagSaveDataPart1Chapter6
	GeneralFlagSaveDataPart1Chapter7
	GeneralFlagSaveDataPart1Chapter8
	GeneralFlagSaveDataPart1ChapterSideStory
	// NOTE: ChapterNo 20 was unused for unknown reason
	GeneralFlagSaveDataPart2Invalid GeneralFlagSaveData = iota + 9
	GeneralFlagSaveDataPart2Chapter0
	GeneralFlagSaveDataPart2Chapter1
	GeneralFlagSaveDataPart2Chapter2
	GeneralFlagSaveDataPart2Chapter3
	GeneralFlagSaveDataPart2Chapter4
	GeneralFlagSaveDataPart2Chapter5
	GeneralFlagSaveDataPart2Chapter6
	GeneralFlagSaveDataPart2Chapter7
	GeneralFlagSaveDataPart2Chapter8
	GeneralFlagSaveDataPart2Chapter9
)

var ErrInvalidGeneralFlagSaveData = errors.New("invalid general flag save data")

func NewGeneralFlagSaveData(t uint8) (GeneralFlagSaveData, error) {
	if t == uint8(GeneralFlagSaveDataPart1Invalid) ||
		t == uint8(GeneralFlagSaveDataPart2Invalid) ||
		t > uint8(GeneralFlagSaveDataPart2Chapter9) {
		return 0, ErrInvalidGeneralFlagSaveData
	}
	return GeneralFlagSaveData(t), nil
}

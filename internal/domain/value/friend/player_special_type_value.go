package value_friend

type PlayerSpecialType uint8

const (
	PlayerSpecialTypeDefault PlayerSpecialType = iota
	PlayerSpecialTypeItemCheat
	PlayerSpecialTypeCharacterCheat
	PlayerSpecialTypeCoinCheat
	PlayerSpecialTypeGemCheat
	PlayerSpecialTypeFriendCheat
	PlayerSpecialTypeKiraraCheat
)

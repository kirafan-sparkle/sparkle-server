package value_friend

type FriendState int8

const (
	FriendStateGuest          FriendState = 0
	FriendStateWaitingApprove FriendState = 1
	FriendStateFriend         FriendState = 2
)

package value_friend

import (
	"errors"

	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

type PlayerId uint

func NewPlayerId(v value_user.UserId, specialType PlayerSpecialType) (*PlayerId, error) {
	if v > 100000000 {
		return nil, errors.New("to prevent collision, playerId must be less than 100000000")
	}
	switch specialType {
	case PlayerSpecialTypeItemCheat:
		return calc.ToPtr(PlayerId(uint(v) + 100000000)), nil
	case PlayerSpecialTypeCharacterCheat:
		return calc.ToPtr(PlayerId(uint(v) + 200000000)), nil
	case PlayerSpecialTypeCoinCheat:
		return calc.ToPtr(PlayerId(uint(v) + 300000000)), nil
	case PlayerSpecialTypeGemCheat:
		return calc.ToPtr(PlayerId(uint(v) + 400000000)), nil
	case PlayerSpecialTypeFriendCheat:
		return calc.ToPtr(PlayerId(uint(v) + 500000000)), nil
	case PlayerSpecialTypeKiraraCheat:
		return calc.ToPtr(PlayerId(uint(v) + 600000000)), nil
	}
	return calc.ToPtr(PlayerId(uint(v))), nil
}

func NewPlayerIdForce(v uint) PlayerId {
	return PlayerId(v)
}

func (v PlayerId) SpecialType() PlayerSpecialType {
	if v < 100000000 {
		return PlayerSpecialTypeDefault
	}
	if v < 200000000 {
		return PlayerSpecialTypeItemCheat
	}
	if v < 300000000 {
		return PlayerSpecialTypeCharacterCheat
	}
	if v < 400000000 {
		return PlayerSpecialTypeCoinCheat
	}
	if v < 500000000 {
		return PlayerSpecialTypeGemCheat
	}
	if v < 600000000 {
		return PlayerSpecialTypeFriendCheat
	}
	if v < 700000000 {
		return PlayerSpecialTypeKiraraCheat
	}
	return PlayerSpecialTypeDefault
}

func (v PlayerId) IsGemCheat() bool {
	return v.SpecialType() == PlayerSpecialTypeGemCheat
}

func (v PlayerId) IsItemCheat() bool {
	return v.SpecialType() == PlayerSpecialTypeItemCheat
}

func (v PlayerId) IsCharacterCheat() bool {
	return v.SpecialType() == PlayerSpecialTypeCharacterCheat
}

func (v PlayerId) IsCoinCheat() bool {
	return v.SpecialType() == PlayerSpecialTypeCoinCheat
}

func (v PlayerId) IsFriendCheat() bool {
	return v.SpecialType() == PlayerSpecialTypeFriendCheat
}

func (v PlayerId) IsKiraraCheat() bool {
	return v.SpecialType() == PlayerSpecialTypeKiraraCheat
}

func (v PlayerId) IsDefault() bool {
	return v.SpecialType() == PlayerSpecialTypeDefault
}

func (v PlayerId) Value() uint {
	switch v.SpecialType() {
	case PlayerSpecialTypeItemCheat:
		return uint(v - 100000000)
	case PlayerSpecialTypeCharacterCheat:
		return uint(v - 200000000)
	case PlayerSpecialTypeCoinCheat:
		return uint(v - 300000000)
	case PlayerSpecialTypeGemCheat:
		return uint(v - 400000000)
	case PlayerSpecialTypeFriendCheat:
		return uint(v - 500000000)
	case PlayerSpecialTypeKiraraCheat:
		return uint(v - 600000000)
	}
	return uint(v)
}

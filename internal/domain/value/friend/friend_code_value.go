package value_friend

import (
	"fmt"
	"strconv"
	"strings"
)

// フレンドコードの改変 チートコード型
type CheatCodeType string

func (c *CheatCodeType) value() string {
	return string(*c)
}

const (
	CheatCodeTypeItem      = "ITM"
	CheatCodeTypeCharacter = "CHR"
	CheatCodeTypeGem       = "GEM"
	CheatCodeTypeCoin      = "CIN"
	CheatCodeTypeKrr       = "KRR"
	CheatCodeTypeResp      = "RESP"
	CheatCodeTypeNpc       = "NPC"
	CheatCodeTypeCrea      = "CREA"
	CheatCodeTypeNone      = ""
)

// フレンドコード (兼チートコード)
type FriendOrCheatCode string

const (
	friendCodeLength = 10
)

func NewFriendOrCheatCode(v string) (FriendOrCheatCode, error) {
	if len(v) != friendCodeLength {
		return "", fmt.Errorf("invalid friend code length, it must be %d characters long", friendCodeLength)
	}
	return FriendOrCheatCode(v), nil
}

func (c *FriendOrCheatCode) value() string {
	return string(*c)
}

var supportedCheatTypes = []CheatCodeType{
	CheatCodeTypeItem,
	CheatCodeTypeCharacter,
	CheatCodeTypeGem,
	CheatCodeTypeCoin,
	CheatCodeTypeKrr,
	CheatCodeTypeResp,
	CheatCodeTypeNpc,
	CheatCodeTypeCrea,
}

func (c *FriendOrCheatCode) GetCheatType() CheatCodeType {
	for _, t := range supportedCheatTypes {
		if strings.HasPrefix(c.value(), t.value()) {
			return t
		}
	}
	return CheatCodeTypeNone
}

func (c *FriendOrCheatCode) IsCheatCode() bool {
	return c.GetCheatType() != CheatCodeTypeNone
}

func (c *FriendOrCheatCode) GetCheatParam() (uint64, error) {
	cheatType := c.GetCheatType()
	switch cheatType {
	case CheatCodeTypeNone:
		return 0, nil
	case CheatCodeTypeCrea:
		return 0, nil
	default:
		cheatParam := strings.Split(c.value(), cheatType.value())[1]
		value, err := strconv.Atoi(cheatParam)
		return uint64(value), err
	}
}

func (c *FriendOrCheatCode) IsSameCode(other FriendOrCheatCode) bool {
	return c.value() == other.value()
}

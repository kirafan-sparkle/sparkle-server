package value_friend

type FriendDirection uint8

const (
	FriendDirectionNone FriendDirection = iota
	FriendDirectionSelfToOther
	FriendDirectionOtherToSelf
)

package value_friend

import (
	"errors"

	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

type ManagedFriendId uint32

func NewManagedFriendId(v uint32) (*ManagedFriendId, error) {
	if v == 0 {
		return nil, errors.New("invalid managed friend id")
	}
	managedFriendId := ManagedFriendId(v)
	return &managedFriendId, nil
}

func NewDummyManagedFriendId() *ManagedFriendId {
	id := ManagedFriendId(calc.GetSparkleRandomNumber())
	return &id
}

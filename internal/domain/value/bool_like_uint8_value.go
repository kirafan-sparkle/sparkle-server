package value

import "errors"

// Since kirafan API uses 0 and 1 as boolean in many cases
type BoolLikeUInt8 uint8

const (
	BoolLikeUIntFalse BoolLikeUInt8 = iota
	BoolLikeUIntTrue
)

var ErrInvalidBoolValue = errors.New("invalid bool value")

func NewBoolLikeUint8(value uint8) (BoolLikeUInt8, error) {
	if value > uint8(BoolLikeUIntTrue) {
		return 0, ErrInvalidBoolValue
	}
	return BoolLikeUInt8(value), nil
}

func (b BoolLikeUInt8) IsTrue() bool {
	return b == BoolLikeUIntTrue
}

func (b BoolLikeUInt8) IsFalse() bool {
	return b == BoolLikeUIntFalse
}

package value_shared

import (
	"encoding/json"
)

type JsonString string

func NewJsonString(obj interface{}) (JsonString, error) {
	parsedJson, err := json.Marshal(obj)
	if err != nil {
		return "", err
	}
	data := string(parsedJson)
	return JsonString(data), nil
}

func (j JsonString) ParseToModel(dst interface{}) error {
	if err := json.Unmarshal([]byte(j), dst); err != nil {
		return err
	}
	return nil
}

func (j JsonString) ParseToString() string {
	return string(j)
}

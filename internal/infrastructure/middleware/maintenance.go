package middleware

import (
	"encoding/json"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/flag"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx/logger"
	"gitlab.com/kirafan/sparkle/server/pkg/encrypt"
	pkg_flag "gitlab.com/kirafan/sparkle/server/pkg/flag"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func NewMaintenanceMiddleware(cipher *encrypt.SparkleCipher) func(next http.HandlerFunc) http.HandlerFunc {
	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			requestCtx := r.Context()
			_, span := observability.Tracer.StartMiddlewareSpan(requestCtx, "MaintenanceMiddleware")
			// Revalidate maintenance flag every CACHE DURATION time passed
			isMaintenance, _ := flag.FlagClient.GetBooleanFlagValue(pkg_flag.IsMaintenance, false)
			if isMaintenance {
				logger := logger.FromContext(requestCtx)
				logger.Ctx(requestCtx).Warn("server is in maintenance mode")
				body := response.NewMaintenanceResponse()
				bufw := &responseBuffer{ResponseWriter: w}
				json.NewEncoder(bufw).Encode(body)
				rawResp := bufw.Buffer().Bytes()
				w.Header().Set("kNNHMre7jsDEHFaURYBP", "0")
				w.Header().Set("Content-Type", "application/json; charset=UTF-8")
				w.Header().Set("X-Star-CC", cipher.GetStarCCByBytes(rawResp, body.ServerTime))
				w.Write(rawResp)
				span.End()
				return
			}
			span.End()
			next(w, r)
		}
	}
}

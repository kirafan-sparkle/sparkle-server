package middleware

import (
	"io"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"gitlab.com/kirafan/sparkle/server/internal/interfaces"
	"gitlab.com/kirafan/sparkle/server/pkg/encrypt"
)

/** Mock handler that return success response if session middleware works well */
func getEncryptionMockHandler() http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		body := map[string]interface{}{
			"newAchievementCount": 1,
			"resultCode":          0,
			"resultMessage":       "",
			"serverTime":          "2023-01-29T23:09:44",
			"serverVersion":       2211181,
		}
		success := 200
		interfaces.EncodeJSONResponse(body, &success, w)
	}
	return http.HandlerFunc(fn)
}

func TestEncryptionMiddleware(t *testing.T) {
	// Initialize required instances to run
	key := encrypt.LoadKey()
	padding := encrypt.LoadPad()
	ignoredRoutes := GetAuthIgnoredRoutes()
	// Test cases
	tests := []struct {
		name string
		k    func(next http.HandlerFunc) http.HandlerFunc
		path string
		want []byte
	}{
		{
			name: "get with valid session return ok",
			k:    NewAppEncryptionMiddleware(&ignoredRoutes, &key, &padding),
			path: "/api/player/push_token/set",
			want: []byte{24, 254, 125, 74, 157, 255, 164, 151, 49, 167, 239, 224, 125, 183, 116, 71, 178, 108, 214, 219, 20, 246, 85, 61, 242, 15, 86, 33, 176, 70, 201, 95, 237, 37, 187, 124, 6, 3, 118, 174, 252, 98, 19, 152, 111, 76, 184, 130, 66, 46, 100, 78, 195, 80, 210, 124, 153, 194, 4, 49, 69, 106, 57, 35, 222, 114, 225, 148, 162, 254, 116, 177, 108, 17, 131, 191, 60, 147, 179, 123, 75, 77, 127, 136, 121, 150, 83, 182, 99, 132, 237, 204, 134, 86, 102, 240, 188, 250, 79, 29, 92, 66, 181, 182, 111, 197, 210, 207, 22, 246, 215, 157, 101, 172, 243, 175, 81, 21, 56, 236, 198, 204, 59, 87, 144, 205, 72, 109},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Create test server
			ts := httptest.NewServer(tt.k(getEncryptionMockHandler()))
			t.Cleanup(func() {
				ts.Close()
			})
			// Create a dummy request
			req, err := http.NewRequest(
				"POST",
				ts.URL+tt.path,
				nil,
			)
			if err != nil {
				t.Error(err)
			}
			req.Header.Set("Content-Type", "application/json; charset=UTF-8")
			// Send a request to test server
			client := &http.Client{}
			resp, err := client.Do(req)
			if err != nil {
				t.Error(err)
			}
			t.Cleanup(func() {
				err := resp.Body.Close()
				if err != nil {
					t.Error(err)
				}
			})
			// Read response
			buf, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Error(err)
			}
			if !reflect.DeepEqual(buf, tt.want) {
				t.Errorf("\ngot %v\nwant %v\n", buf, tt.want)
			}
		})
	}
}

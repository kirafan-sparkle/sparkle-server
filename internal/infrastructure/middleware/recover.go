package middleware

import (
	"encoding/json"
	"net/http"
	"runtime/debug"

	"gitlab.com/kirafan/sparkle/server/pkg/ctx/logger"
	"gitlab.com/kirafan/sparkle/server/pkg/encrypt"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
	"go.uber.org/zap"
)

func NewRecoverMiddleware(cipher *encrypt.SparkleCipher) func(next http.HandlerFunc) http.HandlerFunc {
	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				if err := recover(); err != nil {
					requestCtx := r.Context()
					logger := logger.FromContext(requestCtx)
					logger.Ctx(requestCtx).Error("panic recovered", zap.Any("err", err), zap.String("stacktrace", string(debug.Stack())))
					body := response.NewServerExplodedResponse()
					bufw := &responseBuffer{ResponseWriter: w}
					json.NewEncoder(bufw).Encode(body)
					rawResp := bufw.Buffer().Bytes()

					w.Header().Set("kNNHMre7jsDEHFaURYBP", "0")
					w.Header().Set("Content-Type", "application/json; charset=UTF-8")
					w.Header().Set("X-Star-CC", cipher.GetStarCCByBytes(rawResp, body.ServerTime))
					w.Write(rawResp)
				}
			}()
			next(w, r)
		}
	}
}

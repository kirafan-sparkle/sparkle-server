package middleware

import (
	"fmt"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	gen "gitlab.com/kirafan/sparkle/server/internal/interfaces"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx/user_id"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
	"go.opentelemetry.io/otel/attribute"
)

func NewAppSessionMiddleware(sessionIgnoreRoutes []string, sessionService repository.SessionRepository) func(next http.HandlerFunc) http.HandlerFunc {
	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			requestCtx := r.Context()
			middlewareCtx, span := observability.Tracer.StartMiddlewareSpan(requestCtx, "SessionMiddleware")
			if !calc.Contains(sessionIgnoreRoutes, r.URL.Path) {
				session := r.Header.Get("nVJuf6NNHtyez3xTRKix")
				userId, err := sessionService.Validate(middlewareCtx, session)
				if err != nil {
					span.RecordError(err)
					resp := gen.Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED))
					gen.EncodeJSONResponse(resp.Body, &resp.Code, w)
					return
				}
				span.SetAttributes(attribute.String("user_id", fmt.Sprint(userId)))
				span.End()

				ctx := user_id.WithContext(requestCtx, userId)
				r = r.WithContext(ctx)
			} else {
				span.SetAttributes(attribute.String("user_id", fmt.Sprint(-1)))
				span.End()
			}
			next(w, r)
		}
	}
}

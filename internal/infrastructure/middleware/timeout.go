package middleware

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/kirafan/sparkle/server/pkg/ctx/logger"
	"gitlab.com/kirafan/sparkle/server/pkg/encrypt"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func NewTimeoutMiddleware(cipher *encrypt.SparkleCipher) func(next http.HandlerFunc) http.HandlerFunc {
	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			requestCtx := r.Context()
			timeoutCtx, cancel := context.WithTimeout(requestCtx, 15*time.Second)
			defer cancel()

			processDone := make(chan bool)
			go func() {
				next(w, r)
				processDone <- true
			}()
			select {
			case <-timeoutCtx.Done():
				err := timeoutCtx.Err()
				if err == context.DeadlineExceeded {
					logger := logger.FromContext(requestCtx)
					logger.Ctx(requestCtx).Error("process timeout")
					body := response.NewServerTimeoutResponse()
					bufw := &responseBuffer{ResponseWriter: w}
					json.NewEncoder(bufw).Encode(body)
					rawResp := bufw.Buffer().Bytes()
					w.Header().Set("kNNHMre7jsDEHFaURYBP", "0")
					w.Header().Set("Content-Type", "application/json; charset=UTF-8")
					w.Header().Set("X-Star-CC", cipher.GetStarCCByBytes(rawResp, body.ServerTime))
					w.Write(rawResp)
				}
			case <-processDone:
			}
		}
	}
}

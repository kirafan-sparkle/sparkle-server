package middleware

import (
	"net/http"

	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx/logger"
)

func NewLoggerMiddleware(otelZap *otelzap.Logger) func(next http.HandlerFunc) http.HandlerFunc {
	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			r = r.WithContext(logger.WithContext(r.Context(), otelZap))
			next(w, r)
		}
	}
}

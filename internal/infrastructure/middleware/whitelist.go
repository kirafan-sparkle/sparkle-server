package middleware

import (
	"encoding/json"
	"net"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/encrypt"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

func NewWhitelistMiddleware(allowedRanges *env.IpWhitelistRanges, cipher *encrypt.SparkleCipher) func(next http.HandlerFunc) http.HandlerFunc {
	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			if len(*allowedRanges) == 0 {
				next(w, r)
				return
			}

			requestCtx := r.Context()
			_, span := observability.Tracer.StartMiddlewareSpan(requestCtx, "WhitelistMiddleware")
			// Get client IP address
			ip, _, _ := net.SplitHostPort(r.RemoteAddr)
			clientIP := net.ParseIP(ip)
			// Validate IP address
			allowed := false
			for _, allowedRange := range *allowedRanges {
				if allowedRange.Contains(clientIP) {
					allowed = true
					break
				}
			}
			if !allowed {
				span.AddEvent("IP address is not allowed", trace.WithAttributes(attribute.String("ip", clientIP.String())))
				body := response.NewNotAllowedIpResponse()
				bufw := &responseBuffer{ResponseWriter: w}
				json.NewEncoder(bufw).Encode(body)
				rawResp := bufw.Buffer().Bytes()
				w.Header().Set("kNNHMre7jsDEHFaURYBP", "0")
				w.Header().Set("Content-Type", "application/json; charset=UTF-8")
				w.Header().Set("X-Star-CC", cipher.GetStarCCByBytes(rawResp, body.ServerTime))
				w.Write(rawResp)
				span.End()
				return
			}
			span.End()
			next(w, r)
		}
	}
}

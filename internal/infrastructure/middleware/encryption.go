package middleware

import (
	"bytes"
	"encoding/json"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx/logger"
	"gitlab.com/kirafan/sparkle/server/pkg/encrypt"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
	"go.uber.org/zap"
)

type responseBuffer struct {
	http.ResponseWriter
	buf *bytes.Buffer
}

func (w *responseBuffer) Write(data []byte) (int, error) {
	if w.buf == nil {
		w.buf = new(bytes.Buffer)
	}
	n, err := w.buf.Write(data)
	if err != nil {
		return n, err
	}
	return 0, nil
}

func (w *responseBuffer) WriteHeader(code int) {
	// DO NOT DELETE THIS EMPTY FUNCTION, OTHERWISE THE X-STAR-CC HEADER WILL NOT BE SET
}

func (w *responseBuffer) Buffer() *bytes.Buffer {
	return w.buf
}

func NewAppEncryptionMiddleware(ignoreRoutes *[]string, cipher *encrypt.SparkleCipher) func(next http.HandlerFunc) http.HandlerFunc {
	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			requestCtx := r.Context()
			w.Header().Set("Server", "Sparkle")
			bufw := &responseBuffer{ResponseWriter: w}
			next(bufw, r)
			_, span := observability.Tracer.StartMiddlewareSpan(requestCtx, "EncryptionMiddleware")
			if r.Method == "OPTIONS" {
				return
			}
			rawResp := bufw.Buffer().Bytes()

			var timeReader response.BaseResponse
			// Handle minimal interface layer error
			if err := json.Unmarshal(rawResp, &timeReader); err != nil {
				body := response.NewInvalidRequestResponse(string(rawResp))
				bufw = &responseBuffer{ResponseWriter: w}
				json.NewEncoder(bufw).Encode(body)
				rawResp = bufw.Buffer().Bytes()
				json.Unmarshal(rawResp, &timeReader)
			}
			var resp []byte
			if !calc.Contains(*ignoreRoutes, r.URL.Path) {
				resp = cipher.EncryptAsResponseByBytes(r.URL.Path, rawResp)
				w.Header().Set("kNNHMre7jsDEHFaURYBP", "1")
			} else {
				resp = rawResp
				w.Header().Set("kNNHMre7jsDEHFaURYBP", "0")
			}
			w.Header().Set("X-Star-CC", cipher.GetStarCCByBytes(rawResp, timeReader.ServerTime))
			w.Write(resp)

			logger := logger.FromContext(requestCtx)
			logger.Ctx(requestCtx).Debug(
				"end request handling",
				zap.String("path", r.URL.Path),
				zap.String("query", r.URL.RawQuery),
				zap.String("body", string(rawResp)),
			)
			defer logger.Sync()
			span.End()
		}
	}
}

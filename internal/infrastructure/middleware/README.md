## infrastructure/middleware

- This folder contains middlewares of mux.Router.
- For example
  - CORS
  - logging
  - authorization

main <-> **middleware** <-> router(interfaces) <-> usecase <-> repository <-> model(gorm) <-> db(persistence)

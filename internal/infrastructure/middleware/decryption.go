package middleware

import (
	"bytes"
	"io"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx/logger"
	"gitlab.com/kirafan/sparkle/server/pkg/encrypt"
	"go.uber.org/zap"
)

func NewAppDecryptionMiddleware(ignoreRoutes *[]string, cipher *encrypt.SparkleCipher) func(next http.HandlerFunc) http.HandlerFunc {
	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			requestCtx := r.Context()
			_, span := observability.Tracer.StartMiddlewareSpan(requestCtx, "DecryptionMiddleware")

			body, err := io.ReadAll(r.Body)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			if r.Header.Get("PznxhHrYEDRrfARyBgRp") == "1" {
				decryptedBody, err := cipher.DecryptAsRequestBytes(r.URL.Path, string(body))
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				body = decryptedBody
				r.Body = io.NopCloser(bytes.NewReader(decryptedBody))
				r.ContentLength = int64(len(decryptedBody))
			}
			r.Body = io.NopCloser(bytes.NewReader(body))
			r.ContentLength = int64(len(body))

			logger := logger.FromContext(requestCtx)
			switch logger.Level() {
			case zap.DebugLevel:
				logger.Ctx(requestCtx).Debug(
					"start request handling",
					zap.String("path", r.URL.Path),
					zap.String("query", r.URL.RawQuery),
					zap.String("body", string(body)),
				)
			default:
				logger.Ctx(requestCtx).Info(
					"handling request",
					zap.String("path", r.URL.Path),
				)
			}
			defer logger.Sync()
			span.End()
			next(w, r)
		}
	}
}

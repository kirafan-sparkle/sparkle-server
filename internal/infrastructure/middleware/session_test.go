package middleware

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/migrate"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/seed"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
	"gitlab.com/kirafan/sparkle/server/internal/interfaces"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

/** Mock handler that return success response if session middleware works well */
func getMockHandler() http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		res := interfaces.Response(http.StatusOK, response.NewSuccessResponse())
		interfaces.EncodeJSONResponse(res.Body, &res.Code, w)
	}
	return http.HandlerFunc(fn)
}

type fakeRequest struct {
	PlayerName int `json:"playerName"`
	PlayerPass int `json:"playerPass"`
}

func TestSessionMiddleware(t *testing.T) {
	// Initialize required instances to run
	logger := observability.InitLogger(observability.LogTypeDebug)
	dbConf := database.GetConfig()
	db := database.InitDatabase(dbConf, logger)
	migrate.AutoMigrate(db)
	seed.AutoSeed(db)
	sessionService := persistence.NewSessionRepositoryImpl(db)
	ignoreRoutes := GetAuthIgnoredRoutes()
	sessionMiddleware := NewAppSessionMiddleware(ignoreRoutes, sessionService)
	// Test cases
	tests := []struct {
		name    string
		path    string
		session string
		want    response.BaseResponse
	}{
		{"get with valid session return ok", "/api/player/get_all", "DUMMY_SESSION_ID", response.NewSuccessResponse()},
		{"get with empty session on specific endpoint return ok", "/app/version/get", "", response.NewSuccessResponse()},
		{"get with invalid session return 203", "/api/player/get_all", "INVALID_SESSION_ID", response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)},
		{"get with empty session return 203", "/api/player/get_all", "", response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Create test server
			ts := httptest.NewServer(sessionMiddleware(getMockHandler()))
			t.Cleanup(func() {
				ts.Close()
			})
			// Create dummy request body
			p := new(fakeRequest)
			jsonStr, err := json.Marshal(p)
			if err != nil {
				t.Error(err)
			}
			// Create a dummy request
			req, err := http.NewRequest(
				"GET",
				ts.URL+tt.path,
				bytes.NewBuffer([]byte(jsonStr)),
			)
			if err != nil {
				t.Error(err)
			}
			req.Header.Set("Content-Type", "application/json")
			if tt.session != "" {
				req.Header.Set("nVJuf6NNHtyez3xTRKix", tt.session)
			}
			// Send a request to test server
			client := &http.Client{}
			resp, err := client.Do(req)
			if err != nil {
				t.Error(err)
			}
			t.Cleanup(func() {
				err := resp.Body.Close()
				if err != nil {
					t.Error(err)
				}
			})
			// Read response
			buf, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Error(err)
			}
			// Parse response body
			var got response.BaseResponse
			if json.Unmarshal(buf, &got) != nil {
				t.Error(err)
			}
			if want := tt.want; got != want {
				t.Errorf("\ngot %v\nwant %v\n", got, want)
			}
		})
	}
}

package middleware

import (
	"net/http"

	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/pkg/encrypt"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
)

type middleware func(next http.HandlerFunc) http.HandlerFunc

type Middlewares []middleware

func GetAuthIgnoredRoutes() []string {
	return []string{
		"/api/app/version/get",
		"/api/player/import/offline",
		"/api/player/move/set",
		"/api/player/signup",
		"/api/player/login",
		"/api/player/reset",
	}
}

func GetMiddlewares(
	logger *otelzap.Logger,
	whitelistRanges *env.IpWhitelistRanges,
	key *env.EncryptKey,
	pad *env.EncryptPad,
	ss repository.SessionRepository,
) Middlewares {
	ignoreRoutes := GetAuthIgnoredRoutes()

	if key == nil || pad == nil || *key == "" || *pad == "" {
		panic("no encryption mode is not supported yet")
	}

	cipher := encrypt.NewSparkleCipher(encrypt.Key(*key), encrypt.Pad(*pad))
	ms := []middleware{
		NewCorsMiddleware(),
		NewLoggerMiddleware(logger),
		NewTimeoutMiddleware(cipher),
		NewWhitelistMiddleware(whitelistRanges, cipher),
		// NewMaintenanceMiddleware(cipher),
		NewRecoverMiddleware(cipher),
		NewAppDecryptionMiddleware(&ignoreRoutes, cipher),
		NewAppSessionMiddleware(ignoreRoutes, ss),
		NewAppEncryptionMiddleware(&ignoreRoutes, cipher),
	}
	return ms
}

func (m Middlewares) Then(h http.HandlerFunc) http.HandlerFunc {
	for i := range m {
		h = m[len(m)-1-i](h)
	}
	return h
}

package persistence

import (
	"context"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type expTableCharacterRepositoryImpl struct {
	Conn *gorm.DB
}

func NewExpTableCharacterRepositoryImpl(conn *gorm.DB) repository.ExpTableCharacterRepository {
	return &expTableCharacterRepositoryImpl{Conn: conn}
}

func (rp *expTableCharacterRepositoryImpl) FindExpTableCharacters(ctx context.Context, query *model_exp_table.ExpTableCharacter, criteria map[string]interface{}) ([]*model_exp_table.ExpTableCharacter, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindExpTableCharacters")
	defer span.End()
	var datas []*model_exp_table.ExpTableCharacter
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *expTableCharacterRepositoryImpl) FindExpTableCharacter(ctx context.Context, query *model_exp_table.ExpTableCharacter, criteria map[string]interface{}) (*model_exp_table.ExpTableCharacter, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindExpTableCharacter")
	defer span.End()
	var data *model_exp_table.ExpTableCharacter
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

func (rp *expTableCharacterRepositoryImpl) GetRequiredCoinsForUpgrade(ctx context.Context, currentLevel value_character.CharacterLevel) (uint16, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetRequiredCoinsForUpgrade")
	defer span.End()
	var data *model_exp_table.ExpTableCharacter
	if result := rp.Conn.WithContext(ctx).Where("level = ?", currentLevel).First(&data); result.Error != nil {
		return 0, result.Error
	}
	return data.RequiredCoinPerItem, nil
}

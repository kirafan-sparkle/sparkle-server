package persistence

import (
	"context"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"

	"gorm.io/gorm"
)

type questRepositoryImpl struct {
	Conn *gorm.DB
}

// NewQuestRepositoryImpl quest repositoryのコンストラクタ
func NewQuestRepositoryImpl(conn *gorm.DB) repository.QuestRepository {
	return &questRepositoryImpl{Conn: conn}
}

func (qr *questRepositoryImpl) FindQuests(ctx context.Context, query *model_quest.Quest, criteria map[string]interface{}, associations []string) ([]*model_quest.Quest, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindQuests")
	defer span.End()
	var quests []*model_quest.Quest
	var result *gorm.DB
	chain := qr.Conn.WithContext(ctx)
	for _, association := range associations {
		chain = chain.Preload(association)
	}
	if query != nil {
		result = chain.Where(query).Find(&quests)
	} else {
		result = chain.Where(criteria).Find(&quests)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return quests, nil
}

func (qr *questRepositoryImpl) FindQuest(ctx context.Context, query *model_quest.Quest, criteria map[string]interface{}, associations []string) (*model_quest.Quest, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindQuest")
	defer span.End()
	var quest *model_quest.Quest
	var result *gorm.DB
	chain := qr.Conn.WithContext(ctx)
	for _, association := range associations {
		chain = chain.Preload(association)
	}
	if query != nil {
		result = chain.Where(&query).First(&quest)
	} else {
		result = chain.Where(criteria).First(&quest)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return quest, nil
}

func (qr *questRepositoryImpl) GetQuestCategory(ctx context.Context, questIdOrEventQuestId uint) (*value_quest.QuestCategoryType, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetQuestCategory")
	defer span.End()
	// FIXME: Seems this works for current database, but not sure this is correct or not
	// Please find out how to decide event quest or not, with uses client request
	if questIdOrEventQuestId > 100000 && questIdOrEventQuestId < 999999 {
		return calc.ToPtr(value_quest.QuestCategoryTypeEventAuthorDay), nil
	}
	if questIdOrEventQuestId > 410000000 && questIdOrEventQuestId < 450000000 {
		return calc.ToPtr(value_quest.QuestCategoryTypeMemorialCharacter), nil
	}
	var quest *model_quest.Quest
	res := qr.Conn.WithContext(ctx).Model(&model_quest.Quest{}).Where(&model_quest.Quest{Id: questIdOrEventQuestId}).Select("category").First(&quest)
	if res.Error != nil {
		count := int64(0)
		res := qr.Conn.WithContext(ctx).Model(&model_quest.EventQuest{}).Where("id", questIdOrEventQuestId).Count(&count).Error
		exists := count > 0
		if !exists {
			return nil, res
		}
		return calc.ToPtr(value_quest.QuestCategoryTypeEventAuthorDay), nil
	}
	return &quest.Category, nil
}

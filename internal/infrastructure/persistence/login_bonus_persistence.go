package persistence

import (
	"context"
	"time"

	model_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/model/login_bonus"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type loginBonusRepositoryImpl struct {
	Conn *gorm.DB
}

func NewLoginBonusRepositoryImpl(conn *gorm.DB) repository.LoginBonusRepository {
	return &loginBonusRepositoryImpl{Conn: conn}
}

func (rp *loginBonusRepositoryImpl) FindAvailableLoginBonuses(ctx context.Context) ([]*model_login_bonus.LoginBonus, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindAvailableLoginBonuses")
	defer span.End()
	var data []*model_login_bonus.LoginBonus
	now := time.Now()
	chain := rp.Conn.WithContext(ctx)
	// Add eager loadings
	chain = chain.Preload("BonusDays")
	chain = chain.Preload("BonusDays.BonusItems")
	// Add where filters
	chain = chain.Where("start_at < ?", now).Where("end_at > ?", now)
	// Run the query
	result := chain.Find(&data)
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

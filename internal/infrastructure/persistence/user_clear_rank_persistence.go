package persistence

import (
	"context"
	"fmt"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gorm.io/gorm"
)

type userClearRankRepositoryImpl struct {
	conn *gorm.DB
}

func NewUserClearRankRepositoryImpl(conn *gorm.DB) repository.UserClearRankRepository {
	return &userClearRankRepositoryImpl{conn: conn}
}

func (ur *userClearRankRepositoryImpl) UpsertClearRank(
	ctx context.Context,
	userId value_user.UserId,
	clearRank model_user.UserQuestClearRank,
) error {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "UpsertClearRank")
	defer span.End()

	if clearRank.UserId != userId {
		return fmt.Errorf("user id mismatch expected: %d actual: %d", userId, clearRank.UserId)
	}

	var existingClearRank model_user.UserQuestClearRank
	if res := ur.conn.WithContext(ctx).
		Where("user_id = ? AND quest_id = ?", clearRank.UserId, clearRank.QuestId).
		Limit(1).Find(&existingClearRank); res.Error != nil {
		return res.Error
	}

	// Insert new clear rank if not found
	if existingClearRank.QuestClearRankId == 0 {
		if err := ur.conn.WithContext(ctx).Create(&clearRank).Error; err != nil {
			return err
		}
		return nil
	}
	// Update if new clear rank is greater than existing clear rank
	if clearRank.ClearRank > existingClearRank.ClearRank {
		if err := ur.conn.WithContext(ctx).Model(&existingClearRank).
			Where("user_id = ? AND quest_id = ?", clearRank.UserId, clearRank.QuestId).
			Update("clear_rank", clearRank.ClearRank).Error; err != nil {
			return err
		}
	}
	return nil
}

func (ur *userClearRankRepositoryImpl) GetClearRanks(ctx context.Context, userId value_user.UserId) (model_user.UserQuestClearRanks, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetClearRanks")
	defer span.End()
	var questClearRanks model_user.UserQuestClearRanks
	if result := ur.conn.WithContext(ctx).Model(&model_user.UserQuestClearRank{}).Where("user_id = ?", userId).Find(&questClearRanks); result.Error != nil {
		return nil, result.Error
	}
	return questClearRanks, nil
}

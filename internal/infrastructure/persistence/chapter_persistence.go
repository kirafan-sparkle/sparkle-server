package persistence

import (
	"context"

	model_chapter "gitlab.com/kirafan/sparkle/server/internal/domain/model/chapter"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type chapterRepositoryImpl struct {
	Conn *gorm.DB
}

func NewChapterRepositoryImpl(conn *gorm.DB) repository.ChapterRepository {
	return &chapterRepositoryImpl{Conn: conn}
}

func (qr *chapterRepositoryImpl) GetAll(ctx context.Context) ([]*model_chapter.Chapter, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetAll")
	defer span.End()
	var chapters []*model_chapter.Chapter
	result := qr.Conn.WithContext(ctx).Preload("QuestIds").Find(&chapters)
	if result.Error != nil {
		return nil, result.Error
	}
	return chapters, nil
}

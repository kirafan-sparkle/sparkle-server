package persistence

import (
	"context"

	model_event_banner "gitlab.com/kirafan/sparkle/server/internal/domain/model/event_banner"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type eventBannerRepositoryImpl struct {
	Conn *gorm.DB
}

func NewEventBannerRepositoryImpl(conn *gorm.DB) repository.EventBannerRepository {
	return &eventBannerRepositoryImpl{Conn: conn}
}

func (qr *eventBannerRepositoryImpl) GetEventBanners(ctx context.Context) ([]*model_event_banner.EventBanner, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetEventBanners")
	defer span.End()
	var eventBanners []*model_event_banner.EventBanner
	result := qr.Conn.WithContext(ctx).Find(&eventBanners)
	if result.Error != nil {
		return nil, result.Error
	}
	return eventBanners, nil
}

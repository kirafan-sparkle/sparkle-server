package persistence

import (
	"context"

	model_version "gitlab.com/kirafan/sparkle/server/internal/domain/model/version"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"go.opentelemetry.io/otel/codes"

	"gorm.io/gorm"
)

type versionRepositoryImpl struct {
	Conn *gorm.DB
}

// NewVersionRepositoryImpl version repositoryのコンストラクタ
func NewVersionRepositoryImpl(conn *gorm.DB) repository.VersionRepository {
	return &versionRepositoryImpl{Conn: conn}
}

// Create versionの保存
func (ur *versionRepositoryImpl) Create(ctx context.Context, version *model_version.Version) (*model_version.Version, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "Create")
	defer span.End()
	if err := ur.Conn.WithContext(ctx).Create(&version).Error; err != nil {
		return nil, err
	}

	return version, nil
}

func (ur *versionRepositoryImpl) Update(ctx context.Context, version *model_version.Version) (*model_version.Version, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "Update")
	defer span.End()
	if err := ur.Conn.WithContext(ctx).Model(&version).Updates(&version).Error; err != nil {
		return nil, err
	}

	return version, nil
}

func (ur *versionRepositoryImpl) FindByPlatformAndVersion(ctx context.Context, platform value_version.Platform, version value_version.Version) (*model_version.Version, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindByPlatformAndVersion")
	defer span.End()

	v := &model_version.Version{Platform: platform, Version: version}
	if err := ur.Conn.WithContext(ctx).Where(&v).First(&v).Error; err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return nil, err
	}
	return v, nil
}

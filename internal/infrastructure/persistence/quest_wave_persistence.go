package persistence

import (
	"context"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type questWaveRepositoryImpl struct {
	Conn *gorm.DB
}

func NewQuestWaveRepositoryImpl(conn *gorm.DB) repository.QuestWaveRepository {
	return &questWaveRepositoryImpl{Conn: conn}
}

func (rp *questWaveRepositoryImpl) FindQuestWaves(ctx context.Context, query *model_quest.QuestWave, criteria map[string]interface{}, associations *[]string) ([]*model_quest.QuestWave, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindQuestWaves")
	defer span.End()
	var datas []*model_quest.QuestWave
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		result = chain.Where(criteria).Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *questWaveRepositoryImpl) FindQuestWaveRandoms(ctx context.Context, query *model_quest.QuestWaveRandom, criteria map[string]interface{}, associations *[]string) ([]*model_quest.QuestWaveRandom, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindQuestWaveRandoms")
	defer span.End()
	var datas []*model_quest.QuestWaveRandom
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		result = chain.Where(criteria).Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *questWaveRepositoryImpl) FindQuestWaveDrops(ctx context.Context, query *model_quest.QuestWaveDrop, criteria map[string]interface{}, associations *[]string) ([]*model_quest.QuestWaveDrop, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindQuestWaveDrops")
	defer span.End()
	var datas []*model_quest.QuestWaveDrop
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		result = chain.Where(criteria).Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *questWaveRepositoryImpl) FindQuestWave(ctx context.Context, query *model_quest.QuestWave, criteria map[string]interface{}, associations *[]string) (*model_quest.QuestWave, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindQuestWave")
	defer span.End()
	var data *model_quest.QuestWave
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		result = chain.Where(criteria).First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

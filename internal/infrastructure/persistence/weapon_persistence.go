package persistence

import (
	"context"

	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type weaponRepositoryImpl struct {
	Conn *gorm.DB
}

func NewWeaponRepositoryImpl(conn *gorm.DB) repository.WeaponRepository {
	return &weaponRepositoryImpl{Conn: conn}
}

func (rp *weaponRepositoryImpl) FindByWeaponId(ctx context.Context, weaponId value_weapon.WeaponId) (*model_weapon.Weapon, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindByWeaponId")
	defer span.End()
	var data *model_weapon.Weapon
	result := rp.Conn.WithContext(ctx).Preload("UpgradeBonusMaterialItemIDs").Where("weapon_id = ?", weaponId).First(&data)
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

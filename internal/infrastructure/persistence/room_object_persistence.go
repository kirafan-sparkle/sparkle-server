package persistence

import (
	"context"

	model_room_object "gitlab.com/kirafan/sparkle/server/internal/domain/model/room_object"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type roomObjectRepositoryImpl struct {
	Conn *gorm.DB
}

func NewRoomObjectRepositoryImpl(conn *gorm.DB) repository.RoomObjectRepository {
	return &roomObjectRepositoryImpl{Conn: conn}
}

func (rp *roomObjectRepositoryImpl) FindRoomObject(ctx context.Context, roomObjectId uint) (*model_room_object.RoomObject, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindRoomObject")
	defer span.End()
	var data *model_room_object.RoomObject
	result := rp.Conn.WithContext(ctx).Where(&model_room_object.RoomObject{
		Id: roomObjectId,
	}).First(&data)
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

func (rp *roomObjectRepositoryImpl) FindRoomObjects(ctx context.Context, query *model_room_object.RoomObject, criteria map[string]interface{}, associations *[]string) ([]*model_room_object.RoomObject, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindRoomObjects")
	defer span.End()
	var datas []*model_room_object.RoomObject
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		result = chain.Where(criteria).Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

package persistence

import (
	"context"

	model_mission "gitlab.com/kirafan/sparkle/server/internal/domain/model/mission"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type missionRepositoryImpl struct {
	Conn *gorm.DB
}

func NewMissionRepositoryImpl(conn *gorm.DB) repository.MissionRepository {
	return &missionRepositoryImpl{Conn: conn}
}

func (qr *missionRepositoryImpl) FindMissions(ctx context.Context, query *model_mission.Mission, criteria map[string]interface{}, associations []string) ([]*model_mission.Mission, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindMissions")
	defer span.End()
	var missions []*model_mission.Mission
	var result *gorm.DB
	chain := qr.Conn.WithContext(ctx)
	for _, association := range associations {
		chain = chain.Preload(association)
	}
	if query != nil {
		result = chain.Where(query).Find(&missions)
	} else {
		result = chain.Where(criteria).Find(&missions)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return missions, nil
}

func (qr *missionRepositoryImpl) FindMission(ctx context.Context, query *model_mission.Mission, criteria map[string]interface{}, associations []string) (*model_mission.Mission, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindMission")
	defer span.End()
	var mission *model_mission.Mission
	var result *gorm.DB
	if query != nil {
		result = qr.Conn.WithContext(ctx).Where(&query).First(&mission)
	} else {
		result = qr.Conn.WithContext(ctx).Where(criteria).First(&mission)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return mission, nil
}

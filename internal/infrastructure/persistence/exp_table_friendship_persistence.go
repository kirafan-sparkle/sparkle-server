package persistence

import (
	"context"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type expTableFriendshipRepositoryImpl struct {
	Conn *gorm.DB
}

func NewExpTableFriendshipRepositoryImpl(conn *gorm.DB) repository.ExpTableFriendshipRepository {
	return &expTableFriendshipRepositoryImpl{Conn: conn}
}

func (rp *expTableFriendshipRepositoryImpl) FindExpTableFriendships(ctx context.Context, query *model_exp_table.ExpTableFriendship, criteria map[string]interface{}) ([]*model_exp_table.ExpTableFriendship, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindExpTableFriendships")
	defer span.End()
	var datas []*model_exp_table.ExpTableFriendship
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *expTableFriendshipRepositoryImpl) FindExpTableFriendship(ctx context.Context, query *model_exp_table.ExpTableFriendship, criteria map[string]interface{}) (*model_exp_table.ExpTableFriendship, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindExpTableFriendship")
	defer span.End()
	var data *model_exp_table.ExpTableFriendship
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

package persistence

import (
	"context"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type expTableRankRepositoryImpl struct {
	Conn *gorm.DB
}

func NewExpTableRankRepositoryImpl(conn *gorm.DB) repository.ExpTableRankRepository {
	return &expTableRankRepositoryImpl{Conn: conn}
}

func (rp *expTableRankRepositoryImpl) FindExpTableRanks(ctx context.Context, query *model_exp_table.ExpTableRank, criteria map[string]interface{}) ([]*model_exp_table.ExpTableRank, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindExpTableRanks")
	defer span.End()
	var datas []*model_exp_table.ExpTableRank
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *expTableRankRepositoryImpl) FindExpTableRank(ctx context.Context, query *model_exp_table.ExpTableRank, criteria map[string]interface{}) (*model_exp_table.ExpTableRank, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindExpTableRank")
	defer span.End()
	var data *model_exp_table.ExpTableRank
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

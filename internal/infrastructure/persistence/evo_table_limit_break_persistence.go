package persistence

import (
	"context"

	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type evoTableLimitBreakRepositoryImpl struct {
	Conn *gorm.DB
}

func NewEvoTableLimitBreakRepositoryImpl(conn *gorm.DB) repository.EvoTableLimitBreakRepository {
	return &evoTableLimitBreakRepositoryImpl{Conn: conn}
}

func (rp *evoTableLimitBreakRepositoryImpl) FindByRecipeId(ctx context.Context, recipeId uint) (*model_evo_table.EvoTableLimitBreak, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindByRecipeId")
	defer span.End()
	var data *model_evo_table.EvoTableLimitBreak
	if result := rp.Conn.WithContext(ctx).Where(&model_evo_table.EvoTableLimitBreak{
		RecipeId: recipeId,
	}).Find(&data); result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

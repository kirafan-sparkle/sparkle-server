package persistence

import (
	"context"

	model_trade "gitlab.com/kirafan/sparkle/server/internal/domain/model/trade"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type tradeRecipeRepositoryImpl struct {
	Conn *gorm.DB
}

func NewTradeRecipeRepositoryImpl(conn *gorm.DB) repository.TradeRecipeRepository {
	return &tradeRecipeRepositoryImpl{Conn: conn}
}

func (rp *tradeRecipeRepositoryImpl) GetTradeRecipes(ctx context.Context) ([]model_trade.TradeRecipe, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetTradeRecipes")
	defer span.End()

	var datas []model_trade.TradeRecipe
	result := rp.Conn.WithContext(ctx).Model(&model_trade.TradeRecipe{}).Find(&datas)
	if result.Error != nil {
		return []model_trade.TradeRecipe{}, result.Error
	}
	return datas, nil
}

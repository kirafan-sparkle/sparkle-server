package persistence

import (
	"context"

	model_named_type "gitlab.com/kirafan/sparkle/server/internal/domain/model/named_type"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type namedTypeRepositoryImpl struct {
	Conn *gorm.DB
}

func NewNamedTypeRepositoryImpl(conn *gorm.DB) repository.NamedTypeRepository {
	return &namedTypeRepositoryImpl{Conn: conn}
}

func (rp *namedTypeRepositoryImpl) FindNamedTypes(ctx context.Context, query *model_named_type.NamedType, criteria map[string]interface{}, associations *[]string) ([]*model_named_type.NamedType, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindNamedTypes")
	defer span.End()
	var datas []*model_named_type.NamedType
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		result = chain.Where(criteria).Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *namedTypeRepositoryImpl) FindNamedType(ctx context.Context, query *model_named_type.NamedType, criteria map[string]interface{}, associations *[]string) (*model_named_type.NamedType, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindNamedType")
	defer span.End()
	var data *model_named_type.NamedType
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		result = chain.Where(criteria).First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

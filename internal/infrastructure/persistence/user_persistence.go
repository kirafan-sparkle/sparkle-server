package persistence

import (
	"context"
	"reflect"
	"time"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type userRepositoryImpl struct {
	Conn          *gorm.DB
	relationNames []string
}

// NewUserRepositoryImpl user repositoryのコンストラクタ
func NewUserRepositoryImpl(conn *gorm.DB) repository.UserRepository {
	relationNames := []string{
		"ItemSummary",
		"SupportCharacters",
		"ManagedBattleParties",
		"ManagedCharacters",
		"ManagedNamedTypes",
		"ManagedFieldPartyMembers",
		"ManagedTowns",
		"ManagedFacilities",
		"ManagedRoomObjects",
		"ManagedWeapons",
		"ManagedRooms",
		"ManagedMasterOrbs",
		"FavoriteMembers",
		"OfferTitleTypes",
		"AdvIds",
		"TipIds",
		"Missions",
		"Presents",
		"Achievements",
		"Gachas",
		"Trades",
		"LoginBonuses",
		"Trainings",
		"TrainingSlots",
	}
	return &userRepositoryImpl{Conn: conn, relationNames: relationNames}
}

// Create userの保存
func (ur *userRepositoryImpl) CreateUser(ctx context.Context, user *model_user.User) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "CreateUser")
	defer span.End()
	if err := ur.Conn.WithContext(ctx).Create(&user).Error; err != nil {
		return nil, err
	}

	return user, nil
}

// Update userの更新
func (ur *userRepositoryImpl) UpdateUser(ctx context.Context, user *model_user.User, param repository.UserRepositoryParam) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "UpdateUser")
	defer span.End()
	if param.Entire {
		if err := ur.Conn.WithContext(ctx).Session(&gorm.Session{FullSaveAssociations: true}).Save(&user).Error; err != nil {
			return nil, err
		}
		return user, nil
	}

	tx := ur.Conn.Begin()
	conn := tx.WithContext(ctx)

	// Upsert the relations (user_id should not be changed so they are ignored at DoUpdates)
	for _, field := range ur.relationNames {
		// If the field is set to true in the param struct, replace the association
		if reflect.ValueOf(param).FieldByName(field).Bool() {
			switch field {
			case "ItemSummary":
				if err := conn.Clauses(clause.OnConflict{
					Columns:   []clause.Column{{Name: "item_summary_id"}},
					DoUpdates: clause.AssignmentColumns([]string{"id", "amount"}),
				}).Create(user.ItemSummary).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "SupportCharacters":
				// Bit un-efficient but works for now
				// The support characters length is strict probably but we use upsert for now
				for _, supportCharacter := range user.SupportCharacters {
					if err := conn.Clauses(clause.OnConflict{
						Columns:   []clause.Column{{Name: "id"}},
						DoUpdates: clause.AssignmentColumns([]string{"managed_character_id", "managed_support_id"}),
					}).Create(supportCharacter.ManagedCharacterIds).Error; err != nil {
						tx.Rollback()
						return nil, err
					}
					if err := conn.Clauses(clause.OnConflict{
						Columns:   []clause.Column{{Name: "id"}},
						DoUpdates: clause.AssignmentColumns([]string{"id", "managed_weapon_id", "managed_support_id"}),
					}).Create(supportCharacter.ManagedWeaponIds).Error; err != nil {
						tx.Rollback()
						return nil, err
					}
				}
				if err := conn.Clauses(clause.OnConflict{
					Columns:   []clause.Column{{Name: "managed_support_id"}},
					DoUpdates: clause.AssignmentColumns([]string{"name", "active"}),
				}).Omit("ManagedCharacterIds", "ManagedWeaponIds").Create(user.SupportCharacters).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "ManagedBattleParties":
				if err := conn.Clauses(clause.OnConflict{
					Columns: []clause.Column{{Name: "managed_battle_party_id"}},
					DoUpdates: clause.AssignmentColumns([]string{
						"name",
						"cost_limit",
						"managed_character_id1",
						"managed_character_id2",
						"managed_character_id3",
						"managed_character_id4",
						"managed_character_id5",
						"managed_weapon_id1",
						"managed_weapon_id2",
						"managed_weapon_id3",
						"managed_weapon_id4",
						"managed_weapon_id5",
						"master_orb_id",
					}),
				}).Create(user.ManagedBattleParties).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "ManagedCharacters":
				// NOTE: This is workaround hack to prevent bulk managedCharacters insert. It filter characters only updated in recent 10 minutes.
				// FIXME: Find more better way. This implementation brings database layer problems into user model layer.
				updatedCharacters := make([]model_user.ManagedCharacter, 0, len(user.ManagedCharacters))
				currentTime := time.Now()
				for _, managedCharacter := range user.ManagedCharacters {
					if managedCharacter.UpdatedAt.After(currentTime.Add(-10 * time.Minute)) {
						updatedCharacters = append(updatedCharacters, managedCharacter)
					}
				}
				if err := conn.Clauses(clause.OnConflict{
					Columns: []clause.Column{{Name: "managed_character_id"}},
					DoUpdates: clause.AssignmentColumns([]string{
						"level",
						"level_limit",
						"exp",
						"level_break",
						"duplicated_count",
						"arousal_level",
						"skill_level1",
						"skill_level_limit1",
						"skill_exp1",
						"skill_level2",
						"skill_level_limit2",
						"skill_exp2",
						"skill_level3",
						"skill_level_limit3",
						"skill_exp3",
						"shown",
						"character_id",
						"view_character_id",
					}),
				}).Create(updatedCharacters).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "ManagedNamedTypes":
				// NOTE: This is workaround hack to prevent bulk managedNamedTypes insert. It filter namedTypes only updated in recent 10 minutes.
				// FIXME: Find more better way. This implementation brings database layer problems into user model layer.
				updatedNamedTypes := make([]model_user.ManagedNamedType, 0, len(user.ManagedNamedTypes))
				currentTime := time.Now()
				for _, managedNamedType := range user.ManagedNamedTypes {
					if managedNamedType.UpdatedAt.After(currentTime.Add(-10 * time.Minute)) {
						updatedNamedTypes = append(updatedNamedTypes, managedNamedType)
					}
				}
				// NamedType, TitleType and FriendshipExpTableId won't be updated so they are ignored in onConflict
				if err := conn.Clauses(clause.OnConflict{
					Columns:   []clause.Column{{Name: "managed_named_type_id"}},
					DoUpdates: clause.AssignmentColumns([]string{"level", "exp"}),
				}).Create(updatedNamedTypes).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "ManagedFieldPartyMembers":
				if err := conn.Clauses(clause.OnConflict{
					Columns: []clause.Column{{Name: "managed_party_member_id"}},
					DoUpdates: clause.AssignmentColumns([]string{
						"managed_character_id",
						"managed_facility_id",
						"character_id",
						"room_id",
						"live_idx",
						"schedule_table",
					}),
				}).Create(user.ManagedFieldPartyMembers).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "ManagedTowns":
				if err := conn.Clauses(clause.OnConflict{
					Columns:   []clause.Column{{Name: "managed_town_id"}},
					DoUpdates: clause.AssignmentColumns([]string{"grid_data"}),
				}).Create(user.ManagedTowns).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "ManagedFacilities":
				if err := conn.Clauses(clause.OnConflict{
					Columns: []clause.Column{{Name: "managed_town_facility_id"}},
					DoUpdates: clause.AssignmentColumns([]string{
						"facility_id",
						"created_at",
						"updated_at",
						"deleted_at",
						"build_point_index",
						"level",
						"open_state",
						"action_time",
						"build_time",
					}),
				}).Create(user.ManagedFacilities).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "ManagedRoomObjects":
				if err := conn.Clauses(clause.OnConflict{
					Columns:   []clause.Column{{Name: "managed_room_object_id"}},
					DoNothing: true,
				}).Create(user.ManagedRoomObjects).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "ManagedWeapons":
				// SoldAt is unused field so they are ignored at DoUpdates
				if err := conn.Clauses(clause.OnConflict{
					Columns: []clause.Column{{Name: "managed_weapon_id"}},
					DoUpdates: clause.AssignmentColumns([]string{
						"weapon_id",
						"level",
						"exp",
						"skill_level",
						"skill_exp",
						"state",
						"deleted_at",
					}),
				}).Create(user.ManagedWeapons).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "ManagedRooms":
				for managedRoomNo := range user.ManagedRooms {
					managedRoom := &user.ManagedRooms[managedRoomNo]
					// Update basic room info
					if err := conn.Clauses(clause.OnConflict{
						Columns: []clause.Column{{Name: "managed_room_id"}},
						DoUpdates: clause.AssignmentColumns([]string{
							"floor_id",
							"group_id",
							"active",
						}),
					}).Omit("ArrangeData").Create(&managedRoom).Error; err != nil {
						tx.Rollback()
						return nil, err
					}
					// Delete the arrange data for the room
					existsArrangeDataIds := make([]int, 0, len(managedRoom.ArrangeData))
					for _, arrangeData := range managedRoom.ArrangeData {
						existsArrangeDataIds = append(existsArrangeDataIds, arrangeData.ManagedRoomObjectId)
					}
					if err := conn.Where(
						"managed_room_id = ?", managedRoom.ManagedRoomId,
					).Where(
						"managed_room_object_id NOT IN (?)", existsArrangeDataIds,
					).Delete(
						&model_user.ManagedRoomArrangeData{},
					).Error; err != nil {
						tx.Rollback()
						return nil, err
					}
					// Upsert new room arrange data
					for i := range managedRoom.ArrangeData {
						managedRoom.ArrangeData[i].ManagedRoomId = managedRoom.ManagedRoomId
					}
					if err := conn.Clauses(clause.OnConflict{
						Columns: []clause.Column{
							// Or should we use ManagedRoomObjectId as primary key?
							{Name: "managed_room_object_id"},
						},
						DoUpdates: clause.AssignmentColumns([]string{
							"managed_room_object_id",
							"managed_room_id",
							"room_object_id",
							"room_no",
							"x",
							"y",
							"dir",
						}),
					}).Create(managedRoom.ArrangeData).Error; err != nil {
						tx.Rollback()
						return nil, err
					}
				}
			case "ManagedMasterOrbs":
				if err := conn.Clauses(clause.OnConflict{
					Columns: []clause.Column{{Name: "managed_master_orb_id"}},
					DoUpdates: clause.AssignmentColumns([]string{
						"level",
						"exp",
						"master_orb_id",
					}),
				}).Create(user.ManagedMasterOrbs).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "FavoriteMembers":
				if err := conn.Clauses(clause.OnConflict{
					Columns: []clause.Column{{Name: "id"}},
					DoUpdates: clause.AssignmentColumns([]string{
						"managed_character_id",
						"favorite_index",
						"character_id",
						"arousal_level",
					}),
				}).Create(user.FavoriteMembers).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "OfferTitleTypes":
				if err := conn.Clauses(clause.OnConflict{
					Columns: []clause.Column{{Name: "id"}},
					DoUpdates: clause.AssignmentColumns([]string{
						"title_type",
						"content_room_flg",
						"category",
						"offer_index",
						"offer_point",
						"offer_max_point",
						"state",
						"shown",
						"sub_state",
						"sub_state1",
						"sub_state2",
						"sub_state3",
					}),
				}).Create(user.OfferTitleTypes).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "AdvIds":
				// NOTE: This is workaround hack to prevent bulk advId insert. It filter advId only created in recent 10 minutes.
				// FIXME: Make another persistence layer only for "add", not "update".
				newAdvIds := make([]model_user.ClearedAdvId, 0, len(user.AdvIds))
				currentTime := time.Now()
				for _, advId := range user.AdvIds {
					if advId.CreatedAt.After(currentTime.Add(-10 * time.Minute)) {
						newAdvIds = append(newAdvIds, advId)
					}
				}
				// Ignores update if the adv id is already exists
				if err := conn.Clauses(clause.OnConflict{
					Columns:   []clause.Column{{Name: "id"}},
					DoNothing: true,
				}).Create(newAdvIds).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "TipIds":
				// Ignores update if the tip id is already exists
				if err := conn.Clauses(clause.OnConflict{
					Columns:   []clause.Column{{Name: "id"}},
					DoNothing: true,
				}).Create(user.TipIds).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "Missions":
				if err := conn.Clauses(clause.OnConflict{
					Columns: []clause.Column{{Name: "managed_mission_id"}},
					DoUpdates: clause.AssignmentColumns([]string{
						"updated_at",
						"deleted_at",
						"limit_time",
						"rate",
						"state",
						"mission_id",
					}),
				}).Omit("Mission", "User").Create(user.Missions).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "Presents":
				if err := conn.Clauses(clause.OnConflict{
					Columns: []clause.Column{{Name: "managed_present_id"}},
					DoUpdates: clause.AssignmentColumns([]string{
						"present_id",
						"received_at",
						"deadline_at",
						"type",
						"object_id",
						"amount",
					}),
				}).Omit("Present").Create(user.Presents).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "Achievements":
				if err := conn.Clauses(clause.OnConflict{
					Columns: []clause.Column{{Name: "managed_achievement_id"}},
					DoUpdates: clause.AssignmentColumns([]string{
						"updated_at",
						"enable",
						"is_new",
						"achievement_id",
					}),
				}).Create(user.Achievements).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "Gachas":
				if err := conn.Clauses(clause.OnConflict{
					Columns: []clause.Column{{Name: "user_gacha_id"}},
					DoUpdates: clause.AssignmentColumns([]string{
						"updated_at",
						"deleted_at",
						"gem10_current_step",
						"player_draw_point",
						"gem10_daily",
						"gem10_total",
						"gem1_daily",
						"gem1_total",
						"u_gem1_daily",
						"u_gem1_total",
						"item_daily",
						"item_total",
						"gem1_free_draw_count",
						"gem10_free_draw_count",
						"gacha_id",
					}),
				}).Omit("Gacha", "User").Create(user.Gachas).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "Trades":
				if err := conn.Clauses(clause.OnConflict{
					Columns: []clause.Column{{Name: "id"}},
					DoUpdates: clause.AssignmentColumns([]string{
						"updated_at",
						"deleted_at",
						"total_trade_count",
						"monthly_trade_count",
						"recipe_id",
					}),
				}).Create(user.Trades).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "LoginBonuses":
				if err := conn.Clauses(clause.OnConflict{
					Columns:   []clause.Column{{Name: "user_login_bonus_id"}},
					DoUpdates: clause.AssignmentColumns([]string{"updated_at", "deleted_at", "login_bonus_day_index", "login_bonus_id"}),
				}).Omit("User", "LoginBonus").Create(user.LoginBonuses).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "Trainings":
				if err := conn.Clauses(clause.OnConflict{
					Columns:   []clause.Column{{Name: "user_training_id"}},
					DoUpdates: clause.AssignmentColumns([]string{"updated_at", "cleared"}),
				}).Omit("Training").Create(user.Trainings).Error; err != nil {
					tx.Rollback()
					return nil, err
				}
			case "TrainingSlots":
				for _, userTrainingSlot := range user.TrainingSlots {
					// Save the order model
					if userTrainingSlot.Order != nil {
						if err := conn.Clauses(clause.OnConflict{
							Columns:   []clause.Column{{Name: "order_id"}},
							DoUpdates: clause.AssignmentColumns([]string{"start_at", "end_at", "user_training_id"}),
						}).Omit("UserTraining").Create(&userTrainingSlot.Order).Error; err != nil {
							tx.Rollback()
							return nil, err
						}
						if err := conn.Clauses(clause.OnConflict{
							Columns:   []clause.Column{{Name: "id"}},
							DoUpdates: clause.AssignmentColumns([]string{"updated_at", "user_training_slot_id", "managed_character_id"}),
						}).Create(&userTrainingSlot.ManagedCharacters).Error; err != nil {
							tx.Rollback()
							return nil, err
						}
						// Save the base model
						userTrainingSlot.OrderId = &userTrainingSlot.Order.OrderId
					}
					// Save the base model
					if err := conn.Clauses(clause.OnConflict{
						Columns: []clause.Column{{Name: "user_training_slot_id"}},
						// NOTE: OpenSlot is ignored since it try to save zero value...
						DoUpdates: clause.AssignmentColumns([]string{"order_id"}),
					}).Omit("TrainingSlot", "Order", "ManagedCharacters").Create(&userTrainingSlot).Error; err != nil {
						tx.Rollback()
						return nil, err
					}
				}
			}
		}
	}
	// Update the user base model
	if err := conn.Model(&user).Select(
		"UpdatedAt",
		"UUId",
		"Age",
		"Comment",
		"ContinuousDays",
		"CurrentAchievementId",
		"FacilityLimit",
		"FacilityLimitCount",
		"FriendLimit",
		"Gold",
		"IpAddr",
		"Kirara",
		"KiraraLimit",
		"LastLoginAt",
		"LastPartyAdded",
		"Level",
		"LevelExp",
		"TotalExp",
		"UnlimitedGem",
		"LimitedGem",
		"LoginCount",
		"LoginDays",
		"Name",
		"PartyCost",
		"RecastTime",
		"RoomObjectLimit",
		"RoomObjectLimitCount",
		"Stamina",
		"StaminaMax",
		"StaminaUpdatedAt",
		"SupportLimit",
		"UserAgent",
		"WeaponLimit",
		"WeaponLimitCount",
		"FlagPush",
		"FlagUi",
		"LatestQuestLog",
		"FlagStamina",
		"PushToken",
		"IsNewProduct",
		"IsCloseInfo",
		"CountUpdatedAt",
		"TrainingCount",
		"PresentCount",
		"FriendProposedCount",
		"NewAchievementCount",
		"StepCode",
		"LatestQuestLogID",
		"LastMemberAdded",
		"LastOpenedPart1ChapterId",
		"LastOpenedPart2ChapterId",
		"LastPlayedPart1ChapterQuestId",
		"LastPlayedPart2ChapterQuestId",
		"LastLoginBonus",
		"MoveCode",
		"MoveDeadline",
		"MovePasswordSalt",
	).Omit(ur.relationNames...).Updates(user).Error; err != nil {
		return nil, err
	}
	// Delete the unneeded relations
	for _, field := range ur.relationNames {
		// If the field is set to true in the param struct, replace the association
		if reflect.ValueOf(param).FieldByName(field).Bool() {
			conn := tx.Unscoped().WithContext(ctx)
			switch field {
			case "ItemSummary":
				if deleteErr := conn.Where("amount = 0").Where("user_id = ?", user.Id).Delete(
					&model_user.ItemSummary{},
				).Error; deleteErr != nil {
					tx.Rollback()
					return nil, deleteErr
				}
			case "SupportCharacters":
				// do nothing since the support characters won't be deleted
			case "ManagedBattleParties":
				// do nothing since the managed battle parties won't be deleted
			case "ManagedCharacters":
				// do nothing since the managed characters won't be deleted
			case "ManagedNamedTypes":
				// do nothing since the managed named types won't be deleted
			case "ManagedFieldPartyMembers":
				// do nothing since the managed field party members won't be deleted
			case "ManagedTowns":
				// do nothing since the managed towns won't be deleted
			case "ManagedFacilities":
				if deleteErr := conn.Unscoped().Where("deleted_at IS NOT NULL").Where("user_id = ?", user.Id).Delete(
					&model_user.ManagedTownFacility{},
				).Error; deleteErr != nil {
					tx.Rollback()
					return nil, deleteErr
				}
			case "ManagedRoomObjects":
				if deleteErr := conn.Unscoped().Where("deleted_at IS NOT NULL").Where("user_id = ?", user.Id).Delete(
					&model_user.ManagedRoomObject{},
				).Error; deleteErr != nil {
					tx.Rollback()
					return nil, deleteErr
				}
			case "ManagedWeapons":
				if deleteErr := conn.Unscoped().Where("deleted_at IS NOT NULL").Where("user_id = ?", user.Id).Delete(
					&model_user.ManagedWeapon{},
				).Error; deleteErr != nil {
					tx.Rollback()
					return nil, deleteErr
				}
			case "ManagedRooms":
				// do nothing since the managed rooms won't be deleted
			case "ManagedMasterOrbs":
				// do nothing since the managed master orbs won't be deleted
			case "FavoriteMembers":
				// do nothing since the favorite members won't be deleted
			case "OfferTitleTypes":
				// TODO: Implement me if it is needed
			case "AdvIds":
				// do nothing since the adv ids won't be deleted
			case "TipIds":
				// do nothing since the tip ids won't be deleted
			case "Missions":
				if deleteErr := conn.Unscoped().Where("deleted_at IS NOT NULL").Where("user_id = ?", user.Id).Delete(
					&model_user.UserMission{},
				).Error; deleteErr != nil {
					tx.Rollback()
					return nil, deleteErr
				}
			case "Presents":
				if deleteErr := conn.Where("CURRENT_TIMESTAMP > deadline_at").Where("user_id = ?", user.Id).Delete(
					&model_user.UserPresent{},
				).Error; deleteErr != nil {
					tx.Rollback()
					return nil, deleteErr
				}
			case "Achievements":
				// do nothing since the achievements won't be deleted
			case "Gachas":
				if deleteErr := conn.Unscoped().Where("deleted_at IS NOT NULL").Where("user_id = ?", user.Id).Delete(
					&model_user.UserGacha{},
				).Error; deleteErr != nil {
					tx.Rollback()
					return nil, deleteErr
				}
			case "Trades":
				if deleteErr := conn.Unscoped().Where("deleted_at IS NOT NULL").Where("user_id = ?", user.Id).Delete(
					&model_user.UserTradeRecipe{},
				).Error; deleteErr != nil {
					tx.Rollback()
					return nil, deleteErr
				}
			case "LoginBonuses":
				if deleteErr := conn.Unscoped().Where("deleted_at IS NOT NULL").Where("user_id = ?", user.Id).Delete(
					&model_user.UserLoginBonus{},
				).Error; deleteErr != nil {
					tx.Rollback()
					return nil, deleteErr
				}
			}
		}
	}

	tx.Commit()
	return user, nil
}

func (ur *userRepositoryImpl) FindUserByQuery(ctx context.Context, query *model_user.User, param repository.UserRepositoryParam) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindUserByQuery")
	defer span.End()
	var user *model_user.User
	chain := ur.Conn.WithContext(ctx)

	if param.Entire {
		chain = chain.Preload(clause.Associations)
		chain = chain.Preload("SupportCharacters.ManagedCharacterIds")
		chain = chain.Preload("SupportCharacters.ManagedWeaponIds")
		chain = chain.Preload("ManagedRooms.ArrangeData")
		chain = chain.Preload("LoginBonuses.LoginBonus")
		chain = chain.Preload("LoginBonuses.LoginBonus.BonusDays")
		chain = chain.Preload("LoginBonuses.LoginBonus.BonusDays.BonusItems")
		chain = chain.Preload("Missions")
		chain = chain.Preload("Missions.Mission.Reward")
		if result := chain.Where(query).First(&user); result.Error != nil {
			return nil, result.Error
		}
		return user, nil
	}

	// Define a slice of field names
	fields := []string{
		"ItemSummary",
		"SupportCharacters",
		"ManagedBattleParties",
		"ManagedCharacters",
		"ManagedNamedTypes",
		"ManagedFieldPartyMembers",
		"ManagedTowns",
		"ManagedFacilities",
		"ManagedRoomObjects",
		"ManagedWeapons",
		"ManagedRooms",
		"ManagedMasterOrbs",
		"FavoriteMembers",
		"OfferTitleTypes",
		"AdvIds",
		"TipIds",
		"Missions",
		"Presents",
		"Achievements",
		"Gachas",
		"Trades",
		"LoginBonuses",
		"Trainings",
		"TrainingSlots",
	}
	// Loop through the fields of the param struct and preload associations as needed
	for _, field := range fields {
		if reflect.ValueOf(param).FieldByName(field).Bool() {
			switch field {
			case "SupportCharacters":
				chain = chain.Preload("SupportCharacters")
				chain = chain.Preload("SupportCharacters.ManagedCharacterIds")
				chain = chain.Preload("SupportCharacters.ManagedWeaponIds")
			case "ManagedRooms":
				chain = chain.Preload("ManagedRooms")
				chain = chain.Preload("ManagedRooms.ArrangeData")
			case "LoginBonuses":
				chain = chain.Preload("LoginBonuses")
				chain = chain.Preload("LoginBonuses.LoginBonus")
				chain = chain.Preload("LoginBonuses.LoginBonus.BonusDays")
				chain = chain.Preload("LoginBonuses.LoginBonus.BonusDays.BonusItems")
			case "Gachas":
				chain = chain.Preload("Gachas")
				chain = chain.Preload("Gachas.Gacha")
				chain = chain.Preload("Gachas.Gacha.SelectionCharacterIds")
				chain = chain.Preload("Gachas.Gacha.DrawPoints")
				chain = chain.Preload("Gachas.Gacha.GachaSteps")
				chain = chain.Preload("Gachas.Gacha.GachaSteps.GachaBonusItems")
			case "Trades":
				chain = chain.Preload("Trades.Recipe")
			case "Presents":
				chain = chain.Preload("Presents")
				chain = chain.Preload("Presents.Present")
			case "Missions":
				chain = chain.Preload("Missions")
				chain = chain.Preload("Missions.Mission")
				chain = chain.Preload("Missions.Mission.Reward")
			case "Achievements":
				chain = chain.Preload("Achievements")
				chain = chain.Preload("Achievements.Achievement")
			case "Trainings":
				chain = chain.Preload("Trainings")
				chain = chain.Preload("Trainings.Training")
				chain = chain.Preload("Trainings.Training.Rewards")
				chain = chain.Preload("Trainings.Training.Rewards.DropRate")
			case "TrainingSlots":
				chain = chain.Preload("TrainingSlots")
				chain = chain.Preload("TrainingSlots.TrainingSlot")
				chain = chain.Preload("TrainingSlots.ManagedCharacters")
				chain = chain.Preload("TrainingSlots.Order")
				chain = chain.Preload("TrainingSlots.Order.UserTraining")
				chain = chain.Preload("TrainingSlots.Order.UserTraining.Training")
			default:
				chain = chain.Preload(field)
			}
		}
	}
	if result := chain.Where(query).First(&user); result.Error != nil {
		return nil, result.Error
	}
	return user, nil
}

func (ur *userRepositoryImpl) FindUserById(ctx context.Context, id value_user.UserId, param repository.UserRepositoryParam) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindUserById")
	defer span.End()
	query := &model_user.User{Id: id}
	return ur.FindUserByQuery(ctx, query, param)
}

func (ur *userRepositoryImpl) FindUserByUUID(ctx context.Context, UUId string, param repository.UserRepositoryParam) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindUserByUUID")
	defer span.End()
	query := &model_user.User{UUId: UUId}
	return ur.FindUserByQuery(ctx, query, param)
}

func (ur *userRepositoryImpl) FindUserBySession(ctx context.Context, deviceUUId string, sessionUUId string) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindUserBySession")
	defer span.End()
	query := &model_user.User{UUId: deviceUUId, Session: sessionUUId}
	return ur.FindUserByQuery(ctx, query, repository.UserRepositoryParam{})
}

func (ur *userRepositoryImpl) FindUserByFriendCode(ctx context.Context, code value_friend.FriendOrCheatCode) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindUserByFriendCode")
	defer span.End()
	query := &model_user.User{MyCode: code}
	return ur.FindUserByQuery(ctx, query, repository.UserRepositoryParam{
		FavoriteMembers:   true,
		ManagedCharacters: true,
	})
}

func (ur *userRepositoryImpl) FindUserByMoveCode(ctx context.Context, code string) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindUserByMoveCode")
	defer span.End()
	query := &model_user.User{MoveCode: code}
	return ur.FindUserByQuery(ctx, query, repository.UserRepositoryParam{})
}

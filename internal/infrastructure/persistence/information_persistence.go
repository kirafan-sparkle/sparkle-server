package persistence

import (
	"context"

	model_information "gitlab.com/kirafan/sparkle/server/internal/domain/model/information"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type informationRepositoryImpl struct {
	Conn *gorm.DB
}

func NewInformationRepositoryImpl(conn *gorm.DB) repository.InformationRepository {
	return &informationRepositoryImpl{Conn: conn}
}

func (qr *informationRepositoryImpl) GetInformations(ctx context.Context, platform value_version.Platform) ([]*model_information.Information, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetInformations")
	defer span.End()
	var informations []*model_information.Information
	result := qr.Conn.WithContext(ctx).Where("platform = ?", platform).Find(&informations)
	if result.Error != nil {
		return nil, result.Error
	}
	return informations, nil
}

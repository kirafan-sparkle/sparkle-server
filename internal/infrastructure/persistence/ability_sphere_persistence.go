package persistence

import (
	"context"

	model_ability_sphere "gitlab.com/kirafan/sparkle/server/internal/domain/model/ability_sphere"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_ability_sphere "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_sphere"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type abilitySphereRepositoryImpl struct {
	Conn *gorm.DB
}

func NewAbilitySphereRepositoryImpl(conn *gorm.DB) repository.AbilitySphereRepository {
	return &abilitySphereRepositoryImpl{Conn: conn}
}

func (rp *abilitySphereRepositoryImpl) FindAbilitySphereBySphereItemId(ctx context.Context, itemId value_ability_sphere.AbilitySphereItemId) (*model_ability_sphere.AbilitySphere, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindAbilitySphereBySphereItemId")
	defer span.End()

	var abilitySphere *model_ability_sphere.AbilitySphere
	result := rp.Conn.WithContext(ctx).Where("sphere_item_id = ?", itemId).Limit(1).Find(&abilitySphere)
	if result.Error != nil {
		return nil, result.Error
	}
	return abilitySphere, nil
}

func (rp *abilitySphereRepositoryImpl) FindAbilitySphereRecipeBySphereItemId(ctx context.Context, itemId value_ability_sphere.AbilitySphereItemId) (*model_ability_sphere.AbilitySphereRecipe, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindAbilitySphereRecipeBySphereItemId")
	defer span.End()

	var abilitySphereRecipe *model_ability_sphere.AbilitySphereRecipe
	result := rp.Conn.WithContext(ctx).Where("src_item_id = ?", itemId).Find(&abilitySphereRecipe)
	if result.Error != nil {
		return nil, result.Error
	}
	return abilitySphereRecipe, nil
}

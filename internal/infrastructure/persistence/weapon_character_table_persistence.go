package persistence

import (
	"context"

	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type weaponCharacterTableRepositoryImpl struct {
	Conn *gorm.DB
}

func NewWeaponCharacterTableRepositoryImpl(conn *gorm.DB) repository.WeaponCharacterTableRepository {
	return &weaponCharacterTableRepositoryImpl{Conn: conn}
}

func (rp *weaponCharacterTableRepositoryImpl) FindByCharacterId(ctx context.Context, characterId value_character.CharacterId) (*model_weapon.WeaponCharacterTable, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindByCharacterId")
	defer span.End()
	var data *model_weapon.WeaponCharacterTable
	result := rp.Conn.WithContext(ctx).Where("character_id = ?", characterId).First(&data)
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

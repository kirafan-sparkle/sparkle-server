package persistence

import (
	"context"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type eventQuestRepositoryImpl struct {
	Conn *gorm.DB
}

func NewEventQuestRepositoryImpl(conn *gorm.DB) repository.EventQuestRepository {
	return &eventQuestRepositoryImpl{Conn: conn}
}

func (qr *eventQuestRepositoryImpl) GetEventQuest(ctx context.Context, internalUserId value_user.UserId, eventQuestId uint) (*model_quest.EventQuest, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetEventQuest")
	defer span.End()
	var eventQuest *model_quest.EventQuest
	result := qr.Conn.WithContext(ctx).Preload("Quest").Preload("CondEventQuestIds").Where("id = ?", eventQuestId).Find(&eventQuest)
	if result.Error != nil {
		return nil, result.Error
	}
	return eventQuest, nil
}

func (qr *eventQuestRepositoryImpl) GetEventQuests(ctx context.Context, internalUserId value_user.UserId) ([]*model_quest.EventQuest, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetEventQuests")
	defer span.End()
	var eventQuests []*model_quest.EventQuest
	result := qr.Conn.WithContext(ctx).Preload("Quest").Preload("CondEventQuestIds").Find(&eventQuests)
	if result.Error != nil {
		return nil, result.Error
	}
	return eventQuests, nil
}

func (qr *eventQuestRepositoryImpl) GetEventQuestPeriods(ctx context.Context, internalUserId value_user.UserId) ([]*model_quest.EventQuestPeriod, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetEventQuestPeriods")
	defer span.End()
	var eventQuestPeriods []*model_quest.EventQuestPeriod
	result := qr.Conn.WithContext(ctx).Preload(
		"EventQuestPeriodGroups.EventQuestPeriodGroupQuests.CondEventQuestIds",
	).Find(&eventQuestPeriods)
	if result.Error != nil {
		return nil, result.Error
	}
	return eventQuestPeriods, nil
}

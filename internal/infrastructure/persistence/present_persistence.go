package persistence

import (
	"context"

	model_present "gitlab.com/kirafan/sparkle/server/internal/domain/model/present"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type presentRepositoryImpl struct {
	Conn *gorm.DB
}

func NewPresentRepositoryImpl(conn *gorm.DB) repository.PresentRepository {
	return &presentRepositoryImpl{Conn: conn}
}

func (rp *presentRepositoryImpl) FindPresents(ctx context.Context, query *model_present.Present, criteria map[string]interface{}, associations *[]string) ([]*model_present.Present, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindPresents")
	defer span.End()
	var datas []*model_present.Present
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		result = chain.Where(criteria).Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *presentRepositoryImpl) FindPresent(ctx context.Context, query *model_present.Present, criteria map[string]interface{}, associations *[]string) (*model_present.Present, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindPresent")
	defer span.End()
	var data *model_present.Present
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		result = chain.Where(criteria).First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

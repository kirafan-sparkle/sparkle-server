package persistence

import (
	"context"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type expTableWeaponRepositoryImpl struct {
	Conn *gorm.DB
}

func NewExpTableWeaponRepositoryImpl(conn *gorm.DB) repository.ExpTableWeaponRepository {
	return &expTableWeaponRepositoryImpl{Conn: conn}
}

func (rp *expTableWeaponRepositoryImpl) FindExpTableWeapons(ctx context.Context, query *model_exp_table.ExpTableWeapon, criteria map[string]interface{}) ([]*model_exp_table.ExpTableWeapon, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindExpTableWeapons")
	defer span.End()
	var datas []*model_exp_table.ExpTableWeapon
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		result = chain.Where(criteria).Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *expTableWeaponRepositoryImpl) FindExpTableWeapon(ctx context.Context, query *model_exp_table.ExpTableWeapon, criteria map[string]interface{}) (*model_exp_table.ExpTableWeapon, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindExpTableWeapon")
	defer span.End()
	var data *model_exp_table.ExpTableWeapon
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

func (rp *expTableWeaponRepositoryImpl) GetRequiredCoinsForUpgrade(ctx context.Context, currentLevel uint8) (uint16, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetRequiredCoinsForUpgrade")
	defer span.End()
	var data *model_exp_table.ExpTableWeapon
	if result := rp.Conn.WithContext(ctx).Where("level = ?", currentLevel).First(&data); result.Error != nil {
		return 0, result.Error
	}
	return data.RequiredCoinPerItem, nil
}

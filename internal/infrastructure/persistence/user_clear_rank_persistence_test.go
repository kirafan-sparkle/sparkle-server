package persistence

import (
	"context"
	"testing"

	"github.com/google/go-cmp/cmp"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/seed"
	"gitlab.com/kirafan/sparkle/server/pkg/testutil"
	"gorm.io/gorm"
)

func Test_userClearRankRepositoryImpl_UpsertClearRank(t *testing.T) {
	db := testutil.GetInMemoryDatabase()
	seedOption := seed.NewEmptySeedOption()
	testutil.InitInMemoryDatabase(db, seedOption)

	type args struct {
		userId    value_user.UserId
		clearRank model_user.UserQuestClearRank
	}
	tests := []struct {
		name        string
		postProcess func(db *gorm.DB, repo repository.UserClearRankRepository, args args)
		args        args
		preProcess  func(db *gorm.DB, repo repository.UserClearRankRepository, args args) value_quest.ClearRank
		want        value_quest.ClearRank
		wantErr     bool
	}{
		{
			name: "userId mismatch raises error",
			args: args{
				userId: 1204,
				clearRank: model_user.UserQuestClearRank{
					UserId:    1201,
					QuestId:   0,
					ClearRank: value_quest.ClearRankGold,
				},
			},
			want:    value_quest.ClearRankNone,
			wantErr: true,
		},
		{
			name: "insert clear rank gold success",
			args: args{
				userId: 1,
				clearRank: model_user.UserQuestClearRank{
					UserId:    1,
					QuestId:   1,
					ClearRank: value_quest.ClearRankGold,
				},
			},
			want:    value_quest.ClearRankGold,
			wantErr: false,
		},
		{
			name: "update clear rank from bronze to gold success (affected)",
			postProcess: func(db *gorm.DB, repo repository.UserClearRankRepository, args args) {
				newClearRank := model_user.UserQuestClearRank{
					UserId:    args.userId,
					QuestId:   args.clearRank.QuestId,
					ClearRank: value_quest.ClearRankBronze,
				}
				if res := db.Create(&newClearRank); res.Error != nil {
					t.Fatal(res.Error)
				}
			},
			args: args{
				userId: 2,
				clearRank: model_user.UserQuestClearRank{
					UserId:    2,
					QuestId:   2,
					ClearRank: value_quest.ClearRankGold,
				},
			},
			want:    value_quest.ClearRankGold,
			wantErr: false,
		},
		{
			name: "update clear rank from silver to silver success (not affected)",
			postProcess: func(db *gorm.DB, repo repository.UserClearRankRepository, args args) {
				newClearRank := model_user.UserQuestClearRank{
					UserId:    args.userId,
					QuestId:   args.clearRank.QuestId,
					ClearRank: value_quest.ClearRankSilver,
				}
				if res := db.Create(&newClearRank); res.Error != nil {
					t.Fatal(res.Error)
				}
			},
			args: args{
				userId: 2,
				clearRank: model_user.UserQuestClearRank{
					UserId:    2,
					QuestId:   3,
					ClearRank: value_quest.ClearRankSilver,
				},
			},
			want:    value_quest.ClearRankSilver,
			wantErr: false,
		},
		{
			name: "update clear rank from gold to bronze success (not affected)",
			postProcess: func(db *gorm.DB, repo repository.UserClearRankRepository, args args) {
				newClearRank := model_user.UserQuestClearRank{
					UserId:    args.userId,
					QuestId:   args.clearRank.QuestId,
					ClearRank: value_quest.ClearRankGold,
				}
				if res := db.Create(&newClearRank); res.Error != nil {
					t.Fatal(res.Error)
				}
			},
			args: args{
				userId: 2,
				clearRank: model_user.UserQuestClearRank{
					UserId:    2,
					QuestId:   4,
					ClearRank: value_quest.ClearRankBronze,
				},
			},
			want:    value_quest.ClearRankGold,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx := context.Background()
			repo := NewUserClearRankRepositoryImpl(db)

			// Run post process
			if tt.postProcess != nil {
				tt.postProcess(db, repo, tt.args)
			}
			// Execute process
			if err := repo.UpsertClearRank(ctx, tt.args.userId, tt.args.clearRank); err != nil {
				if !tt.wantErr {
					t.Errorf("userClearRankRepositoryImpl.UpsertClearRank() error = %v, wantErr %v", err, tt.wantErr)
				}
				return
			}
			// Run pre process
			var res value_quest.ClearRank
			if tt.preProcess != nil {
				res = tt.preProcess(db, repo, tt.args)
			} else {
				clearRanks, err := repo.GetClearRanks(ctx, tt.args.userId)
				if err != nil {
					t.Errorf("userClearRankRepositoryImpl.GetClearRanks() error = %v", err)
				}
				res = clearRanks.GetClearRank(tt.args.clearRank.QuestId)
			}

			// Compare the result
			if cmp.Equal(res, tt.want) != true {
				t.Errorf("userClearRankRepositoryImpl.GetClearRank() Diff = %+v", cmp.Diff(res, tt.want))
			}
		})
	}
}

package persistence

import (
	"context"

	model_trade "gitlab.com/kirafan/sparkle/server/internal/domain/model/trade"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type packageItemRepositoryImpl struct {
	Conn *gorm.DB
}

func NewPackageItemRepositoryImpl(conn *gorm.DB) repository.PackageItemRepository {
	return &packageItemRepositoryImpl{Conn: conn}
}

func (rp *packageItemRepositoryImpl) FindByPK(ctx context.Context, packageItemId uint) (*model_trade.PackageItem, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindByPK")
	defer span.End()
	var data *model_trade.PackageItem
	result := rp.Conn.WithContext(ctx).Preload("Contents").Where("id = ?", packageItemId).First(&data)
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

func (rp *packageItemRepositoryImpl) GetAll(ctx context.Context) ([]*model_trade.PackageItem, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetAll")
	defer span.End()
	var data []*model_trade.PackageItem
	result := rp.Conn.WithContext(ctx).Preload("Contents").Find(&data)
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

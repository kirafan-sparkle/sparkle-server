package persistence

import (
	"context"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type userPresentRepositoryImpl struct {
	Conn *gorm.DB
}

func NewUserPresentRepositoryImpl(conn *gorm.DB) repository.UserPresentRepository {
	return &userPresentRepositoryImpl{Conn: conn}
}

func (r *userPresentRepositoryImpl) GetReceivableUserPresents(ctx context.Context, userId value_user.UserId) ([]*model_user.UserPresent, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetReceivableUserPresents")
	defer span.End()

	var presents []*model_user.UserPresent
	result := r.Conn.WithContext(ctx).Where("user_id = ?", userId).Where("received_at IS NULL").Preload("Present").Order("managed_present_id DESC").Limit(1000).Find(&presents)
	if result.Error != nil {
		return nil, result.Error
	}

	return presents, nil
}

func (r *userPresentRepositoryImpl) GetReceivedUserPresents(ctx context.Context, userId value_user.UserId) ([]*model_user.UserPresent, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetReceivedUserPresents")
	defer span.End()

	var presents []*model_user.UserPresent
	result := r.Conn.WithContext(ctx).Where("user_id = ?", userId).Where("received_at IS NOT NULL").Preload("Present").Order("managed_present_id DESC").Limit(100).Find(&presents)
	if result.Error != nil {
		return nil, result.Error
	}
	return presents, nil
}

func (r *userPresentRepositoryImpl) ReceiveUserPresents(ctx context.Context, userId value_user.UserId, managedPresentIds []int64) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "ReceiveUserPresents")
	defer span.End()

	result := r.Conn.WithContext(ctx).Model(&model_user.UserPresent{}).Where("user_id = ?", userId).Where("managed_present_id IN ?", managedPresentIds).Update("received_at", "CURRENT_TIMESTAMP")
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func (r *userPresentRepositoryImpl) DeleteExpiredUserPresents(ctx context.Context, userId value_user.UserId) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "DeleteExpiredUserPresents")
	defer span.End()

	result := r.Conn.WithContext(ctx).Where("user_id = ?", userId).Where("CURRENT_TIMESTAMP > deadline_at").Delete(&model_user.UserPresent{})
	if result.Error != nil {
		return result.Error
	}
	return nil
}

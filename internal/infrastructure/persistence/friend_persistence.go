package persistence

import (
	"context"
	"errors"
	"fmt"
	"strings"

	model_character "gitlab.com/kirafan/sparkle/server/internal/domain/model/character"
	model_friend "gitlab.com/kirafan/sparkle/server/internal/domain/model/friend"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gorm.io/gorm"
)

type friendRepositoryImpl struct {
	Conn *gorm.DB
}

func NewFriendRepositoryImpl(conn *gorm.DB) repository.FriendRepository {
	return &friendRepositoryImpl{Conn: conn}
}

func (qr *friendRepositoryImpl) IsAlreadyFriend(ctx context.Context, userId1 value_friend.PlayerId, userId2 value_friend.PlayerId) (bool, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "IsAlreadyFriend")
	defer span.End()
	var count int64
	result := qr.Conn.WithContext(ctx).Model(
		&model_friend.Friend{},
	).Where(
		"player_id1 = ? AND player_id2 = ?", userId1, userId2,
	).Or(
		"player_id1 = ? AND player_id2 = ?", userId2, userId1,
	).Count(&count)
	if result.Error != nil {
		return false, result.Error
	}
	return count > 0, nil
}

func (qr *friendRepositoryImpl) AddFriend(ctx context.Context, friend model_friend.Friend) (*value_friend.ManagedFriendId, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "AddFriend")
	defer span.End()

	result := qr.Conn.WithContext(ctx).Create(&friend)
	if result.Error != nil {
		return nil, result.Error
	}
	managedFriendId, err := value_friend.NewManagedFriendId(uint32(friend.ManagedFriendId))
	if err != nil {
		return nil, err
	}
	return managedFriendId, nil
}

func (qr *friendRepositoryImpl) DeleteFriend(ctx context.Context, managedFriendId value_friend.ManagedFriendId) error {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "DeleteFriend")
	defer span.End()

	result := qr.Conn.WithContext(ctx).Where("managed_friend_id = ?", managedFriendId).Delete(&model_friend.Friend{})
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func (qr *friendRepositoryImpl) GetFriendCount(ctx context.Context, userId value_friend.PlayerId) (int64, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetFriendCount")
	defer span.End()

	var count int64
	result := qr.Conn.WithContext(ctx).Model(
		&model_friend.Friend{},
	).Where(
		"player_id1 = ? OR player_id2 = ?", userId, userId,
	).Count(&count)
	if result.Error != nil {
		return 0, result.Error
	}
	return count, nil
}

func (qr *friendRepositoryImpl) fetchFriendRelations(ctx context.Context, internalUserId value_friend.PlayerId) ([]*model_friend.Friend, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "fetchFriendIds")
	defer span.End()

	var friends []*model_friend.Friend
	result := qr.Conn.WithContext(ctx).Where("player_id1 = ? OR player_id2 = ?", internalUserId, internalUserId).Find(&friends)
	if result.Error != nil {
		return nil, result.Error
	}

	// Friend relation can be swapped, so we need to find out which player is the friend
	// If the current user is not Player2, swap Player1 and Player2.
	// This ensures that Player1 is always the friend and Player2 is the current user.
	for _, friend := range friends {
		if friend.PlayerId2 == internalUserId {
			friend.PlayerId1, friend.PlayerId2 = friend.PlayerId2, friend.PlayerId1
		}
	}

	return friends, nil
}

func (qr *friendRepositoryImpl) fetchFriendUserInfosAsMap(ctx context.Context, friendPlayerIds []value_friend.PlayerId) (map[value_friend.PlayerId]*model_user.User, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "fetchFriendUserInfosAsMap")
	defer span.End()

	friendUsers := make([]*model_user.User, len(friendPlayerIds))
	result := qr.Conn.WithContext(ctx).Model(&model_user.User{}).Where("id IN (?)", friendPlayerIds).Find(&friendUsers)
	if result.Error != nil {
		return nil, result.Error
	}
	friendUserMap := make(map[value_friend.PlayerId]*model_user.User)
	for _, friendUser := range friendUsers {
		playerId, err := value_friend.NewPlayerId(friendUser.Id, value_friend.PlayerSpecialTypeDefault)
		if err != nil {
			return nil, err
		}
		friendUserMap[*playerId] = friendUser
	}

	return friendUserMap, nil
}

func (qr *friendRepositoryImpl) fetchFriendFavoriteMembersAsMap(ctx context.Context, friendPlayerIds []value_friend.PlayerId) (map[value_friend.PlayerId]*model_user.FavoriteMember, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "fetchFriendFavoriteMembers")
	defer span.End()

	// Fetch friend favorite members
	favoriteMembers := make([]*model_user.FavoriteMember, 0, len(friendPlayerIds)*8)
	result := qr.Conn.WithContext(ctx).Model(
		&model_user.FavoriteMember{},
	).Where("user_id IN (?)", friendPlayerIds).Where("favorite_index = 0").Find(&favoriteMembers)
	if result.Error != nil {
		return nil, result.Error
	}
	favoriteMemberMap := make(map[value_friend.PlayerId]*model_user.FavoriteMember)
	for _, favoriteMember := range favoriteMembers {
		playerId, err := value_friend.NewPlayerId(favoriteMember.UserId, value_friend.PlayerSpecialTypeDefault)
		if err != nil {
			return nil, err
		}
		favoriteMemberMap[*playerId] = favoriteMember
	}

	return favoriteMemberMap, nil
}

func (qr *friendRepositoryImpl) fetchFriendSupportPartiesAsMap(ctx context.Context, friendPlayerIds []value_friend.PlayerId) (map[value_friend.PlayerId]*model_user.SupportCharacter, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "fetchFriendSupportParties")
	defer span.End()

	// Fetch support parties
	supportParties := make([]*model_user.SupportCharacter, 0, len(friendPlayerIds)*8)
	result := qr.Conn.WithContext(ctx).Model(
		&model_user.SupportCharacter{},
	).Where("user_id IN (?)", friendPlayerIds).Where("active = ?", value.BoolLikeUIntTrue).Preload("ManagedCharacterIds").Preload("ManagedWeaponIds").Find(&supportParties)
	if result.Error != nil {
		return nil, result.Error
	}
	supportPartyMap := make(map[value_friend.PlayerId]*model_user.SupportCharacter)
	for _, supportParty := range supportParties {
		playerId, err := value_friend.NewPlayerId(supportParty.UserId, value_friend.PlayerSpecialTypeDefault)
		if err != nil {
			return nil, err
		}
		supportPartyMap[*playerId] = supportParty
	}

	return supportPartyMap, nil
}

func (qr *friendRepositoryImpl) formatSupportPartyMapAsAllManagedCharacterIds(ctx context.Context, supportPartyMap map[value_friend.PlayerId]*model_user.SupportCharacter) []value_user.ManagedCharacterId {
	_, span := observability.Tracer.StartPersistenceSpan(ctx, "formatSupportPartyMapAsManagedCharacterIds")
	defer span.End()

	managedCharacterIds := make([]value_user.ManagedCharacterId, 0, len(supportPartyMap)*8)
	for _, supportParty := range supportPartyMap {
		for _, supportCharacterManagedCharacterId := range supportParty.ManagedCharacterIds {
			managedCharacterIds = append(managedCharacterIds, supportCharacterManagedCharacterId.ManagedCharacterId)
		}
	}
	return managedCharacterIds
}

func (qr *friendRepositoryImpl) formatSupportPartyMapAsAllManagedWeaponIds(ctx context.Context, supportPartyMap map[value_friend.PlayerId]*model_user.SupportCharacter) []value_user.ManagedWeaponId {
	_, span := observability.Tracer.StartPersistenceSpan(ctx, "formatSupportPartyMapAsManagedWeaponIds")
	defer span.End()

	managedWeaponIds := make([]value_user.ManagedWeaponId, 0, len(supportPartyMap)*8)
	for _, supportParty := range supportPartyMap {
		for _, supportCharacterManagedCharacterId := range supportParty.ManagedWeaponIds {
			managedWeaponIds = append(managedWeaponIds, supportCharacterManagedCharacterId.ManagedWeaponId)
		}
	}
	return managedWeaponIds
}

func (qr *friendRepositoryImpl) formatSupportPartyMapAsManagedCharacterIdDict(ctx context.Context, supportPartyMap map[value_friend.PlayerId]*model_user.SupportCharacter) map[value_friend.PlayerId][]value_user.ManagedCharacterId {
	_, span := observability.Tracer.StartPersistenceSpan(ctx, "formatSupportPartyMapAsManagedCharacterIdDict")
	defer span.End()

	managedCharacterIdDict := make(map[value_friend.PlayerId][]value_user.ManagedCharacterId)
	for _, supportParty := range supportPartyMap {
		playerId, err := value_friend.NewPlayerId(supportParty.UserId, value_friend.PlayerSpecialTypeDefault)
		if err != nil {
			return nil
		}
		managedCharacterIdDict[*playerId] = make([]value_user.ManagedCharacterId, 0, 8)
		for _, supportCharacterManagedCharacterId := range supportParty.ManagedCharacterIds {
			managedCharacterIdDict[*playerId] = append(managedCharacterIdDict[*playerId], supportCharacterManagedCharacterId.ManagedCharacterId)
		}
	}
	return managedCharacterIdDict
}

func (qr *friendRepositoryImpl) formatSupportPartyMapAsManagedWeaponIdDict(ctx context.Context, supportPartyMap map[value_friend.PlayerId]*model_user.SupportCharacter) map[value_friend.PlayerId][]value_user.ManagedWeaponId {
	_, span := observability.Tracer.StartPersistenceSpan(ctx, "formatSupportPartyMapAsManagedWeaponIdDict")
	defer span.End()

	managedWeaponIdDict := make(map[value_friend.PlayerId][]value_user.ManagedWeaponId)
	for _, supportParty := range supportPartyMap {
		playerId, err := value_friend.NewPlayerId(supportParty.UserId, value_friend.PlayerSpecialTypeDefault)
		if err != nil {
			return nil
		}
		managedWeaponIds := make([]value_user.ManagedWeaponId, 0, 8)
		for _, supportCharacterManagedWeaponId := range supportParty.ManagedWeaponIds {
			managedWeaponIds = append(managedWeaponIds, supportCharacterManagedWeaponId.ManagedWeaponId)
		}
		managedWeaponIdDict[*playerId] = managedWeaponIds
	}
	return managedWeaponIdDict
}

func (qr *friendRepositoryImpl) fetchManagedCharactersAsMap(ctx context.Context, allManagedCharacterIds []value_user.ManagedCharacterId) (map[value_user.ManagedCharacterId]*model_user.ManagedCharacter, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "fetchManagedCharacters")
	defer span.End()

	// Fetch managed characters
	managedCharacters := make([]*model_user.ManagedCharacter, 0, len(allManagedCharacterIds))
	result := qr.Conn.WithContext(ctx).Model(&model_user.ManagedCharacter{}).Where("managed_character_id IN (?)", allManagedCharacterIds).Find(&managedCharacters)
	if result.Error != nil {
		return nil, result.Error
	}
	managedCharacterDict := make(map[value_user.ManagedCharacterId]*model_user.ManagedCharacter)
	for _, managedCharacter := range managedCharacters {
		managedCharacterDict[managedCharacter.ManagedCharacterId] = managedCharacter
	}

	// Insert dummy -1 for handle empty data slot
	managedCharacterDict[value_user.NewManagedCharacterId(-1)] = &model_user.ManagedCharacter{
		ManagedCharacterId: value_user.NewManagedCharacterId(-1),
		CharacterId:        value_character.CharacterId(0),
	}
	return managedCharacterDict, nil
}

func (qr *friendRepositoryImpl) fetchManagedWeaponsAsMap(ctx context.Context, allManagedWeaponIds []value_user.ManagedWeaponId) (map[value_user.ManagedWeaponId]*model_user.ManagedWeapon, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "fetchManagedWeapons")
	defer span.End()

	// Fetch managed weapons
	managedWeapons := make([]*model_user.ManagedWeapon, len(allManagedWeaponIds))
	result := qr.Conn.WithContext(ctx).Model(&model_user.ManagedWeapon{}).Where("managed_weapon_id IN (?)", allManagedWeaponIds).Find(&managedWeapons)
	if result.Error != nil {
		return nil, result.Error
	}
	managedWeaponDict := make(map[value_user.ManagedWeaponId]*model_user.ManagedWeapon)
	for _, managedWeapon := range managedWeapons {
		managedWeaponDict[managedWeapon.ManagedWeaponId] = managedWeapon
	}
	// Insert dummy -1 for handle empty data slot
	managedWeaponDict[value_user.ManagedWeaponId(-1)] = &model_user.ManagedWeapon{
		ManagedWeaponId: value_user.ManagedWeaponId(-1),
		WeaponId:        value_weapon.WeaponId(0),
	}
	return managedWeaponDict, nil
}

func (qr *friendRepositoryImpl) fetchNamedTypeIdMap(ctx context.Context, managedCharacterMap map[value_user.ManagedCharacterId]*model_user.ManagedCharacter) (map[value_user.ManagedCharacterId]uint16, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "formatManagedCharactersAsNamedTypeIds")
	defer span.End()

	// Format as characterIds
	characterIds := make([]value_character.CharacterId, len(managedCharacterMap))
	idx := 0
	for _, managedCharacter := range managedCharacterMap {
		characterIds[idx] = managedCharacter.CharacterId
		idx++
	}
	// Fetch named types
	var datas []*model_character.Character
	result := qr.Conn.WithContext(ctx).Model(&model_character.Character{}).Select("named_type").Where(
		"character_id IN (?)", characterIds,
	).Find(&datas)
	if result.Error != nil {
		return nil, result.Error
	}
	// Format as managedCharacterNamedTypeDict
	managedCharacterNamedTypeDict := make(map[value_user.ManagedCharacterId]uint16)
	for _, data := range datas {
		for managedCharacterId, managedCharacter := range managedCharacterMap {
			if value_character.CharacterId(data.CharacterId) != managedCharacter.CharacterId {
				continue
			}
			managedCharacterNamedTypeDict[managedCharacterId] = data.NamedType
		}
	}
	return managedCharacterNamedTypeDict, nil
}

func (qr *friendRepositoryImpl) fetchManagedCharacterNamedTypeLevels(
	ctx context.Context,
	managedCharacterIdDict map[value_friend.PlayerId][]value_user.ManagedCharacterId,
	namedTypeMap map[value_user.ManagedCharacterId]uint16,
) (map[value_user.ManagedCharacterId]uint8, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "fetchManagedCharacterNamedTypeLevels")
	defer span.End()
	// Define conditions
	var conditions []string
	var args []interface{}
	for playerId, managedCharacters := range managedCharacterIdDict {
		for _, managedCharacterId := range managedCharacters {
			namedType := namedTypeMap[managedCharacterId]
			conditions = append(conditions, "(user_id = ? AND named_type = ?)")
			args = append(args, playerId, namedType)
		}
	}
	conditionString := strings.Join(conditions, " OR ")
	// Fetch named type levels
	var datas []*model_user.ManagedNamedType
	result := qr.Conn.WithContext(ctx).Model(&model_user.ManagedNamedType{}).Where(
		conditionString, args...,
	).Select("user_id", "named_type", "level").Find(&datas)
	if result.Error != nil {
		return nil, result.Error
	}
	// Format as managedCharacterIdDict
	managedCharacterIdLevelDict := make(map[value_user.ManagedCharacterId]uint8)
	for playerId, managedCharacters := range managedCharacterIdDict {
		for _, managedCharacterId := range managedCharacters {
			namedType := namedTypeMap[managedCharacterId]
			for _, data := range datas {
				if value_friend.PlayerId(data.UserId) == playerId && data.NamedType == namedType {
					managedCharacterIdLevelDict[managedCharacterId] = uint8(data.Level)
					break
				}
			}
		}
	}
	managedCharacterIdLevelDict[value_user.ManagedCharacterId(-1)] = 0
	return managedCharacterIdLevelDict, nil
}

func (qr *friendRepositoryImpl) combineFriendDataAsFriendFull(
	ctx context.Context,
	friends []*model_friend.Friend,
	friendUserMap map[value_friend.PlayerId]*model_user.User,
	favoriteMemberMap map[value_friend.PlayerId]*model_user.FavoriteMember,
	supportPartyMemberMap map[value_friend.PlayerId]*model_user.SupportCharacter,
	managedCharacterIdDict map[value_friend.PlayerId][]value_user.ManagedCharacterId,
	managedWeaponIdDict map[value_friend.PlayerId][]value_user.ManagedWeaponId,
	managedCharacterDict map[value_user.ManagedCharacterId]*model_user.ManagedCharacter,
	managedWeaponDict map[value_user.ManagedWeaponId]*model_user.ManagedWeapon,
	namedIdTypeMap map[value_user.ManagedCharacterId]uint16,
	managedCharacterFriendShipMap map[value_user.ManagedCharacterId]uint8,
) ([]*model_friend.FriendFull, error) {
	_, span := observability.Tracer.StartPersistenceSpan(ctx, "formatFriendDataAsUser")
	defer span.End()

	// Assign support parties to users
	outFriends := make([]*model_friend.FriendFull, len(friendUserMap))
	for fidx, friend := range friends {
		friendPlayerId := friend.PlayerId2
		// Get base infos from dict
		friendUser := friendUserMap[friendPlayerId]
		if friendUser == nil {
			return nil, fmt.Errorf("friend user for for playerId: %v was not found", friendPlayerId)
		}
		favoriteMember := favoriteMemberMap[friendPlayerId]
		if favoriteMember == nil {
			return nil, fmt.Errorf("favorite member for playerId: %v was not found", friendPlayerId)
		}
		supportParty := supportPartyMemberMap[friendPlayerId]
		if supportParty == nil {
			return nil, fmt.Errorf("support party for playerId:%v was not found", friendPlayerId)
		}
		// Get managed weapons from dict
		managedWeaponIds := managedWeaponIdDict[friendPlayerId]
		if managedWeaponIds == nil {
			return nil, fmt.Errorf("managed weapon ids for playerId: %v was not found", friendPlayerId)
		}
		managedWeapons := make([]model_user.ManagedWeapon, len(managedWeaponIds))
		for i, managedWeaponId := range managedWeaponIds {
			managedWeapon := managedWeaponDict[managedWeaponId]
			if managedWeapon == nil {
				return nil, fmt.Errorf("managed weapon for id: %v was not found", managedWeaponId)
			}
			managedWeapons[i] = *managedWeapon
		}
		// Get managed characters from dict
		managedCharacterIds := managedCharacterIdDict[friendPlayerId]
		if managedCharacterIds == nil {
			return nil, fmt.Errorf("managed character ids for playerId: %v was not found", friendPlayerId)
		}
		managedCharacters := make([]model_user.ManagedCharacter, len(managedCharacterIds))
		for i, managedCharacterId := range managedCharacterIds {
			managedCharacter := managedCharacterDict[managedCharacterId]
			if managedCharacter == nil {
				return nil, fmt.Errorf("managed character for id: %v was not found", managedCharacterId)
			}
			managedCharacters[i] = *managedCharacter
		}
		// Get managed named types from dict
		managedNamedTypes := make([]model_user.ManagedNamedType, len(managedCharacterIds))
		for i, managedCharacterId := range managedCharacterIds {
			namedType := namedIdTypeMap[managedCharacterId]
			if namedType > 255 {
				return nil, fmt.Errorf("managed named type for managedCharacterId: %v was not found", managedCharacterId)
			}
			friendShipLevel := managedCharacterFriendShipMap[managedCharacterId]
			managedNamedTypes[i] = model_user.ManagedNamedType{
				NamedType: namedType,
				Level:     friendShipLevel,
			}
		}
		if len(managedCharacters) != len(managedWeapons) || len(managedWeapons) != len(managedNamedTypes) {
			return nil, errors.New("managed characters, weapons and named types are not same length")
		}
		// Format as favorite member
		outFavoriteMember := model_friend.FriendSupportCharacter{
			CharacterId:  favoriteMember.CharacterId,
			ArousalLevel: int8(favoriteMember.ArousalLevel),
			// They used only as icon so below fields are unused
			ManagedCharacterId: 0,
			Level:              0,
			Exp:                0,
			LevelBreak:         0,
			SkillLevel1:        0,
			SkillLevel2:        0,
			SkillLevel3:        0,
			AbilityBoardId:     -1,
			EquipItemIds:       make([]value_item.ItemId, 0),
			DuplicatedCount:    0,
			NamedLevel:         0,
			NamedExp:           0,
			WeaponId:           0,
			WeaponLevel:        0,
			WeaponSkillLevel:   0,
			WeaponSkillExp:     0,
		}
		// Format as support characters
		outSupportCharacters := make([]model_friend.FriendSupportCharacter, 0, 8)
		for i := range supportParty.ManagedCharacterIds {
			if managedCharacters[i].CharacterId == 0 {
				continue
			}
			// Fulfill character fields
			supportCharacter := model_friend.FriendSupportCharacter{
				ManagedCharacterId: int(supportParty.ManagedCharacterIds[i].ManagedCharacterId),
				CharacterId:        managedCharacters[i].CharacterId,
				Level:              uint8(managedCharacters[i].Level),
				Exp:                int64(managedCharacters[i].Exp),
				LevelBreak:         managedCharacters[i].LevelBreak,
				SkillLevel1:        managedCharacters[i].SkillLevel1,
				SkillLevel2:        managedCharacters[i].SkillLevel2,
				SkillLevel3:        managedCharacters[i].SkillLevel3,
				ArousalLevel:       int8(managedCharacters[i].ArousalLevel),
				DuplicatedCount:    int8(managedCharacters[i].DuplicatedCount),
				NamedLevel:         managedNamedTypes[i].Level,
				NamedExp:           0,
				AbilityBoardId:     -1,
				EquipItemIds:       make([]value_item.ItemId, 0),
			}
			// Fulfill weapon fields
			if managedWeapons[i].WeaponId != 0 {
				supportCharacter.WeaponId = int32(managedWeapons[i].WeaponId)
				supportCharacter.WeaponLevel = managedWeapons[i].Level
				supportCharacter.WeaponSkillLevel = managedWeapons[i].SkillLevel
				supportCharacter.WeaponSkillExp = uint64(managedWeapons[i].SkillExp)
			}
			outSupportCharacters = append(outSupportCharacters, supportCharacter)
		}
		// Format the out friend model
		outFriends[fidx] = &model_friend.FriendFull{
			ManagedFriendId:      friend.ManagedFriendId,
			PlayerId:             value_friend.PlayerId(friendUser.Id),
			State:                value_friend.FriendStateFriend,
			Direction:            value_friend.FriendDirectionOtherToSelf,
			Name:                 friendUser.Name,
			MyCode:               friendUser.MyCode,
			Comment:              friendUser.Comment,
			CurrentAchievementId: int64(friendUser.CurrentAchievementId),
			Level:                friendUser.Level,
			LastLoginAt:          friendUser.LastLoginAt,
			TotalExp:             uint8(friendUser.TotalExp),
			SupportCharacters:    outSupportCharacters,
			FirstFavoriteMember:  outFavoriteMember,
			SupportName:          supportParty.Name,
			// Unneeded values...
			SupportLimit: 8,
			NamedTypes:   nil,
		}
	}

	return outFriends, nil
}

func (qr *friendRepositoryImpl) GetFriends(ctx context.Context, internalUserId value_friend.PlayerId) ([]*model_friend.FriendFull, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetFriends")
	defer span.End()

	// Fetch friends
	friends, err := qr.fetchFriendRelations(ctx, internalUserId)
	if err != nil {
		return nil, err
	}
	friendPlayerIds := make([]value_friend.PlayerId, len(friends))
	for i, friend := range friends {
		friendPlayerIds[i] = friend.PlayerId2
	}

	// Fetch friend user infos
	friendUserMap, err := qr.fetchFriendUserInfosAsMap(ctx, friendPlayerIds)
	if err != nil {
		return nil, err
	}
	favoriteMemberMap, err := qr.fetchFriendFavoriteMembersAsMap(ctx, friendPlayerIds)
	if err != nil {
		return nil, err
	}
	supportPartyMemberMap, err := qr.fetchFriendSupportPartiesAsMap(ctx, friendPlayerIds)
	if err != nil {
		return nil, err
	}

	// Map the support parties
	allManagedCharacterIds := qr.formatSupportPartyMapAsAllManagedCharacterIds(ctx, supportPartyMemberMap)
	allManagedWeaponIds := qr.formatSupportPartyMapAsAllManagedWeaponIds(ctx, supportPartyMemberMap)
	managedCharacterNamedTypeLevelDict := qr.formatSupportPartyMapAsManagedCharacterIdDict(ctx, supportPartyMemberMap)
	if managedCharacterNamedTypeLevelDict == nil {
		return nil, errors.New("managedCharacterIdDict is nil")
	}
	managedWeaponIdDict := qr.formatSupportPartyMapAsManagedWeaponIdDict(ctx, supportPartyMemberMap)
	if managedWeaponIdDict == nil {
		return nil, errors.New("managedWeaponIdDict is nil")
	}

	// Fetch managed characters and weapons
	managedCharacterDict, err := qr.fetchManagedCharactersAsMap(ctx, allManagedCharacterIds)
	if err != nil {
		return nil, err
	}
	managedWeaponDict, err := qr.fetchManagedWeaponsAsMap(ctx, allManagedWeaponIds)
	if err != nil {
		return nil, err
	}

	// Fetch friendship data
	namedTypeMap, err := qr.fetchNamedTypeIdMap(ctx, managedCharacterDict)
	if err != nil {
		return nil, err
	}
	characterFriendShipMap, err := qr.fetchManagedCharacterNamedTypeLevels(ctx, managedCharacterNamedTypeLevelDict, namedTypeMap)
	if err != nil {
		return nil, err
	}

	// Assign support parties to users
	outFriends, err := qr.combineFriendDataAsFriendFull(
		ctx,
		friends,
		friendUserMap,
		favoriteMemberMap,
		supportPartyMemberMap,
		managedCharacterNamedTypeLevelDict,
		managedWeaponIdDict,
		managedCharacterDict,
		managedWeaponDict,
		namedTypeMap,
		characterFriendShipMap,
	)
	if err != nil {
		return nil, err
	}
	return outFriends, nil
}

func (qr *friendRepositoryImpl) GetFriend(ctx context.Context, friendId value_friend.ManagedFriendId) (*model_friend.Friend, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetFriend")
	defer span.End()
	var friend *model_friend.Friend
	result := qr.Conn.WithContext(ctx).Where("managed_friend_id = ?", friendId).Find(&friend)
	if result.Error != nil {
		return nil, result.Error
	}
	return friend, nil
}

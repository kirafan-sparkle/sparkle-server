package persistence

import (
	"context"

	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_evolution "gitlab.com/kirafan/sparkle/server/internal/domain/value/evolution"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type evoTableEvolutionRepositoryImpl struct {
	Conn *gorm.DB
}

func NewEvoTableEvolutionRepositoryImpl(conn *gorm.DB) repository.EvoTableEvolutionRepository {
	return &evoTableEvolutionRepositoryImpl{Conn: conn}
}

func (rp *evoTableEvolutionRepositoryImpl) FindByCharacterId(ctx context.Context, characterId value_character.CharacterId) (*model_evo_table.EvoTableEvolution, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindByCharacterId")
	defer span.End()
	var data *model_evo_table.EvoTableEvolution
	if result := rp.Conn.WithContext(ctx).Preload("RequiredItems").Where(&model_evo_table.EvoTableEvolution{
		SrcCharacterId: characterId,
		// TODO: Implement EvolutionRecipeTypeTitleItem and check client works or not
		RecipeType: value_evolution.EvolutionRecipeTypeDefault,
	}).Find(&data); result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

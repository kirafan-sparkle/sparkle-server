package persistence

import (
	"context"

	model_friend "gitlab.com/kirafan/sparkle/server/internal/domain/model/friend"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gorm.io/gorm"
)

type friendRequestRepositoryImpl struct {
	Conn *gorm.DB
}

func NewFriendRequestRepositoryImpl(conn *gorm.DB) repository.FriendRequestRepository {
	return &friendRequestRepositoryImpl{Conn: conn}
}

func (qr *friendRequestRepositoryImpl) IsExistFriendRequest(ctx context.Context, userId1 value_friend.PlayerId, userId2 value_friend.PlayerId) (bool, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "IsExistFriendRequest")
	defer span.End()
	var count int64
	result := qr.Conn.WithContext(ctx).Model(
		&model_friend.FriendRequest{},
	).Where(
		"player_id1 = ? AND player_id2 = ?", userId1, userId2,
	).Count(&count)
	if result.Error != nil {
		return false, result.Error
	}
	return count > 0, nil
}

func (qr *friendRequestRepositoryImpl) AddFriendRequest(ctx context.Context, friendRequest model_friend.FriendRequest) (*value_friend.ManagedFriendId, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "AddFriendRequest")
	defer span.End()
	result := qr.Conn.WithContext(ctx).Create(&friendRequest)
	if result.Error != nil {
		return nil, result.Error
	}
	managedFriendId, err := value_friend.NewManagedFriendId(uint32(friendRequest.ManagedFriendId))
	if err != nil {
		return nil, err
	}
	return managedFriendId, nil
}

func (qr *friendRequestRepositoryImpl) GetFriendRequest(ctx context.Context, managedFriendId value_friend.ManagedFriendId) (*model_friend.FriendRequest, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetFriendRequest")
	defer span.End()
	var friendRequest *model_friend.FriendRequest
	result := qr.Conn.WithContext(ctx).Where("managed_friend_id = ?", managedFriendId).Find(&friendRequest)
	if result.Error != nil {
		return nil, result.Error
	}
	return friendRequest, nil
}

func (qr *friendRequestRepositoryImpl) GetOutGoingFriendRequests(ctx context.Context, userId value_friend.PlayerId) ([]*model_friend.FriendRequest, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetOutGoingFriendRequests")
	defer span.End()
	var friendRequests []*model_friend.FriendRequest
	result := qr.Conn.WithContext(ctx).Where("player_id1 = ?", userId).Where("state = ?", value_friend.FriendStateWaitingApprove).Preload("Player2").Find(&friendRequests)
	if result.Error != nil {
		return nil, result.Error
	}
	return friendRequests, nil
}

func (qr *friendRequestRepositoryImpl) GetInComingFriendRequests(ctx context.Context, userId value_friend.PlayerId) ([]*model_friend.FriendRequest, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetInComingFriendRequests")
	defer span.End()
	var friendRequests []*model_friend.FriendRequest
	result := qr.Conn.WithContext(ctx).Where("player_id2 = ?", userId).Where("state = ?", value_friend.FriendStateWaitingApprove).Preload("Player1").Find(&friendRequests)
	if result.Error != nil {
		return nil, result.Error
	}
	return friendRequests, nil
}

func (qr *friendRequestRepositoryImpl) DeleteFriendRequest(ctx context.Context, managedFriendId value_friend.ManagedFriendId) error {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "DeleteFriendRequest")
	defer span.End()
	result := qr.Conn.WithContext(ctx).Where("managed_friend_id = ?", managedFriendId).Delete(&model_friend.FriendRequest{})
	if result.Error != nil {
		return result.Error
	}
	return nil
}

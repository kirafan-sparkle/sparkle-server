package persistence

import (
	"context"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type expTableSkillRepositoryImpl struct {
	Conn *gorm.DB
}

func NewExpTableSkillRepositoryImpl(conn *gorm.DB) repository.ExpTableSkillRepository {
	return &expTableSkillRepositoryImpl{Conn: conn}
}

func (rp *expTableSkillRepositoryImpl) FindExpTableSkills(ctx context.Context, query *model_exp_table.ExpTableSkill, criteria map[string]interface{}) ([]*model_exp_table.ExpTableSkill, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindExpTableSkills")
	defer span.End()
	var datas []*model_exp_table.ExpTableSkill
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *expTableSkillRepositoryImpl) FindExpTableSkill(ctx context.Context, query *model_exp_table.ExpTableSkill, criteria map[string]interface{}) (*model_exp_table.ExpTableSkill, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindExpTableSkill")
	defer span.End()
	var data *model_exp_table.ExpTableSkill
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

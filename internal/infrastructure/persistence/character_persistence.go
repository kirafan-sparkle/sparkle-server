package persistence

import (
	"context"

	model_character "gitlab.com/kirafan/sparkle/server/internal/domain/model/character"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type characterRepositoryImpl struct {
	Conn *gorm.DB
}

func NewCharacterRepositoryImpl(conn *gorm.DB) repository.CharacterRepository {
	return &characterRepositoryImpl{Conn: conn}
}

func (rp *characterRepositoryImpl) FindCharacters(ctx context.Context, query *model_character.Character, criteria map[string]interface{}, associations *[]string) ([]*model_character.Character, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindCharacters")
	defer span.End()
	var datas []*model_character.Character
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		result = chain.Where(criteria).Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *characterRepositoryImpl) FindCharacter(ctx context.Context, query *model_character.Character, criteria map[string]interface{}, associations *[]string) (*model_character.Character, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindCharacter")
	defer span.End()
	var data *model_character.Character
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		result = chain.Where(criteria).First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

func (rp *characterRepositoryImpl) GetNamedTypesByIds(ctx context.Context, characterIds []value_character.CharacterId) ([]uint16, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindCharacter")
	defer span.End()

	var datas []*model_character.Character
	result := rp.Conn.WithContext(ctx).Model(&model_character.Character{}).Select("named_type").Where(
		"character_id IN (?)", characterIds,
	).Find(&datas)
	if result.Error != nil {
		return nil, result.Error
	}

	namedTypes := make([]uint16, len(datas))
	for i := range datas {
		namedTypes[i] = datas[i].NamedType
	}
	return namedTypes, nil
}

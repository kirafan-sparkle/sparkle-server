package persistence

import (
	"context"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type characterQuestRepositoryImpl struct {
	Conn *gorm.DB
}

func NewCharacterQuestRepositoryImpl(conn *gorm.DB) repository.CharacterQuestRepository {
	return &characterQuestRepositoryImpl{Conn: conn}
}

func (qr *characterQuestRepositoryImpl) GetCharacterQuest(ctx context.Context, internalUserId value_user.UserId, characterQuestId uint) (*model_quest.CharacterQuest, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetCharacterQuest")
	defer span.End()
	var characterQuest *model_quest.CharacterQuest
	result := qr.Conn.WithContext(ctx).Preload("Quest").Where("id = ?", characterQuestId).Find(&characterQuest)
	if result.Error != nil {
		return nil, result.Error
	}
	return characterQuest, nil
}

func (qr *characterQuestRepositoryImpl) GetCharacterQuests(ctx context.Context, internalUserId value_user.UserId) ([]model_quest.CharacterQuest, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetCharacterQuests")
	defer span.End()
	var characterQuests []model_quest.CharacterQuest
	result := qr.Conn.WithContext(ctx).Preload("Quest").Find(&characterQuests)
	if result.Error != nil {
		return nil, result.Error
	}
	return characterQuests, nil
}

package persistence

import (
	"context"

	model_ability_board "gitlab.com/kirafan/sparkle/server/internal/domain/model/ability_board"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_ability_board "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_board"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type abilityBoardRepositoryImpl struct {
	Conn *gorm.DB
}

func NewAbilityBoardRepositoryImpl(conn *gorm.DB) repository.AbilityBoardRepository {
	return &abilityBoardRepositoryImpl{Conn: conn}
}

func (rp *abilityBoardRepositoryImpl) FindAbilityBoardByItemId(ctx context.Context, itemId value_item.ItemId) (*model_ability_board.AbilityBoard, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindAbilityBoardByItemId")
	defer span.End()

	var abilityBoard *model_ability_board.AbilityBoard
	result := rp.Conn.WithContext(ctx).Where("item_id = ?", itemId).Find(&abilityBoard)
	if result.Error != nil {
		return nil, result.Error
	}
	return abilityBoard, nil
}

func (rp *abilityBoardRepositoryImpl) FindAbilityBoard(ctx context.Context, abilityBoardId value_ability_board.AbilityBoardId) (*model_ability_board.AbilityBoard, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindAbilityBoard")
	defer span.End()

	var abilityBoard *model_ability_board.AbilityBoard
	result := rp.Conn.WithContext(ctx).Where("ability_board_id = ?", abilityBoardId).Find(&abilityBoard)
	if result.Error != nil {
		return nil, result.Error
	}
	return abilityBoard, nil
}

func (rp *abilityBoardRepositoryImpl) FindAbilityBoardSlot(
	ctx context.Context,
	abilityBoardId value_ability_board.AbilityBoardId,
	abilityBoardSlotIndex value_ability_board.AbilityBoardSlotIndex,
) (*model_ability_board.AbilityBoardSlot, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindAbilityBoardSlot")
	defer span.End()

	var abilityBoardSlot *model_ability_board.AbilityBoardSlot
	result := rp.Conn.WithContext(ctx).Where("ability_board_id = ?", abilityBoardId).Where("slot_index = ?", abilityBoardSlotIndex).Find(&abilityBoardSlot)
	if result.Error != nil {
		return nil, result.Error
	}
	return abilityBoardSlot, nil
}

func (rp *abilityBoardRepositoryImpl) FindAbilityBoardSlotRecipe(ctx context.Context, abilityBoardSlotRecipeId value_ability_board.AbilityBoardSlotRecipeId) (*model_ability_board.AbilityBoardSlotRecipe, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindAbilityBoardSlotRecipe")
	defer span.End()

	var abilityBoardSlotRecipe *model_ability_board.AbilityBoardSlotRecipe
	result := rp.Conn.WithContext(ctx).Where("ability_board_slot_recipe_id = ?", abilityBoardSlotRecipeId).Find(&abilityBoardSlotRecipe)
	if result.Error != nil {
		return nil, result.Error
	}
	return abilityBoardSlotRecipe, nil
}

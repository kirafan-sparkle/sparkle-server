package persistence

import (
	"context"

	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type weaponRecipeRepositoryImpl struct {
	Conn *gorm.DB
}

func NewWeaponRecipeRepositoryImpl(conn *gorm.DB) repository.WeaponRecipeRepository {
	return &weaponRecipeRepositoryImpl{Conn: conn}
}

func (rp *weaponRecipeRepositoryImpl) FindByRecipeId(ctx context.Context, recipeId uint32) (*model_weapon.WeaponRecipe, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindByRecipeId")
	defer span.End()
	var data *model_weapon.WeaponRecipe
	result := rp.Conn.WithContext(ctx).Preload("RecipeMaterials").Where("recipe_id = ?", recipeId).First(&data)
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

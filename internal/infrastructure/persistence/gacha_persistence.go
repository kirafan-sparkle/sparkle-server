package persistence

import (
	"context"

	model_gacha "gitlab.com/kirafan/sparkle/server/internal/domain/model/gacha"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type gachaRepositoryImpl struct {
	Conn *gorm.DB
}

func NewGachaRepositoryImpl(conn *gorm.DB) repository.GachaRepository {
	return &gachaRepositoryImpl{Conn: conn}
}

func (rp *gachaRepositoryImpl) FindGachas(ctx context.Context, query *model_gacha.Gacha, criteria map[string]interface{}, associations *[]string) ([]*model_gacha.Gacha, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindGachas")
	defer span.End()
	var datas []*model_gacha.Gacha
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *gachaRepositoryImpl) FindGacha(ctx context.Context, query *model_gacha.Gacha, criteria map[string]interface{}, associations *[]string) (*model_gacha.Gacha, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindGacha")
	defer span.End()
	var data *model_gacha.Gacha
	var result *gorm.DB
	chain := rp.Conn.WithContext(ctx)
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		result = chain.Where(criteria).First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

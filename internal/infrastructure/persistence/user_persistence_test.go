package persistence_test

import (
	"context"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"github.com/lithammer/shortuuid/v3"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/seed"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
	"gitlab.com/kirafan/sparkle/server/pkg/testutil"
)

func getUserInstanceWithModify(ctx context.Context, ur repository.UserRepository, setter func(ur repository.UserRepository, user *model_user.User)) (*model_user.User, error) {
	newUser := model_user.NewUser(strings.ToUpper(shortuuid.New())[:10], "test")
	user, err := ur.CreateUser(ctx, &newUser)
	if err != nil {
		return nil, err
	}
	setter(ur, user)
	return user, nil
}

func Test_userRepositoryImpl_UpdateUser_ItemSummary(t *testing.T) {
	db := testutil.GetInMemoryDatabase()
	seedOption := seed.NewEmptySeedOption()
	seedOption.Users = true
	seedOption.Items = true
	testutil.InitInMemoryDatabase(db, seedOption)

	ur := persistence.NewUserRepositoryImpl(db)
	ctx := context.Background()

	opts := []cmp.Option{
		cmpopts.IgnoreFields(model_user.ItemSummary{}, "ItemSummaryId", "UserId", "CreatedAt", "UpdatedAt"),
	}

	tests := []struct {
		name  string
		mod   func(ur repository.UserRepository, user *model_user.User)
		param repository.UserRepositoryParam
		want  []model_user.ItemSummary
	}{
		{
			name: "insert item success with entire",
			param: repository.UserRepositoryParam{
				Entire: true,
			},
			mod: func(ur repository.UserRepository, u *model_user.User) {
				u.ItemSummary = []model_user.ItemSummary{
					{
						Id:     1,
						Amount: 10,
					},
				}
			},
			want: []model_user.ItemSummary{
				{
					Id:     1,
					Amount: 10,
				},
			},
		},
		{
			name: "insert, get, and insert success with entire",
			param: repository.UserRepositoryParam{
				Entire: true,
			},
			mod: func(ur repository.UserRepository, u *model_user.User) {
				u.ItemSummary = []model_user.ItemSummary{
					{
						Id:     1,
						Amount: 10,
					},
				}
				u, err := ur.UpdateUser(ctx, u, repository.UserRepositoryParam{Entire: true})
				if err != nil {
					t.Errorf("userRepositoryImpl.UpdateUser() error = %v", err)
					return
				}
				u.ItemSummary = append(u.ItemSummary, model_user.ItemSummary{
					Id:     2,
					Amount: 20,
				})
			},
			want: []model_user.ItemSummary{
				{
					Id:     1,
					Amount: 10,
				},
				{
					Id:     2,
					Amount: 20,
				},
			},
		},
		{
			name: "insert item success with only",
			param: repository.UserRepositoryParam{
				ItemSummary: true,
			},
			mod: func(ur repository.UserRepository, u *model_user.User) {
				u.ItemSummary = []model_user.ItemSummary{
					{
						Id:     1,
						Amount: 10,
						UserId: u.Id,
					},
				}
			},
			want: []model_user.ItemSummary{
				{
					Id:     1,
					Amount: 10,
				},
			},
		},
		{
			name: "insert, get, and insert success with only",
			param: repository.UserRepositoryParam{
				ItemSummary: true,
			},
			mod: func(ur repository.UserRepository, u *model_user.User) {
				u.ItemSummary = []model_user.ItemSummary{
					{
						Id:     1,
						Amount: 10,
						UserId: u.Id,
					},
				}
				u, err := ur.UpdateUser(ctx, u, repository.UserRepositoryParam{Entire: true})
				if err != nil {
					t.Errorf("userRepositoryImpl.UpdateUser() error = %v", err)
					return
				}
				u.ItemSummary = append(u.ItemSummary, model_user.ItemSummary{
					Id:     2,
					Amount: 20,
					UserId: u.Id,
				})
			},
			want: []model_user.ItemSummary{
				{
					Id:     1,
					Amount: 10,
				},
				{
					Id:     2,
					Amount: 20,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			user, err := getUserInstanceWithModify(ctx, ur, tt.mod)
			if err != nil {
				t.Errorf("userRepositoryImpl.CreateUser() error = %v", err)
				return
			}
			_, err = ur.UpdateUser(ctx, user, tt.param)
			if err != nil {
				t.Errorf("userRepositoryImpl.UpdateUser() error = %v", err)
				return
			}
			recv, err := ur.FindUserById(ctx, user.Id, repository.UserRepositoryParam{ItemSummary: true})
			if err != nil {
				t.Errorf("userRepositoryImpl.FindUser() error = %v", err)
			}

			for i := range tt.want {
				if cmp.Equal(&recv.ItemSummary[i], &tt.want[i], opts...) != true {
					t.Errorf("userRepositoryImpl.UpdateUser() Diff = %+v", cmp.Diff(&recv.ItemSummary[i], tt.want[i], opts...))
				}
			}
		})
	}
}

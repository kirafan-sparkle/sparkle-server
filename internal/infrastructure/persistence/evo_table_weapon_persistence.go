package persistence

import (
	"context"

	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type evoTableWeaponRepositoryImpl struct {
	Conn *gorm.DB
}

func NewEvoTableWeaponRepositoryImpl(conn *gorm.DB) repository.EvoTableWeaponRepository {
	return &evoTableWeaponRepositoryImpl{Conn: conn}
}

func (rp *evoTableWeaponRepositoryImpl) FindByWeaponId(ctx context.Context, weaponId value_weapon.WeaponId) (*model_evo_table.EvoTableWeapon, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindByWeaponId")
	defer span.End()
	var data *model_evo_table.EvoTableWeapon
	if result := rp.Conn.WithContext(ctx).Preload("RecipeMaterials").Where(&model_evo_table.EvoTableWeapon{
		SrcWeaponId: weaponId,
	}).Find(&data); result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

package persistence

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"net/http"

	model_schedule "gitlab.com/kirafan/sparkle/server/internal/domain/model/schedule"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
)

type scheduleRepositoryImpl struct {
	endpoint env.ScheduleEndpoint
}

var ErrRefreshScheduleInvalidParamLength = errors.New("characterIds and schedules must have same length")

type ScheduleCodeResponse uint16

const (
	ScheduleCodeResponseSuccess ScheduleCodeResponse = 200
	ScheduleCodeResponseError   ScheduleCodeResponse = 400
)

// NOTE: These request types depends sub server implementation. So intentionally not to placed domain model.
type CreateScheduleRequest struct {
	CharacterIds []value_character.CharacterId `json:"chara_ids"`
	GridData     string                        `json:"town_data"`
	CreateEmpty  bool                          `json:"create_empty"`
}
type CreateScheduleResponse struct {
	StatusCode int                                                     `json:"status_code"`
	Message    string                                                  `json:"message"`
	Schedules  map[value_character.CharacterId]model_schedule.Schedule `json:"schedules"`
}

type UpdatePlayTimeRequest []string
type UpdatePlayTimeResponse struct {
	StatusCode int      `json:"status_code"`
	Message    string   `json:"message"`
	Schedules  []string `json:"schedule_tables"`
}

func NewScheduleRepositoryImpl(endpoint env.ScheduleEndpoint) repository.ScheduleRepository {
	return &scheduleRepositoryImpl{endpoint}
}

func (rp *scheduleRepositoryImpl) Create(ctx context.Context, characterIds []value_character.CharacterId, gridData string) ([]*model_schedule.Schedule, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "Create")
	defer span.End()
	// Create request
	reqData := CreateScheduleRequest{
		CharacterIds: characterIds,
		GridData:     gridData,
		CreateEmpty:  false,
	}
	jsonData, err := json.Marshal(reqData)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", string(rp.endpoint)+"/create_schedule", bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, err
	}
	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	// Parse response
	var scheduleResp CreateScheduleResponse
	if err := json.NewDecoder(resp.Body).Decode(&scheduleResp); err != nil {
		return nil, err
	}
	if scheduleResp.StatusCode != int(ScheduleCodeResponseSuccess) {
		return nil, errors.New(scheduleResp.Message)
	}
	// Convert response to domain model
	var datas []*model_schedule.Schedule
	for _, schedule := range scheduleResp.Schedules {
		datas = append(datas, &model_schedule.Schedule{
			ScheduleTable: schedule.ScheduleTable,
			DropItems:     schedule.DropItems,
		})
	}
	return datas, nil
}

func (rp *scheduleRepositoryImpl) Refresh(ctx context.Context, characterSchedules []string) ([]string, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "Refresh")
	defer span.End()
	// Create request
	jsonData, err := json.Marshal(characterSchedules)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", string(rp.endpoint)+"/update_playtime", bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, err
	}
	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	// Parse response
	var scheduleResp UpdatePlayTimeResponse
	if err := json.NewDecoder(resp.Body).Decode(&scheduleResp); err != nil {
		return nil, err
	}
	if scheduleResp.StatusCode != int(ScheduleCodeResponseSuccess) {
		return nil, errors.New(scheduleResp.Message)
	}
	return scheduleResp.Schedules, nil
}

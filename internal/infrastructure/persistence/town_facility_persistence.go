package persistence

import (
	"context"

	model_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/model/town_facility"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type townFacilityRepositoryImpl struct {
	Conn *gorm.DB
}

func NewTownFacilityRepositoryImpl(conn *gorm.DB) repository.TownFacilityRepository {
	return &townFacilityRepositoryImpl{Conn: conn}
}

func (rp *townFacilityRepositoryImpl) FindTownFacility(ctx context.Context, facilityId uint32) (*model_town_facility.TownFacility, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindTownFacility")
	defer span.End()
	var data *model_town_facility.TownFacility
	result := rp.Conn.WithContext(ctx).Where("facility_id = ?", facilityId).First(&data)
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}

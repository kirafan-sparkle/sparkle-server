package persistence

import (
	"context"

	model_training "gitlab.com/kirafan/sparkle/server/internal/domain/model/training"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type trainingRepositoryImpl struct {
	Conn *gorm.DB
}

func NewTrainingRepositoryImpl(conn *gorm.DB) repository.TrainingRepository {
	return &trainingRepositoryImpl{Conn: conn}
}

func (qr *trainingRepositoryImpl) GetTraining(ctx context.Context, trainingId uint) (*model_training.Training, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetTraining")
	defer span.End()
	var training *model_training.Training
	result := qr.Conn.WithContext(ctx).Preload("Rewards").Where("training_id = ?", trainingId).Find(&training)
	if result.Error != nil {
		return nil, result.Error
	}
	return training, nil
}

func (qr *trainingRepositoryImpl) GetTrainings(ctx context.Context, categoryId uint8) ([]*model_training.Training, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "GetTrainings")
	defer span.End()
	var trainings []*model_training.Training
	result := qr.Conn.WithContext(ctx).Preload("Rewards").Where("category = ?", categoryId).Find(&trainings)
	if result.Error != nil {
		return nil, result.Error
	}
	return trainings, nil
}

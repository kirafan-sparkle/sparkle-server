package persistence

import (
	"context"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_ability_board "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_board"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type userAbilityBoardRepositoryImpl struct {
	Conn *gorm.DB
}

func NewUserAbilityBoardRepositoryImpl(conn *gorm.DB) repository.ManagedAbilityBoardRepository {
	return &userAbilityBoardRepositoryImpl{Conn: conn}
}

func (qr *userAbilityBoardRepositoryImpl) IsAbilityBoardExist(ctx context.Context, userId value_user.UserId, abilityBoardId value_ability_board.AbilityBoardId) (bool, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "IsManagedAbilityBoardExist")
	defer span.End()

	var board *model_user.ManagedAbilityBoard
	result := qr.Conn.WithContext(ctx).Where("user_id = ?", userId).Where("ability_board_id = ?", abilityBoardId).Limit(1).Find(&board)
	if result.Error != nil {
		return false, result.Error
	}
	return result.RowsAffected != 0, nil
}

func (qr *userAbilityBoardRepositoryImpl) FindManagedAbilityBoard(ctx context.Context, userId value_user.UserId, managedAbilityBoardId value_user.ManagedAbilityBoardId) (*model_user.ManagedAbilityBoard, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindManagedAbilityBoard")
	defer span.End()

	var board model_user.ManagedAbilityBoard
	result := qr.Conn.WithContext(ctx).Where("user_id = ?", userId).Where("managed_ability_board_id = ?", managedAbilityBoardId).Preload("EquipItemIds").Take(&board)
	if result.Error != nil {
		return nil, result.Error
	}
	return &board, nil
}

func (qr *userAbilityBoardRepositoryImpl) FindManagedAbilityBoards(ctx context.Context, userId value_user.UserId) ([]model_user.ManagedAbilityBoard, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "FindManagedAbilityBoards")
	defer span.End()

	var boards []model_user.ManagedAbilityBoard
	result := qr.Conn.WithContext(ctx).Where("user_id = ?", userId).Preload("EquipItemIds").Find(&boards)
	if result.Error != nil {
		return nil, result.Error
	}
	return boards, nil
}

func (qr *userAbilityBoardRepositoryImpl) InsertManagedAbilityBoard(ctx context.Context, userId value_user.UserId, board *model_user.ManagedAbilityBoard) (*model_user.ManagedAbilityBoard, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "InsertManagedAbilityBoard")
	defer span.End()

	result := qr.Conn.WithContext(ctx).Create(&board)
	if result.Error != nil {
		return nil, result.Error
	}
	return board, nil
}

func (qr *userAbilityBoardRepositoryImpl) UpdateManagedAbilityBoard(ctx context.Context, userId value_user.UserId, board *model_user.ManagedAbilityBoard) (*model_user.ManagedAbilityBoard, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "UpdateManagedAbilityBoard")
	defer span.End()

	tx := qr.Conn.Begin()

	if err := tx.WithContext(ctx).Clauses(clause.OnConflict{
		Columns: []clause.Column{{Name: "id"}},
		DoUpdates: clause.AssignmentColumns([]string{
			"managed_ability_board_id",
			"item_id",
		}),
	}).Create(board.EquipItemIds).Error; err != nil {
		tx.Rollback()
		return nil, err
	}
	if err := tx.WithContext(ctx).Clauses(clause.OnConflict{
		Columns: []clause.Column{{Name: "managed_ability_board_id"}},
		DoUpdates: clause.AssignmentColumns([]string{
			"ability_board_id",
			"managed_character_id",
		}),
	}).Omit("EquipItemIds").Create(board).Error; err != nil {
		tx.Rollback()
		return nil, err
	}

	result := tx.WithContext(ctx).Save(&board)
	if result.Error != nil {
		return nil, result.Error
	}

	tx.Commit()
	return board, nil
}

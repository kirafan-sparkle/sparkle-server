package persistence

import (
	"context"
	"fmt"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"

	"gorm.io/gorm"
)

type sessionRepositoryImpl struct {
	Conn *gorm.DB
}

// NewSessionRepositoryImpl
func NewSessionRepositoryImpl(conn *gorm.DB) repository.SessionRepository {
	return &sessionRepositoryImpl{Conn: conn}
}

// Find the user session and check the request is valid
func (sr *sessionRepositoryImpl) Validate(ctx context.Context, session string) (value_user.UserId, error) {
	ctx, span := observability.Tracer.StartPersistenceSpan(ctx, "Validate")
	defer span.End()
	if session == "" {
		return 0, fmt.Errorf("session is empty")
	}
	user := &model_user.User{}
	if err := sr.Conn.WithContext(ctx).Where(&model_user.User{Session: session}).First(&user).Error; err != nil {
		return 0, err
	}
	return user.Id, nil
}

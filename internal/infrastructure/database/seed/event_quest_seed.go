package seed

import (
	"encoding/json"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedEventQuests(db *gorm.DB, conf *env.SeedConfig) {
	// Read the JSON file into a byte slice
	questsFile, err := Read("event_quests", conf)
	if err != nil {
		return
	}

	// Unmarshal the JSON byte slice into a slice of Quest struct
	var eventQuests []model_quest.EventQuest
	err = json.Unmarshal(questsFile, &eventQuests)
	if err != nil {
		panic(err)
	}

	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(eventQuests, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

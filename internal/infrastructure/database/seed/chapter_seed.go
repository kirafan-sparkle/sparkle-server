package seed

import (
	"encoding/json"

	model_chapter "gitlab.com/kirafan/sparkle/server/internal/domain/model/chapter"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedChapters(db *gorm.DB, conf *env.SeedConfig) {
	chaptersFile, err := Read("chapters", conf)
	if err != nil {
		return
	}
	var chapters []model_chapter.Chapter
	err = json.Unmarshal(chaptersFile, &chapters)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(chapters, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

package seed

import (
	"encoding/json"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedEventQuestPeriods(db *gorm.DB, conf *env.SeedConfig) {
	// Read the JSON file into a byte slice
	periodsFile, err := Read("event_quest_periods", conf)
	if err != nil {
		return
	}

	// Unmarshal the JSON byte slice into a slice of Quest period struct
	var eventQuestPeriods []model_quest.EventQuestPeriod
	err = json.Unmarshal(periodsFile, &eventQuestPeriods)
	if err != nil {
		panic(err)
	}

	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(eventQuestPeriods, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

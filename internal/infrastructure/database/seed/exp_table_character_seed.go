package seed

import (
	"encoding/json"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedExpTableCharacter(db *gorm.DB, conf *env.SeedConfig) {
	tableFile, err := Read("exp_table_characters", conf)
	if err != nil {
		return
	}
	var expTableCharacters []model_exp_table.ExpTableCharacter
	err = json.Unmarshal(tableFile, &expTableCharacters)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(expTableCharacters, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

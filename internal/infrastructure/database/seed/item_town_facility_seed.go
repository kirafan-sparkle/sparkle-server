package seed

import (
	"encoding/json"

	model_item_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/item_table"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedItemTownFacilities(db *gorm.DB, conf *env.SeedConfig) {
	itemTableTownFacilitiesFile, err := Read("item_table_town_facilities", conf)
	if err != nil {
		return
	}
	var itemTableTownFacilities []model_item_table.ItemTableTownFacility
	err = json.Unmarshal(itemTableTownFacilitiesFile, &itemTableTownFacilities)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(itemTableTownFacilities, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

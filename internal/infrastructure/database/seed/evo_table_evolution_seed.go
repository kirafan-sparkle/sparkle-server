package seed

import (
	"encoding/json"

	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedEvoTableEvolution(db *gorm.DB, conf *env.SeedConfig) {
	tableFile, err := Read("evo_table_evolution", conf)
	if err != nil {
		return
	}
	var evoTableEvolution []model_evo_table.EvoTableEvolution
	err = json.Unmarshal(tableFile, &evoTableEvolution)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(evoTableEvolution, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

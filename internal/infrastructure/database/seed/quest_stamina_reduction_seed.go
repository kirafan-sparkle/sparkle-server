package seed

import (
	"encoding/json"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedQuestStaminaReductions(db *gorm.DB, conf *env.SeedConfig) {
	// Read the JSON file into a byte slice
	questStaminaReductionsFile, err := Read("quest_stamina_reductions", conf)
	if err != nil {
		return
	}

	// Unmarshal the JSON byte slice into a slice of Quest struct
	var questStaminaReductions []model_quest.QuestStaminaReduction
	err = json.Unmarshal(questStaminaReductionsFile, &questStaminaReductions)
	if err != nil {
		panic(err)
	}

	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(questStaminaReductions, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

package seed

import (
	"encoding/json"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedCharacterQuests(db *gorm.DB, conf *env.SeedConfig) {
	questsFile, err := Read("character_quests", conf)
	if err != nil {
		return
	}
	var quests []model_quest.CharacterQuest
	err = json.Unmarshal(questsFile, &quests)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(quests, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

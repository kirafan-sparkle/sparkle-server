package seed

import (
	"encoding/json"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedQuestWaves(db *gorm.DB, conf *env.SeedConfig) {
	questWavesFile, err := Read("wave_list", conf)
	if err != nil {
		return
	}
	var questWaves []model_quest.QuestWave
	err = json.Unmarshal(questWavesFile, &questWaves)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(questWaves, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

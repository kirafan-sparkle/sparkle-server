package seed

import (
	"encoding/json"

	model_achievement "gitlab.com/kirafan/sparkle/server/internal/domain/model/achievement"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedAchievements(db *gorm.DB, conf *env.SeedConfig) {
	achievementsFile, err := Read("achievements", conf)
	if err != nil {
		return
	}
	var achievements []model_achievement.Achievement
	err = json.Unmarshal(achievementsFile, &achievements)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(achievements, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

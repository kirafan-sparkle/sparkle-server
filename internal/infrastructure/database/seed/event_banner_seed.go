package seed

import (
	"encoding/json"

	model_event_banner "gitlab.com/kirafan/sparkle/server/internal/domain/model/event_banner"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedEventBanners(db *gorm.DB, conf *env.SeedConfig) {
	eventBannersFile, err := Read("event_banners", conf)
	if err != nil {
		return
	}
	var eventBanners []model_event_banner.EventBanner
	err = json.Unmarshal(eventBannersFile, &eventBanners)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(eventBanners, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

package seed

import (
	"encoding/json"

	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedWeapons(db *gorm.DB, conf *env.SeedConfig) {
	tablesFile, err := Read("weapons", conf)
	if err != nil {
		return
	}
	var tables []model_weapon.Weapon
	err = json.Unmarshal(tablesFile, &tables)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(tables, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

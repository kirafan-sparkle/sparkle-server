package seed

import (
	"encoding/json"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedExpTableWeapon(db *gorm.DB, conf *env.SeedConfig) {
	tableFile, err := Read("exp_table_weapons", conf)
	if err != nil {
		return
	}
	var expTableWeapons []model_exp_table.ExpTableWeapon
	err = json.Unmarshal(tableFile, &expTableWeapons)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(expTableWeapons, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

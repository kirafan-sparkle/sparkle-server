package seed

import (
	"encoding/json"

	model_level_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/level_table"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedLevelTownFacilities(db *gorm.DB, conf *env.SeedConfig) {
	levelTableTownFacilitiesFile, err := Read("level_table_town_facilities", conf)
	if err != nil {
		return
	}
	var levelTableTownFacilities []model_level_table.LevelTableTownFacility
	err = json.Unmarshal(levelTableTownFacilitiesFile, &levelTableTownFacilities)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(levelTableTownFacilities, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

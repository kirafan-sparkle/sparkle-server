package seed

import (
	"encoding/json"

	model_training "gitlab.com/kirafan/sparkle/server/internal/domain/model/training"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedTrainings(db *gorm.DB, conf *env.SeedConfig) {
	trainingsFile, err := Read("trainings", conf)
	if err != nil {
		return
	}
	var trainings []model_training.Training
	err = json.Unmarshal(trainingsFile, &trainings)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(trainings, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

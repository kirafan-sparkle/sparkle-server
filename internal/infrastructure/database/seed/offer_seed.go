package seed

import (
	"encoding/json"

	model_offer "gitlab.com/kirafan/sparkle/server/internal/domain/model/offer"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedOffers(db *gorm.DB, conf *env.SeedConfig) {
	offersFile, err := Read("offers", conf)
	if err != nil {
		return
	}
	var offers []model_offer.Offer
	err = json.Unmarshal(offersFile, &offers)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(offers, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

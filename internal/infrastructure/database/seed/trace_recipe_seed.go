package seed

import (
	"encoding/json"

	model_trade "gitlab.com/kirafan/sparkle/server/internal/domain/model/trade"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedTradeRecipes(db *gorm.DB, conf *env.SeedConfig) {
	tradeRecipesFile, err := Read("trade_recipes", conf)
	if err != nil {
		return
	}
	var tradeRecipes []model_trade.TradeRecipe
	err = json.Unmarshal(tradeRecipesFile, &tradeRecipes)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(tradeRecipes, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

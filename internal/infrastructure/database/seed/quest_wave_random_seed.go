package seed

import (
	"encoding/json"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedQuestWaveRandoms(db *gorm.DB, conf *env.SeedConfig) {
	questWaveRandomFile, err := Read("wave_random_list", conf)
	if err != nil {
		return
	}
	var questWaveRandoms []model_quest.QuestWaveRandom
	err = json.Unmarshal(questWaveRandomFile, &questWaveRandoms)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(questWaveRandoms, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

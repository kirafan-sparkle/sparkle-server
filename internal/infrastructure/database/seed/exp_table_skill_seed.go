package seed

import (
	"encoding/json"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedExpTableSkill(db *gorm.DB, conf *env.SeedConfig) {
	tableFile, err := Read("exp_table_skills", conf)
	if err != nil {
		return
	}
	var expTableSkills []model_exp_table.ExpTableSkill
	err = json.Unmarshal(tableFile, &expTableSkills)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(expTableSkills, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

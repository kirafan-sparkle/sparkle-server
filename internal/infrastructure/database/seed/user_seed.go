package seed

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
)

func SeedUsers(db *gorm.DB, conf *env.SeedConfig) {
	users := []model_user.User{
		model_user.NewUserWithSpecifySession("DUMMY_DEVICE_UUID", "DUMMY_SESSION_ID", "Sparkle"),
	}
	for _, user := range users {
		db.Create(&user)
	}
}

package seed

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"

	"gitlab.com/kirafan/sparkle/server/pkg/env"
)

func readRemoteFile(endpoint string, path string) (*[]byte, error) {
	resp, err := http.Get(endpoint + path)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	bytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return &bytes, nil
}

func Read(filename string, conf *env.SeedConfig) ([]byte, error) {
	switch conf.Source {
	case "remote":
		endpoint := fmt.Sprint(conf.Endpoint)
		if endpoint == "" {
			fmt.Print("[WARN] Seed file endpoint was not specified\n")
			return nil, errors.New("endpoint was not specified")
		}
		fmt.Printf("[INFO] Reading seed file from remote\n")
		file, err := readRemoteFile(endpoint, fmt.Sprintf("/%v.json", filename))
		// Read the JSON file into a byte slice
		if err != nil {
			fmt.Printf("[WARN] Seed file %v was not found\n", filename)
			return nil, err
		}
		return *file, nil
	case "local":
		fmt.Printf("[INFO] Reading seed file from local")
		file, err := os.ReadFile(fmt.Sprintf("assets/%v.json", filename))
		// Read the JSON file into a byte slice
		if err != nil {
			fmt.Printf("[WARN] Seed file %v was not found\n", filename)
			return nil, err
		}
		return file, nil
	default:
		fmt.Printf("[WARN] Seed file source was not specified\n")
		return nil, errors.New("source was not specified")
	}
}

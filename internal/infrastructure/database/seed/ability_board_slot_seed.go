package seed

import (
	"encoding/json"

	model_ability_board "gitlab.com/kirafan/sparkle/server/internal/domain/model/ability_board"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedAbilityBoardSlots(db *gorm.DB, conf *env.SeedConfig) {
	tablesFile, err := Read("ability_board_slots", conf)
	if err != nil {
		return
	}
	var tables []model_ability_board.AbilityBoardSlot
	err = json.Unmarshal(tablesFile, &tables)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(tables, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

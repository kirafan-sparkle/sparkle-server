package seed

import (
	"encoding/json"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedExpTableRank(db *gorm.DB, conf *env.SeedConfig) {
	tableFile, err := Read("exp_table_ranks", conf)
	if err != nil {
		return
	}
	var expTableRanks []model_exp_table.ExpTableRank
	err = json.Unmarshal(tableFile, &expTableRanks)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(expTableRanks, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

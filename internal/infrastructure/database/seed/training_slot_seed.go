package seed

import (
	"time"

	model_training "gitlab.com/kirafan/sparkle/server/internal/domain/model/training"
	value_training "gitlab.com/kirafan/sparkle/server/internal/domain/value/training"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedTrainingSlots(db *gorm.DB, conf *env.SeedConfig) {
	JST := time.FixedZone("Asia/Tokyo", 9*60*60)
	trainingSlots := []model_training.TrainingSlot{
		{
			TrainingSlotPk: 1,
			TrainingSlotId: value_training.SlotId0,
			CreatedAt:      time.Date(2023, 11, 4, 19, 0, 0, 0, JST),
			UpdatedAt:      time.Date(2023, 11, 4, 19, 0, 0, 0, JST),
			DeletedAt:      nil,
			NeedRank:       0,
		},
		{
			TrainingSlotPk: 2,
			TrainingSlotId: value_training.SlotId1,
			CreatedAt:      time.Date(2023, 11, 4, 19, 0, 0, 0, JST),
			UpdatedAt:      time.Date(2023, 11, 4, 19, 0, 0, 0, JST),
			DeletedAt:      nil,
			NeedRank:       0,
		},
		{
			TrainingSlotPk: 3,
			TrainingSlotId: value_training.SlotId2,
			CreatedAt:      time.Date(2023, 11, 4, 19, 0, 0, 0, JST),
			UpdatedAt:      time.Date(2023, 11, 4, 19, 0, 0, 0, JST),
			DeletedAt:      nil,
			NeedRank:       50,
		},
		{
			TrainingSlotPk: 4,
			TrainingSlotId: value_training.SlotId3,
			CreatedAt:      time.Date(2023, 11, 4, 19, 0, 0, 0, JST),
			UpdatedAt:      time.Date(2023, 11, 4, 19, 0, 0, 0, JST),
			DeletedAt:      nil,
			NeedRank:       80,
		},
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).Model(&model_training.TrainingSlot{}).Select(
		[]string{"TrainingSlotPk", "TrainingSlotId", "CreatedAt", "UpdatedAt", "DeletedAt", "NeedRank"},
	).Create(trainingSlots)
	if result.Error != nil {
		panic(result.Error)
	}
}

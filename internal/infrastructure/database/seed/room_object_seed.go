package seed

import (
	"encoding/json"

	model_room_object "gitlab.com/kirafan/sparkle/server/internal/domain/model/room_object"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedRoomObjects(db *gorm.DB, conf *env.SeedConfig) {
	roomObjectsFile, err := Read("room_objects", conf)
	if err != nil {
		return
	}
	var roomObjects []model_room_object.RoomObject
	err = json.Unmarshal(roomObjectsFile, &roomObjects)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(roomObjects, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

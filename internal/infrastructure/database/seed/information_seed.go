package seed

import (
	"encoding/json"

	model_information "gitlab.com/kirafan/sparkle/server/internal/domain/model/information"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedInformations(db *gorm.DB, conf *env.SeedConfig) {
	informationsFile, err := Read("informations", conf)
	if err != nil {
		return
	}
	var informations []model_information.Information
	err = json.Unmarshal(informationsFile, &informations)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(informations, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

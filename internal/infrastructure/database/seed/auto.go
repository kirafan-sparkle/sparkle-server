package seed

import (
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
)

func AutoSeed(db *gorm.DB, conf *env.SeedConfig, option SeedOption) {
	if db.Dialector.Name() == "sqlite" {
		if res := db.Exec("PRAGMA synchronous=OFF;PRAGMA journal_mode=MEMORY;", nil); res.Error != nil {
			panic(res.Error)
		}
	}
	if option.Versions {
		SeedVersions(db, conf)
	}
	if option.Informations {
		SeedInformations(db, conf)
	}
	if option.EventBanners {
		SeedEventBanners(db, conf)
	}
	if option.Quests {
		SeedQuests(db, conf)
	}
	if option.QuestWaves {
		SeedQuestWaves(db, conf)
	}
	if option.QuestWaveDrops {
		SeedQuestWaveDrops(db, conf)
	}
	if option.QuestWaveRandoms {
		SeedQuestWaveRandoms(db, conf)
	}
	if option.Chapters {
		SeedChapters(db, conf)
	}
	if option.EventQuests {
		SeedEventQuests(db, conf)
	}
	if option.EventQuestPeriods {
		SeedEventQuestPeriods(db, conf)
	}
	if option.QuestStaminaReductions {
		SeedQuestStaminaReductions(db, conf)
	}
	if option.CharacterQuests {
		SeedCharacterQuests(db, conf)
	}
	if option.Missions {
		SeedMissions(db, conf)
	}
	if option.Items {
		SeedItems(db, conf)
	}
	if option.RoomObjects {
		SeedRoomObjects(db, conf)
	}
	if option.TownFacilities {
		SeedTownFacilities(db, conf)
	}
	if option.ItemTownFacilities {
		SeedItemTownFacilities(db, conf)
	}
	if option.LevelTownFacilities {
		SeedLevelTownFacilities(db, conf)
	}
	if option.Characters {
		SeedCharacters(db, conf)
	}
	if option.NamedTypes {
		SeedNamedTypes(db, conf)
	}
	if option.Presents {
		SeedPresents(db, conf)
	}
	if option.Offers {
		SeedOffers(db, conf)
	}
	if option.Gachas {
		SeedGachas(db, conf)
	}
	if option.GachaTables {
		// SeedGachaTables(db, conf)
	}
	if option.Achievements {
		SeedAchievements(db, conf)
	}
	if option.Users {
		SeedUsers(db, conf)
	}
	if option.ExpTableRank {
		SeedExpTableRank(db, conf)
	}
	if option.ExpTableCharacter {
		SeedExpTableCharacter(db, conf)
	}
	if option.ExpTableWeapon {
		SeedExpTableWeapon(db, conf)
	}
	if option.ExpTableFriendship {
		SeedExpTableFriendship(db, conf)
	}
	if option.ExpTableSkill {
		SeedExpTableSkill(db, conf)
	}
	if option.EvoTableLimitBreak {
		SeedEvoTableLimitBreak(db, conf)
	}
	if option.EvoTableEvolution {
		SeedEvoTableEvolution(db, conf)
	}
	if option.EvoTableWeapon {
		SeedEvoTableWeapon(db, conf)
	}
	if option.LoginBonuses {
		SeedLoginBonuses(db, conf)
	}
	if option.WeaponCharacterTables {
		SeedWeaponCharacterTables(db, conf)
	}
	if option.Weapons {
		SeedWeapons(db, conf)
	}
	if option.WeaponRecipes {
		SeedWeaponRecipes(db, conf)
	}
	if option.TradeRecipes {
		SeedTradeRecipes(db, conf)
	}
	if option.PackageItems {
		SeedPackageItems(db, conf)
	}
	if option.Trainings {
		SeedTrainings(db, conf)
	}
	if option.TrainingSlots {
		SeedTrainingSlots(db, conf)
	}
	if option.AbilityBoard {
		SeedAbilityBoard(db, conf)
	}
	if option.AbilityBoardSlots {
		SeedAbilityBoardSlots(db, conf)
	}
	if option.AbilityBoardSlotRecipes {
		SeedAbilityBoardSlotRecipes(db, conf)
	}
	if option.AbilitySphere {
		SeedAbilitySphere(db, conf)
	}
	if option.AbilitySphereRecipe {
		SeedAbilitySphereRecipe(db, conf)
	}
	if db.Dialector.Name() == "sqlite" {
		if res := db.Exec("PRAGMA synchronous=NORMAL;PRAGMA journal_mode=DELETE;", nil); res.Error != nil {
			panic(res.Error)
		}
	}
}

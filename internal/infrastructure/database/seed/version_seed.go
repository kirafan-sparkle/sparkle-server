package seed

import (
	model_version "gitlab.com/kirafan/sparkle/server/internal/domain/model/version"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedVersions(db *gorm.DB, conf *env.SeedConfig) {
	versions := []model_version.Version{
		model_version.NewVersion(value_version.PlatformIOS, "3.2.17", 60, "321d1767653ccfd106384a7aeac5be75"),
		model_version.NewVersion(value_version.PlatformIOS, "3.4.0", 60, "321d1767653ccfd106384a7aeac5be75"),
		model_version.NewVersion(value_version.PlatformIOS, "3.6.0", 60, "321d1767653ccfd106384a7aeac5be75"),
		model_version.NewVersion(value_version.PlatformAndroid, "3.2.17", 60, "321d1767653ccfd106384a7aeac5be75"),
		model_version.NewVersion(value_version.PlatformAndroid, "3.4.0", 60, "321d1767653ccfd106384a7aeac5be75"),
		model_version.NewVersion(value_version.PlatformAndroid, "3.6.0", 60, "321d1767653ccfd106384a7aeac5be75"),
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(versions, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

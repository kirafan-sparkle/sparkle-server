package seed

import (
	"encoding/json"

	model_present "gitlab.com/kirafan/sparkle/server/internal/domain/model/present"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedPresents(db *gorm.DB, conf *env.SeedConfig) {
	presentsFile, err := Read("presents", conf)
	if err != nil {
		return
	}
	var presents []model_present.Present
	err = json.Unmarshal(presentsFile, &presents)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(presents, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

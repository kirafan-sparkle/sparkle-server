package seed

import (
	"encoding/json"

	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedEvoTableWeapon(db *gorm.DB, conf *env.SeedConfig) {
	tableFile, err := Read("evo_table_weapon", conf)
	if err != nil {
		return
	}
	var evoTableWeapon []model_evo_table.EvoTableWeapon
	err = json.Unmarshal(tableFile, &evoTableWeapon)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(evoTableWeapon, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

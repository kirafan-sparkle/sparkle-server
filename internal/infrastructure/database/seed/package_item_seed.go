package seed

import (
	"encoding/json"

	model_trade "gitlab.com/kirafan/sparkle/server/internal/domain/model/trade"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedPackageItems(db *gorm.DB, conf *env.SeedConfig) {
	packageItemsFile, err := Read("package_item_list", conf)
	if err != nil {
		return
	}
	var packageItems []model_trade.PackageItem
	err = json.Unmarshal(packageItemsFile, &packageItems)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(packageItems, 50)
	if result.Error != nil {
		panic(result.Error)
	}

	packageItemsContentFile, err := Read("package_item_contents", conf)
	if err != nil {
		return
	}
	var packageItemContents []model_trade.PackageItemContent
	err = json.Unmarshal(packageItemsContentFile, &packageItemContents)
	if err != nil {
		panic(err)
	}
	result2 := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(packageItemContents, 50)
	if result2.Error != nil {
		panic(result2.Error)
	}
}

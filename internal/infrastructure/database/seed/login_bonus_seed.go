package seed

import (
	"encoding/json"

	model_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/model/login_bonus"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedLoginBonuses(db *gorm.DB, conf *env.SeedConfig) {
	loginBonusesFile, err := Read("login_bonuses", conf)
	if err != nil {
		return
	}
	var loginBonuses []model_login_bonus.LoginBonus
	err = json.Unmarshal(loginBonusesFile, &loginBonuses)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(loginBonuses, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}

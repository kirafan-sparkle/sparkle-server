package database

import (
	"fmt"

	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/driver/mysql"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func newMySQLDatabase(dbConfig *env.DatabaseConfig, gormConfig *gorm.Config) *gorm.DB {
	dsn := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		dbConfig.User,
		dbConfig.Pass,
		dbConfig.Host,
		fmt.Sprint(dbConfig.Port),
		dbConfig.Name,
	)
	db, err := gorm.Open(mysql.Open(dsn), gormConfig)
	if err != nil {
		panic(err)
	}
	return db
}

func newSQLiteDatabase(dbConfig *env.DatabaseConfig, gormConfig *gorm.Config) *gorm.DB {
	db, err := gorm.Open(sqlite.Open(fmt.Sprint(dbConfig.Name)), gormConfig)
	if err != nil {
		panic(err)
	}
	return db
}

func newSQLiteInMemoryDatabase(gormConfig *gorm.Config) *gorm.DB {
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), gormConfig)
	if err != nil {
		panic(err)
	}
	return db
}

package migrate

import (
	model_ability_board "gitlab.com/kirafan/sparkle/server/internal/domain/model/ability_board"
	model_ability_sphere "gitlab.com/kirafan/sparkle/server/internal/domain/model/ability_sphere"
	model_achievement "gitlab.com/kirafan/sparkle/server/internal/domain/model/achievement"
	model_chapter "gitlab.com/kirafan/sparkle/server/internal/domain/model/chapter"
	model_character "gitlab.com/kirafan/sparkle/server/internal/domain/model/character"
	model_event_banner "gitlab.com/kirafan/sparkle/server/internal/domain/model/event_banner"
	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	model_friend "gitlab.com/kirafan/sparkle/server/internal/domain/model/friend"
	model_gacha "gitlab.com/kirafan/sparkle/server/internal/domain/model/gacha"
	model_information "gitlab.com/kirafan/sparkle/server/internal/domain/model/information"
	model_item "gitlab.com/kirafan/sparkle/server/internal/domain/model/item"
	model_item_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/item_table"
	model_level_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/level_table"
	model_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/model/login_bonus"
	model_mission "gitlab.com/kirafan/sparkle/server/internal/domain/model/mission"
	model_named_type "gitlab.com/kirafan/sparkle/server/internal/domain/model/named_type"
	model_offer "gitlab.com/kirafan/sparkle/server/internal/domain/model/offer"
	model_present "gitlab.com/kirafan/sparkle/server/internal/domain/model/present"
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	model_room_object "gitlab.com/kirafan/sparkle/server/internal/domain/model/room_object"
	model_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/model/town_facility"
	model_trade "gitlab.com/kirafan/sparkle/server/internal/domain/model/trade"
	model_training "gitlab.com/kirafan/sparkle/server/internal/domain/model/training"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	model_version "gitlab.com/kirafan/sparkle/server/internal/domain/model/version"
	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
	"gorm.io/gorm"
)

func AutoMigrate(db *gorm.DB) {
	// db.Exec("PRAGMA foreign_keys = ON", nil)
	// NOTE: Be careful! "these models have to be in the correct order"
	// The models must be ordered from parent to child.
	db.AutoMigrate(
		// Version models
		&model_version.Version{},
		// Information models
		&model_information.Information{},
		&model_event_banner.EventBanner{},
		// Chapter models
		&model_chapter.Chapter{},
		&model_chapter.ChapterQuestId{},
		// Mission models
		&model_mission.Mission{},
		&model_mission.MissionReward{},
		// Item models
		&model_item.Item{},
		// Trade models
		&model_trade.TradeRecipe{},
		&model_trade.PackageItem{},
		&model_trade.PackageItemContent{},
		// RoomObject models
		&model_room_object.RoomObject{},
		// TownFacility models
		&model_town_facility.TownFacility{},
		&model_level_table.LevelTableTownFacility{},
		&model_level_table.LevelTableTownFacilityFieldItemDropId{},
		&model_level_table.LevelTableTownFacilityFieldItemDropProbability{},
		&model_item_table.ItemTableTownFacility{},
		// Offer models
		&model_offer.Offer{},
		&model_offer.OfferReward{},
		// Training models
		&model_training.Training{},
		&model_training.TrainingRewardDrop{},
		&model_training.TrainingReward{},
		&model_training.TrainingSlot{},
		// Character models
		&model_character.Character{},
		&model_named_type.NamedType{},
		// Weapon models
		&model_weapon.Weapon{},
		&model_weapon.WeaponCharacterTable{},
		&model_weapon.WeaponRecipe{},
		&model_weapon.WeaponRecipeMaterial{},
		&model_weapon.WeaponUpgradeBonusMaterial{},
		// Present models
		&model_present.Present{},
		// LoginBonus models
		&model_login_bonus.LoginBonus{},
		&model_login_bonus.LoginBonusDay{},
		&model_login_bonus.LoginBonusDayBonusItem{},
		// Gacha models
		&model_gacha.Gacha{},
		&model_gacha.GachaSelectionCharacter{},
		&model_gacha.GachaDrawPointTradable{},
		&model_gacha.GachaStep{},
		&model_gacha.GachaStepBonusItem{},
		&model_gacha.GachaDailyFree{},
		&model_gacha.GachaTable{},
		// ExpTable models
		&model_exp_table.ExpTableRank{},
		&model_exp_table.ExpTableCharacter{},
		&model_exp_table.ExpTableWeapon{},
		&model_exp_table.ExpTableFriendship{},
		&model_exp_table.ExpTableSkill{},
		// EvoTable models
		&model_evo_table.EvoTableLimitBreak{},
		&model_evo_table.EvoTableEvolution{},
		&model_evo_table.EvoTableEvolutionRequiredItem{},
		&model_evo_table.EvoTableWeapon{},
		&model_evo_table.EvoTableWeaponRecipeMaterial{},
		// Achievement models
		&model_achievement.Achievement{},
		// AbilityBoard models
		&model_ability_board.AbilityBoardSlotRecipe{},
		&model_ability_board.AbilityBoardSlot{},
		&model_ability_board.AbilityBoard{},
		// AbilitySphere models
		&model_ability_sphere.AbilitySphereRecipe{},
		&model_ability_sphere.AbilitySphere{},
		// User models
		&model_user.User{},
		&model_user.ClearedAdvId{},
		&model_user.FavoriteMember{},
		&model_user.ItemSummary{},
		&model_user.ManagedAbilityBoard{},
		&model_user.ManagedAbilityBoardEquipItemId{},
		&model_user.ManagedBattleParty{},
		&model_user.ManagedCharacter{},
		&model_user.ManagedTown{},
		&model_user.ManagedTownFacility{},
		&model_user.ManagedFieldPartyMember{},
		&model_user.ManagedNamedType{},
		&model_user.ManagedRoom{},
		&model_user.ManagedRoomArrangeData{},
		&model_user.ManagedRoomObject{},
		&model_user.ManagedMasterOrb{},
		&model_user.ManagedWeapon{},
		&model_user.OfferTitleType{},
		&model_user.ShownTipId{},
		&model_user.SupportCharacterManagedCharacterId{},
		&model_user.SupportCharacterManagedWeaponId{},
		&model_user.SupportCharacter{},
		&model_user.UserQuestClearRank{},
		&model_user.UserMission{},
		&model_user.UserPresent{},
		&model_user.UserGacha{},
		&model_user.UserLoginBonus{},
		&model_user.UserAchievement{},
		&model_user.UserTraining{},
		&model_user.UserTrainingSlot{},
		&model_user.UserTrainingSlotManagedCharacter{},
		&model_user.UserTrainingOrder{},
		&model_user.UserTradeRecipe{},
		// Friend models
		&model_friend.Friend{},
		&model_friend.FriendCache{},
		&model_friend.FriendRequest{},
		// Quest models
		&model_quest.Quest{},
		&model_quest.QuestNpc{},
		&model_quest.QuestFirstClearReward{},
		&model_quest.QuestStaminaReduction{},
		&model_quest.OfferQuest{},
		&model_quest.CharacterQuest{},
		&model_quest.EventQuest{},
		&model_quest.EventQuestCondId{},
		&model_quest.EventQuestPeriod{},
		&model_quest.EventQuestPeriodGroup{},
		&model_quest.EventQuestPeriodGroupQuest{},
		&model_quest.EventQuestPeriodGroupQuestCondEventQuestId{},
		&model_quest.QuestWave{},
		&model_quest.QuestWaveRandom{},
		&model_quest.QuestWaveDrop{},
	)
}

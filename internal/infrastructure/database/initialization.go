package database

import (
	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"moul.io/zapgorm2"
)

// Get a gorm database instance with specified database config
func GetDatabase(dbConfig *env.DatabaseConfig, gormConfig *gorm.Config) *gorm.DB {
	switch dbConfig.Type {
	case env.DatabaseTypeInMemory:
		return newSQLiteInMemoryDatabase(gormConfig)
	case env.DatabaseTypeSqlite:
		return newSQLiteDatabase(dbConfig, gormConfig)
	case env.DatabaseTypeMysql:
		return newMySQLDatabase(dbConfig, gormConfig)
	default:
		panic("Unknown database type")
	}
}

func GetDatabaseWithObservability(otelZapLogger *otelzap.Logger, dbConfig *env.DatabaseConfig) *gorm.DB {
	// Wrap the zap logger as gorm controllable logger
	wrappedLogger := zapgorm2.New(otelZapLogger.Logger)
	wrappedLogger.SetAsDefault()
	gormConfig := &gorm.Config{
		Logger:          wrappedLogger.LogMode(logger.Info),
		CreateBatchSize: 150,
	}
	db := GetDatabase(dbConfig, gormConfig)
	wrappedDb := observability.InjectObservabilityToGorm(db)
	return wrappedDb
}

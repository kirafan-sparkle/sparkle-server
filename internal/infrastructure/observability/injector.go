package observability

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/uptrace/opentelemetry-go-extra/otelgorm"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gorilla/mux/otelmux"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"gorm.io/gorm"
)

// This function injects net/http and gorilla/mux observability to router
func InjectObservabilityToRouter(router *mux.Router) http.Handler {
	// gorilla/mux middleware
	router.Use(otelmux.Middleware("sparkle-server"))
	// net/http middleware
	wrappedRouter := otelhttp.NewHandler(router, "net/http",
		otelhttp.WithMessageEvents(otelhttp.ReadEvents, otelhttp.WriteEvents),
	)
	return wrappedRouter
}

// This function injects gorm observability to gorm
func InjectObservabilityToGorm(db *gorm.DB) *gorm.DB {
	if err := db.Use(otelgorm.NewPlugin()); err != nil {
		panic(err)
	}
	return db
}

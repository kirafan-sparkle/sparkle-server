package tracer_provider

import (
	"context"
	"log"

	"go.opentelemetry.io/contrib/propagators/aws/xray"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
)

func InitJaegerTracer(ctx context.Context, dsn string, resource *resource.Resource, insecure bool) func(context.Context) error {
	var (
		exporter *otlptrace.Exporter
		err      error
	)
	if insecure {
		exporter, err = otlptracehttp.New(
			ctx,
			otlptracehttp.WithInsecure(),
			otlptracehttp.WithEndpoint(dsn),
			otlptracehttp.WithCompression(otlptracehttp.GzipCompression),
		)
	} else {
		exporter, err = otlptracehttp.New(
			ctx,
			otlptracehttp.WithEndpoint(dsn),
			otlptracehttp.WithCompression(otlptracehttp.GzipCompression),
		)
	}
	if err != nil {
		log.Fatal(err)
	}

	bsp := sdktrace.NewBatchSpanProcessor(exporter,
		sdktrace.WithMaxQueueSize(1000),
		sdktrace.WithMaxExportBatchSize(1000))

	tp := sdktrace.NewTracerProvider(
		sdktrace.WithResource(resource),
		sdktrace.WithIDGenerator(xray.NewIDGenerator()),
	)
	tp.RegisterSpanProcessor(bsp)

	otel.SetTracerProvider(tp)

	return bsp.Shutdown
}

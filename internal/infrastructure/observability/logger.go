package observability

import (
	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"go.uber.org/zap"
)

type LogType = uint8

const (
	LogTypeDebug LogType = iota
	LogTypeLocal
	LogTypeProduction
)

var Logger = otelzap.L()

func InitLogger(logType LogType) *otelzap.Logger {
	var (
		logger *zap.Logger
		err    error
	)
	switch logType {
	default:
		fallthrough
	case LogTypeDebug:
		logger, err = zap.NewDevelopment()
		if err != nil {
			panic(err)
		}
	case LogTypeLocal:
		logger, err = zap.NewDevelopment()
		if err != nil {
			panic(err)
		}
	case LogTypeProduction:
		logger, err = zap.NewProduction()
		// config := zap.NewProductionConfig()
		// config.Level = zap.NewAtomicLevelAt(zap.InfoLevel)
		// logger, err = config.Build()
		if err != nil {
			panic(err)
		}
	}
	// NOTE: WithMinLevel must be specified because of default minLevel is Warn
	minLevel := zap.InfoLevel
	switch logType {
	case LogTypeDebug, LogTypeLocal:
		minLevel = zap.DebugLevel
	case LogTypeProduction:
		minLevel = zap.InfoLevel
	}

	wrappedLogger := otelzap.New(logger, otelzap.WithMinLevel(minLevel))
	otelzap.ReplaceGlobals(wrappedLogger)
	return wrappedLogger
}

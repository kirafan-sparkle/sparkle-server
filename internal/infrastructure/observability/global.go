package observability

import (
	"context"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
)

type TracerWrapper struct {
	tracer trace.Tracer
}

func NewCustomTracer(tracer trace.Tracer) *TracerWrapper {
	return &TracerWrapper{
		tracer: tracer,
	}
}

func (t *TracerWrapper) StartMiddlewareSpan(ctx context.Context, methodName string) (context.Context, trace.Span) {
	ctx, span := t.tracer.Start(ctx, methodName+" (M)", LAYER_ATTR_MIDDLEWARE)
	return ctx, span
}

func (t *TracerWrapper) StartInterfaceSpan(ctx context.Context, methodName string) (context.Context, trace.Span) {
	ctx, span := t.tracer.Start(ctx, methodName+" (I)", LAYER_ATTR_INTERFACE)
	return ctx, span
}

func (t *TracerWrapper) StartAppServiceSpan(ctx context.Context, methodName string) (context.Context, trace.Span) {
	ctx, span := t.tracer.Start(ctx, methodName+" (A)", LAYER_ATTR_APP_SERVICE)
	return ctx, span
}

func (t *TracerWrapper) StartUsecaseSpan(ctx context.Context, methodName string) (context.Context, trace.Span) {
	ctx, span := t.tracer.Start(ctx, methodName+" (U)", LAYER_ATTR_USECASE)
	return ctx, span
}

func (t *TracerWrapper) StartPersistenceSpan(ctx context.Context, methodName string) (context.Context, trace.Span) {
	ctx, span := t.tracer.Start(ctx, methodName+" (P)", LAYER_ATTR_PERSISTENCE)
	return ctx, span
}

// Get active span from context
func SpanFromContext(ctx context.Context) trace.Span {
	return trace.SpanFromContext(ctx)
}

var Tracer = NewCustomTracer(otel.Tracer("gitlab.com/kirafan/sparkle/server"))
var Meter = otel.Meter("gitlab.com/kirafan/sparkle/server")

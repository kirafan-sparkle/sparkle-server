package flag

import (
	of "github.com/open-feature/go-sdk/openfeature"
	"gitlab.com/kirafan/sparkle/server/pkg/flag"
)

var FlagClient *flag.SparkleFlagClient

func InitSparkleFlagClient(cl *of.Client) {
	FlagClient = flag.NewSparkleFlagClient(cl)
}

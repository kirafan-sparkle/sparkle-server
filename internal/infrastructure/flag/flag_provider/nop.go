package flag_provider

import (
	"github.com/open-feature/go-sdk/openfeature"
)

func GetNopProvider() openfeature.FeatureProvider {
	return openfeature.NoopProvider{}
}

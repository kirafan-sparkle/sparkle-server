package flag_provider

import (
	flagsmithClient "github.com/Flagsmith/flagsmith-go-client/v3"
	flagsmith "github.com/open-feature/go-sdk-contrib/providers/flagsmith/pkg"
)

func GetFlagsmithClient(envKey string, endpoint string) *flagsmith.Provider {
	client := func() *flagsmithClient.Client {
		if endpoint != "" {
			return flagsmithClient.NewClient(envKey, flagsmithClient.WithBaseURL(endpoint))
		} else {
			return flagsmithClient.NewClient(envKey)
		}
	}()
	// Initialize the flagsmith provider
	provider := flagsmith.NewProvider(client, flagsmith.WithUsingBooleanConfigValue())
	return provider
}

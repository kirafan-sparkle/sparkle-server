/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

import (
	"context"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/internal/application/service"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx/user_id"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// PlayerResumeApiService is a service that implements the logic for the PlayerResumeApiServicer
// This service should implement the business logic for every endpoint for the PlayerResumeApi API.
// Include any external packages or services that will be required by this service.
type PlayerResumeApiService struct {
	rs service.PlayerResumeService
}

// NewPlayerResumeApiService creates a default api service
func NewPlayerResumeApiService(rs service.PlayerResumeService) PlayerResumeApiServicer {
	return &PlayerResumeApiService{rs}
}

// GetPlayerResume - Get Player Resume (WIP)
func (s *PlayerResumeApiService) GetPlayerResume(ctx context.Context, playerId int64) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "GetPlayerResume")
	defer span.End()

	internalUserId, valid := user_id.FromContext(ctx)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	user, err := s.rs.RefreshPlayerStatus(ctx, internalUserId)
	if err != nil {
		return Response(http.StatusOK, err.AsResponse()), nil
	}

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, GetPlayerResumeResponse{
		Stamina:             int64(user.Stamina),
		StaminaMax:          int64(user.StaminaMax),
		RecastTime:          int64(user.RecastTime),
		RecastTimeMax:       int64(user.RecastTimeMax),
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}

package interfaces

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
)

// FIXME: There is no way to separate this DTO(database to response model conversion)
// The DTO depends interfaces package's models, but this router also located at interfaces package
// So, I just put this conversion code here.
// If you have any idea, please let me know.

func toItemSummaryResponse(itemSummary []model_user.ItemSummary) []ItemSummaryArrayObject {
	itemSummaryResp := make([]ItemSummaryArrayObject, len(itemSummary))
	for i := range itemSummary {
		itemSummaryResp[i] = ItemSummaryArrayObject{
			Id:     int64(itemSummary[i].Id),
			Amount: int64(itemSummary[i].Amount),
		}
	}
	return itemSummaryResp
}

func ToManagedCharactersResponse(managedCharacters []model_user.ManagedCharacter) []ManagedCharactersArrayObject {
	managedCharactersResp := make([]ManagedCharactersArrayObject, len(managedCharacters))
	for i, obj := range managedCharacters {
		managedCharactersResp[i] = ManagedCharactersArrayObject{
			ArousalLevel:       int64(obj.ArousalLevel),
			CharacterId:        int64(obj.CharacterId),
			DuplicatedCount:    int64(obj.DuplicatedCount),
			Exp:                int64(obj.Exp),
			Level:              int64(obj.Level),
			LevelBreak:         int64(obj.LevelBreak),
			LevelLimit:         int64(obj.LevelLimit),
			ManagedCharacterId: int64(obj.ManagedCharacterId),
			PlayerId:           int64(obj.PlayerId),
			Shown:              int64(obj.Shown),
			SkillExp1:          int64(obj.SkillExp1),
			SkillExp2:          int64(obj.SkillExp2),
			SkillExp3:          int64(obj.SkillExp3),
			SkillLevel1:        int64(obj.SkillLevel1),
			SkillLevel2:        int64(obj.SkillLevel2),
			SkillLevel3:        int64(obj.SkillLevel3),
			SkillLevelLimit1:   int64(obj.SkillLevelLimit1),
			SkillLevelLimit2:   int64(obj.SkillLevelLimit2),
			SkillLevelLimit3:   int64(obj.SkillLevelLimit3),
			ViewCharacterId:    int64(obj.ViewCharacterId),
		}
	}
	return managedCharactersResp
}

func ToManagedNamedTypesResponse(managedNamedTypes []model_user.ManagedNamedType) []ManagedNamedTypesArrayObject {
	managedNamedTypesResp := make([]ManagedNamedTypesArrayObject, len(managedNamedTypes))
	for i := range managedNamedTypes {
		managedNamedTypesResp[i] = ManagedNamedTypesArrayObject{
			Exp:                  int64(managedNamedTypes[i].Exp),
			FriendshipExpTableId: int64(managedNamedTypes[i].FriendshipExpTableId),
			Level:                int64(managedNamedTypes[i].Level),
			ManagedNamedTypeId:   int64(managedNamedTypes[i].ManagedNamedTypeId),
			NamedType:            int64(managedNamedTypes[i].NamedType),
			TitleType:            int64(managedNamedTypes[i].TitleType),
		}
	}
	return managedNamedTypesResp
}

func ToManagedRoomObjectsResponse(managedRoomObjects []model_user.ManagedRoomObject) []ManagedRoomObjectsArrayObject {
	managedRoomObjectsResp := make([]ManagedRoomObjectsArrayObject, len(managedRoomObjects))
	for i := range managedRoomObjects {
		managedRoomObjectsResp[i] = ManagedRoomObjectsArrayObject{
			ManagedRoomObjectId: int64(managedRoomObjects[i].ManagedRoomObjectId),
			RoomObjectId:        int64(managedRoomObjects[i].RoomObjectId),
		}
	}
	return managedRoomObjectsResp
}

func ToManagedWeaponsResponse(managedWeapons []model_user.ManagedWeapon) []ManagedWeaponsArrayObject {
	managedWeaponsResp := make([]ManagedWeaponsArrayObject, len(managedWeapons))
	for i := range managedWeapons {
		managedWeaponsResp[i] = ManagedWeaponsArrayObject{
			Exp:             int64(managedWeapons[i].Exp),
			Level:           int64(managedWeapons[i].Level),
			ManagedWeaponId: int64(managedWeapons[i].ManagedWeaponId),
			SkillExp:        int64(managedWeapons[i].SkillExp),
			SkillLevel:      int64(managedWeapons[i].SkillLevel),
			SoldAt:          "0001-01-01T00:00:00",
			State:           int64(managedWeapons[i].State),
			WeaponId:        int64(managedWeapons[i].WeaponId),
		}
	}
	return managedWeaponsResp
}

func ToManagedRoomsResponse(managedRooms []model_user.ManagedRoom) []ManagedRoomsArrayObject {
	managedRoomsResp := make([]ManagedRoomsArrayObject, len(managedRooms))
	for i := range managedRooms {
		arrangeData := make([]ArrangeDataArrayObject, len(managedRooms[i].ArrangeData))
		for i2 := range managedRooms[i].ArrangeData {
			arrangeData[i2] = ArrangeDataArrayObject{
				Dir:                 int64(managedRooms[i].ArrangeData[i2].Dir),
				ManagedRoomObjectId: int64(managedRooms[i].ArrangeData[i2].ManagedRoomObjectId),
				RoomNo:              int64(managedRooms[i].ArrangeData[i2].RoomNo),
				RoomObjectId:        int64(managedRooms[i].ArrangeData[i2].RoomObjectId),
				X:                   int64(managedRooms[i].ArrangeData[i2].X),
				Y:                   int64(managedRooms[i].ArrangeData[i2].Y),
			}
		}
		managedRoomsResp[i] = ManagedRoomsArrayObject{
			ManagedRoomId: int64(managedRooms[i].ManagedRoomId),
			FloorId:       int64(managedRooms[i].FloorId),
			GroupId:       int64(managedRooms[i].GroupId),
			Active:        int64(managedRooms[i].Active),
			ArrangeData:   arrangeData,
		}
	}
	return managedRoomsResp
}

func ToAdvIdsResponse(advIds []model_user.ClearedAdvId) []int64 {
	advIdsResp := make([]int64, len(advIds))
	for i := range advIds {
		advIdsResp[i] = int64(advIds[i].AdvId)
	}
	return advIdsResp
}

func ToFavoriteMembersResponse(favoriteMembers []model_user.FavoriteMember) []FavoriteMembersArrayObject {
	favoriteMembersResp := make([]FavoriteMembersArrayObject, len(favoriteMembers))
	for i, obj := range favoriteMembers {
		favoriteMembersResp[i] = FavoriteMembersArrayObject{
			ArousalLevel:       int64(obj.ArousalLevel),
			CharacterId:        int64(obj.CharacterId),
			FavoriteIndex:      int64(obj.FavoriteIndex),
			ManagedCharacterId: int64(obj.ManagedCharacterId),
		}
	}
	return favoriteMembersResp
}

func ToManagedAbilityBoardsResponse(managedAbilityBoards []model_user.ManagedAbilityBoard) []PlayerAbilityBoardsArrayObject {
	managedAbilityBoardsResp := make([]PlayerAbilityBoardsArrayObject, len(managedAbilityBoards))
	for i, obj := range managedAbilityBoards {
		equipItemIds := make([]int64, len(obj.EquipItemIds))
		for j, equipItemObj := range obj.EquipItemIds {
			equipItemIds[j] = int64(equipItemObj.ItemId)
		}
		managedAbilityBoardsResp[i] = PlayerAbilityBoardsArrayObject{
			ManagedAbilityBoardId: int64(obj.ManagedAbilityBoardId),
			AbilityBoardId:        int64(obj.AbilityBoardId),
			ManagedCharacterId:    int64(obj.ManagedCharacterId),
			EquipItemIds:          equipItemIds,
		}
	}
	return managedAbilityBoardsResp
}

func ToManagedBattlePartiesResponse(managedBattleParties []model_user.ManagedBattleParty) []ManagedBattlePartiesArrayObject {
	managedBattlePartiesResp := make([]ManagedBattlePartiesArrayObject, len(managedBattleParties))
	for i, obj := range managedBattleParties {
		managedCharacterIds := []int64{
			int64(obj.ManagedCharacterId1),
			int64(obj.ManagedCharacterId2),
			int64(obj.ManagedCharacterId3),
			int64(obj.ManagedCharacterId4),
			int64(obj.ManagedCharacterId5),
		}
		managedWeaponIds := []int64{
			int64(obj.ManagedWeaponId1),
			int64(obj.ManagedWeaponId2),
			int64(obj.ManagedWeaponId3),
			int64(obj.ManagedWeaponId4),
			int64(obj.ManagedWeaponId5),
		}
		managedBattlePartiesResp[i] = ManagedBattlePartiesArrayObject{
			CostLimit:            int64(obj.CostLimit),
			ManagedBattlePartyId: int64(obj.ManagedBattlePartyId),
			ManagedCharacterId1:  managedCharacterIds[0],
			ManagedCharacterId2:  managedCharacterIds[1],
			ManagedCharacterId3:  managedCharacterIds[2],
			ManagedCharacterId4:  managedCharacterIds[3],
			ManagedCharacterId5:  managedCharacterIds[4],
			ManagedCharacterIds:  managedCharacterIds,
			ManagedWeaponId1:     managedWeaponIds[0],
			ManagedWeaponId2:     managedWeaponIds[1],
			ManagedWeaponId3:     managedWeaponIds[2],
			ManagedWeaponId4:     managedWeaponIds[3],
			ManagedWeaponId5:     managedWeaponIds[4],
			ManagedWeaponIds:     managedWeaponIds,
			MasterOrbId:          int64(obj.MasterOrbId),
			Name:                 obj.Name,
		}
	}
	return managedBattlePartiesResp
}

func ToManagedFieldPartyMembersResponse(managedFieldPartyMembers []model_user.ManagedFieldPartyMember) []ManagedFieldPartyMembersArrayObject {
	managedFieldPartyMembersResp := make([]ManagedFieldPartyMembersArrayObject, len(managedFieldPartyMembers))
	for i, managedFieldPartyMember := range managedFieldPartyMembers {
		managedFieldPartyMembersResp[i] = ManagedFieldPartyMembersArrayObject{
			ArousalLevel:         0,
			Character:            nil,
			CharacterId:          int64(managedFieldPartyMember.CharacterId),
			Flag:                 int64(managedFieldPartyMember.Flag),
			LiveIdx:              int64(managedFieldPartyMember.LiveIdx),
			ManagedCharacterId:   int64(managedFieldPartyMember.ManagedCharacterId),
			ManagedFacilityId:    int64(managedFieldPartyMember.ManagedFacilityId),
			ManagedPartyMemberId: int64(managedFieldPartyMember.ManagedPartyMemberId),
			PartyDropPresents:    nil,
			ScheduleTable: func() string {
				if managedFieldPartyMember.ScheduleTable == nil {
					return ""
				}
				return *managedFieldPartyMember.ScheduleTable
			}(),
			RoomId:            int64(managedFieldPartyMember.RoomId),
			ScheduleId:        int64(managedFieldPartyMember.ScheduleId),
			ScheduleTag:       int64(managedFieldPartyMember.ScheduleTag),
			TouchItemResultNo: int64(managedFieldPartyMember.TouchItemResultNo),
		}
	}
	return managedFieldPartyMembersResp
}

func ToManagedMasterOrbsResponse(managedMasterOrbs []model_user.ManagedMasterOrb) []ManagedMasterOrbsArrayObject {
	managedMasterOrbsResp := make([]ManagedMasterOrbsArrayObject, len(managedMasterOrbs))
	for i, managedMasterOrb := range managedMasterOrbs {
		managedMasterOrbsResp[i] = ManagedMasterOrbsArrayObject{
			Exp:                int64(managedMasterOrb.Exp),
			Level:              int64(managedMasterOrb.Level),
			ManagedMasterOrbId: int64(managedMasterOrb.ManagedMasterOrbId),
			MasterOrbId:        int64(managedMasterOrb.MasterOrbId),
		}
	}
	return managedMasterOrbsResp
}

func ToManagedTownFacilitiesResponse(managedTownFacilities []model_user.ManagedTownFacility) []ManagedTownFacilitiesArrayObject {
	managedTownFacilitiesResp := make([]ManagedTownFacilitiesArrayObject, len(managedTownFacilities))
	for i, managedFacility := range managedTownFacilities {
		managedTownFacilitiesResp[i] = ManagedTownFacilitiesArrayObject{
			ActionTime:            int64(managedFacility.ActionTime),
			BuildPointIndex:       int64(managedFacility.BuildPointIndex),
			BuildTime:             int64(managedFacility.BuildTime),
			FacilityId:            int64(managedFacility.FacilityId),
			Level:                 int64(managedFacility.Level),
			ManagedTownFacilityId: int64(managedFacility.ManagedTownFacilityId),
			OpenState:             int64(managedFacility.OpenState),
		}
	}
	return managedTownFacilitiesResp
}

func ToManagedTownsResponse(managedTowns []model_user.ManagedTown) []ManagedTownsArrayObject {
	managedTownsResp := make([]ManagedTownsArrayObject, len(managedTowns))
	for i, managedTown := range managedTowns {
		managedTownsResp[i] = ManagedTownsArrayObject{
			GridData:      string(managedTown.GridData),
			ManagedTownId: int64(managedTown.ManagedTownId),
		}
	}
	return managedTownsResp
}

func ToOfferTitleTypesResponse(offerTitleTypes []model_user.OfferTitleType) []OfferTitleTypesArrayObject {
	offerTitleTypesResp := make([]OfferTitleTypesArrayObject, len(offerTitleTypes))
	for i, offerTitleType := range offerTitleTypes {
		offerTitleTypesResp[i] = OfferTitleTypesArrayObject{
			Category:       int64(offerTitleType.Category),
			ContentRoomFlg: int64(offerTitleType.ContentRoomFlg),
			OfferIndex:     int64(offerTitleType.OfferIndex),
			OfferMaxPoint:  int64(offerTitleType.OfferMaxPoint),
			OfferPoint:     int64(offerTitleType.OfferPoint),
			Shown:          int64(offerTitleType.Shown),
			State:          int64(offerTitleType.State),
			SubState:       int64(offerTitleType.SubState),
			SubState1:      int64(offerTitleType.SubState1),
			SubState2:      int64(offerTitleType.SubState2),
			SubState3:      int64(offerTitleType.SubState3),
			TitleType:      int64(offerTitleType.TitleType),
		}
	}
	return offerTitleTypesResp
}

func ToSupportCharactersResponse(supportCharacters []model_user.SupportCharacter) []SupportCharactersArrayObject {
	supportCharactersResp := make([]SupportCharactersArrayObject, len(supportCharacters))
	for i, supportCharacter := range supportCharacters {
		managedCharacterIds := make([]int64, len(supportCharacter.ManagedCharacterIds))
		for i2, managedCharacterIdWrap := range supportCharacter.ManagedCharacterIds {
			managedCharacterIds[i2] = int64(managedCharacterIdWrap.ManagedCharacterId)
		}
		managedWeaponIds := make([]int64, len(supportCharacter.ManagedWeaponIds))
		for i2, managedWeaponIdWrap := range supportCharacter.ManagedWeaponIds {
			managedWeaponIds[i2] = int64(managedWeaponIdWrap.ManagedWeaponId)
		}
		supportCharactersResp[i] = SupportCharactersArrayObject{
			Active:              int64(supportCharacter.Active),
			ManagedCharacterIds: managedCharacterIds,
			ManagedSupportId:    int64(supportCharacter.ManagedSupportId),
			ManagedWeaponIds:    managedWeaponIds,
			Name:                supportCharacter.Name,
		}
	}
	return supportCharactersResp
}

func ToTipIdsResponse(tipIds []model_user.ShownTipId) []int64 {
	tipIdsResp := make([]int64, len(tipIds))
	for i := range tipIds {
		tipIdsResp[i] = int64(tipIds[i].TipId)
	}
	return tipIdsResp
}

package interfaces

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func ToBasePlayerResponse(user *model_user.User) BasePlayer {
	return BasePlayer{
		Age:                  int64(user.Age),
		CharacterLimit:       int64(user.CharacterLimit),
		CharacterWeaponCount: int64(user.CharacterWeaponCount),
		Comment:              user.Comment,
		ContinuousDays:       int64(user.ContinuousDays),
		CreatedAt:            response.ToSparkleTime(user.CreatedAt),
		CurrentAchievementId: int64(user.CurrentAchievementId),
		FacilityLimit:        int64(user.FacilityLimit),
		FacilityLimitCount:   int64(user.FacilityLimitCount),
		FriendLimit:          int64(user.FriendLimit),
		Gold:                 int64(user.Gold),
		Id:                   int64(user.Id),
		IpAddr:               user.IpAddr,
		ItemLimit:            int64(user.ItemLimit),
		Kirara:               int64(user.Kirara),
		KiraraLimit:          int64(user.KiraraLimit),
		LastLoginAt:          response.ToSparkleTime(user.LastLoginAt),
		// TODO: Fix below line can get sparkleTime or nil
		LastPartyAdded:       "0001-01-01T00:00:00",
		Level:                int64(user.Level),
		LevelExp:             int64(user.LevelExp),
		LimitedGem:           int64(user.LimitedGem),
		LoginCount:           int64(user.LoginCount),
		LoginDays:            int64(user.LoginDays),
		LotteryTicket:        int64(user.LotteryTicket),
		MyCode:               string(user.MyCode),
		Name:                 user.Name,
		PartyCost:            int64(user.PartyCost),
		RecastTime:           int64(user.RecastTime),
		RecastTimeMax:        int64(user.RecastTimeMax),
		RoomObjectLimit:      int64(user.RoomObjectLimit),
		RoomObjectLimitCount: int64(user.RoomObjectLimitCount),
		Stamina:              int64(user.Stamina),
		StaminaMax:           int64(user.StaminaMax),
		StaminaUpdatedAt:     response.ToSparkleTime(user.StaminaUpdatedAt),
		State:                int64(user.State),
		SupportLimit:         int64(user.SupportLimit),
		TotalExp:             int64(user.TotalExp),
		UnlimitedGem:         int64(user.UnlimitedGem),
		UserAgent:            user.UserAgent,
		WeaponLimit:          int64(user.WeaponLimit),
		WeaponLimitCount:     int64(user.WeaponLimitCount),
	}
}

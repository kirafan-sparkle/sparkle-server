/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

import (
	"context"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/internal/application/service"
	value_stamina "gitlab.com/kirafan/sparkle/server/internal/domain/value/stamina"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx/user_id"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// PlayerStaminaApiService is a service that implements the logic for the PlayerStaminaApiServicer
// This service should implement the business logic for every endpoint for the PlayerStaminaApi API.
// Include any external packages or services that will be required by this service.
type PlayerStaminaApiService struct {
	ps service.PlayerStaminaService
}

// NewPlayerStaminaApiService creates a default api service
func NewPlayerStaminaApiService(ps service.PlayerStaminaService) PlayerStaminaApiServicer {
	return &PlayerStaminaApiService{ps}
}

// AddPlayerStamina - Add Player Stamina (WIP)
func (s *PlayerStaminaApiService) AddPlayerStamina(ctx context.Context, req AddPlayerStaminaRequest) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "AddPlayerStamina")
	defer span.End()

	internalId, valid := user_id.FromContext(ctx)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}
	consumeType, err := value_stamina.NewStaminaConsumeType(uint8(req.Type))
	if err != nil {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
	}
	consumeItem, err := value_stamina.NewStaminaItemType(int16(req.ItemId))
	if err != nil {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
	}
	consumeAmount, err := value_stamina.NewStaminaItemAmount(uint16(req.Num))
	if err != nil {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
	}

	user, serr := s.ps.AddPlayerStamina(ctx, internalId, consumeType, consumeItem, consumeAmount)
	if serr != nil {
		span.RecordError(serr.GetRawError())
		return Response(http.StatusOK, serr.AsResponse()), nil
	}

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, AddPlayerStaminaResponse{
		Player:              ToBasePlayerResponse(user),
		ItemSummary:         toItemSummaryResponse(user.ItemSummary),
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}

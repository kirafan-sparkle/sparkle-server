package interfaces

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func toCompletePlayerQuestLogResponse(
	u *model_user.User,
	success *response.BaseResponse,
	isFirstClear bool,
) *CompletePlayerQuestLogResponse {
	// Format player
	var outPlayer BasePlayer
	calc.Copy(&outPlayer, &u)
	outPlayer.CreatedAt = response.ToSparkleTime(u.CreatedAt)
	outPlayer.LastLoginAt = response.ToSparkleTime(u.LastLoginAt)
	outPlayer.LastPartyAdded = "0001-01-01T00:00:00"
	outPlayer.StaminaUpdatedAt = response.ToSparkleTime(u.StaminaUpdatedAt)

	var outManagedCharacters []ManagedCharactersArrayObject
	calc.Copy(&outManagedCharacters, &u.ManagedCharacters)
	var outManagedNamedTypes []ManagedNamedTypesArrayObject
	calc.Copy(&outManagedNamedTypes, &u.ManagedNamedTypes)
	var outManagedWeapons []ManagedWeaponsArrayObject
	calc.Copy(&outManagedWeapons, &u.ManagedWeapons)
	var outOfferTitleTypes []OfferTitleTypesArrayObject
	calc.Copy(&outOfferTitleTypes, &u.OfferTitleTypes)
	var outItemSummary []ItemSummaryArrayObject
	calc.Copy(&outItemSummary, &u.ItemSummary)

	base := CompletePlayerQuestLogResponse{
		IsContinuous:      0,
		IsFirstComplete:   1,
		ItemSummary:       outItemSummary,
		ManagedCharacters: outManagedCharacters,
		ManagedNamedTypes: outManagedNamedTypes,
		ManagedWeapons:    outManagedWeapons,
		Player:            outPlayer,
		OfferTitleTypes:   outOfferTitleTypes,
		Offers:            make([]interface{}, 0),
		// Event clear rewards
		// TODO: implement me
		QuestNoticeTitle:   nil,
		QuestNoticeMessage: nil,
		PointEventResult:   nil,
		// First clear rewards
		// TODO: implement me
		RewardFirst: nil,
		RewardGroup: nil,
		RewardComp:  nil,
		// Template response
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}
	if !isFirstClear {
		base.IsContinuous = 1
		base.IsFirstComplete = 0
	}
	return &base
}

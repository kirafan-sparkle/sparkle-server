/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type ArousalResultsArrayObject struct {
	// Alternative item if the character arousal level is max
	ArousalItemAmount int64 `json:"arousalItemAmount"`

	ArousalItemId int64 `json:"arousalItemId"`

	ArousalLevel int64 `json:"arousalLevel"`

	CharacterId int64 `json:"characterId"`

	DuplicatedCount int64 `json:"duplicatedCount"`
}

// AssertArousalResultsArrayObjectRequired checks if the required fields are not zero-ed
func AssertArousalResultsArrayObjectRequired(obj ArousalResultsArrayObject) error {
	elements := map[string]interface{}{
		"arousalItemAmount": obj.ArousalItemAmount,
		"arousalItemId":     obj.ArousalItemId,
		"arousalLevel":      obj.ArousalLevel,
		"characterId":       obj.CharacterId,
		"duplicatedCount":   obj.DuplicatedCount,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	return nil
}

// AssertRecurseArousalResultsArrayObjectRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of ArousalResultsArrayObject (e.g. [][]ArousalResultsArrayObject), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseArousalResultsArrayObjectRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aArousalResultsArrayObject, ok := obj.(ArousalResultsArrayObject)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertArousalResultsArrayObjectRequired(aArousalResultsArrayObject)
	})
}

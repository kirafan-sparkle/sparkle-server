/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

import (
	"context"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/internal/application/service"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx/user_id"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// PlayerMoveApiService is a service that implements the logic for the PlayerMoveApiServicer
// This service should implement the business logic for every endpoint for the PlayerMoveApi API.
// Include any external packages or services that will be required by this service.
type PlayerMoveApiService struct {
	pms service.PlayerMoveService
}

// NewPlayerMoveApiService creates a default api service
func NewPlayerMoveApiService(pms service.PlayerMoveService) PlayerMoveApiServicer {
	return &PlayerMoveApiService{pms}
}

// GetPlayerMove - Get Player Move (WIP)
func (s *PlayerMoveApiService) GetPlayerMove(ctx context.Context, req GetPlayerMoveRequest) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "GetPlayerMove")
	defer span.End()

	internalId, valid := user_id.FromContext(ctx)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}
	// TODO: add password value model
	if len(req.Password) < 4 || len(req.Password) > 16 {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
	}

	user, newMoveCode, err := s.pms.SetPlayerAccountTransfer(ctx, internalId, req.Password)
	if err != nil {
		span.RecordError(err.GetRawError())
		return Response(http.StatusOK, err.AsResponse()), nil
	}

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, GetPlayerMoveResponse{
		MoveCode:            newMoveCode,
		MoveDeadline:        response.ToSparkleTime(user.MoveDeadline),
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}

// SetPlayerMove - Set Player Move (WIP)
func (s *PlayerMoveApiService) SetPlayerMove(ctx context.Context, req SetPlayerMoveRequest) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "SetPlayerMove")
	defer span.End()

	platform, err := value_version.NewPlatform(uint8(req.Platform))
	if err != nil {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
	}

	user, serr := s.pms.UsePlayerAccountTransfer(ctx, req.MoveCode, req.MovePassword, req.Uuid, platform)
	if serr != nil {
		span.RecordError(serr.GetRawError())
		return Response(http.StatusOK, serr.AsResponse()), nil
	}

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, SetPlayerMoveResponse{
		AccessToken: user.Session,
		PlayerId:    int64(user.Id),
		Uuid:        user.UUId,
		// Template
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}

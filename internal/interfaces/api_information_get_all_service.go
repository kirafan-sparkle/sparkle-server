/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

import (
	"context"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/internal/application/service"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// InformationGetAllApiService is a service that implements the logic for the InformationGetAllApiServicer
// This service should implement the business logic for every endpoint for the InformationGetAllApi API.
// Include any external packages or services that will be required by this service.
type InformationGetAllApiService struct {
	is service.InformationService
}

// NewInformationGetAllApiService creates a default api service
func NewInformationGetAllApiService(is service.InformationService) InformationGetAllApiServicer {
	return &InformationGetAllApiService{is}
}

// GetAllInformation - Get All Information (WIP)
func (s *InformationGetAllApiService) GetAllInformation(ctx context.Context, platform int64) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "GetAllInformation")
	defer span.End()
	platformValue, err := value_version.NewPlatform(uint8(platform))
	if err != nil {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
	}

	infos, err := s.is.GetAllInformation(ctx, platformValue)
	if err != nil {
		span.RecordError(err)
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_DB_ERROR)), nil
	}

	var infosResp []InformationsArrayObject
	calc.Copy(&infosResp, &infos)
	for i := range infosResp {
		infosResp[i].StartAt = response.ToSparkleTime(infos[i].StartAt)
		infosResp[i].EndAt = response.ToSparkleTime(infos[i].EndAt)
		infosResp[i].DispStartAt = response.ToSparkleTime(infos[i].DispStartAt)
		infosResp[i].DispEndAt = response.ToSparkleTime(infos[i].DispEndAt)
	}
	success := response.NewSuccessResponse()
	return Response(http.StatusOK, GetAllInformationResponse{
		Informations:        infosResp,
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}

/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

import (
	"context"
	"net/http"

	schema_weapon "gitlab.com/kirafan/sparkle/server/internal/application/schemas/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/application/service"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx/user_id"
	"gitlab.com/kirafan/sparkle/server/pkg/parser"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// PlayerWeaponApiService is a service that implements the logic for the PlayerWeaponApiServicer
// This service should implement the business logic for every endpoint for the PlayerWeaponApi API.
// Include any external packages or services that will be required by this service.
type PlayerWeaponApiService struct {
	as service.PlayerWeaponService
}

// NewPlayerWeaponApiService creates a default api service
func NewPlayerWeaponApiService(as service.PlayerWeaponService) PlayerWeaponApiServicer {
	return &PlayerWeaponApiService{as}
}

// AddPlayerWeaponLimit - Add Player Weapon Limit
func (s *PlayerWeaponApiService) AddPlayerWeaponLimit(ctx context.Context, req AddPlayerWeaponLimitRequest) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "AddPlayerWeaponLimit")
	defer span.End()
	internalUserId, valid := user_id.FromContext(ctx)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	user, err := s.as.AddPlayerWeaponLimit(ctx, internalUserId)
	if err != nil {
		span.RecordError(err)
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_DB_ERROR)), nil
	}

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, AddPlayerWeaponLimitResponse{
		Player:              ToBasePlayerResponse(user),
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil

}

// EvolutionPlayerWeapon - Evolution Player Weapon
func (s *PlayerWeaponApiService) EvolutionPlayerWeapon(ctx context.Context, req EvolutionPlayerWeaponRequest) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "EvolutionPlayerWeapon")
	defer span.End()
	internalUserId, valid := user_id.FromContext(ctx)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	managedWeaponId := value_user.NewManagedWeaponId(req.ManagedWeaponId)
	resp, err := s.as.EvoluteWeapon(ctx, internalUserId, managedWeaponId, uint(req.WeaponEvolutionId))
	if err != nil {
		span.RecordError(err)
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_DB_ERROR)), nil
	}

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, EvolutionPlayerWeaponResponse{
		Gold:        int64(resp.Gold),
		ItemSummary: toItemSummaryResponse(resp.ItemSummary),
		ManagedWeapon: EvolutionPlayerWeaponResponseManagedWeapon{
			Exp:             int64(resp.ManagedWeapon.Exp),
			Level:           int64(resp.ManagedWeapon.Level),
			ManagedWeaponId: int64(resp.ManagedWeapon.ManagedWeaponId),
			SkillExp:        int64(resp.ManagedWeapon.SkillExp),
			SkillLevel:      int64(resp.ManagedWeapon.SkillLevel),
			// NOTE: This value is same as official server's one (weird...)
			SoldAt:   "0001-01-01T00:00:00",
			State:    int64(resp.ManagedWeapon.State),
			WeaponId: int64(resp.ManagedWeapon.WeaponId),
		},
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil

}

// MakePlayerWeapon - Make Player Weapon
func (s *PlayerWeaponApiService) MakePlayerWeapon(ctx context.Context, req MakePlayerWeaponRequest) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "MakePlayerWeapon")
	defer span.End()
	internalUserId, valid := user_id.FromContext(ctx)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	user, serr := s.as.MakeWeapon(ctx, internalUserId, uint32(req.RecipeId))
	if serr != nil {
		span.RecordError(serr.GetRawError())
		return Response(http.StatusOK, serr.AsResponse()), nil
	}

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, MakePlayerWeaponResponse{
		ItemSummary:         toItemSummaryResponse(user.ItemSummary),
		ManagedWeapons:      ToManagedWeaponsResponse(user.ManagedWeapons),
		Player:              ToBasePlayerResponse(user),
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}

// ReceivePlayerWeapon - Receive Player Weapon
func (s *PlayerWeaponApiService) ReceivePlayerWeapon(ctx context.Context, req ReceivePlayerWeaponRequest) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "ReceivePlayerWeapon")
	defer span.End()
	internalUserId, valid := user_id.FromContext(ctx)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	managedCharacterIds := make([]value_user.ManagedCharacterId, len(req.ManagedCharacterIds))
	for i := 0; i < len(req.ManagedCharacterIds); i++ {
		managedCharacterIds[i] = value_user.NewManagedCharacterId(req.ManagedCharacterIds[i])
	}
	resp, err := s.as.ReceiveWeapon(ctx, internalUserId, managedCharacterIds)
	if err != nil {
		span.RecordError(err)
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_DB_ERROR)), nil
	}

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, ReceivePlayerWeaponResponse{
		ManagedWeapons:      ToManagedWeaponsResponse(resp.NewWeapons),
		WeaponLimit:         int64(resp.WeaponLimit),
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}

// SalePlayerWeapon - Sale Player Weapon
func (s *PlayerWeaponApiService) SalePlayerWeapon(ctx context.Context, req SalePlayerWeaponRequest) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "SalePlayerWeapon")
	defer span.End()
	internalUserId, valid := user_id.FromContext(ctx)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	// Convert string to uint
	managedWeaponIdBases := parser.ParseCommaStringAsInt64Array(req.ManagedWeaponId)
	managedWeaponIds := make([]value_user.ManagedWeaponId, len(managedWeaponIdBases))
	for i := 0; i < len(managedWeaponIdBases); i++ {
		managedWeaponIds[i] = value_user.NewManagedWeaponId(managedWeaponIdBases[i])
	}
	user, err := s.as.SaleWeapon(ctx, internalUserId, managedWeaponIds)
	if err != nil {
		span.RecordError(err)
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_DB_ERROR)), nil
	}

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, SalePlayerWeaponResponse{
		ManagedWeapons:      ToManagedWeaponsResponse(user.ManagedWeapons),
		Player:              ToBasePlayerResponse(user),
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}

// UpgradePlayerWeapon - Upgrade Player Weapon
func (s *PlayerWeaponApiService) UpgradePlayerWeapon(ctx context.Context, req UpgradePlayerWeaponRequest) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "UpgradePlayerWeapon")
	defer span.End()
	internalUserId, valid := user_id.FromContext(ctx)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	// create param
	itemsRaw := parser.ParseCommaStringAsInt64Array(req.ItemId)
	amountsRaw := parser.ParseCommaStringAsInt64Array(req.Amount)
	if len(itemsRaw) != len(amountsRaw) {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
	}
	items := make([]schema_weapon.ConsumeItem, len(itemsRaw))
	for i := 0; i < len(itemsRaw); i++ {
		itemId, err := value_item.NewItemId(itemsRaw[i])
		if err != nil {
			return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
		}
		items[i] = schema_weapon.ConsumeItem{
			ItemId: itemId,
			Count:  uint16(amountsRaw[i]),
		}
	}

	managedWeaponId := value_user.NewManagedWeaponId(req.ManagedWeaponId)
	resp, serr := s.as.UpgradeWeapon(ctx, internalUserId, managedWeaponId, items)
	if serr != nil {
		span.RecordError(serr.GetRawError())
		return Response(http.StatusOK, serr.AsResponse()), nil
	}

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, UpgradePlayerWeaponResponse{
		Gold:        int64(resp.Gold),
		ItemSummary: toItemSummaryResponse(resp.ItemSummary),
		ManagedWeapon: UpgradePlayerWeaponResponseManagedWeapon{
			Exp:             int64(resp.ManagedWeapon.Exp),
			Level:           int64(resp.ManagedWeapon.Level),
			ManagedWeaponId: int64(resp.ManagedWeapon.ManagedWeaponId),
			SkillExp:        int64(resp.ManagedWeapon.SkillExp),
			SkillLevel:      int64(resp.ManagedWeapon.SkillLevel),
			SoldAt:          "0001-01-01T00:00:00",
			State:           int64(resp.ManagedWeapon.State),
			WeaponId:        int64(resp.ManagedWeapon.WeaponId),
		},
		SuccessType:         int64(resp.UpgradeResult),
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}

func (s *PlayerWeaponApiService) SkillUpPlayerWeapon(ctx context.Context, req SkillUpPlayerWeaponRequest) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "UpgradePlayerWeapon")
	defer span.End()
	internalUserId, valid := user_id.FromContext(ctx)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	// Why this endpoint take multiple items...? (only one item can be specified from the client side)
	if len(req.ItemIds) != 1 || len(req.Amounts) != 1 {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
	}
	itemId, err := value_item.NewItemId(req.ItemIds[0])
	if err != nil {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
	}
	if _, err := value_weapon.NewSkillUpItemType(itemId); err != nil {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
	}

	items := make([]schema_weapon.ConsumeItem, 1)
	items[0] = schema_weapon.ConsumeItem{
		ItemId: value_weapon.SkillUpItemIdSkillUpPowder,
		Count:  uint16(req.Amounts[0]),
	}

	managedWeaponId := value_user.NewManagedWeaponId(req.ManagedWeaponId)

	resp, serr := s.as.SkillUpPlayerWeapon(ctx, internalUserId, managedWeaponId, items)
	if serr != nil {
		span.RecordError(serr.GetRawError())
		return Response(http.StatusOK, serr.AsResponse()), nil
	}

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, SkillUpPlayerWeaponResponse{
		ItemSummary: toItemSummaryResponse(resp.ItemSummary),
		ManagedWeapon: ManagedWeaponsArrayObject{
			Exp:             int64(resp.ManagedWeapon.Exp),
			Level:           int64(resp.ManagedWeapon.Level),
			ManagedWeaponId: int64(resp.ManagedWeapon.ManagedWeaponId),
			SkillExp:        int64(resp.ManagedWeapon.SkillExp),
			SkillLevel:      int64(resp.ManagedWeapon.SkillLevel),
			SoldAt:          "0001-01-01T00:00:00",
			State:           int64(resp.ManagedWeapon.State),
			WeaponId:        int64(resp.ManagedWeapon.WeaponId),
		},
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}

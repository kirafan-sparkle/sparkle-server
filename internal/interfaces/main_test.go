package interfaces

import (
	"context"
	"os"
	"testing"

	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/migrate"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/seed"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx/user_id"
	"gorm.io/gorm"
)

var db *gorm.DB
var validUserId1Context = user_id.WithContext(context.TODO(), uint(1))
var baseIgnoreFields = []string{
	"NewAchievementCount",
	"ResultCode",
	"ResultMessage",
	"ServerVersion",
	"ServerTime",
}

func TestMain(m *testing.M) {
	logger := observability.InitLogger(observability.LogTypeDebug)
	dbConf := database.GetConfig()
	db = database.InitDatabase(dbConf, logger)
	migrate.AutoMigrate(db)
	seed.AutoSeed(db)

	exitVal := m.Run()
	os.Exit(exitVal)
}

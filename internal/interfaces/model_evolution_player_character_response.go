/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type EvolutionPlayerCharacterResponse struct {
	Gold int64 `json:"gold"`

	ItemSummary []ItemSummaryArrayObject `json:"itemSummary"`

	ManagedCharacter EvolutionPlayerCharacterResponseManagedCharacter `json:"managedCharacter"`

	// Player's unread new achievement count (most useless value)
	NewAchievementCount int32 `json:"newAchievementCount"`

	// Generic server resultCode response type (Ex. 0)
	ResultCode int32 `json:"resultCode"`

	// Generic server resultMessage type (Ex. SUCCESS)
	ResultMessage string `json:"resultMessage"`

	// Generic server time response type (Ex. 2023-01-20T00:49:57)
	ServerTime string `json:"serverTime"`

	// Generic server version response type (Ex. 2211181)
	ServerVersion int32 `json:"serverVersion"`
}

// AssertEvolutionPlayerCharacterResponseRequired checks if the required fields are not zero-ed
func AssertEvolutionPlayerCharacterResponseRequired(obj EvolutionPlayerCharacterResponse) error {
	elements := map[string]interface{}{
		"gold":                obj.Gold,
		"itemSummary":         obj.ItemSummary,
		"managedCharacter":    obj.ManagedCharacter,
		"newAchievementCount": obj.NewAchievementCount,
		"resultCode":          obj.ResultCode,
		"resultMessage":       obj.ResultMessage,
		"serverTime":          obj.ServerTime,
		"serverVersion":       obj.ServerVersion,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	for _, el := range obj.ItemSummary {
		if err := AssertItemSummaryArrayObjectRequired(el); err != nil {
			return err
		}
	}
	if err := AssertEvolutionPlayerCharacterResponseManagedCharacterRequired(obj.ManagedCharacter); err != nil {
		return err
	}
	return nil
}

// AssertRecurseEvolutionPlayerCharacterResponseRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of EvolutionPlayerCharacterResponse (e.g. [][]EvolutionPlayerCharacterResponse), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseEvolutionPlayerCharacterResponseRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aEvolutionPlayerCharacterResponse, ok := obj.(EvolutionPlayerCharacterResponse)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertEvolutionPlayerCharacterResponseRequired(aEvolutionPlayerCharacterResponse)
	})
}

/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

import (
	"context"
	"net/http"

	schema_training "gitlab.com/kirafan/sparkle/server/internal/application/schemas/training"
	"gitlab.com/kirafan/sparkle/server/internal/application/service"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_training "gitlab.com/kirafan/sparkle/server/internal/domain/value/training"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx/user_id"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// PlayerTrainingApiService is a service that implements the logic for the PlayerTrainingApiServicer
// This service should implement the business logic for every endpoint for the PlayerTrainingApi API.
// Include any external packages or services that will be required by this service.
type PlayerTrainingApiService struct {
	service service.PlayerTrainingService
}

// NewPlayerTrainingApiService creates a default api service
func NewPlayerTrainingApiService(srv service.PlayerTrainingService) PlayerTrainingApiServicer {
	return &PlayerTrainingApiService{srv}
}

func (s *PlayerTrainingApiService) formatTrainingInfoResponse(userTrainings []*model_user.UserTraining) []TrainingInfoArrayObject {
	outTrainingInfo := make([]TrainingInfoArrayObject, len(userTrainings))
	for i, userTraining := range userTrainings {
		calc.Copy(&outTrainingInfo[i], &userTraining)
		outTrainingInfo[i].Clear = int64(userTraining.Cleared)
		training := userTraining.Training
		calc.Copy(&outTrainingInfo[i], &training)
		outTrainingInfo[i].Rewards = make([]RewardsArrayObject, len(training.Rewards))
		for j, reward := range training.Rewards {
			calc.Copy(&outTrainingInfo[i].Rewards[j], &reward)
		}
	}
	return outTrainingInfo
}

func (s *PlayerTrainingApiService) formatTrainingSlotResponse(userTrainingSlots []*model_user.UserTrainingSlot) []SlotInfoArrayObject {
	slotInfo := make([]SlotInfoArrayObject, len(userTrainingSlots))
	for i, slot := range userTrainingSlots {
		slotInfo[i].ManagedCharacterIds = make([]int64, len(slot.ManagedCharacters))
		for j, managedCharacterInfo := range slot.ManagedCharacters {
			slotInfo[i].ManagedCharacterIds[j] = int64(managedCharacterInfo.ManagedCharacterId)
		}
		slotInfo[i].NeedRank = int64(slot.TrainingSlot.NeedRank)
		slotInfo[i].OpenState = int64(slot.OpenSlot)
		slotInfo[i].OrderAt = "0001-01-01T00:00:00"
		slotInfo[i].OrderId = -1
		slotInfo[i].SlotId = int64(slot.TrainingSlotId)
		if slot.Order != nil {
			slotInfo[i].OrderAt = response.ToSparkleTime(slot.Order.StartAt)
			slotInfo[i].OrderId = int64(*slot.OrderId)
			slotInfo[i].TrainingData = &SlotInfoArrayObjectTrainingdata{}
			calc.Copy(&slotInfo[i].TrainingData, &slot.Order.UserTraining.Training)
		}
	}
	return slotInfo
}

// CancelPlayerTraining - Cancel Player Training (WIP)
func (s *PlayerTrainingApiService) CancelPlayerTraining(ctx context.Context, req CancelPlayerTrainingRequest) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "CancelPlayerTraining")
	defer span.End()

	internalId, valid := user_id.FromContext(ctx)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	// Run process
	result, err := s.service.CancelPlayerTraining(ctx, internalId, uint(req.OrderId))
	if err != nil {
		span.RecordError(err)
		return Response(http.StatusOK, err.AsResponse()), nil
	}

	// Format response models
	player := ToBasePlayerResponse(&result.BasePlayer)
	outSlotInfo := s.formatTrainingSlotResponse(result.SlotInfo)
	outTrainingInfo := s.formatTrainingInfoResponse(result.TrainingInfo)

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, CancelPlayerTrainingResponse{
		Player:              player,
		SlotInfo:            outSlotInfo,
		TrainingInfo:        outTrainingInfo,
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}

// CompletesPlayerTraining - Completes Player Training (WIP)
func (s *PlayerTrainingApiService) CompletesPlayerTraining(ctx context.Context, req CompletesPlayerTrainingRequest) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "CompletesPlayerTraining")
	defer span.End()

	internalId, valid := user_id.FromContext(ctx)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	// Format request models
	params := make(schema_training.CompletesTrainingRequestSchema, len(req.TrainingCompleteParams))
	for i, r := range req.TrainingCompleteParams {
		skipGem, err := value.NewBoolLikeUint8(uint8(r.SkipGem))
		if err != nil {
			return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
		}
		params[i] = schema_training.CompletesTrainingRequestPartSchema{
			OrderId: uint(r.OrderId),
			SkipGem: skipGem,
		}
	}

	// Run process
	result, err := s.service.CompletesPlayerTraining(ctx, internalId, params)
	if err != nil {
		span.RecordError(err)
		return Response(http.StatusOK, err.AsResponse()), nil
	}

	// Format response models
	player := ToBasePlayerResponse(&result.BasePlayer)
	itemSummary := toItemSummaryResponse(result.ItemSummary)
	managedCharacters := ToManagedCharactersResponse(result.ManagedCharacters)
	managedNamedTypes := ToManagedNamedTypesResponse(result.ManagedNamedTypes)
	outSlotInfo := s.formatTrainingSlotResponse(result.SlotInfo)
	outTrainingInfo := s.formatTrainingInfoResponse(result.TrainingInfo)

	trainingCompleteAwards := make([]TrainingCompleteRewardsArrayObject, len(result.Rewards))
	for i, reward := range result.Rewards {
		trainingCompleteAwards[i].SlotId = int64(reward.SlotId)
		trainingCompleteAwards[i].FirstGem = int64(reward.FirstGem)
		trainingCompleteAwards[i].RewardCharaExp = int64(reward.RewardCharaExp)
		trainingCompleteAwards[i].RewardFriendship = int64(reward.RewardFriendship)
		trainingCompleteAwards[i].RewardGold = int64(reward.RewardGold)
		trainingCompleteAwards[i].RewardKRRPoint = int64(reward.RewardKRRPoint)
		trainingCompleteAwards[i].TrainingRewardItems = make([]TrainingRewardItemsArrayObject, len(reward.TrainingRewardItems))
		for j, item := range reward.TrainingRewardItems {
			trainingCompleteAwards[i].TrainingRewardItems[j].ItemId = int64(item.ItemId)
			trainingCompleteAwards[i].TrainingRewardItems[j].ItemAmount = int64(item.ItemAmount)
			trainingCompleteAwards[i].TrainingRewardItems[j].RareDisp = int64(item.RareDisp)
		}
	}

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, CompletesPlayerTrainingResponse{
		NewAchievementCount:     success.NewAchievementCount,
		ResultCode:              success.ResultCode,
		ResultMessage:           success.ResultMessage,
		ServerTime:              success.ServerTime,
		ServerVersion:           success.ServerVersion,
		Player:                  player,
		ItemSummary:             itemSummary,
		ManagedCharacters:       managedCharacters,
		ManagedNamedTypes:       managedNamedTypes,
		SlotInfo:                outSlotInfo,
		TrainingInfo:            outTrainingInfo,
		TrainingCompleteRewards: trainingCompleteAwards,
	}), nil
}

// GetListPlayerTraining - Get List Player Training
func (s *PlayerTrainingApiService) GetListPlayerTraining(ctx context.Context, body map[string]interface{}) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "GetListPlayerTraining")
	defer span.End()

	internalId, valid := user_id.FromContext(ctx)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	userTrainings, userTrainingSlots, err := s.service.GetListPlayerTraining(ctx, internalId)
	if err != nil {
		span.RecordError(err)
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_DB_ERROR)), nil
	}

	outTrainingInfo := s.formatTrainingInfoResponse(userTrainings)
	outSlotInfo := s.formatTrainingSlotResponse(userTrainingSlots)

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, GetListPlayerTrainingResponse{
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
		SlotInfo:            outSlotInfo,
		TrainingInfo:        outTrainingInfo,
	}), nil
}

// OrdersPlayerTraining - Orders Player Training
func (s *PlayerTrainingApiService) OrdersPlayerTraining(ctx context.Context, req OrdersPlayerTrainingRequest) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "OrdersPlayerTraining")
	defer span.End()

	internalId, valid := user_id.FromContext(ctx)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	// Format request models
	params := make(schema_training.OrdersTrainingRequestSchema, len(req.TrainingOrderParams))
	for i, trainingOrderParam := range req.TrainingOrderParams {
		params[i].ManagedCharacterIds = make([]value_user.ManagedCharacterId, len(trainingOrderParam.ManagedCharacterIds))
		for j, managedCharacterId := range trainingOrderParam.ManagedCharacterIds {
			params[i].ManagedCharacterIds[j] = value_user.NewManagedCharacterId(managedCharacterId)
		}
		params[i].TrainingId = uint(trainingOrderParam.TrainingId)
		slotId, err := value_training.NewSlotId(uint8(trainingOrderParam.SlotId))
		if err != nil {
			span.RecordError(err)
			return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
		}
		params[i].SlotId = slotId
	}

	// Call process
	result, err := s.service.OrdersPlayerTraining(ctx, internalId, params)
	if err != nil {
		span.RecordError(err)
		return Response(http.StatusOK, err.AsResponse()), nil
	}

	// Format response models
	player := ToBasePlayerResponse(&result.BasePlayer)
	outSlotInfo := s.formatTrainingSlotResponse(result.SlotInfo)
	outTrainingInfo := s.formatTrainingInfoResponse(result.TrainingInfo)

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, OrdersPlayerTrainingResponse{
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
		Player:              player,
		SlotInfo:            outSlotInfo,
		TrainingInfo:        outTrainingInfo,
	}), nil
}

/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type SetNamePlayerCharacterSupportRequest struct {
	ManagedSupportId int64 `json:"managedSupportId"`

	Name string `json:"name"`
}

// AssertSetNamePlayerCharacterSupportRequestRequired checks if the required fields are not zero-ed
func AssertSetNamePlayerCharacterSupportRequestRequired(obj SetNamePlayerCharacterSupportRequest) error {
	elements := map[string]interface{}{
		"managedSupportId": obj.ManagedSupportId,
		"name":             obj.Name,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	return nil
}

// AssertRecurseSetNamePlayerCharacterSupportRequestRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of SetNamePlayerCharacterSupportRequest (e.g. [][]SetNamePlayerCharacterSupportRequest), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseSetNamePlayerCharacterSupportRequestRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aSetNamePlayerCharacterSupportRequest, ok := obj.(SetNamePlayerCharacterSupportRequest)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertSetNamePlayerCharacterSupportRequestRequired(aSetNamePlayerCharacterSupportRequest)
	})
}

/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

import (
	"context"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/internal/application/service"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// PlayerSignupApiService is a service that implements the logic for the PlayerSignupApiServicer
// This service should implement the business logic for every endpoint for the PlayerSignupApi API.
// Include any external packages or services that will be required by this service.
type PlayerSignupApiService struct {
	pss service.PlayerSignupService
}

// NewPlayerSignupApiService creates a default api service
func NewPlayerSignupApiService(pss service.PlayerSignupService) PlayerSignupApiServicer {
	return &PlayerSignupApiService{pss}
}

// SignupPlayer - Signup Player (WIP)
// Create a new user with the given name and returns it.
// If the user already exists, it will return an error.
func (s *PlayerSignupApiService) SignupPlayer(ctx context.Context, signupPlayerRequest SignupPlayerRequest) (ImplResponse, error) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "SignupPlayer")
	defer span.End()

	platform, err := value_version.NewPlatform(uint8(signupPlayerRequest.Platform))
	if err != nil {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
	}
	stepCode, err := value_user.NewStepCode(int8(signupPlayerRequest.StepCode))
	if err != nil {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
	}

	user, serr := s.pss.SignupPlayer(
		ctx,
		signupPlayerRequest.Name,
		platform,
		stepCode,
		signupPlayerRequest.Uuid,
	)
	if serr != nil {
		span.RecordError(serr.GetRawError())
		return Response(http.StatusOK, serr.AsResponse()), nil
	}

	success := response.NewSuccessResponse()
	return Response(http.StatusOK, SignupPlayerResponse{
		AccessToken:         user.Session,
		PlayerId:            int64(user.Id),
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}), nil
}

/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

import (
	"net/http"
	"strings"
)

// PlayerResumeApiController binds http requests to an api service and writes the service results to the http response
type PlayerResumeApiController struct {
	service      PlayerResumeApiServicer
	errorHandler ErrorHandler
}

// PlayerResumeApiOption for how the controller is set up.
type PlayerResumeApiOption func(*PlayerResumeApiController)

// WithPlayerResumeApiErrorHandler inject ErrorHandler into controller
func WithPlayerResumeApiErrorHandler(h ErrorHandler) PlayerResumeApiOption {
	return func(c *PlayerResumeApiController) {
		c.errorHandler = h
	}
}

// NewPlayerResumeApiController creates a default api controller
func NewPlayerResumeApiController(s PlayerResumeApiServicer, opts ...PlayerResumeApiOption) Router {
	controller := &PlayerResumeApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}

	for _, opt := range opts {
		opt(controller)
	}

	return controller
}

// Routes returns all the api routes for the PlayerResumeApiController
func (c *PlayerResumeApiController) Routes() Routes {
	return Routes{
		{
			"GetPlayerResume",
			strings.ToUpper("Get"),
			"/api/player/resume/get",
			c.GetPlayerResume,
		},
	}
}

// GetPlayerResume - Get Player Resume (WIP)
func (c *PlayerResumeApiController) GetPlayerResume(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	playerIdParam, err := parseInt64Parameter(query.Get("playerId"), false)
	if err != nil {
		c.errorHandler(w, r, &ParsingError{Err: err}, nil)
		return
	}
	result, err := c.service.GetPlayerResume(r.Context(), playerIdParam)
	// If an error occurred, encode the error with the status code
	if err != nil {
		c.errorHandler(w, r, err, &result)
		return
	}
	// If no error, encode the body and the result code
	EncodeJSONResponse(result.Body, &result.Code, w)

}

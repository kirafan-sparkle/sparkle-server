package interfaces

import (
	schema_quest "gitlab.com/kirafan/sparkle/server/internal/application/schemas/quest"
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// FIXME: There is no way to separate this DTO(database to response model conversion)
// The DTO depends interfaces package's models, but this router also located at interfaces package
// So, I just put this conversion code here.
// If you have any idea, please let me know.

func toQuestNpcsArrayObject(npc model_quest.QuestNpc) QuestNpcsArrayObject {
	return QuestNpcsArrayObject{
		Id:                  int64(npc.Id),
		CharacterId:         int64(npc.CharacterId),
		CharacterLevel:      int64(npc.CharacterLevel),
		CharacterLimitBreak: int64(npc.CharacterLimitBreak),
		CharacterSkillLevel: int64(npc.CharacterSkillLevel),
		WeaponId:            int64(npc.WeaponId),
		WeaponLevel:         int64(npc.WeaponLevel),
		WeaponSkillLevel:    int64(npc.WeaponSkillLevel),
	}
}
func toQuestNpcsArray(npcs []model_quest.QuestNpc) []QuestNpcsArrayObject {
	out := make([]QuestNpcsArrayObject, 0, len(npcs))
	for i, v := range npcs {
		out[i] = toQuestNpcsArrayObject(v)
	}
	return out
}
func toQuestsArrayObjectQuest(quest model_quest.Quest) QuestsArrayObjectQuest {
	out := QuestsArrayObjectQuest{
		Id:         int64(quest.Id),         // Quest Id
		Name:       quest.Name,              // Quest Name
		TypeIconId: int64(quest.TypeIconId), // Quest Icon
		AdvId1:     quest.AdvId1,            // Story Id before battle
		AdvId2:     quest.AdvId2,            // Story Id after battle
		AdvId3:     quest.AdvId3,            // Story Id after lose (For Part2-1-8 Daemon Shadow Mistress)
		AdvOnly:    int64(quest.AdvOnly),    // Story only quest if 1 specified (default 0)
		Category:   int64(quest.Category),   // display category of quest menu (maybe)
		ExId2:      int64(quest.ExId2),      // Required item to start quest
		Ex2Amount:  int64(quest.Ex2Amount),  // Required item amount to start quest
		Stamina:    int64(quest.Stamina),    // Required stamina to start quest
		WaveId1:    quest.WaveId1,           // Waves of battles
		WaveId2:    quest.WaveId2,
		WaveId3:    quest.WaveId3,
		WaveId4:    quest.WaveId4,
		WaveId5:    quest.WaveId5,
		WaveIds:    []int64{quest.WaveId1, quest.WaveId2, quest.WaveId3, quest.WaveId4, quest.WaveId5},
		QuestFirstClearReward: EventQuestsArrayObjectEventquestQuestQuestFirstClearReward{
			Amount1:          int64(quest.QuestFirstClearReward.Amount1),
			Amount2:          int64(quest.QuestFirstClearReward.Amount2),
			Amount3:          int64(quest.QuestFirstClearReward.Amount3),
			Gem:              int64(quest.QuestFirstClearReward.Gem),
			Gold:             int64(quest.QuestFirstClearReward.Gold),
			ItemId1:          int64(quest.QuestFirstClearReward.ItemId1),
			ItemId2:          int64(quest.QuestFirstClearReward.ItemId2),
			ItemId3:          int64(quest.QuestFirstClearReward.ItemId3),
			RoomObjectAmount: int64(quest.QuestFirstClearReward.RoomObjectAmount),
			RoomObjectId:     int64(quest.QuestFirstClearReward.RoomObjectId),
			WeaponAmount:     int64(quest.QuestFirstClearReward.WeaponAmount),
			WeaponId:         int64(quest.QuestFirstClearReward.WeaponId),
		}, // First clear reward fields
		Amount1:             int64(quest.QuestFirstClearReward.Amount1), // Same as first clear field struct. Mystery fields but required...
		Amount2:             int64(quest.QuestFirstClearReward.Amount2),
		Amount3:             int64(quest.QuestFirstClearReward.Amount3),
		ItemId1:             int64(quest.QuestFirstClearReward.ItemId1),
		ItemId2:             int64(quest.QuestFirstClearReward.ItemId2),
		ItemId3:             int64(quest.QuestFirstClearReward.ItemId3),
		RoomObjectAmount:    int64(quest.QuestFirstClearReward.RoomObjectAmount),
		RoomObjectId:        int64(quest.QuestFirstClearReward.RoomObjectId),
		WeaponAmount:        int64(quest.QuestFirstClearReward.WeaponAmount),
		WeaponId:            int64(quest.QuestFirstClearReward.WeaponId),
		Gem1:                int64(quest.QuestFirstClearReward.Gem),
		Gold1:               int64(quest.QuestFirstClearReward.Gold),
		RewardCharacterExp:  int64(quest.RewardCharacterExp), // Every clear reward fields
		RewardFriendshipExp: int64(quest.RewardFriendshipExp),
		RewardMoney:         int64(quest.RewardMoney),
		RewardUserExp:       int64(quest.RewardUserExp),
		BgId:                int64(quest.BgId),                 // Background Id
		BgmCue:              quest.BgmCue,                      // BGM Id
		LastBgmCue:          quest.LastBgmCue,                  // LastWave BGM Id
		IsNpcOnly:           quest.IsNpcOnly,                   // Hide friend characters at support selection if true (default false)
		QuestNpcs:           toQuestNpcsArray(quest.QuestNpcs), // Recommended support characters for this quest
		IsHideElement:       int64(quest.IsHideElement),        // Hide enemy element type on quest list if 1 specified (default 0)
		RetryLimit:          int64(quest.RetryLimit),           // Disallow retry feature if 1 specified (default 0)
		LoserBattle:         int64(quest.LoserBattle),          // Always clear as gold even lose if 1 specified (default 0)
		Warn:                int64(quest.Warn),                 // Maybe player level warn (which range of level makes warn?) (Default 0)
		StoreReview:         int64(quest.StoreReview),          // Maybe shows store review link (Looks like not working) (Default 0)
		Section:             int64(quest.Section),              // Mystery value, maybe not used in main quests. (Default -1)
		MainFlg:             int64(quest.MainFlg),              // Mystery value, last event was set 1 but other quests were -1. (Default -1)
	}
	return out
}
func toInnerEventQuestObject(eventQuest *model_quest.EventQuest, innerQuest EventQuestsArrayObjectEventquestQuest) EventQuestsArrayObjectEventquest {
	outInnerEventQuest := EventQuestsArrayObjectEventquest{
		Id:        int64(eventQuest.Id),
		EventType: int64(eventQuest.EventType),
		StartAt:   response.ToSparkleTime(eventQuest.StartAt),
		EndAt:     response.ToSparkleTime(eventQuest.EndAt),
		ChestId:   int64(eventQuest.ChestId),
		// FIXME: Implement condEventQuestIds later
		CondEventQuestIds: make([]interface{}, 0),
		ExAlchemist:       int64(eventQuest.ExAlchemist),
		ExFighter:         int64(eventQuest.ExFighter),
		ExKnight:          int64(eventQuest.ExKnight),
		ExMagician:        int64(eventQuest.ExMagician),
		ExPriest:          int64(eventQuest.ExPriest),
		ExFire:            int64(eventQuest.ExFire),
		ExWater:           int64(eventQuest.ExWater),
		ExWind:            int64(eventQuest.ExWind),
		ExEarth:           int64(eventQuest.ExEarth),
		ExSun:             int64(eventQuest.ExSun),
		ExMoon:            int64(eventQuest.ExMoon),
		ExName:            int64(eventQuest.ExName),
		ExCost:            int64(eventQuest.ExCost),
		ExRarity:          int64(eventQuest.ExRarity),
		ExTitle:           int64(eventQuest.ExTitle),
		GroupId:           int64(eventQuest.GroupId),
		GroupName:         eventQuest.GroupName,
		GroupSkip:         int64(eventQuest.GroupSkip),
		Mon:               int64(eventQuest.Mon),
		Tue:               int64(eventQuest.Tue),
		Wed:               int64(eventQuest.Wed),
		Thu:               int64(eventQuest.Thu),
		Fri:               int64(eventQuest.Fri),
		Sat:               int64(eventQuest.Sat),
		Sun:               int64(eventQuest.Sun),
		Freq:              int64(eventQuest.Freq),
		Interval:          int64(eventQuest.Interval),
		Day:               int64(eventQuest.Day),
		Month:             int64(eventQuest.Month),
		OrderLimit:        int64(eventQuest.OrderLimit),
		Quest:             innerQuest,
		MapFlg:            int64(eventQuest.MapFlg),
		TradeGroupId:      int64(eventQuest.TradeGroupId),
	}
	return outInnerEventQuest
}
func toOuterEventQuestObject(eventQuest *model_quest.EventQuest, innerEventQuest EventQuestsArrayObjectEventquest, clearRanks model_user.UserQuestClearRanks) EventQuestsArrayObject {
	outOuterEventQuest := EventQuestsArrayObject{
		EventQuest:          innerEventQuest,
		ChestCostItemAmount: int64(eventQuest.ChestCostItemAmount),
		ChestCostItemId:     int64(eventQuest.ChestCostItemId),
		ClearRank:           int64(clearRanks.GetClearRank(uint(innerEventQuest.Quest.Id))),
		// Maybe dead parameters
		IsRead:     0,
		OrderDaily: 0,
		OrderTotal: 0,
	}
	return outOuterEventQuest
}
func toEventQuestsArrayObject(eventQuest *model_quest.EventQuest, clearRanks model_user.UserQuestClearRanks) EventQuestsArrayObject {
	// FIXME: don't use copy, use same field type instead this one
	var innerQuest EventQuestsArrayObjectEventquestQuest
	calc.Copy(&innerQuest, toQuestsArrayObjectQuest(eventQuest.Quest))
	innerEventQuest := toInnerEventQuestObject(eventQuest, innerQuest)
	outerEventQuest := toOuterEventQuestObject(eventQuest, innerEventQuest, clearRanks)

	return outerEventQuest
}
func toEventQuestsArray(eventQuests []*model_quest.EventQuest, clearRanks model_user.UserQuestClearRanks) []EventQuestsArrayObject {
	out := make([]EventQuestsArrayObject, len(eventQuests))
	for i, v := range eventQuests {
		out[i] = toEventQuestsArrayObject(v, clearRanks)
	}
	return out
}

func toGetAllPlayerQuestResponse(res *schema_quest.AllQuestInfoWithClearRanksSchema, success *response.BaseResponse) *GetAllPlayerQuestResponse {
	// Format resp->quests field
	outQuestPart1s := make([]QuestsArrayObject, len(res.QuestPart1s))
	for i := range outQuestPart1s {
		var copiedQuest QuestsArrayObjectQuest
		calc.Copy(&copiedQuest, &res.QuestPart1s[i])
		calc.Copy(&copiedQuest, &copiedQuest.QuestFirstClearReward)
		copiedQuest.Gem1 = copiedQuest.QuestFirstClearReward.Gem
		copiedQuest.Gold1 = copiedQuest.QuestFirstClearReward.Gold
		// Fill empty array
		if len(copiedQuest.QuestNpcs) == 0 {
			copiedQuest.QuestNpcs = make([]QuestNpcsArrayObject, 0)
		}
		// Fill duplicated value
		copiedQuest.WaveIds = []int64{
			copiedQuest.WaveId1,
			copiedQuest.WaveId2,
			copiedQuest.WaveId3,
			copiedQuest.WaveId4,
			copiedQuest.WaveId5,
		}
		// Fill Mystery values
		copiedQuest.MainFlg = 0
		copiedQuest.Section = -1
		// Finalize QuestsArrayObject
		clearRank := int64(0)
		for i2 := range res.ClearRanks {
			if res.ClearRanks[i2].QuestId == uint(copiedQuest.Id) {
				clearRank = int64(res.ClearRanks[i2].ClearRank)
				break
			}
		}
		outQuestPart1s[i] = QuestsArrayObject{
			ClearRank: clearRank,
			Quest:     copiedQuest,
		}
	}
	// Format resp->questPart2s field
	outQuestPart2s := make([]Questpart2sArrayObject, len(res.QuestPart2s))
	for i := range outQuestPart2s {
		var copiedQuest Questpart2sArrayObjectQuest
		calc.Copy(&copiedQuest, &res.QuestPart2s[i])
		calc.Copy(&copiedQuest, &copiedQuest.QuestFirstClearReward)
		copiedQuest.Gem1 = copiedQuest.QuestFirstClearReward.Gem
		copiedQuest.Gold1 = copiedQuest.QuestFirstClearReward.Gold
		// Fill empty array
		if len(copiedQuest.QuestNpcs) == 0 {
			copiedQuest.QuestNpcs = make([]QuestNpcsArrayObject, 0)
		}
		// Fill duplicated value
		copiedQuest.WaveIds = []int64{
			copiedQuest.WaveId1,
			copiedQuest.WaveId2,
			copiedQuest.WaveId3,
			copiedQuest.WaveId4,
			copiedQuest.WaveId5,
		}
		// Fill Mystery values
		copiedQuest.MainFlg = 0
		copiedQuest.Section = -1
		// Finalize QuestsArrayObject
		clearRank := int64(0)
		for i2 := range res.ClearRanks {
			if res.ClearRanks[i2].QuestId == uint(copiedQuest.Id) {
				clearRank = int64(res.ClearRanks[i2].ClearRank)
				break
			}
		}
		outQuestPart2s[i] = Questpart2sArrayObject{
			ClearRank: clearRank,
			Quest:     copiedQuest,
		}
	}
	// Format resp->characterQuests field
	outCharacterQuests := make([]CharacterQuestsArrayObject, len(res.CharacterQuests))
	for i := range outCharacterQuests {
		var copiedQuest CharacterQuestsArrayObjectQuest
		calc.Copy(&copiedQuest, &res.CharacterQuests[i].Quest)
		calc.Copy(&copiedQuest, &copiedQuest.QuestFirstClearReward)
		copiedQuest.Gem1 = copiedQuest.QuestFirstClearReward.Gem
		copiedQuest.Gold1 = copiedQuest.QuestFirstClearReward.Gold
		// Fill empty array
		if len(copiedQuest.QuestNpcs) == 0 {
			copiedQuest.QuestNpcs = make([]QuestNpcsArrayObject, 0)
		}
		// Fill duplicated value
		copiedQuest.WaveIds = []int64{
			copiedQuest.WaveId1,
			copiedQuest.WaveId2,
			copiedQuest.WaveId3,
			copiedQuest.WaveId4,
			copiedQuest.WaveId5,
		}
		// Fill Mystery values
		copiedQuest.MainFlg = 0
		copiedQuest.Section = -1
		// Finalize QuestsArrayObject
		clearRank := int64(0)
		for i2 := range res.ClearRanks {
			if res.ClearRanks[i2].QuestId == uint(copiedQuest.Id) {
				clearRank = int64(res.ClearRanks[i2].ClearRank)
				break
			}
		}
		outCharacterQuests[i] = CharacterQuestsArrayObject{
			ClearRank:   ClearRank(clearRank),
			Quest:       copiedQuest,
			CharacterId: int64(res.CharacterQuests[i].CharacterId),
			// If same as other structure, maybe this field is dead
			IsRead: 0,
		}
	}

	outEventQuests := toEventQuestsArray(res.EventQuests, res.ClearRanks)

	outEventQuestPeriods := make([]EventQuestPeriodsArrayObject, len(res.EventQuestPeriods))
	for i := range outEventQuestPeriods {
		var out EventQuestPeriodsArrayObject
		calc.Copy(&out, &res.EventQuestPeriods[i])
		out.StartAt = response.ToSparkleTime(res.EventQuestPeriods[i].StartAt)
		out.EndAt = response.ToSparkleTime(res.EventQuestPeriods[i].EndAt)
		var lossTimeEndAt *string
		if res.EventQuestPeriods[i].LossTimeEndAt != nil {
			lossTimeEndAt = calc.ToPtr(response.ToSparkleTime(*res.EventQuestPeriods[i].LossTimeEndAt))
		}
		out.LossTimeEndAt = lossTimeEndAt

		outEventQuestPeriodGroups := make([]EventQuestPeriodGroupsArrayObject, len(res.EventQuestPeriods[i].EventQuestPeriodGroups))
		for i2 := range outEventQuestPeriodGroups {
			var outPeriodGroup EventQuestPeriodGroupsArrayObject
			calc.Copy(&outPeriodGroup, &res.EventQuestPeriods[i].EventQuestPeriodGroups[i2])
			outPeriodGroup.StartAt = response.ToSparkleTime(res.EventQuestPeriods[i].EventQuestPeriodGroups[i2].StartAt)
			outPeriodGroup.EndAt = response.ToSparkleTime(res.EventQuestPeriods[i].EventQuestPeriodGroups[i2].EndAt)
			outEventQuestPeriodGroupQuests := make([]EventQuestPeriodGroupQuestsArrayObject, len(res.EventQuestPeriods[i].EventQuestPeriodGroups[i2].EventQuestPeriodGroupQuests))
			for i3 := range outEventQuestPeriodGroupQuests {
				var outPeriodGroupQuest EventQuestPeriodGroupQuestsArrayObject
				calc.Copy(&outPeriodGroupQuest, &res.EventQuestPeriods[i].EventQuestPeriodGroups[i2].EventQuestPeriodGroupQuests[i3])
				outPeriodGroupQuest.StartAt = response.ToSparkleTime(res.EventQuestPeriods[i].EventQuestPeriodGroups[i2].EventQuestPeriodGroupQuests[i3].StartAt)
				outPeriodGroupQuest.EndAt = response.ToSparkleTime(res.EventQuestPeriods[i].EventQuestPeriodGroups[i2].EventQuestPeriodGroupQuests[i3].EndAt)
				// FIXME: This is a workaround to avoid wrong model
				outPeriodGroupQuest.CondEventQuestIds = make([]interface{}, 0)
				outEventQuestPeriodGroupQuests[i3] = outPeriodGroupQuest
			}
			outPeriodGroup.EventQuestPeriodGroupQuests = outEventQuestPeriodGroupQuests
			outEventQuestPeriodGroups[i2] = outPeriodGroup
		}
		out.EventQuestPeriodGroups = outEventQuestPeriodGroups
		outEventQuestPeriods[i] = out
	}

	return &GetAllPlayerQuestResponse{
		Quests:                    outQuestPart1s,
		QuestPart2s:               outQuestPart2s,
		EventQuests:               outEventQuests,
		EventQuestPeriods:         outEventQuestPeriods,
		QuestStaminaReductions:    []QuestStaminaReductionsArrayObject{},
		CharacterQuests:           outCharacterQuests,
		PlayerOfferQuests:         []PlayerOfferQuestsArrayObject{},
		LastPlayedChapterQuestIds: []int64{int64(res.LastPlayedChapterQuestIdPart1), int64(res.LastPlayedChapterQuestIdPart2)},
		PlayedOpenChapterIds:      []int64{int64(res.PlayedOpenChapterIdPart1), int64(res.PlayedOpenChapterIdPart2)},
		ReadGroups:                make([]interface{}, 0),
		// Template responses
		ServerVersion:       success.ServerVersion,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		NewAchievementCount: success.NewAchievementCount,
	}
}

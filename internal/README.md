# internal

This is core implementation directory.
Welcome to DDD architecture!

## Folder structure

```
internal
├─ interfaces
├─ application
│  ├─ service
│  └─ usecase
├─ domain
│  ├─ model
│  └─ repository
│  └─ value
└─ infrastructure
   ├─ database
   └─ middleware
   └─ observability
   └─ persistence
   └─ router.go
```

## Dependency tree

### Base

1. main.go (entrypoint)
2. router.go
3. interfaces
4. application/service
5. application/usecase
6. domain/repository
7. domain/model+value
8. infrastructure/persistence

### Extra

1. main.go
2. router.go
3. middleware or observability

## Folder guide

- `interfaces`
  - request & response handler (a.k.a controller layer)
  - they converts domain objects and raw request/response structures
- `application/service`
  - core game logics (a.k.a usecase layer)
  - they use domain usecase and domain objects for do a single endpoint handling
- `application/usecase`
  - they implements a usecase for use at application/service
- `domain/model` / `domain/value`
  - domain objects
  - they are used at application/usecase and application/service
- `domain/repository`
  - interface for domain model saving and loading
  - they are used at application/usecase and infrastructure/persistence
- `infrastructure/persistence`
  - handle domain model saving and loading
  - they are used at domain/repository
- `registry`
  - dependency injection (by google/wire)
  - they are used at main.go

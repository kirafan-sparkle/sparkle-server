//go:build wireinject
// +build wireinject

package registry

import (
	"github.com/google/wire"
	"github.com/gorilla/mux"
	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/kirafan/sparkle/server/internal/application/service"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/middleware"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
	registry_providers_interfaces "gitlab.com/kirafan/sparkle/server/internal/registry/providers/interfaces"
	registry_providers_usecase "gitlab.com/kirafan/sparkle/server/internal/registry/providers/usecase"
	"gitlab.com/kirafan/sparkle/server/pkg/env"
	"gorm.io/gorm"
)

func InitializeMiddlewares(logger *otelzap.Logger, db *gorm.DB, serverGlobalConf *env.ServerGlobalConfig) middleware.Middlewares {
	wire.Build(
		env.GetEncryptKey,
		env.GetEncryptPad,
		env.GetIpWhitelistRanges,
		persistence.NewSessionRepositoryImpl,
		middleware.GetMiddlewares,
	)
	return middleware.Middlewares{}
}

func InitializeRouter(logger *otelzap.Logger, db *gorm.DB, serverGlobalConf *env.ServerGlobalConfig) *mux.Router {
	wire.Build(
		// Env value (special)
		env.GetScheduleEndpoint,
		// Usecase (DDD)
		registry_providers_usecase.UserUsecaseSet,
		registry_providers_usecase.VersionUsecaseSet,
		registry_providers_usecase.ChapterUsecaseSet,
		registry_providers_usecase.QuestUsecaseSet,
		registry_providers_usecase.MissionUsecaseSet,
		registry_providers_usecase.PresentUsecaseSet,
		registry_providers_usecase.QuestWaveUsecaseSet,
		registry_providers_usecase.GachaUsecaseSet,
		registry_providers_usecase.NamedTypeUsecaseSet,
		registry_providers_usecase.CharacterUsecaseSet,
		registry_providers_usecase.ItemUsecaseSet,
		registry_providers_usecase.TradeRecipeUsecaseSet,
		registry_providers_usecase.PackageItemUsecaseSet,
		registry_providers_usecase.ScheduleUsecaseSet,
		registry_providers_usecase.LoginBonusUsecaseSet,
		registry_providers_usecase.ExpTableWeaponUsecaseSet,
		registry_providers_usecase.ExpTableCharacterUsecaseSet,
		registry_providers_usecase.ExpTableSkillUsecaseSet,
		registry_providers_usecase.ExpTableRankUsecaseSet,
		registry_providers_usecase.ExpTableFriendshipUsecaseSet,
		registry_providers_usecase.EvoTableLimitBreakUsecaseSet,
		registry_providers_usecase.EvoTableEvolutionUsecaseSet,
		registry_providers_usecase.RoomObjectUsecaseSet,
		registry_providers_usecase.TownFacilityUsecaseSet,
		registry_providers_usecase.LevelTableTownFacilityUsecaseSet,
		registry_providers_usecase.ItemTableTownFacilityUsecaseSet,
		registry_providers_usecase.WeaponCharacterTableUsecaseSet,
		registry_providers_usecase.WeaponTableUsecaseSet,
		registry_providers_usecase.WeaponRecipeUsecaseSet,
		registry_providers_usecase.EvoTableWeaponUsecaseSet,
		registry_providers_usecase.EventQuestUsecaseSet,
		registry_providers_usecase.TrainingUsecaseSet,
		registry_providers_usecase.FriendUsecaseSet,
		registry_providers_usecase.InformationUsecaseSet,
		registry_providers_usecase.EventBannerUsecaseSet,
		registry_providers_usecase.UserPresentUsecaseSet,
		registry_providers_usecase.AbilityBoardUsecaseSet,
		registry_providers_usecase.UserAbilityBoardUsecaseSet,
		registry_providers_usecase.AbilitySphereUsecaseSet,
		registry_providers_usecase.CharacterQuestUsecaseSet,
		// Service (DDD)
		service.NewAppVersionService,
		service.NewPlayerQuestService,
		service.NewPlayerGachaService,
		service.NewPlayerPresentService,
		service.NewPlayerLoginService,
		service.NewPlayerScheduleService,
		service.NewPlayerTownFacilityService,
		service.NewPlayerRoomObjectService,
		service.NewPlayerRoomService,
		service.NewPlayerCharacterService,
		service.NewPlayerAgeService,
		service.NewPlayerBattlePartyService,
		service.NewPlayerBadgeService,
		service.NewPlayerAbilityService,
		service.NewQuestChapterService,
		service.NewRoomObjectService,
		service.NewEventBannerService,
		service.NewInformationService,
		service.NewPlayerAchievementService,
		service.NewPlayerAdvService,
		service.NewPlayerExchangeShopService,
		service.NewPlayerFavoriteMemberService,
		service.NewPlayerFieldPartyService,
		service.NewCheatService,
		service.NewPlayerFriendService,
		service.NewPlayerChestService,
		service.NewPlayerContentRoomService,
		service.NewPlayerGeneralFlagSaveService,
		service.NewPlayerSetService,
		service.NewPlayerImportService,
		service.NewPlayerWeaponService,
		service.NewPlayerPushNotificationService,
		service.NewPlayerPushTokenService,
		service.NewPlayerTrainingService,
		service.NewPlayerItemService,
		service.NewPlayerGetAllService,
		service.NewPlayerLoginBonusService,
		service.NewPlayerResumeService,
		service.NewPlayerTutorialService,
		service.NewPlayerStaminaService,
		service.NewPlayerMoveService,
		service.NewPlayerTownService,
		service.NewPlayerMissionService,
		service.NewPlayerSignupService,
		// Service (router)
		registry_providers_interfaces.ApiServicerSet,
		// Middleware (router)
		InitializeMiddlewares,
		// Injectable (router)
		registry_providers_interfaces.InjectableControllerSet,
		// Registerable (router)
		infrastructure.NewRouter,
	)
	// Below line is unused, just for passing lint check
	return &mux.Router{}
}

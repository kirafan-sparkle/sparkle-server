package service

import (
	"context"
	"errors"

	schema_training "gitlab.com/kirafan/sparkle/server/internal/application/schemas/training"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_training "gitlab.com/kirafan/sparkle/server/internal/domain/model/training"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_training "gitlab.com/kirafan/sparkle/server/internal/domain/value/training"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type PlayerTrainingService interface {
	GetListPlayerTraining(ctx context.Context, internalUserId value_user.UserId) ([]*model_user.UserTraining, []*model_user.UserTrainingSlot, error)
	OrdersPlayerTraining(ctx context.Context, internalUserId value_user.UserId, params schema_training.OrdersTrainingRequestSchema) (*schema_training.OrdersTrainingResponseSchema, *errwrap.SparkleError)
	CompletesPlayerTraining(ctx context.Context, internalUserId value_user.UserId, params schema_training.CompletesTrainingRequestSchema) (*schema_training.CompletesTrainingResponseSchema, *errwrap.SparkleError)
	CancelPlayerTraining(ctx context.Context, internalUserId value_user.UserId, orderId uint) (*schema_training.CancelTrainingResponseSchema, *errwrap.SparkleError)
}

type playerTrainingService struct {
	uu  usecase.UserUsecase
	tu  usecase.TrainingUsecase
	cu  usecase.CharacterUsecase
	ecu usecase.ExpTableCharacterUsecase
	efu usecase.ExpTableFriendshipUsecase
}

func NewPlayerTrainingService(uu usecase.UserUsecase, tu usecase.TrainingUsecase, cu usecase.CharacterUsecase, ecu usecase.ExpTableCharacterUsecase, efu usecase.ExpTableFriendshipUsecase) PlayerTrainingService {
	return &playerTrainingService{uu, tu, cu, ecu, efu}
}

func (s *playerTrainingService) updateManagedCharacterLevels(
	ctx context.Context,
	user *model_user.User,
	targetManagedIds []value_user.ManagedCharacterId,
) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "updateManagedCharacterLevels")
	defer span.End()
	for i, character := range user.ManagedCharacters {
		if !calc.Contains(targetManagedIds, character.ManagedCharacterId) {
			continue
		}
		// Adjust the exp limitations
		limitCharacterExp, err := s.ecu.GetLevelExpTableCharacter(ctx, user.ManagedCharacters[i].LevelLimit)
		if err != nil {
			return err
		}
		user.ManagedCharacters[i].AdjustExp(limitCharacterExp.TotalExp)
		// Update levels
		if !user.ManagedCharacters[i].IsMaxLevel() {
			nextCharacterLevel, err := s.ecu.GetNextExpTableCharacter(ctx, character.Exp)
			if err != nil {
				return err
			}
			user.ManagedCharacters[i].UpdateLevel(nextCharacterLevel.Level)
		}
	}
	return nil
}

func (s *playerTrainingService) updateManagedNamedTypeLevels(
	ctx context.Context,
	user *model_user.User,
	targetNamedTypes []uint16,
) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "updateManagedNamedTypeLevels")
	defer span.End()
	for i, managedNamedType := range user.ManagedNamedTypes {
		if !calc.Contains(targetNamedTypes, managedNamedType.NamedType) {
			continue
		}
		nextNamedTypeLevel, err := s.efu.GetNextExpTableFriendship(ctx, uint64(managedNamedType.Exp))
		if err != nil {
			return err
		}
		user.ManagedNamedTypes[i].UpdateLevel(uint8(nextNamedTypeLevel.Level))
	}
	return nil
}

func (s *playerTrainingService) getSelectableTrainings(ctx context.Context, user *model_user.User) []*model_user.UserTraining {
	_, span := observability.Tracer.StartAppServiceSpan(ctx, "getSelectableTrainings")
	defer span.End()

	outingTrainingIds := user.TrainingSlots.GetOutingUserTrainingIds()
	validTrainings := user.Trainings.GetUserTrainingsExceptOuting(outingTrainingIds)
	return validTrainings
}

func (s *playerTrainingService) GetListPlayerTraining(ctx context.Context, internalUserId value_user.UserId) ([]*model_user.UserTraining, []*model_user.UserTrainingSlot, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetPlayerTrainings")
	defer span.End()

	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		Trainings:     true,
		TrainingSlots: true,
	})
	if err != nil {
		return nil, nil, err
	}

	selectableTrainings := s.getSelectableTrainings(ctx, user)

	return selectableTrainings, user.TrainingSlots, nil
}

func (s *playerTrainingService) OrdersPlayerTraining(
	ctx context.Context,
	internalUserId value_user.UserId,
	// NOTE: params are array of training request infos
	params schema_training.OrdersTrainingRequestSchema,
) (*schema_training.OrdersTrainingResponseSchema, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "OrdersPlayerTraining")
	defer span.End()

	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		Trainings:         true,
		TrainingSlots:     true,
		ManagedCharacters: true,
	})
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	outingTrainingIds := user.TrainingSlots.GetOutingUserTrainingIds()

	for _, v := range params {
		// Validate the training slots are available
		slot, err := user.TrainingSlots.GetSlotById(v.SlotId)
		if err != nil {
			span.RecordError(err)
			return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		}
		if !slot.IsOpened() {
			err = errors.New("slot is not open")
			span.RecordError(err)
			return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_ALREADY_PROCESSED, err)
		}
		// Validate the specified training is not duplicated
		if calc.Contains(outingTrainingIds, v.TrainingId) {
			err = errors.New("training is already out")
			span.RecordError(err)
			return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_ALREADY_PROCESSED, err)
		}
		// Get & validate the specified training id is exist
		training, err := user.Trainings.GetUserTrainingById(v.TrainingId)
		if err != nil {
			span.RecordError(err)
			return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_PARAMETERS, err)
		}
		// Validate the specified members length is valid
		if len(v.ManagedCharacterIds) != 5 {
			err = errors.New("invalid members length")
			span.RecordError(err)
			return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_PARAMETERS, err)
		}
		// Validate the specified members are available
		// FIXME: implement correct validation, this one is wrong so comment outed and trusts the client requests as it is
		// (It excludes the characters that are already out.)
		// outingCharacterIdsInSlot := slot.GetOutingManagedCharacterIds()
		// slotCharacterIds := user.GetCharacterIdsByManagedCharacterIds(outingCharacterIdsInSlot)
		// outingCharacterIds := user.GetOutingCharacterIds()
		// requestedCharacterIds := user.GetCharacterIdsByManagedCharacterIds(v.ManagedCharacterIds)
		// for _, id := range requestedCharacterIds {
		// 	if slices.Contains(outingCharacterIds, id) && !slices.Contains(slotCharacterIds, id) {
		// 		err = errors.New("character is already out")
		// 		span.RecordError(err)
		// 		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_PARAMETERS, err)
		// 	}
		// }
		// Consume the required resources
		costType, cost := training.GetCostInfo()
		switch costType {
		case value_training.TrainingCostTypeCoin:
			if consumed := user.ConsumeGold(cost); !consumed {
				err = errors.New("not enough gold")
				span.RecordError(err)
				return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_GOLD_IS_SHORT, err)
			}
		case value_training.TrainingCostTypeKirara:
			if consumed := user.ConsumeKirara(uint32(cost)); !consumed {
				err = errors.New("not enough kirara")
				span.RecordError(err)
				return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_KIRARA_IS_SHORT, err)
			}
		case value_training.TrainingCostTypeNone:
			// do nothing
		}
		// Insert new training info
		slot.SetNewOrder(training, v.ManagedCharacterIds)
	}

	// Save the changes
	if _, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		TrainingSlots: true,
	}); err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	// TODO: the result doesn't included the trainingSLot.Order.UserTraining.Training, so it requires refresh
	user, err = s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		Trainings:     true,
		TrainingSlots: true,
	})
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	// Filtering for training infos
	// It must not contains the training infos that are on-going.
	selectableTrainings := s.getSelectableTrainings(ctx, user)

	resp := schema_training.OrdersTrainingResponseSchema{
		BasePlayer:   *user,
		SlotInfo:     user.TrainingSlots,
		TrainingInfo: selectableTrainings,
	}
	return &resp, nil
}

func (s *playerTrainingService) CancelPlayerTraining(
	ctx context.Context,
	internalUserId value_user.UserId,
	orderId uint,
) (*schema_training.CancelTrainingResponseSchema, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "CancelPlayerTraining")
	defer span.End()

	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		Trainings:     true,
		TrainingSlots: true,
	})
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	// Clear the trainingSlot
	slot, err := user.TrainingSlots.GetSlotByOrderId(orderId)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_ALREADY_PROCESSED, err)
	}
	slot.SetOrderCleared()

	// Save the changes
	if _, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		TrainingSlots: true,
	}); err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	// TODO: the result doesn't included the trainingSLot.Order.UserTraining.Training, so it requires refresh
	user, err = s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		Trainings:     true,
		TrainingSlots: true,
	})
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	// Filtering for training infos
	selectableTrainings := s.getSelectableTrainings(ctx, user)

	resp := schema_training.CancelTrainingResponseSchema{
		BasePlayer:   *user,
		SlotInfo:     user.TrainingSlots,
		TrainingInfo: selectableTrainings,
	}

	return &resp, nil
}

func (s *playerTrainingService) calculateDropItems(
	ctx context.Context,
	user *model_user.User,
	userTraining *model_user.UserTraining,
) []model_training.TrainingReward {
	_, span := observability.Tracer.StartAppServiceSpan(ctx, "calculateDropItems")
	defer span.End()

	drops := userTraining.Training.Rewards
	droppedRewards := make([]model_training.TrainingReward, 0, len(drops))

	for _, drop := range drops {
		// Calculate the drop amount
		dropWeights := make([]int64, len(drop.DropRate))
		for i, drop := range drop.DropRate {
			dropWeights[i] = int64(drop.Probability)
		}
		choseIndex := calc.CumulativeRandomChoice(dropWeights, nil)
		choseDrop := drop.DropRate[choseIndex]
		// If the drop amount is 0, skip the process
		if choseDrop.Amount == 0 {
			continue
		}
		// Add the item to the user
		user.AddItem(drop.ItemId, choseDrop.Amount)
		droppedReward := model_training.TrainingReward{
			Id:         drop.Id,
			TrainingId: drop.TrainingId,
			ItemId:     drop.ItemId,
			RareDisp:   drop.RareDisp,
			DropRate: []model_training.TrainingRewardDrop{
				{
					Id:          drop.Id,
					RewardId:    choseDrop.RewardId,
					Probability: choseDrop.Probability,
					Amount:      choseDrop.Amount,
				},
			},
		}
		droppedRewards = append(droppedRewards, droppedReward)
	}
	return droppedRewards
}

func (s *playerTrainingService) CompletesPlayerTraining(
	ctx context.Context,
	internalUserId value_user.UserId,
	params schema_training.CompletesTrainingRequestSchema,
) (*schema_training.CompletesTrainingResponseSchema, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "CompletesPlayerTraining")
	defer span.End()

	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		Trainings:         true,
		TrainingSlots:     true,
		ManagedCharacters: true,
		ManagedNamedTypes: true,
		ItemSummary:       true,
	})
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	rewards := make([]schema_training.CompletesTrainingResponseRewardSchema, len(params))
	for i, v := range params {
		// find the slot
		slot, err := user.TrainingSlots.GetSlotByOrderId(v.OrderId)
		if err != nil {
			span.RecordError(err)
			return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_TRAINING_ALREADY_COMPLETED, err)
		}
		// find the user training
		userTraining, err := user.Trainings.GetUserTrainingByUserTrainingId(slot.Order.UserTrainingId)
		if err != nil {
			span.RecordError(err)
			return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_TRAINING_ALREADY_COMPLETED, err)
		}
		// read base training info
		training := userTraining.Training

		// prepare the base response
		rewards[i].SlotId = slot.TrainingSlotId
		rewards[i].RewardCharaExp = training.RewardCharaExp
		rewards[i].RewardFriendship = training.RewardFriendship

		// Consume gems if the skipGem is true
		// Otherwise, check the training is done or not
		if v.SkipGem.IsTrue() {
			if consumed := user.ConsumeGem(training.SkipGem); !consumed {
				err = errors.New("not enough gem")
				span.RecordError(err)
				return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_GEM_IS_SHORT, err)
			}
		} else {
			// validate the training is done or not
			if !slot.IsEnded() {
				err = errors.New("training is not done")
				span.RecordError(err)
				return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_TRAINING_COMPLETE_TIME_ERROR, err)
			}
		}
		// add currency reward
		switch training.RewardCurrencyType {
		case value_training.TrainingRewardCurrencyTypeCoin:
			user.AddGold(training.RewardCurrencyAmount)
			rewards[i].RewardGold = training.RewardCurrencyAmount
		case value_training.TrainingRewardCurrencyTypeKirara:
			rewards[i].RewardKRRPoint = training.RewardCurrencyAmount
			// TODO: Send overflowed kirara to user's present box
			user.AddKirara(uint32(training.RewardCurrencyAmount))
		}
		// add user character exps + namedType exps
		targetManagedCharacterIds := slot.GetOutingManagedCharacterIds()
		user.AddCharacterExps(
			training.RewardCharaExp,
			targetManagedCharacterIds,
		)
		targetCharacterIds := user.GetCharacterIdsByManagedCharacterIds(targetManagedCharacterIds)
		targetNamedTypes, err := s.cu.GetNamedTypesByIds(ctx, targetCharacterIds)
		if err != nil {
			span.RecordError(err)
			return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		}
		// add user namedType exps
		user.AddNamedTypeExps(
			uint32(training.RewardFriendship),
			targetNamedTypes,
		)
		if err := s.updateManagedCharacterLevels(ctx, user, targetManagedCharacterIds); err != nil {
			span.RecordError(err)
			return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		}
		if err := s.updateManagedNamedTypeLevels(ctx, user, targetNamedTypes); err != nil {
			span.RecordError(err)
			return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		}
		// add user gem if the training is first cleared
		if userTraining.IsFirstClear() {
			rewards[i].FirstGem = training.FirstGem
			user.AddLimitedGem(training.FirstGem)
			userTraining.SetClearedState()
		}
		// change the training slot as open (with keep managedCharacterIds)
		slot.SetOrderCleared()
		// calculate the drop items
		dropRewards := s.calculateDropItems(ctx, user, userTraining)
		rewards[i].TrainingRewardItems = make([]schema_training.CompletesTrainingResponseRewardItemSchema, len(dropRewards))
		for j, dropReward := range dropRewards {
			rewards[i].TrainingRewardItems[j] = schema_training.CompletesTrainingResponseRewardItemSchema{
				ItemId:     dropReward.ItemId,
				ItemAmount: int64(dropReward.DropRate[0].Amount),
				RareDisp:   int64(dropReward.RareDisp),
			}
		}
	}

	// Save the changes
	if _, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		Trainings:         true,
		TrainingSlots:     true,
		ManagedCharacters: true,
		ManagedNamedTypes: true,
		ItemSummary:       true,
	}); err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	// Filtering for training infos
	// It must not contains the training infos that are on-going.
	selectableTrainings := s.getSelectableTrainings(ctx, user)

	resp := schema_training.CompletesTrainingResponseSchema{
		BasePlayer:        *user,
		SlotInfo:          user.TrainingSlots,
		TrainingInfo:      selectableTrainings,
		ItemSummary:       user.ItemSummary,
		ManagedCharacters: user.ManagedCharacters,
		ManagedNamedTypes: user.ManagedNamedTypes,
		Rewards:           rewards,
	}
	return &resp, nil
}

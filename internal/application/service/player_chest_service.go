package service

import (
	"context"
	"errors"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_chest "gitlab.com/kirafan/sparkle/server/internal/domain/model/chest"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type PlayerChestService interface {
	// Draw player chest
	DrawPlayerChest(ctx context.Context, internalUserId value_user.UserId, chestId uint, count uint8) (model_user.User, model_chest.Chest, []model_chest.PrizeResult, error)
	// Get all player chest
	GetAllPlayerChest(ctx context.Context, internalUserId value_user.UserId) ([]model_chest.Chest, error)
	// Get step player chest
	GetStepPlayerChest(ctx context.Context, internalUserId value_user.UserId, chestId int, step uint16) ([]model_chest.ResetChestPrize, error)
	// Reset player chest
	ResetPlayerChest(ctx context.Context, internalUserId value_user.UserId, chestId int) (model_chest.Chest, error)
}

type playerChestService struct {
	uu usecase.UserUsecase
}

func NewPlayerChestService(uu usecase.UserUsecase) PlayerChestService {
	return &playerChestService{uu}
}

func (s *playerChestService) DrawPlayerChest(ctx context.Context, internalUserId value_user.UserId, chestId uint, count uint8) (model_user.User, model_chest.Chest, []model_chest.PrizeResult, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "DrawPlayerChest")
	defer span.End()
	return model_user.User{}, model_chest.Chest{}, []model_chest.PrizeResult{}, errors.New("not implemented")
}

func (s *playerChestService) GetAllPlayerChest(ctx context.Context, internalUserId value_user.UserId) ([]model_chest.Chest, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetAllPlayerChest")
	defer span.End()
	// STUB
	return []model_chest.Chest{}, nil
}

func (s *playerChestService) GetStepPlayerChest(ctx context.Context, internalUserId value_user.UserId, chestId int, step uint16) ([]model_chest.ResetChestPrize, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetStepPlayerChest")
	defer span.End()
	return nil, errors.New("not implemented")
}

func (s *playerChestService) ResetPlayerChest(ctx context.Context, internalUserId value_user.UserId, chestId int) (model_chest.Chest, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "ResetPlayerChest")
	defer span.End()
	return model_chest.Chest{}, errors.New("not implemented")
}

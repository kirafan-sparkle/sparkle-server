package service

import (
	"context"
	"fmt"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_friend "gitlab.com/kirafan/sparkle/server/internal/domain/model/friend"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type CheatService interface {
	// display the specified cheat info
	HandleCheatDisplay(ctx context.Context, targetMyCode value_friend.FriendOrCheatCode) (*model_friend.FriendFull, *errwrap.SparkleError)
	// execute the specified cheat
	HandleCheatPerform(ctx context.Context, user *model_user.User, targetPlayerId value_friend.PlayerId) (*value_friend.ManagedFriendId, *errwrap.SparkleError)
}

func NewCheatService(
	uu usecase.UserUsecase,
	iu usecase.ItemUsecase,
	cu usecase.CharacterUsecase,
	nu usecase.NamedTypeUsecase,
) CheatService {
	return &cheatService{uu, iu, cu, nu}
}

type cheatService struct {
	uu usecase.UserUsecase
	iu usecase.ItemUsecase
	cu usecase.CharacterUsecase
	nu usecase.NamedTypeUsecase
}

func (s *cheatService) displayCharacterCheat(ctx context.Context, targetMyCode value_friend.FriendOrCheatCode) (*model_friend.FriendFull, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "displayCharacterCheat")
	defer span.End()
	// Convert extra 7 digits to number
	// Ex: CHR0000000 -> 0
	characterIdNum, err := targetMyCode.GetCheatParam()
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "Character ID conversion failed")
	}
	characterId, err := value_character.NewCharacterId(uint32(characterIdNum * 10))
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "not found")
	}
	character, err := s.cu.GetCharacterById(ctx, characterId)
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "not found")
	}
	dummyUserId := value_user.NewUserId(characterId)
	playerId, err := value_friend.NewPlayerId(dummyUserId, value_friend.PlayerSpecialTypeCharacterCheat)
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PLAYER_NOT_FOUND, "not found")
	}

	name := character.CharacterName + " x1"
	comment := "Are you sure you want to add this character?\nTo keep original game experience, you shouldn't do this."
	icon := characterId
	dummyFriend := model_friend.NewDummyFriendFull(*playerId, name, comment, targetMyCode, icon)
	return &dummyFriend, nil
}

func (s *cheatService) displayItemCheat(ctx context.Context, targetMyCode value_friend.FriendOrCheatCode) (*model_friend.FriendFull, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "displayItemCheat")
	defer span.End()
	// Convert extra 6 digits to number
	// Ex: ITEM000000 -> 0
	itemIdRaw, err := targetMyCode.GetCheatParam()
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "Item ID conversion failed")
	}
	itemId, err := value_item.NewItemId(itemIdRaw)
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "Item ID conversion failed")
	}
	item, err := s.iu.GetItemById(ctx, itemId)
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "not found")
	}
	dummyUserId := value_user.NewUserId(itemIdRaw)
	playerId, err := value_friend.NewPlayerId(dummyUserId, value_friend.PlayerSpecialTypeItemCheat)
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PLAYER_NOT_FOUND, "not found")
	}

	name := item.Name + " x100"
	comment := "Are you sure you want to add this item?\nTo keep original game experience, you shouldn't do this."
	icon := value_character.CharacterId(32042001)
	dummyFriend := model_friend.NewDummyFriendFull(*playerId, name, comment, targetMyCode, icon)
	return &dummyFriend, nil
}

func (s *cheatService) displayCoinCheat(ctx context.Context, targetMyCode value_friend.FriendOrCheatCode) (*model_friend.FriendFull, *errwrap.SparkleError) {
	_, span := observability.Tracer.StartAppServiceSpan(ctx, "displayCoinCheat")
	defer span.End()
	// Convert extra 7 digits to number
	// Ex: CIN0000000 -> 0
	coinAmount, err := targetMyCode.GetCheatParam()
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "Coin amount conversion failed")
	}
	dummyUserId := value_user.NewUserId(coinAmount)
	playerId, err := value_friend.NewPlayerId(dummyUserId, value_friend.PlayerSpecialTypeCoinCheat)
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PLAYER_NOT_FOUND, "not found")
	}
	name := "Coin x" + fmt.Sprint(coinAmount)
	comment := "Are you sure you want to add these coins?\nTo keep original game experience, you shouldn't do this."
	icon := value_character.CharacterId(46021001)
	dummyFriend := model_friend.NewDummyFriendFull(*playerId, name, comment, targetMyCode, icon)
	return &dummyFriend, nil
}

func (s *cheatService) displayKiraraCheat(ctx context.Context, targetMyCode value_friend.FriendOrCheatCode) (*model_friend.FriendFull, *errwrap.SparkleError) {
	_, span := observability.Tracer.StartAppServiceSpan(ctx, "displayKiraraCheat")
	defer span.End()
	// Convert extra 7 digits to number
	// Ex: CIN0000000 -> 0
	kiraraAmount, err := targetMyCode.GetCheatParam()
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "Kirara amount conversion failed")
	}
	if kiraraAmount > 100000 {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "Kirara amount is too big")
	}
	dummyUserId := value_user.NewUserId(kiraraAmount)
	playerId, err := value_friend.NewPlayerId(dummyUserId, value_friend.PlayerSpecialTypeKiraraCheat)
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PLAYER_NOT_FOUND, "not found")
	}
	name := "Kirara x" + fmt.Sprint(kiraraAmount)
	comment := "Are you sure you want to add these kiraras?\nTo keep original game experience, you shouldn't do this."
	icon := value_character.CharacterId(32062000)
	dummyFriend := model_friend.NewDummyFriendFull(*playerId, name, comment, targetMyCode, icon)
	return &dummyFriend, nil
}

func (s *cheatService) displayGemCheat(ctx context.Context, targetMyCode value_friend.FriendOrCheatCode) (*model_friend.FriendFull, *errwrap.SparkleError) {
	_, span := observability.Tracer.StartAppServiceSpan(ctx, "displayGemCheat")
	defer span.End()
	// Convert extra 7 digits to number
	// Ex: GEM0000000 -> 0
	gemAmount, err := targetMyCode.GetCheatParam()
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "Gem amount conversion failed")
	}
	dummyUserId := value_user.NewUserId(gemAmount)
	playerId, err := value_friend.NewPlayerId(dummyUserId, value_friend.PlayerSpecialTypeGemCheat)
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PLAYER_NOT_FOUND, "not found")
	}

	name := "Gem x" + fmt.Sprint(gemAmount)
	comment := "Are you sure you want to add these gems?\nTo keep original game experience, you shouldn't do this."
	icon := value_character.CharacterId(32022010)
	dummyFriend := model_friend.NewDummyFriendFull(*playerId, name, comment, targetMyCode, icon)
	return &dummyFriend, nil
}

func (s *cheatService) displayRespCheat(ctx context.Context, targetMyCode value_friend.FriendOrCheatCode) (*model_friend.FriendFull, *errwrap.SparkleError) {
	_, span := observability.Tracer.StartAppServiceSpan(ctx, "displayRespCheat")
	defer span.End()
	respCode, err := targetMyCode.GetCheatParam()
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "Invalid response code")
	}
	if respCode < 1 || respCode > 999 {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "not found")
	}
	return nil, errwrap.NewSparkleError(response.ResultCode(respCode), "error caused by cheat code")
}

func (s *cheatService) displayNpcCheat(ctx context.Context, targetMyCode value_friend.FriendOrCheatCode) (*model_friend.FriendFull, *errwrap.SparkleError) {
	_, span := observability.Tracer.StartAppServiceSpan(ctx, "displayNpcCheat")
	defer span.End()

	// Convert to number
	characterIdNum, err := targetMyCode.GetCheatParam()
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "NPC character ID conversion failed")
	}
	characterId, err := value_character.NewCharacterId(uint32(characterIdNum * 10))
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "not found")
	}
	character, err := s.cu.GetCharacterById(ctx, characterId)
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "not found")
	}
	dummyUserId := value_user.NewUserId(characterId)
	playerId, err := value_friend.NewPlayerId(dummyUserId, value_friend.PlayerSpecialTypeFriendCheat)
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PLAYER_NOT_FOUND, "not found")
	}

	name := character.CharacterName + " x1"
	comment := "Are you sure you want to add NPC friend?\nYou need 100 gems to add NPC friend."
	icon := characterId
	dummyFriend := model_friend.NewDummyFriendFull(*playerId, name, comment, targetMyCode, icon)
	return &dummyFriend, nil
}

func (s *cheatService) HandleCheatDisplay(ctx context.Context, targetMyCode value_friend.FriendOrCheatCode) (*model_friend.FriendFull, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "HandleCheatDisplay")
	defer span.End()

	cheatType := targetMyCode.GetCheatType()
	switch cheatType {
	case value_friend.CheatCodeTypeItem:
		return s.displayItemCheat(ctx, targetMyCode)
	case value_friend.CheatCodeTypeCharacter:
		return s.displayCharacterCheat(ctx, targetMyCode)
	case value_friend.CheatCodeTypeCoin:
		return s.displayCoinCheat(ctx, targetMyCode)
	case value_friend.CheatCodeTypeKrr:
		return s.displayKiraraCheat(ctx, targetMyCode)
	case value_friend.CheatCodeTypeGem:
		return s.displayGemCheat(ctx, targetMyCode)
	case value_friend.CheatCodeTypeResp:
		return s.displayRespCheat(ctx, targetMyCode)
	case value_friend.CheatCodeTypeNpc:
		return s.displayNpcCheat(ctx, targetMyCode)
	}
	return nil, nil
}

func (s *cheatService) performItemCheat(ctx context.Context, user *model_user.User, itemId value_item.ItemId) (*value_friend.ManagedFriendId, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "performItemCheat")
	defer span.End()

	user.AddItem(itemId, 100)
	user.Name = "I cheated"
	_, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ItemSummary: true,
	})
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return value_friend.NewDummyManagedFriendId(), nil
}

func (s *cheatService) performCharacterCheat(ctx context.Context, user *model_user.User, characterIdLike uint32) (*value_friend.ManagedFriendId, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "performCharacterCheat")
	defer span.End()

	characterId, err := value_character.NewCharacterId(characterIdLike)
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "not found")
	}
	character, err := s.cu.GetCharacterById(ctx, characterId)
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PLAYER_NOT_FOUND, "character info not found")
	}
	namedType, err := s.nu.GetNamedTypeById(ctx, uint(character.NamedType))
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PLAYER_NOT_FOUND, "named type not found")
	}
	user.AddNamedType(uint16(namedType.NamedType), namedType.TitleType)
	duplicated := user.AddCharacter(characterId)
	if duplicated {
		user.AddItem(character.AltItemID, 1)
	}
	user.Name = "I cheated"
	if _, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ItemSummary:       true,
		ManagedCharacters: true,
		ManagedNamedTypes: true,
	}); err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return value_friend.NewDummyManagedFriendId(), nil
}

func (s *cheatService) performCoinCheat(ctx context.Context, user *model_user.User, goldAmount uint64) (*value_friend.ManagedFriendId, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "performCoinCheat")
	defer span.End()

	user.Gold += goldAmount
	user.Name = "I cheated"
	_, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{})
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return value_friend.NewDummyManagedFriendId(), nil
}

func (s *cheatService) performKiraraCheat(ctx context.Context, user *model_user.User, kiraraAmount uint64) (*value_friend.ManagedFriendId, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "performKiraraCheat")
	defer span.End()

	user.Kirara = calc.Min(uint32(kiraraAmount)+user.Kirara, 100000)
	user.Name = "I cheated"
	_, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{})
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return value_friend.NewDummyManagedFriendId(), nil
}

func (s *cheatService) performGemCheat(ctx context.Context, user *model_user.User, gemAmount uint32) (*value_friend.ManagedFriendId, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "performGemCheat")
	defer span.End()

	user.AddUnlimitedGem(gemAmount)
	user.Name = "I cheated"
	_, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{})
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return value_friend.NewDummyManagedFriendId(), nil
}

func (s *cheatService) performFriendCheat(ctx context.Context, user *model_user.User, characterIdLike uint32) (*value_friend.ManagedFriendId, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "performFriendCheat")
	defer span.End()

	if consumed := user.ConsumeGem(100); !consumed {
		return nil, errwrap.NewSparkleError(response.RESULT_GEM_IS_SHORT, "not enough gem")
	}

	characterId, err := value_character.NewCharacterId(characterIdLike)
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "not found")
	}
	if _, err := s.cu.GetCharacterById(ctx, characterId); err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PLAYER_NOT_FOUND, "character info not found")
	}

	// TODO: Inject and save NPC friend

	return value_friend.NewDummyManagedFriendId(), nil
}

func (s *cheatService) HandleCheatPerform(ctx context.Context, user *model_user.User, targetPlayerId value_friend.PlayerId) (*value_friend.ManagedFriendId, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "handleCheatPerform")
	defer span.End()

	switch targetPlayerId.SpecialType() {
	case value_friend.PlayerSpecialTypeItemCheat:
		itemId, err := value_item.NewItemId(targetPlayerId.Value())
		if err != nil {
			return nil, errwrap.NewSparkleError(response.RESULT_PROMOTION_CODE_NOT_FOUND, "Item ID conversion failed")
		}
		return s.performItemCheat(ctx, user, itemId)
	case value_friend.PlayerSpecialTypeCharacterCheat:
		return s.performCharacterCheat(ctx, user, uint32(targetPlayerId.Value()))
	case value_friend.PlayerSpecialTypeCoinCheat:
		return s.performCoinCheat(ctx, user, uint64(targetPlayerId.Value()))
	case value_friend.PlayerSpecialTypeKiraraCheat:
		return s.performKiraraCheat(ctx, user, uint64(targetPlayerId.Value()))
	case value_friend.PlayerSpecialTypeGemCheat:
		return s.performGemCheat(ctx, user, uint32(targetPlayerId.Value()))
	case value_friend.PlayerSpecialTypeFriendCheat:
		return s.performFriendCheat(ctx, user, uint32(targetPlayerId.Value()))
	}
	return nil, nil
}

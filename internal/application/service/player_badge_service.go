package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type PlayerBadgeService interface {
	GetPlayerBadge(ctx context.Context, internalUserId value_user.UserId) (uint8, uint8, uint8, error)
}

type playerBadgeService struct {
	uu usecase.UserUsecase
	fu usecase.FriendUsecase
}

func NewPlayerBadgeService(uu usecase.UserUsecase, fu usecase.FriendUsecase) PlayerBadgeService {
	return &playerBadgeService{uu, fu}
}

func (s *playerBadgeService) GetPlayerBadge(ctx context.Context, internalUserId value_user.UserId) (uint8, uint8, uint8, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetPlayerBadge")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		return 0, 0, 0, err
	}

	playerId, err := value_friend.NewPlayerId(internalUserId, value_friend.PlayerSpecialTypeDefault)
	if err != nil {
		span.RecordError(err)
		return 0, 0, 0, err
	}

	if user.ShouldRefreshUICounts() {
		// Refresh training count
		user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
			TrainingSlots: true,
		})
		if err != nil {
			span.RecordError(err)
			return 0, 0, 0, err
		}
		user.TrainingCount = user.TrainingSlots.GetEndedSlotAmount()
		// Refresh friend proposed count
		incomingFriendRequests, err2 := s.fu.GetInComingFriendRequests(ctx, *playerId)
		if err2 != nil {
			span.RecordError(err2.GetRawError())
			return 0, 0, 0, err2.GetRawError()
		}
		user.FriendProposedCount = uint8(len(incomingFriendRequests))
		// Refresh present count
		user.PresentCount = 0

		// Update user
		if _, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{}); err != nil {
			span.RecordError(err)
			return 0, 0, 0, err
		}
	}

	// Get player badge
	friendProposedCount := user.FriendProposedCount
	presentCount := user.PresentCount
	trainingCount := user.TrainingCount
	return friendProposedCount, presentCount, trainingCount, nil
}

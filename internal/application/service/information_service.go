package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_information "gitlab.com/kirafan/sparkle/server/internal/domain/model/information"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type InformationService interface {
	GetAllInformation(ctx context.Context, platform value_version.Platform) ([]*model_information.Information, error)
}

type informationService struct {
	uc usecase.InformationUsecase
}

func NewInformationService(uc usecase.InformationUsecase) InformationService {
	return &informationService{uc}
}

func (s *informationService) GetAllInformation(ctx context.Context, platform value_version.Platform) ([]*model_information.Information, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetAllInformation")
	defer span.End()

	infos, err := s.uc.GetAllInformations(ctx, platform)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return infos, nil
}

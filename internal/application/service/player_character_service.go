package service

import (
	"context"
	"errors"
	"time"

	schema_character "gitlab.com/kirafan/sparkle/server/internal/application/schemas/character"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/upgrade"
)

type PlayerCharacterService interface {
	// Consume item and add exps to character
	UpgradeCharacter(ctx context.Context, internalUserId value_user.UserId, managedCharacterId value_user.ManagedCharacterId, items []schema_character.ConsumeItem) (schema_character.UpgradeCharacterResponseSchema, error)
	// Consume item and evolute the character
	EvoluteCharacter(ctx context.Context, internalUserId value_user.UserId, managedCharacterId value_user.ManagedCharacterId, recipeId int64) (schema_character.EvoluteCharacterResponseSchema, error)
	// Consume item and increase level limit
	LimitBreakCharacter(ctx context.Context, internalUserId value_user.UserId, managedCharacterId value_user.ManagedCharacterId, itemIds []value_item.ItemId) (schema_character.LimitBreakCharacterResponseSchema, error)
	// Set character's view evolute or not
	SetViewCharacter(ctx context.Context, internalUserId value_user.UserId, managedCharacterIds []value_user.ManagedCharacterId, evolved []bool) ([]model_user.ManagedCharacter, error)
	// Reset all character's view to default
	ResetViewAllCharacter(ctx context.Context, internalUserId value_user.UserId) ([]model_user.ManagedCharacter, error)
	// Remove character's NEW notify from screen
	SetShownCharacter(ctx context.Context, internalUserId value_user.UserId, managedCharacterId value_user.ManagedCharacterId, shown bool) error
	// Set player's support party
	SetSupportCharacters(ctx context.Context, internalUserId value_user.UserId, req schema_character.SetSupportCharacterRequestSchema) error
	// Set player's support party name
	SetSupportCharactersName(ctx context.Context, internalUserId value_user.UserId, managedSupportId value_user.ManagedSupportId, name string) error
}

type playerCharacterService struct {
	uu  usecase.UserUsecase
	iu  usecase.ItemUsecase
	cu  usecase.CharacterUsecase
	eu  usecase.ExpTableCharacterUsecase
	elu usecase.EvoTableLimitBreakUsecase
	eeu usecase.EvoTableEvolutionUsecase
	ch  upgrade.UpgradeCharacterHandler
}

func NewPlayerCharacterService(
	uu usecase.UserUsecase,
	iu usecase.ItemUsecase,
	cu usecase.CharacterUsecase,
	eu usecase.ExpTableCharacterUsecase,
	eeu usecase.EvoTableEvolutionUsecase,
	elu usecase.EvoTableLimitBreakUsecase,
) PlayerCharacterService {
	ch := upgrade.NewUpgradeCharacterHandler(nil)
	return &playerCharacterService{uu, iu, cu, eu, elu, eeu, ch}
}

func (s *playerCharacterService) UpgradeCharacter(ctx context.Context, internalUserId value_user.UserId, managedCharacterId value_user.ManagedCharacterId, items []schema_character.ConsumeItem) (schema_character.UpgradeCharacterResponseSchema, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "UpgradeCharacter")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ManagedCharacters: true,
		ItemSummary:       true,
	})
	if err != nil {
		return schema_character.UpgradeCharacterResponseSchema{}, err
	}

	// Validate character exists
	managedCharacter, err := user.GetManagedCharacter(managedCharacterId)
	if err != nil {
		return schema_character.UpgradeCharacterResponseSchema{}, err
	}

	// Consume items and calculate required coins
	baseRequiredGolds, err := s.eu.GetRequiredCoinsForUpgrade(ctx, managedCharacter.Level)
	requiredGolds := uint64(0)
	for _, item := range items {
		if consumed := user.ConsumeItem(item.ItemId, uint32(item.Count)); !consumed {
			return schema_character.UpgradeCharacterResponseSchema{}, err
		}
		requiredGolds += uint64(item.Count) * baseRequiredGolds.ToValue()
	}
	if consumed := user.ConsumeGold(requiredGolds); !consumed {
		return schema_character.UpgradeCharacterResponseSchema{}, err
	}

	character, err := s.cu.GetCharacterById(ctx, managedCharacter.CharacterId)
	if err != nil {
		return schema_character.UpgradeCharacterResponseSchema{}, err
	}
	increaseExps := uint64(0)
	for _, item := range items {
		element, amount, err := s.iu.GetCharacterUpgradeAmount(ctx, item.ItemId)
		if err != nil {
			return schema_character.UpgradeCharacterResponseSchema{}, err
		}
		// Add class bonus
		if character.Element == element && element != value_character.ElementTypeNone {
			// FIXME: Move this constant to something else
			// Source: https://kirarafantasia.boom-app.wiki/entry/266
			amount += value_exp.CharacterExp(float32(amount) * 0.2)
		}
		increaseExps += uint64(amount) * uint64(item.Count)
	}

	// Roll bonus
	// Source: https://kirarafantasia.miraheze.org/wiki/Upgrade
	bonus := s.ch.Roll()
	switch bonus {
	case upgrade.CharacterUpgradeResultPerfect:
		// FIXME: Move this constant to something else
		increaseExps += uint64(float32(increaseExps))
	case upgrade.CharacterUpgradeResultGreat:
		// FIXME: Move this constant to something else
		increaseExps += uint64(float32(increaseExps) * 0.5)
	}

	// Increase character exp
	limitCharacterExp, _ := s.eu.GetLevelExpTableCharacter(ctx, managedCharacter.LevelLimit)
	increaseExps = calc.Min(increaseExps, limitCharacterExp.TotalExp-uint64(limitCharacterExp.NextExp)-managedCharacter.Exp)
	managedCharacter.AddExp(increaseExps)

	// Recalculate character level
	nextCharacterLevel, err := s.eu.GetNextExpTableCharacter(ctx, managedCharacter.Exp)
	if err != nil {
		return schema_character.UpgradeCharacterResponseSchema{}, err
	}
	managedCharacter.UpdateLevel(nextCharacterLevel.Level)

	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ItemSummary:       true,
		ManagedCharacters: true,
	})
	if err != nil {
		return schema_character.UpgradeCharacterResponseSchema{}, err
	}

	resp := schema_character.UpgradeCharacterResponseSchema{
		ManagedCharacter: *managedCharacter,
		ItemSummary:      user.ItemSummary,
		Gold:             user.Gold,
		UpgradeResult:    bonus,
	}
	return resp, nil
}

func (s *playerCharacterService) EvoluteCharacter(ctx context.Context, internalUserId value_user.UserId, managedCharacterId value_user.ManagedCharacterId, recipeId int64) (schema_character.EvoluteCharacterResponseSchema, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "EvoluteCharacter")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ManagedCharacters: true,
		ItemSummary:       true,
	})
	if err != nil {
		return schema_character.EvoluteCharacterResponseSchema{}, err
	}
	// Get managed character
	managedCharacter, err := user.GetManagedCharacter(managedCharacterId)
	if err != nil {
		return schema_character.EvoluteCharacterResponseSchema{}, err
	}
	characterId := managedCharacter.CharacterId
	if characterId.IsEvolved() {
		return schema_character.EvoluteCharacterResponseSchema{}, errors.New("character already evolved")
	}
	// Get evolution recipe
	evolutionRecipe, err := s.eeu.GetEvolutionRecipe(ctx, characterId)
	if err != nil {
		return schema_character.EvoluteCharacterResponseSchema{}, err
	}

	// Consume golds
	requiredGolds := uint64(evolutionRecipe.RequiredCoin)
	if consumed := user.ConsumeGold(requiredGolds); !consumed {
		return schema_character.EvoluteCharacterResponseSchema{}, err
	}
	// Consume items
	for _, itemInfo := range evolutionRecipe.RequiredItems {
		if consumed := user.ConsumeItem(itemInfo.ItemId, uint32(itemInfo.Amount)); !consumed {
			return schema_character.EvoluteCharacterResponseSchema{}, err
		}
	}

	// Updates levelLimit and display only
	// Source: https://kirarafantasia.miraheze.org/wiki/Overview_of_how_to_max_a_character%27s_level
	managedCharacter.LevelLimit += 10
	newCharacterId := characterId.SetEvoluteState(true)
	managedCharacter.CharacterId = newCharacterId
	managedCharacter.ViewCharacterId = newCharacterId
	// After evolved, the character's duplicated count will be reset
	managedCharacter.LevelBreak = 0
	managedCharacter.UpdatedAt = time.Now()

	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ItemSummary:       true,
		ManagedCharacters: true,
	})
	if err != nil {
		return schema_character.EvoluteCharacterResponseSchema{}, err
	}

	resp := schema_character.EvoluteCharacterResponseSchema{
		ManagedCharacter: *managedCharacter,
		ItemSummary:      user.ItemSummary,
		Gold:             user.Gold,
	}
	return resp, nil
}

func (s *playerCharacterService) LimitBreakCharacter(ctx context.Context, internalUserId value_user.UserId, managedCharacterId value_user.ManagedCharacterId, itemIds []value_item.ItemId) (schema_character.LimitBreakCharacterResponseSchema, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "LimitBreakCharacter")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ManagedCharacters: true,
		ItemSummary:       true,
	})
	if err != nil {
		return schema_character.LimitBreakCharacterResponseSchema{}, err
	}

	// Validate character exists
	managedCharacter, err := user.GetManagedCharacter(managedCharacterId)
	if err != nil {
		return schema_character.LimitBreakCharacterResponseSchema{}, err
	}
	// Validate character can limit break
	if !managedCharacter.IsLimitBreakable(uint8(len(itemIds))) {
		return schema_character.LimitBreakCharacterResponseSchema{}, errors.New("can not limit break specified count for this character")
	}

	// Get character's limit break item recipe
	character, err := s.cu.GetCharacterById(ctx, managedCharacter.CharacterId)
	if err != nil {
		return schema_character.LimitBreakCharacterResponseSchema{}, err
	}
	recipeId := uint(character.LimitBreakRecipeID)

	// Get limitBreak recipe
	recipe, err := s.elu.GetLimitBreakRecipe(ctx, recipeId)
	if err != nil {
		return schema_character.LimitBreakCharacterResponseSchema{}, err
	}

	// Consume golds
	baseRequiredGolds := value_user.NewGold(uint64(recipe.RequiredCoinPerItem))
	requiredGolds := value_user.NewGold(0)
	for range itemIds {
		requiredGolds += baseRequiredGolds
	}
	if consumed := user.ConsumeGold(requiredGolds.ToValue()); !consumed {
		return schema_character.LimitBreakCharacterResponseSchema{}, err
	}

	// Consume items
	titleType := managedCharacter.CharacterId.GetContent().ToTitleType()
	// FIXME: Move this constant to somewhere else
	targetTitleItemId, err := value_item.NewItemId(uint32(5000) + uint32(titleType))
	if err != nil {
		return schema_character.LimitBreakCharacterResponseSchema{}, err
	}
	// TODO: Read Item Amount fields from recipe. It ignored since it was not used in game.
	for _, itemId := range itemIds {
		switch itemId {
		case recipe.AllClassItemId:
			fallthrough
		case recipe.ClassItemId:
			if consumed := user.ConsumeItem(itemId, 1); !consumed {
				return schema_character.LimitBreakCharacterResponseSchema{}, err
			}
		default:
			if itemId != targetTitleItemId {
				return schema_character.LimitBreakCharacterResponseSchema{}, errors.New("invalid item id")
			}
			if consumed := user.ConsumeItem(itemId, 1); !consumed {
				return schema_character.LimitBreakCharacterResponseSchema{}, err
			}
		}
	}

	for range itemIds {
		managedCharacter.IncreaseLevelBreakCount()
	}

	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ItemSummary:       true,
		ManagedCharacters: true,
	})
	if err != nil {
		return schema_character.LimitBreakCharacterResponseSchema{}, err
	}

	resp := schema_character.LimitBreakCharacterResponseSchema{
		ManagedCharacter: *managedCharacter,
		ItemSummary:      user.ItemSummary,
		Gold:             user.Gold,
	}
	return resp, nil
}

func (s *playerCharacterService) SetViewCharacter(ctx context.Context, internalUserId value_user.UserId, managedCharacterIds []value_user.ManagedCharacterId, evolved []bool) ([]model_user.ManagedCharacter, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SetViewCharacter")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ManagedCharacters: true,
	})
	if err != nil {
		return []model_user.ManagedCharacter{}, err
	}
	// Update managed character view character id
	for i, managedCharacterId := range managedCharacterIds {
		managedCharacter, err := user.GetManagedCharacter(managedCharacterId)
		if err != nil {
			return []model_user.ManagedCharacter{}, err
		}
		managedCharacter.ViewCharacterId = managedCharacter.CharacterId.SetEvoluteState(evolved[i])
	}
	// Save the changes
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ManagedCharacters: true,
	})
	if err != nil {
		return []model_user.ManagedCharacter{}, err
	}
	return user.ManagedCharacters, nil
}

func (s *playerCharacterService) ResetViewAllCharacter(ctx context.Context, internalUserId value_user.UserId) ([]model_user.ManagedCharacter, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "ResetViewAllCharacter")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ManagedCharacters: true,
	})
	if err != nil {
		return []model_user.ManagedCharacter{}, err
	}
	// Reset all character's view character id
	for i := range user.ManagedCharacters {
		user.ManagedCharacters[i].ViewCharacterId = user.ManagedCharacters[i].CharacterId
	}
	// Save the changes
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ManagedCharacters: true,
	})
	if err != nil {
		return []model_user.ManagedCharacter{}, err
	}
	return user.ManagedCharacters, nil
}

func (s *playerCharacterService) SetShownCharacter(ctx context.Context, internalUserId value_user.UserId, managedCharacterId value_user.ManagedCharacterId, shown bool) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SetShownCharacter")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ManagedCharacters: true,
	})
	if err != nil {
		return err
	}
	// Reset all character's view character id
	managedCharacter, err := user.GetManagedCharacter(managedCharacterId)
	if err != nil {
		return err
	}
	// NOTE: Ignore shown param since there is no use case to un-shown it
	managedCharacter.Shown = 1
	// Save the changes
	_, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ManagedCharacters: true,
	})
	if err != nil {
		return err
	}
	return nil
}

func (s *playerCharacterService) SetSupportCharacters(ctx context.Context, internalUserId value_user.UserId, req schema_character.SetSupportCharacterRequestSchema) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SetSupportCharacters")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		SupportCharacters: true,
	})
	if err != nil {
		return err
	}
	// Set support characters
	for _, reqSupport := range req.SupportCharacters {
		supportCharacter, err := user.GetSupportCharacter(reqSupport.ManagedSupportId)
		if err != nil {
			return err
		}
		supportCharacter.Name = reqSupport.Name
		supportCharacter.Active = reqSupport.Active
		for i := range supportCharacter.ManagedWeaponIds {
			supportCharacter.ManagedWeaponIds[i].ManagedWeaponId = reqSupport.ManagedWeaponIds[i]
		}
		for i := range supportCharacter.ManagedCharacterIds {
			supportCharacter.ManagedCharacterIds[i].ManagedCharacterId = reqSupport.ManagedCharacterIds[i]
		}
	}
	// Save the changes
	_, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		SupportCharacters: true,
	})
	if err != nil {
		return err
	}
	return nil
}

func (s *playerCharacterService) SetSupportCharactersName(ctx context.Context, internalUserId value_user.UserId, managedSupportId value_user.ManagedSupportId, name string) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SetSupportCharactersName")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		SupportCharacters: true,
	})
	if err != nil {
		return err
	}
	// Change the support name
	supportCharacter, err := user.GetSupportCharacter(managedSupportId)
	if err != nil {
		return err
	}
	supportCharacter.Name = name
	// Save the changes
	_, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		SupportCharacters: true,
	})
	if err != nil {
		return err
	}
	return nil
}

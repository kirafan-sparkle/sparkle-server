package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_stamina "gitlab.com/kirafan/sparkle/server/internal/domain/value/stamina"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type PlayerStaminaService interface {
	AddPlayerStamina(
		ctx context.Context,
		internalUserId value_user.UserId,
		consumeType value_stamina.StaminaConsumeType,
		consumeItem value_stamina.StaminaItemType,
		consumeAmount value_stamina.StaminaItemAmount,
	) (*model_user.User, *errwrap.SparkleError)
}

type playerStaminaService struct {
	uu usecase.UserUsecase
}

func NewPlayerStaminaService(uu usecase.UserUsecase) PlayerStaminaService {
	return &playerStaminaService{uu}
}

func (s *playerStaminaService) AddPlayerStamina(
	ctx context.Context,
	internalUserId value_user.UserId,
	consumeType value_stamina.StaminaConsumeType,
	consumeItem value_stamina.StaminaItemType,
	consumeAmount value_stamina.StaminaItemAmount,
) (*model_user.User, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "AddPlayerStamina")
	defer span.End()

	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{ItemSummary: true})
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_NOT_FOUND, err)
	}

	switch consumeType {
	case value_stamina.StaminaConsumeTypeItem:
		itemId, err := value_item.NewItemId(consumeItem)
		if err != nil {
			span.RecordError(err)
			return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		}
		if !user.ConsumeItem(itemId, uint32(consumeAmount)) {
			return nil, errwrap.NewSparkleError(response.RESULT_ITEM_IS_SHORT, "you don't have enough items")
		}
		for i := 0; i < int(consumeAmount); i++ {
			switch consumeItem {
			case value_stamina.StaminaItemTypeGold:
				user.AddStaminaGold()
			case value_stamina.StaminaItemTypeSilver:
				user.AddStaminaSilver()
			case value_stamina.StaminaItemTypeBronze:
				user.AddStaminaBronze()
			}
		}
	case value_stamina.StaminaConsumeTypeGem:
		if !user.ConsumeGem(uint32(value_stamina.StaminaItemAmountForGem)) {
			return nil, errwrap.NewSparkleError(response.RESULT_GEM_IS_SHORT, "you don't have enough gems")
		}
		user.AddStaminaGold()
	}

	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{ItemSummary: true})
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return user, nil
}

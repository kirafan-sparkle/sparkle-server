package service

import (
	"context"
	"fmt"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_present "gitlab.com/kirafan/sparkle/server/internal/domain/value/present"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type PlayerPresentService interface {
	GetPresentList(ctx context.Context, userId value_user.UserId) ([]*model_user.UserPresent, *errwrap.SparkleError)
	GetReceivedPresentList(ctx context.Context, userId value_user.UserId) ([]*model_user.UserPresent, *errwrap.SparkleError)
	ReceivePresents(ctx context.Context, userId value_user.UserId, managedPresentIds []int64, stepCode int64) (*model_user.User, []model_user.UserPresent, *errwrap.SparkleError)
}

type playerPresentService struct {
	uu usecase.UserUsecase
	cu usecase.CharacterUsecase
	nu usecase.NamedTypeUsecase
	pu usecase.UserPresentUsecase
}

func NewPlayerPresentService(
	uu usecase.UserUsecase,
	cu usecase.CharacterUsecase,
	nu usecase.NamedTypeUsecase,
	pu usecase.UserPresentUsecase,
) PlayerPresentService {
	return &playerPresentService{uu: uu, cu: cu, nu: nu, pu: pu}
}

func (s *playerPresentService) GetPresentList(ctx context.Context, userId value_user.UserId) ([]*model_user.UserPresent, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetPresentList")
	defer span.End()

	presents, err := s.pu.GetReceivableUserPresents(ctx, userId)
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return presents, nil
}

func (s *playerPresentService) GetReceivedPresentList(ctx context.Context, userId value_user.UserId) ([]*model_user.UserPresent, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetReceivedPresentList")
	defer span.End()

	presents, err := s.pu.GetReceivedUserPresents(ctx, userId)
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return presents, nil
}

func (s *playerPresentService) ReceivePresents(ctx context.Context, userId value_user.UserId, managedPresentIds []int64, stepCode int64) (*model_user.User, []model_user.UserPresent, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "ReceivePresents")
	defer span.End()
	// Get presents that belong to the player
	user, err := s.uu.GetUserByInternalId(ctx, userId, repository.UserRepositoryParam{Presents: true})
	if err != nil || user.Presents == nil {
		if err != nil {
			span.RecordError(err)
		}
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_NOT_FOUND, err)
	}
	// Validate present ids
	presentIds := []int64{}
	for _, present := range user.Presents {
		presentIds = append(presentIds, int64(present.ManagedPresentId))
	}
	for _, managedPresentId := range managedPresentIds {
		if !calc.Contains(presentIds, managedPresentId) {
			return nil, nil, errwrap.NewSparkleError(
				response.RESULT_PRESENT_OUT_OF_PERIOD,
				fmt.Sprintf("specified present id %d was not found", managedPresentId),
			)
		}
	}
	// Get target user
	user, err = s.uu.GetUserByInternalId(ctx, userId, repository.UserRepositoryParam{
		Presents:           true,
		ItemSummary:        true,
		ManagedCharacters:  true,
		ManagedNamedTypes:  true,
		ManagedMasterOrbs:  true,
		ManagedRoomObjects: true,
		ManagedFacilities:  true,
		ManagedWeapons:     true,
		OfferTitleTypes:    true,
	})
	if err != nil || user.Presents == nil {
		span.RecordError(err)
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_NOT_FOUND, err)
	}
	// Receive present
	received := []model_user.UserPresent{}
	for _, p := range user.Presents {
		var present *model_user.UserPresent
		present, err = user.ReceivePresent(p.ManagedPresentId)
		if err != nil {
			span.RecordError(err)
			return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		}
		received = append(received, *present)
	}

	for _, received := range received {
		// Add NamedType
		if received.Type == value_present.PresentTypeCharacter {
			characterId, err := value_character.NewCharacterId(uint32(received.ObjectId))
			if err != nil {
				span.RecordError(err)
				return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
			}
			character, err := s.cu.GetCharacterById(ctx, characterId)
			if err != nil {
				span.RecordError(err)
				return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
			}
			namedType, err := s.nu.GetNamedTypeById(ctx, uint(character.NamedType))
			if err != nil {
				span.RecordError(err)
				return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
			}
			// NOTE: Ignore the duplicated error
			user.AddNamedType(uint16(namedType.NamedType), namedType.TitleType)
		}
	}

	// Update stepCode
	newStepCode, err := value_user.NewStepCode(int8(stepCode))
	if err != nil {
		span.RecordError(err)
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_UNKNOWN_ERROR, err)
	}
	user.UpdateStepCode(newStepCode)

	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{Presents: true, ItemSummary: true, ManagedCharacters: true, ManagedNamedTypes: true})
	if err != nil {
		span.RecordError(err)
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return user, received, nil
}

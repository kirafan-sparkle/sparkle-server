package service

import (
	"context"
	"errors"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type PlayerContentRoomService interface {
	// Get All Player Content Room Member (WIP)
	GetAllPlayerContentRoomMember(ctx context.Context, internalUserId value_user.UserId) ([]model_user.ManagedCharacter, error)
	// Set All Player Content Room Member (WIP)
	SetAllPlayerContentRoomMember(ctx context.Context, internalUserId value_user.UserId, managedCharacterIds []int32, titleType value_character.TitleType) error
	// Set Shown Player Content Room (WIP)
	SetShownPlayerContentRoom(ctx context.Context, internalUserId value_user.UserId, titleTypes []value_character.TitleType) ([]model_user.OfferTitleType, error)
}

type playerContentRoomService struct {
	uu usecase.UserUsecase
}

func NewPlayerContentRoomService(uu usecase.UserUsecase) PlayerContentRoomService {
	return &playerContentRoomService{uu}
}

func (s *playerContentRoomService) GetAllPlayerContentRoomMember(ctx context.Context, internalUserId value_user.UserId) ([]model_user.ManagedCharacter, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetAllPlayerContentRoomMember")
	defer span.End()
	// Get user
	// user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
	// 	ContentRoom: true,
	// })
	// if err != nil {
	// 	return nil, err
	// }
	// return user.ContentRoom.Members, nil
	return []model_user.ManagedCharacter{}, errors.New("not implemented")
}

func (s *playerContentRoomService) SetAllPlayerContentRoomMember(ctx context.Context, internalUserId value_user.UserId, managedCharacterIds []int32, titleType value_character.TitleType) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SetAllPlayerContentRoomMember")
	defer span.End()
	// Get user
	// user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
	// 	ContentRoom: true,
	// })
	// if err != nil {
	// 	return err
	// }
	// // Set members
	// user.ContentRoom.Members = make([]model_user.UserContentRoomMember, len(titleTypes))
	// for i, titleType := range titleTypes {
	// 	user.ContentRoom.Members[i] = model_user.UserContentRoomMember{
	// 		TitleType: value_character.TitleType(titleType),
	// 	}
	// }
	// // Save user
	// _, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{})
	// return err
	return errors.New("not implemented")
}

func (s *playerContentRoomService) SetShownPlayerContentRoom(ctx context.Context, internalUserId value_user.UserId, titleTypes []value_character.TitleType) ([]model_user.OfferTitleType, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SetShownPlayerContentRoom")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{OfferTitleTypes: true})
	if err != nil {
		return []model_user.OfferTitleType{}, errors.New("user not found")
	}

	for i := range titleTypes {
		user.SetShownPlayerContentRoom(titleTypes[i])
	}

	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{OfferTitleTypes: true})
	if err != nil {
		return []model_user.OfferTitleType{}, errors.New("database save error")
	}
	return user.OfferTitleTypes, err
}

package service

import (
	"context"
	"errors"
	"fmt"

	schema_weapon "gitlab.com/kirafan/sparkle/server/internal/application/schemas/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
	"gitlab.com/kirafan/sparkle/server/pkg/upgrade"
)

type PlayerWeaponService interface {
	// Consume gems and extend the weapon inventory limit
	AddPlayerWeaponLimit(ctx context.Context, internalUserId value_user.UserId) (*model_user.User, error)
	// Consume item and evolute specified weapon
	EvoluteWeapon(ctx context.Context, internalUserId value_user.UserId, managedWeaponId value_user.ManagedWeaponId, weaponEvolutionId uint) (*schema_weapon.EvoluteWeaponResponseSchema, error)
	// Consume item and add weapon to the user
	MakeWeapon(ctx context.Context, internalUserId value_user.UserId, recipeId uint32) (*model_user.User, *errwrap.SparkleError)
	// Find the specified managed character and insert a special weapon for the character
	ReceiveWeapon(ctx context.Context, internalUserId value_user.UserId, managedCharacterIds []value_user.ManagedCharacterId) (*schema_weapon.ReceiveWeaponResponseSchema, error)
	// Sale weapon and add golds to the user
	SaleWeapon(ctx context.Context, internalUserId value_user.UserId, managedWeaponIds []value_user.ManagedWeaponId) (*model_user.User, error)
	// Consume item and add exps to the weapon
	UpgradeWeapon(ctx context.Context, internalUserId value_user.UserId, managedWeaponId value_user.ManagedWeaponId, items []schema_weapon.ConsumeItem) (*schema_weapon.UpgradeWeaponResponseSchema, *errwrap.SparkleError)
	// Consume skill up items and add skill exps to the weapon
	SkillUpPlayerWeapon(ctx context.Context, internalUserId value_user.UserId, managedWeaponId value_user.ManagedWeaponId, items []schema_weapon.ConsumeItem) (*schema_weapon.SkillUpWeaponResponseSchema, *errwrap.SparkleError)
}

type playerWeaponService struct {
	uu  usecase.UserUsecase
	iu  usecase.ItemUsecase
	cu  usecase.CharacterUsecase
	wu  usecase.WeaponUsecase
	wru usecase.WeaponRecipeUsecase
	wcu usecase.WeaponCharacterTableUsecase
	eu  usecase.ExpTableWeaponUsecase
	esu usecase.ExpTableSkillUsecase
	evu usecase.EvoTableWeaponUsecase
	ch  upgrade.UpgradeWeaponHandler
}

func NewPlayerWeaponService(
	uu usecase.UserUsecase,
	iu usecase.ItemUsecase,
	cu usecase.CharacterUsecase,
	wu usecase.WeaponUsecase,
	wru usecase.WeaponRecipeUsecase,
	wcu usecase.WeaponCharacterTableUsecase,
	eu usecase.ExpTableWeaponUsecase,
	esu usecase.ExpTableSkillUsecase,
	evu usecase.EvoTableWeaponUsecase,
) PlayerWeaponService {
	ch := upgrade.NewUpgradeWeaponHandler(nil)
	return &playerWeaponService{uu, iu, cu, wu, wru, wcu, eu, esu, evu, ch}
}

func (s *playerWeaponService) AddPlayerWeaponLimit(ctx context.Context, internalUserId value_user.UserId) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "AddPlayerWeaponLimit")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ManagedWeapons: true,
	})
	if err != nil {
		return nil, err
	}

	// TODO: Move this static value to config
	if consumed := user.ConsumeGem(5); !consumed {
		return nil, errors.New("not enough gems")
	}
	user.ExtendManagedWeaponLimit()

	// Update user
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{})
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *playerWeaponService) EvoluteWeapon(ctx context.Context, internalUserId value_user.UserId, managedWeaponId value_user.ManagedWeaponId, weaponEvolutionId uint) (*schema_weapon.EvoluteWeaponResponseSchema, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "EvoluteWeapon")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ManagedWeapons: true,
		ItemSummary:    true,
	})
	if err != nil {
		return nil, err
	}
	// Get managed weapon
	managedWeapon, err := user.GetManagedWeapon(managedWeaponId)
	if err != nil {
		return nil, err
	}
	// Get weapon and validate it can evolve
	weapon, err := s.wu.GetWeaponById(ctx, managedWeapon.WeaponId)
	if err != nil {
		return nil, err
	}
	if !weapon.CanEvolve() {
		return nil, errors.New("weapon can not evolve anymore")
	}

	// Get evolution recipe
	evolutionRecipe, err := s.evu.GetEvolutionRecipe(ctx, managedWeapon.WeaponId)
	if err != nil {
		return nil, err
	}
	// Consume golds
	requiredGolds := uint64(evolutionRecipe.RequiredCoin)
	if consumed := user.ConsumeGold(requiredGolds); !consumed {
		return nil, err
	}
	// Consume items
	for _, itemInfo := range evolutionRecipe.RecipeMaterials {
		if consumed := user.ConsumeItem(itemInfo.ItemId, uint32(itemInfo.Amount)); !consumed {
			return nil, err
		}
	}
	// Update weapon id
	managedWeapon.WeaponId = evolutionRecipe.DestWeaponId

	// Save the changes
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ManagedWeapons: true,
		ItemSummary:    true,
	})
	if err != nil {
		return nil, err
	}

	// Return response
	resp := schema_weapon.EvoluteWeaponResponseSchema{
		ManagedWeapon: *managedWeapon,
		ItemSummary:   user.ItemSummary,
		Gold:          value_user.Gold(user.Gold),
	}
	return &resp, nil
}

func (s *playerWeaponService) MakeWeapon(ctx context.Context, internalUserId value_user.UserId, recipeId uint32) (*model_user.User, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "MakeWeapon")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ManagedWeapons: true,
		ItemSummary:    true,
	})
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	recipe, err := s.wru.GetWeaponRecipeById(ctx, recipeId)
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	// Consume golds
	if consumed := user.ConsumeGold(uint64(recipe.BuyAmount)); !consumed {
		return nil, errwrap.NewSparkleError(response.RESULT_GOLD_IS_SHORT, "not enough gold")
	}
	// Consume items
	for _, itemInfo := range recipe.RecipeMaterials {
		if consumed := user.ConsumeItem(itemInfo.ItemId, itemInfo.Amount); !consumed {
			return nil, errwrap.NewSparkleError(response.RESULT_ITEM_IS_SHORT, "not enough item")
		}
	}
	// Add weapon
	err = user.AddWeapon(recipe.WeaponId, false)
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	// Save user
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ItemSummary:    true,
		ManagedWeapons: true,
	})
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return user, nil
}

func (s *playerWeaponService) ReceiveWeapon(ctx context.Context, internalUserId value_user.UserId, managedCharacterIds []value_user.ManagedCharacterId) (*schema_weapon.ReceiveWeaponResponseSchema, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "ReceiveWeapon")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ManagedWeapons:    true,
		ManagedCharacters: true,
	})
	if err != nil {
		return nil, err
	}

	// make a slice of weapon ids for get managed weapons later
	newWeaponIds := make([]value_weapon.WeaponId, len(managedCharacterIds))
	for i, managedCharacterId := range managedCharacterIds {
		// Get managed character
		managedCharacter, err := user.GetManagedCharacter(managedCharacterId)
		if err != nil {
			return nil, err
		}
		if managedCharacter.Level != 100 {
			return nil, errors.New("character level is not 100 : " + fmt.Sprint(managedCharacter.CharacterId))
		}
		// Get weaponID for the character
		weaponId, err := s.wcu.GetWeaponCharacterTableById(ctx, managedCharacter.CharacterId)
		if err != nil {
			return nil, err
		}
		newWeaponIds[i] = *weaponId
		if user.HasWeapon(*weaponId) {
			return nil, errors.New("user already has a weapon for the character : " + fmt.Sprint(managedCharacter.CharacterId))
		}
		// Insert weapon
		user.AddWeapon(*weaponId, true)
	}

	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ManagedWeapons: true,
	})
	if err != nil {
		return nil, err
	}

	// Create new weapons slice for response
	// NOTE: This endpoint wants "only" new weapons for unknown reason (tedious)
	newManagedWeapons := make([]model_user.ManagedWeapon, len(newWeaponIds))
	for i := range newWeaponIds {
		managedWeapon, err := user.GetManagedWeaponByWeaponId(newWeaponIds[i])
		if err != nil {
			return nil, err
		}
		newManagedWeapons[i] = *managedWeapon
	}
	resp := schema_weapon.ReceiveWeaponResponseSchema{
		NewWeapons:  newManagedWeapons,
		WeaponLimit: user.WeaponLimit,
	}

	return &resp, nil
}

func (s *playerWeaponService) SaleWeapon(ctx context.Context, internalUserId value_user.UserId, managedWeaponIds []value_user.ManagedWeaponId) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SaleWeapon")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ManagedWeapons: true,
	})
	if err != nil {
		return nil, err
	}

	for _, managedWeaponId := range managedWeaponIds {
		// Validate weapon exists
		managedWeapon, err := user.GetManagedWeapon(managedWeaponId)
		if err != nil {
			return nil, err
		}
		// Get weapon
		weapon, err := s.wu.GetWeaponById(ctx, managedWeapon.WeaponId)
		if err != nil {
			return nil, err
		}
		// Calculate sale price
		salePrice := weapon.SaleAmount
		// Add golds
		user.AddGold(uint64(salePrice))
		// Remove weapon
		if removed := user.RemoveWeapon(managedWeaponId); !removed {
			return nil, errors.New("failed to remove weapon")
		}
	}

	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ManagedWeapons: true,
	})
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (s *playerWeaponService) UpgradeWeapon(ctx context.Context, internalUserId value_user.UserId, managedWeaponId value_user.ManagedWeaponId, items []schema_weapon.ConsumeItem) (*schema_weapon.UpgradeWeaponResponseSchema, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "UpgradeWeapon")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ManagedWeapons: true,
		ItemSummary:    true,
	})
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	// Validate weapon exists
	managedWeapon, err := user.GetManagedWeapon(managedWeaponId)
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	// Consume items and calculate required coins
	baseRequiredGolds, err := s.eu.GetRequiredCoinsForUpgrade(ctx, managedWeapon.Level)
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	requiredGolds := uint64(0)
	for _, item := range items {
		if consumed := user.ConsumeItem(item.ItemId, uint32(item.Count)); !consumed {
			return nil, errwrap.NewSparkleError(response.RESULT_ITEM_IS_SHORT, "not enough item")
		}
		requiredGolds += uint64(item.Count) * baseRequiredGolds.ToValue()
	}
	if consumed := user.ConsumeGold(requiredGolds); !consumed {
		return nil, errwrap.NewSparkleError(response.RESULT_GOLD_IS_SHORT, "not enough gold")
	}

	weapon, err := s.wu.GetWeaponById(ctx, managedWeapon.WeaponId)
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	increaseExps, _ := value_exp.NewWeaponExp(0)
	for _, item := range items {
		amount, err := s.iu.GetWeaponUpgradeAmount(ctx, item.ItemId)
		if err != nil {
			return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		}
		// Add class bonus
		if isBonus := weapon.IsUpgradeBonusItem(item.ItemId); isBonus {
			// FIXME: Move this constant to something else
			// Source: https://kirarabbs.com/index.cgi?read=2670
			amount += value_exp.WeaponExp(uint32(float32(amount) * 0.2))
		}
		increaseExps += value_exp.WeaponExp(uint32(amount) * uint32(item.Count))
	}

	// Roll bonus
	// Source: https://kirarabbs.com/index.cgi?read=1201#bbsform
	bonus := s.ch.Roll()
	switch bonus {
	case upgrade.WeaponUpgradeResultPerfect:
		// FIXME: Move this constant to something else
		increaseExps += increaseExps
	case upgrade.WeaponUpgradeResultGreat:
		// FIXME: Move this constant to something else
		increaseExps += value_exp.WeaponExp(float32(increaseExps) * 0.5)
	}
	//Get Weapon exp at limit lv
	limitWeaponLevel, _ := s.eu.GetLevelExpTableWeapon(ctx, weapon.LimitLv, weapon.ExpTableId)
	increaseExps = calc.Min(increaseExps, value_exp.WeaponExp(limitWeaponLevel.TotalExp-limitWeaponLevel.NextExp-uint32(managedWeapon.Exp)))
	// Increase weapon exp
	managedWeapon.AddWeaponExp(increaseExps)
	// Recalculate weapon level
	nextWeaponLevel, err := s.eu.GetNextExpTableWeapon(ctx, managedWeapon.Exp, weapon.ExpTableId)
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	// Limit display level (it doesn't affect the actual total exps)
	// FIXME: This code has chance to make over limit of total exps
	newLv := uint8(nextWeaponLevel.Level)
	if newLv > weapon.LimitLv {
		newLv = weapon.LimitLv
	}
	managedWeapon.UpdateWeaponLevel(newLv)

	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ItemSummary:    true,
		ManagedWeapons: true,
	})
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	resp := schema_weapon.UpgradeWeaponResponseSchema{
		ManagedWeapon: *managedWeapon,
		ItemSummary:   user.ItemSummary,
		Gold:          value_user.Gold(user.Gold),
		UpgradeResult: bonus,
	}
	return &resp, nil
}

func (s *playerWeaponService) SkillUpPlayerWeapon(ctx context.Context, internalUserId value_user.UserId, managedWeaponId value_user.ManagedWeaponId, items []schema_weapon.ConsumeItem) (*schema_weapon.SkillUpWeaponResponseSchema, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SkillUpPlayerWeapon")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ManagedWeapons: true,
		ItemSummary:    true,
	})
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	// Validate weapon exists
	managedWeapon, err := user.GetManagedWeapon(managedWeaponId)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_PARAMETERS, err)
	}

	// Consume items
	totalCount := uint16(0)
	for _, itemInfo := range items {
		if consumed := user.ConsumeItem(itemInfo.ItemId, uint32(itemInfo.Count)); !consumed {
			span.RecordError(err)
			return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_ITEM_IS_SHORT, err)
		}
		totalCount += itemInfo.Count
	}

	// Add skill exp
	managedWeapon.AddWeaponSkillExp(uint32(totalCount))

	// Get weapon info
	weapon, err := s.wu.GetWeaponById(ctx, managedWeapon.WeaponId)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	skillType := value_exp.ExpTableSkillTypeWeapon
	// Adjust the exp limitations
	limitWeaponSkillExp, err := s.esu.GetLevelExpTableSkill(ctx, uint32(weapon.SkillLimitLv), skillType)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	managedWeapon.AdjustWeaponSkillExp(limitWeaponSkillExp.TotalExp - limitWeaponSkillExp.NextExp)
	// Update levels
	nextWeaponSkillLevel, err := s.esu.GetNextExpTableSkill(ctx, uint32(managedWeapon.SkillExp), skillType)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	managedWeapon.UpdateWeaponSkillLevel(nextWeaponSkillLevel.Level)

	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ItemSummary:    true,
		ManagedWeapons: true,
	})
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return &schema_weapon.SkillUpWeaponResponseSchema{
		ManagedWeapon: *managedWeapon,
		ItemSummary:   user.ItemSummary,
	}, nil
}

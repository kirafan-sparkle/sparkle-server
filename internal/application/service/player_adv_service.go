package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type PlayerAdvService interface {
	AddPlayerAdv(ctx context.Context, internalUserId value_user.UserId, advId uint64, stepCode value_user.StepCode) error
}

type playerAdvService struct {
	uu usecase.UserUsecase
}

func NewPlayerAdvService(uu usecase.UserUsecase) PlayerAdvService {
	return &playerAdvService{uu}
}

func (s *playerAdvService) AddPlayerAdv(ctx context.Context, internalUserId value_user.UserId, advId uint64, stepCode value_user.StepCode) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "AddPlayerAdv")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{AdvIds: true})
	if err != nil {
		return err
	}

	user.AddClearedAdv(advId)
	user.UpdateStepCode(stepCode)

	if _, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{AdvIds: true}); err != nil {
		return err
	}
	return nil
}

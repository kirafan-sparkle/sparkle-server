package service

import (
	"context"
	"errors"

	schema_gacha "gitlab.com/kirafan/sparkle/server/internal/application/schemas/gacha"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_gacha "gitlab.com/kirafan/sparkle/server/internal/domain/value/gacha"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

var ErrGachaServiceUserNotFound = errors.New("user not found")
var ErrGachaServiceGachaNotAvailable = errors.New("gacha is not available")
var ErrGachaServiceServerExplode = errors.New("server exploded")
var ErrGachaServiceUserItemShort = errors.New("user item is short")
var ErrGachaServiceUserGemShort = errors.New("user gem is short")
var ErrGachaServiceUserUnlimitedGemShort = errors.New("user unlimited gem is short")

// TODO: Rename model name (ManagedCharacter -> UserManagedCharacter)
type PlayerGachaService interface {
	refreshPlayerGachaByModel(ctx context.Context, user *model_user.User) (*model_user.User, error)
	RefreshPlayerGacha(ctx context.Context, internalUserId value_user.UserId) (*model_user.User, error)
	DrawPlayerGacha(ctx context.Context, internalUserId value_user.UserId, gachaId uint, param schema_gacha.GachaDrawParamSchema) (*model_user.User, []schema_gacha.GachaResult, error)
	FixPlayerGacha(ctx context.Context, internalUserId value_user.UserId) (*model_user.User, error)
	GetAllPlayerGacha(ctx context.Context, internalUserId value_user.UserId) ([]model_user.UserGacha, error)
	GetPlayerGachaBox(ctx context.Context, gachaId uint) ([]value_character.CharacterId, []value_character.CharacterId, error)
	PointToCharacterPlayerGacha(ctx context.Context, internalUserId value_user.UserId, gachaId uint, CharacterId value_character.CharacterId) (schema_gacha.GachaPointToCharacterResult, error)
}

type playerGachaService struct {
	uu usecase.UserUsecase
	gu usecase.GachaUsecase
	cu usecase.CharacterUsecase
	iu usecase.ItemUsecase
	nu usecase.NamedTypeUsecase
}

func NewPlayerGachaService(
	uu usecase.UserUsecase,
	gu usecase.GachaUsecase,
	iu usecase.ItemUsecase,
	cu usecase.CharacterUsecase,
	nu usecase.NamedTypeUsecase,
) PlayerGachaService {
	return &playerGachaService{uu, gu, cu, iu, nu}
}

func (s *playerGachaService) refreshPlayerGachaByModel(ctx context.Context, user *model_user.User) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "refreshPlayerGachaByModel")
	defer span.End()
	// Get available gachas
	availableGachas, err := s.gu.GetAvailableGachas(ctx)
	if err != nil {
		return nil, ErrGachaServiceServerExplode
	}
	// Create availableGachaIds
	availableGachaIds := make([]uint, len(availableGachas))
	for i := range availableGachas {
		availableGachaIds[i] = availableGachas[i].GachaId
	}

	userGachaIds := make([]uint, len(user.Gachas))
	// Remove gacha from user if needed
	for i, userGacha := range user.Gachas {
		// Remove unavailable gacha
		if !calc.Contains(availableGachaIds, userGacha.GachaId) {
			user.RemoveGacha(userGacha.GachaId)
			continue
		}
		// Force remove tutorial gacha
		if userGacha.GachaId == 1 {
			user.RemoveGacha(userGacha.GachaId)
			continue
		}
		if !userGacha.Gacha.IsUnconditional() && !user.HasItemMoreThan(userGacha.Gacha.ItemId, uint32(userGacha.Gacha.ItemAmount)) {
			user.RemoveGacha(userGacha.GachaId)
			continue
		}
		userGachaIds[i] = userGacha.GachaId
	}

	// Add gachas to user if needed
	for _, gacha := range availableGachas {
		if calc.Contains(userGachaIds, gacha.GachaId) {
			continue
		}
		if gacha.IsUnconditional() {
			user.Gachas = append(user.Gachas, model_user.NewUserGacha(user.Id, gacha.GachaId))
		} else {
			if user.HasItemMoreThan(gacha.ItemId, uint32(gacha.ItemAmount)) {
				user.Gachas = append(user.Gachas, model_user.NewUserGacha(user.Id, gacha.GachaId))
			}
		}
	}
	return user, nil
}

func (s *playerGachaService) RefreshPlayerGacha(ctx context.Context, internalUserId value_user.UserId) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "RefreshPlayerGacha")
	defer span.End()
	// Check the user is available or not
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{Gachas: true})
	if err != nil {
		return nil, ErrGachaServiceUserNotFound
	}
	user, err = s.refreshPlayerGachaByModel(ctx, user)
	if err != nil {
		return nil, ErrGachaServiceServerExplode
	}
	return user, nil
}

func (s *playerGachaService) DrawPlayerGacha(ctx context.Context, internalUserId value_user.UserId, gachaId uint, param schema_gacha.GachaDrawParamSchema) (*model_user.User, []schema_gacha.GachaResult, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "DrawPlayerGacha")
	defer span.End()
	// Check the user is available or not
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		Gachas:            true,
		ManagedCharacters: true,
		ManagedNamedTypes: true,
		ItemSummary:       true,
	})
	if err != nil {
		return nil, nil, ErrGachaServiceUserNotFound
	}
	// Check the gacha is available or not
	gacha, err := s.gu.GetSpecificGacha(ctx, gachaId)
	if err != nil || gacha == nil {
		return nil, nil, ErrGachaServiceGachaNotAvailable
	}

	// Consume item or gem
	switch param.DrawType {
	case value_gacha.GachaDrawTypeUnlimitedGem1:
		if consumed := user.ConsumeUnlimitedGem(uint32(gacha.UnlimitedGem)); !consumed {
			return nil, nil, ErrGachaServiceUserUnlimitedGemShort
		}
	case value_gacha.GachaDrawTypeGem1:
		if consumed := user.ConsumeGem(uint32(gacha.Gem1)); !consumed {
			return nil, nil, ErrGachaServiceUserGemShort
		}
	case value_gacha.GachaDrawTypeGem10:
		if consumed := user.ConsumeGem(uint32(gacha.Gem10)); !consumed {
			return nil, nil, ErrGachaServiceUserGemShort
		}
	case value_gacha.GachaDrawTypeItem:
		if gacha.ItemAmount != -1 {
			consumeCount := uint32(gacha.ItemAmount)
			if param.Is10Roll {
				consumeCount *= 10
			}
			if consumed := user.ConsumeItem(gacha.ItemId, consumeCount); !consumed {
				return nil, nil, ErrGachaServiceUserItemShort
			}
		}
	}
	if param.IsChanceUp {
		if gacha.RateUpTicketId == -1 {
			return nil, nil, ErrGachaServiceGachaNotAvailable
		}
		consumeCount := uint32(1)
		if param.Is10Roll {
			consumeCount = 10
		}
		if consumed := user.ConsumeItem(gacha.RateUpTicketId, consumeCount); !consumed {
			return nil, nil, ErrGachaServiceUserItemShort
		}
	}

	// Draw the gacha
	characterIds, err := s.gu.DrawGacha(ctx, gachaId, param)
	if err != nil || len(characterIds) == 0 {
		return nil, nil, ErrGachaServiceServerExplode
	}

	// Update roll count / step of gacha
	for _, gacha := range user.Gachas {
		if gacha.GachaId != gachaId {
			continue
		}
		switch param.DrawType {
		case value_gacha.GachaDrawTypeUnlimitedGem1:
			gacha.UGem1Daily++
			gacha.UGem1Total++
		case value_gacha.GachaDrawTypeGem1:
			gacha.Gem1Daily++
			gacha.Gem1Total++
		case value_gacha.GachaDrawTypeGem10:
			gacha.Gem10CurrentStep++
			gacha.Gem10Daily++
			gacha.Gem10Total++
		case value_gacha.GachaDrawTypeItem:
			if param.Is10Roll {
				gacha.ItemDaily += 10
				gacha.ItemTotal += 10
			} else {
				gacha.ItemDaily++
				gacha.ItemTotal++
			}
		}
		break
	}

	// Create Gacha result object
	gachaResults := []schema_gacha.GachaResult{}

	// Updates userManagedCharacters and userManagedNamedTypes field
	for _, characterId := range characterIds {
		duplicated := user.AddCharacter(characterId)
		character, err := s.cu.GetCharacterById(ctx, characterId)
		if err != nil {
			return nil, nil, ErrGachaServiceServerExplode
		}
		if duplicated {
			user.AddItem(character.AltItemID, 1)
			gachaResults = append(gachaResults, schema_gacha.GachaResult{
				CharacterId: characterId,
				ItemId:      character.AltItemID,
				ItemAmount:  1,
			})
		} else {
			gachaResults = append(gachaResults, schema_gacha.GachaResult{
				CharacterId: characterId,
				ItemId:      -1,
				ItemAmount:  0,
			})
		}
		namedType, err := s.nu.GetNamedTypeById(ctx, uint(character.NamedType))
		if err != nil {
			return nil, nil, ErrGachaServiceServerExplode
		}
		// NOTE: Ignore the duplicated error
		user.AddNamedType(uint16(namedType.NamedType), namedType.TitleType)
	}

	// Updates playerGachas field
	user, err = s.refreshPlayerGachaByModel(ctx, user)
	if err != nil {
		return nil, nil, ErrGachaServiceServerExplode
	}

	// FIXME: this is workaround, the result should be return to the interface layer
	itemId, _ := value_item.NewItemId(uint32(9001))
	if param.Is10Roll {
		user.AddItem(itemId, uint32(10))
	} else {
		user.AddItem(itemId, uint32(1))
	}

	// Save the user model changes
	_, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		Gachas:            true,
		ManagedCharacters: true,
		ManagedNamedTypes: true,
		ItemSummary:       true,
	})
	if err != nil {
		return nil, nil, ErrGachaServiceServerExplode
	}

	if user.StepCode == value_user.StepCodeAdvCreaDone {
		user, err = s.uu.SetupUser(ctx, internalUserId)
		if err != nil {
			return nil, nil, ErrGachaServiceServerExplode
		}
	}

	// TODO: Make a better response type, model_user.ManagedCharacter is too big
	return user, gachaResults, nil
}

func (s *playerGachaService) FixPlayerGacha(ctx context.Context, internalUserId value_user.UserId) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "FixPlayerGacha")
	defer span.End()
	// STUB
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ItemSummary:       true,
		ManagedCharacters: true,
		ManagedNamedTypes: true,
		OfferTitleTypes:   true,
	})
	if err != nil {
		return nil, errors.New("user not found")
	}
	return user, nil
}

func (s *playerGachaService) GetAllPlayerGacha(ctx context.Context, internalUserId value_user.UserId) ([]model_user.UserGacha, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetAllPlayerGacha")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{Gachas: true})
	if err != nil || user.Gachas == nil {
		return nil, errors.New("user not found")
	}
	return user.Gachas, nil
}

func (s *playerGachaService) GetPlayerGachaBox(ctx context.Context, gachaId uint) ([]value_character.CharacterId, []value_character.CharacterId, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetPlayerGachaBox")
	defer span.End()
	gacha, err := s.gu.GetSpecificGacha(ctx, uint(gachaId))
	if err != nil || gacha == nil {
		return nil, nil, errors.New("gacha not found")
	}
	normalCharacters := gacha.GetNormalCharacters()
	pickUpCharacters := gacha.GetPickupCharacters()
	return normalCharacters, pickUpCharacters, nil
}

func (s *playerGachaService) PointToCharacterPlayerGacha(
	ctx context.Context, internalUserId value_user.UserId, gachaId uint, CharacterId value_character.CharacterId,
) (schema_gacha.GachaPointToCharacterResult, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "PointToCharacterPlayerGacha")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{Gachas: true})
	if err != nil || user.Gachas == nil {
		return schema_gacha.GachaPointToCharacterResult{}, errors.New("user not found")
	}
	return schema_gacha.GachaPointToCharacterResult{}, errors.New("this endpoint is not implemented yet")
}

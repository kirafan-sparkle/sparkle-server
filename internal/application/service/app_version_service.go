package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_version "gitlab.com/kirafan/sparkle/server/internal/domain/model/version"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type AppVersionService interface {
	GetAppVersion(ctx context.Context, platform value_version.Platform, version value_version.Version) (*model_version.Version, error)
}

func NewAppVersionService(
	vu usecase.VersionUsecase,
) AppVersionService {
	return &appVersionService{vu}
}

type appVersionService struct {
	vu usecase.VersionUsecase
}

func (s *appVersionService) GetAppVersion(ctx context.Context, platform value_version.Platform, version value_version.Version) (*model_version.Version, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetAppVersion")
	defer span.End()

	versionInfo, err := s.vu.FindByPlatformAndVersion(ctx, platform, version)
	return versionInfo, err
}

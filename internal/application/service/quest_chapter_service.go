package service

import (
	"context"
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_chapter "gitlab.com/kirafan/sparkle/server/internal/domain/model/chapter"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type QuestChapterService interface {
	GetAllQuestChapter(ctx context.Context) ([]*model_chapter.Chapter, *time.Time, error)
}

type questChapterService struct {
	qu usecase.ChapterUsecase
}

func NewQuestChapterService(qu usecase.ChapterUsecase) QuestChapterService {
	return &questChapterService{qu}
}

func (s *questChapterService) GetAllQuestChapter(ctx context.Context) ([]*model_chapter.Chapter, *time.Time, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetAllQuestChapter")
	defer span.End()

	chapters, newDispEndAt, err := s.qu.GetAll(ctx)
	return chapters, newDispEndAt, err
}

package service

import (
	"context"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	schema_gacha "gitlab.com/kirafan/sparkle/server/internal/application/schemas/gacha"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_gacha "gitlab.com/kirafan/sparkle/server/internal/domain/value/gacha"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/migrate"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/seed"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

func Test_playerGachaService_DrawPlayerGacha(t *testing.T) {
	logger := observability.InitLogger(observability.LogTypeDebug)
	dbConf := database.GetConfig()
	db := database.InitDatabase(dbConf, logger)
	migrate.AutoMigrate(db)
	seed.AutoSeed(db)

	ur := persistence.NewUserRepositoryImpl(db)
	s := InitializePlayerGachaService(db)
	ctx := context.Background()

	ignoreFields := []string{
		"CreatedAt",
		"UpdatedAt",
		"UUId",
		"Session",
		"ContinuousDays",
		"FacilityLimit",
		"FriendLimit",
		"IpAddr",
		"KiraraLimit",
		"LastLoginAt",
		"Level",
		"MyCode",
		"Name",
		"PartyCost",
		"RecastTimeMax",
		"RoomObjectLimit",
		"Stamina",
		"StaminaMax",
		"StaminaUpdatedAt",
		"State",
		"SupportLimit",
		"UserAgent",
		"WeaponLimit",
		"SupportCharacters",
		"ManagedBattleParties",
		"ManagedFacilities",
		"ManagedFieldPartyMembers",
		"ManagedRoomObjects",
		"ManagedRooms",
		"ManagedTowns",
		"ManagedMasterOrbs",
		"ManagedWeapons",
		"FavoriteMembers",
		"OfferTitleTypes",
		"AdvIds",
		"TipIds",
		"FlagUi",
		"FlagPush",
		"FlagStamina",
		"IsCloseInfo",
		"StepCode",
		"OrderReceiveId",
		"LastMemberAdded",
		"LastOpenedPart1ChapterId",
		"LastOpenedPart2ChapterId",
		"LastPlayedPart1ChapterQuestId",
		"LastPlayedPart2ChapterQuestId",
		"Missions",
		"Gachas",
	}
	opts := []cmp.Option{
		cmpopts.IgnoreFields(model_user.User{}, ignoreFields...),
		cmpopts.IgnoreFields(model_user.ItemSummary{}, "CreatedAt", "UpdatedAt"),
	}

	type args struct {
		userId  uint
		gachaId uint
		param   schema_gacha.GachaDrawParamSchema
	}
	tests := []struct {
		name            string
		user            *model_user.User
		args            args
		wantUser        *model_user.User
		wantReceivedLen int
		wantErr         error
	}{
		{
			name: "draw first selectable gacha success",
			args: args{
				userId:  1,
				gachaId: 1,
				param: schema_gacha.GachaDrawParamSchema{
					DrawType:            value_gacha.GachaDrawTypeGem1,
					IsChanceUp:          false,
					Is10Roll:            false,
					SelectedCharacterId: 30012020,
				},
			},
			wantUser: &model_user.User{
				Id:                   2,
				Age:                  0,
				CharacterLimit:       0,
				CharacterWeaponCount: 0,
				CurrentAchievementId: 0,
				FacilityLimitCount:   0,
				Gold:                 0,
				ItemLimit:            0,
				Kirara:               0,
				LastPartyAdded:       nil,
				LevelExp:             0,
				UnlimitedGem:         0,
				LimitedGem:           0,
				LoginCount:           0,
				LoginDays:            0,
				LotteryTicket:        0,
				RecastTime:           0,
				RoomObjectLimitCount: 0,
				Stamina:              0,
				TotalExp:             0,
				WeaponLimitCount:     0,
				Comment:              nil,
				ItemSummary:          []model_user.ItemSummary{},
				Presents:             []model_user.UserPresent{},
				ManagedBattleParties: []model_user.ManagedBattleParty{},
				ManagedCharacters: []model_user.ManagedCharacter{
					{
						ManagedCharacterId: 1,
						PlayerId:           2,
						Level:              1,
						LevelLimit:         50,
						Exp:                0,
						LevelBreak:         0,
						DuplicatedCount:    0,
						ArousalLevel:       0,
						SkillLevel1:        1,
						SkillLevelLimit1:   5,
						SkillExp1:          0,
						SkillLevel2:        1,
						SkillLevelLimit2:   5,
						SkillExp2:          0,
						SkillLevel3:        1,
						SkillLevelLimit3:   5,
						SkillExp3:          0,
						Shown:              0,
						CharacterId:        30012020,
						ViewCharacterId:    30012020,
					},
				},
				ManagedNamedTypes: []model_user.ManagedNamedType{
					{
						ManagedNamedTypeId:   1,
						UserId:               2,
						NamedType:            117,
						Level:                1,
						Exp:                  0,
						TitleType:            20,
						FriendshipExpTableId: 0,
					},
				},
				ManagedFieldPartyMembers: nil,
				IsNewProduct:             0,
				TrainingCount:            0,
				FriendProposedCount:      0,
				NewAchievementCount:      0,
				StepCode:                 0,
				Gachas:                   []model_user.UserGacha{},
			},
			wantReceivedLen: 1,
			wantErr:         nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u, err := ur.CreateUser(ctx, tt.user)
			if err != nil {
				t.Errorf("userRepo.CreateUser error = %v, wantErr nil", err)
			}
			gotUser, gotReceived, err := s.DrawPlayerGacha(context.Background(), u.Id, 1, tt.args.param)
			if err != tt.wantErr {
				t.Errorf("playerGachaService.DrawPlayerGacha() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if cmp.Equal(*gotUser, *tt.wantUser, opts...) != true {
				t.Errorf("playerGachaService.DrawPlayerGacha() Diff = %+v", cmp.Diff(tt.wantUser, gotUser, opts...))
				return
			}
			if len(gotReceived) != tt.wantReceivedLen {
				t.Errorf("playerGachaService.DrawPlayerGacha() ReceivedLen = %v, want %v", len(gotReceived), tt.wantReceivedLen)
				return
			}
		})
	}
}

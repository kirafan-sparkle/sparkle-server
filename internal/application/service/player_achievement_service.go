package service

import (
	"context"
	"fmt"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type PlayerAchievementService interface {
	// Get All Player Achievement (WIP)
	GetAllPlayerAchievement(ctx context.Context, internalUserId value_user.UserId) ([]model_user.UserAchievement, error)
	// Set Player Achievement (WIP)
	SetPlayerAchievement(ctx context.Context, internalUserId value_user.UserId, achievementId uint64) (*model_user.User, error)
	// Shown Player Achievement (WIP)
	ShownPlayerAchievement(ctx context.Context, internalUserId value_user.UserId, titleTypes []value_character.TitleType) error
}

type playerAchievementService struct {
	uu usecase.UserUsecase
}

func NewPlayerAchievementService(uu usecase.UserUsecase) PlayerAchievementService {
	return &playerAchievementService{uu}
}

func (s *playerAchievementService) GetAllPlayerAchievement(ctx context.Context, internalUserId value_user.UserId) ([]model_user.UserAchievement, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetAllPlayerAchievement")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		Achievements: true,
	})
	if err != nil {
		return nil, err
	}
	return user.Achievements, nil
}

func (s *playerAchievementService) SetPlayerAchievement(ctx context.Context, internalUserId value_user.UserId, achievementId uint64) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SetPlayerAchievement")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		Achievements: true,
	})
	if err != nil {
		return nil, err
	}

	if !user.Achievements.HasAchievement(uint(achievementId)) {
		return nil, fmt.Errorf("achievementId %d does not exist for user %d", achievementId, internalUserId)
	}
	user.CurrentAchievementId = achievementId

	// Save user
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{})
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *playerAchievementService) ShownPlayerAchievement(ctx context.Context, internalUserId value_user.UserId, titleTypes []value_character.TitleType) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "ShownPlayerAchievement")
	defer span.End()
	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		Achievements: true,
	})
	if err != nil {
		return err
	}

	user.Achievements.SetAchievementsShownByTitleTypes(titleTypes)

	// Save user
	_, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		Achievements: true,
	})
	if err != nil {
		return err
	}
	return nil
}

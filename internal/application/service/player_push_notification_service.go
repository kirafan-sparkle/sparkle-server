package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type PlayerPushNotificationService interface {
	SetPlayerPushNotification(ctx context.Context, internalUserId value_user.UserId, flagPush value.BoolLikeUInt8, flagUi value.BoolLikeUInt8, flagStamina value.BoolLikeUInt8) error
}

type playerPushNotificationService struct {
	uu usecase.UserUsecase
}

func NewPlayerPushNotificationService(uu usecase.UserUsecase) PlayerPushNotificationService {
	return &playerPushNotificationService{uu}
}

func (s *playerPushNotificationService) SetPlayerPushNotification(ctx context.Context, internalUserId value_user.UserId, flagPush value.BoolLikeUInt8, flagUi value.BoolLikeUInt8, flagStamina value.BoolLikeUInt8) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SetPlayerPushNotification")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		return err
	}

	user.FlagPush = flagPush
	user.FlagUi = flagUi
	user.FlagStamina = flagStamina

	if _, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{}); err != nil {
		return err
	}
	return nil
}

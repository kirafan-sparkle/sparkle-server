package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type PlayerGetAllService interface {
	GetPlayerAllInfo(ctx context.Context, internalUserId value_user.UserId) (*model_user.User, []model_user.ManagedAbilityBoard, *errwrap.SparkleError)
}

type playerGetAllService struct {
	uu  usecase.UserUsecase
	fu  usecase.FriendUsecase
	abu usecase.UserAbilityBoardUsecase
}

func NewPlayerGetAllService(uu usecase.UserUsecase, fu usecase.FriendUsecase, abu usecase.UserAbilityBoardUsecase) PlayerGetAllService {
	return &playerGetAllService{uu, fu, abu}
}

func (s *playerGetAllService) GetPlayerAllInfo(ctx context.Context, internalUserId value_user.UserId) (*model_user.User, []model_user.ManagedAbilityBoard, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetPlayerAllInfo")

	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		AdvIds:                   true,
		FavoriteMembers:          true,
		ItemSummary:              true,
		ManagedBattleParties:     true,
		ManagedCharacters:        true,
		ManagedFieldPartyMembers: true,
		ManagedMasterOrbs:        true,
		ManagedNamedTypes:        true,
		ManagedRooms:             true,
		ManagedRoomObjects:       true,
		ManagedFacilities:        true,
		ManagedTowns:             true,
		ManagedWeapons:           true,
		OfferTitleTypes:          true,
		SupportCharacters:        true,
		TipIds:                   true,
	})
	if err != nil {
		span.RecordError(err)
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_SESSION_EXPIRED, err)
	}

	abilityBoards, err := s.abu.GetAbilityBoards(ctx, internalUserId)
	if err != nil {
		span.RecordError(err)
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	playerId, err := value_friend.NewPlayerId(internalUserId, value_friend.PlayerSpecialTypeDefault)
	if err != nil {
		span.RecordError(err)
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_PARAMETERS, err)
	}

	// TODO: refactor this refresh process as usecase
	if user.ShouldRefreshUICounts() {
		// Refresh training count
		user.TrainingCount = user.TrainingSlots.GetEndedSlotAmount()
		// Refresh friend proposed count
		incomingFriendRequests, err2 := s.fu.GetInComingFriendRequests(ctx, *playerId)
		if err2 != nil {
			span.RecordError(err2.GetRawError())
			return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		}
		user.FriendProposedCount = uint8(len(incomingFriendRequests))
		// Refresh present count
		user.PresentCount = 0
		// Update user
		if _, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{}); err != nil {
			span.RecordError(err)
			return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		}
	}

	return user, abilityBoards, nil
}

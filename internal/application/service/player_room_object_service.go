package service

import (
	"context"
	"errors"

	schema_room_object "gitlab.com/kirafan/sparkle/server/internal/application/schemas/room_object"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/parser"
)

type PlayerRoomObjectService interface {
	SalePlayerRoomObject(
		ctx context.Context,
		internalUserId value_user.UserId,
		managedRoomObjectId uint,
	) (*model_user.User, error)
	BuySetPlayerRoomObject(
		ctx context.Context,
		internalUserId value_user.UserId,
		param schema_room_object.BuySetPlayerRoomObject,
	) (*model_user.User, *string, error)
	AddPlayerRoomObjectLimit(
		ctx context.Context,
		internalUserId value_user.UserId,
	) (*model_user.User, error)
}

var ErrPlayerRoomObjectNotEnoughGem = errors.New("user room facility: not enough gem")
var ErrPlayerRoomObjectNotEnoughCoin = errors.New("user room facility: not enough coin")
var ErrPlayerRoomObjectRoomNotFound = errors.New("user room facility: room not found")

type playerRoomObjectService struct {
	uu usecase.UserUsecase
	ru usecase.RoomObjectUsecase
}

func NewPlayerRoomObjectService(
	uu usecase.UserUsecase,
	ru usecase.RoomObjectUsecase,
) PlayerRoomObjectService {
	return &playerRoomObjectService{uu, ru}
}

func (s *playerRoomObjectService) SalePlayerRoomObject(
	ctx context.Context,
	internalUserId value_user.UserId,
	managedRoomObjectId uint,
) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SalePlayerRoomObject")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{ManagedRoomObjects: true})
	if err != nil {
		return nil, err
	}

	managedRoomObject, err := user.GetRoomObject(managedRoomObjectId)
	if err != nil {
		return nil, err
	}
	roomObject, err := s.ru.GetRoomObject(ctx, managedRoomObject.RoomObjectId)
	if err != nil {
		return nil, err
	}

	// Add coin
	user.AddGold(uint64(roomObject.SaleAmount))
	user.RemoveRoomObject(int(managedRoomObject.RoomObjectId), uint32(managedRoomObject.ManagedRoomObjectId))

	// Update user
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{ManagedRoomObjects: true})
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *playerRoomObjectService) buyPlayerRoomObject(
	ctx context.Context,
	internalUserId value_user.UserId,
	param schema_room_object.BuySetPlayerRoomObject,
) ([]int, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "buyPlayerRoomObject")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx,
		internalUserId,
		repository.UserRepositoryParam{ManagedRoomObjects: true},
	)
	if err != nil {
		return nil, err
	}

	roomObject, err := s.ru.GetRoomObject(ctx, param.RoomObjectId)
	if err != nil {
		return nil, err
	}

	newManagedRoomObjectIds := make([]int, int(param.BuyAmount))
	for i := 0; i < int(param.BuyAmount); i++ {
		consumed := user.ConsumeGold(uint64(roomObject.BuyAmount))
		if !consumed {
			return nil, ErrPlayerRoomObjectNotEnoughCoin
		}
		user.AddRoomObject(param.RoomObjectId)
		user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{ManagedRoomObjects: true})
		if err != nil {
			return nil, err
		}
		newManagedRoomObjectIds[i] = *calc.ToPtr(user.ManagedRoomObjects[len(user.ManagedRoomObjects)-1].ManagedRoomObjectId)
	}
	return newManagedRoomObjectIds, nil
}

func (s *playerRoomObjectService) setPlayerRoom(
	ctx context.Context,
	internalUserId value_user.UserId,
	param schema_room_object.BuySetPlayerRoomObject,
	newIds []int,
) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "setPlayerRoom")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx,
		internalUserId,
		repository.UserRepositoryParam{ManagedRooms: true, ManagedRoomObjects: true},
	)
	if err != nil {
		return nil, err
	}

	for i := 0; i < int(param.BuyAmount); i++ {
		for i2 := 0; i2 < len(param.ArrangeData); i2++ {
			if param.ArrangeData[i2].ManagedRoomObjectId == -2 && param.ArrangeData[i2].RoomObjectId == param.RoomObjectId {
				param.ArrangeData[i2].ManagedRoomObjectId = newIds[i]
			}
		}
	}

	if err := user.UpdateRoom(param.ManagedRoomId, param.ArrangeData); err != nil {
		return nil, err
	}

	// Update user
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{ManagedRooms: true, ManagedRoomObjects: true})
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *playerRoomObjectService) BuySetPlayerRoomObject(
	ctx context.Context,
	internalUserId value_user.UserId,
	param schema_room_object.BuySetPlayerRoomObject,
) (*model_user.User, *string, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "BuySetPlayerRoomObject")
	defer span.End()
	newIds, err := s.buyPlayerRoomObject(ctx, internalUserId, param)
	if err != nil {
		span.RecordError(err)
		return nil, nil, err
	}

	newManagedRoomObjectIds := parser.ParseToManagedRoomObjectIdsResponse(newIds)
	// When buy a room object from architect, this arrangeData can be empty
	if len(param.ArrangeData) != 0 {
		user, err := s.setPlayerRoom(ctx, internalUserId, param, newIds)
		if err != nil {
			span.RecordError(err)
			return nil, nil, err
		}
		return user, &newManagedRoomObjectIds, nil
	}
	// When buy a room object in a room, this arrangeData is not empty
	user, err := s.uu.GetUserByInternalId(ctx,
		internalUserId,
		repository.UserRepositoryParam{ManagedRooms: true, ManagedRoomObjects: true},
	)
	if err != nil {
		span.RecordError(err)
		return nil, nil, err
	}
	return user, &newManagedRoomObjectIds, nil
}

func (s *playerRoomObjectService) AddPlayerRoomObjectLimit(
	ctx context.Context,
	internalUserId value_user.UserId,
) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "AddPlayerRoomObjectLimit")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		return nil, err
	}

	// TODO: Move this static value to config
	if consumed := user.ConsumeGem(5); !consumed {
		return nil, ErrPlayerRoomObjectNotEnoughGem
	}
	user.ExtendManagedRoomObjectLimit()

	// Update user
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{})
	if err != nil {
		return nil, err
	}
	return user, nil
}

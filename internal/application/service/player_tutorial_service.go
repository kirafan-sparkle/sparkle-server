package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type PlayerTutorialService interface {
	AddPlayerTutorialStep(ctx context.Context, internalUserId value_user.UserId) *errwrap.SparkleError
	AddPlayerTutorialTip(ctx context.Context, internalUserId value_user.UserId, tipId uint32) *errwrap.SparkleError
}

type playerTutorialService struct {
	uu usecase.UserUsecase
}

func NewPlayerTutorialService(
	uu usecase.UserUsecase,
) PlayerTutorialService {
	return &playerTutorialService{uu}
}

func (s *playerTutorialService) AddPlayerTutorialStep(
	ctx context.Context,
	internalUserId value_user.UserId,
) *errwrap.SparkleError {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "AddPlayerTutorialStep")
	defer span.End()

	player, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		span.RecordError(err)
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_NOT_FOUND, err)
	}

	player.UpdateStepCode(value_user.StepCodeTutorialDone)

	_, err = s.uu.UpdateUser(ctx, player, repository.UserRepositoryParam{})
	if err != nil {
		span.RecordError(err)
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return nil
}

func (s *playerTutorialService) AddPlayerTutorialTip(
	ctx context.Context,
	internalUserId value_user.UserId,
	tipId uint32,
) *errwrap.SparkleError {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "AddPlayerTutorialTip")
	defer span.End()

	player, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		span.RecordError(err)
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_NOT_FOUND, err)
	}

	player.AddTutorialTip(tipId)

	_, err = s.uu.UpdateUser(ctx, player, repository.UserRepositoryParam{
		TipIds: true,
	})
	if err != nil {
		span.RecordError(err)
		errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return nil
}

package service

import (
	"context"
	"errors"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_ability_board "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_board"
	value_ability_sphere "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_sphere"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type PlayerAbilityService interface {
	// Equip Player Ability
	EquipPlayerAbility(
		ctx context.Context,
		internalUserId value_user.UserId,
		managedAbilityBoardId value_user.ManagedAbilityBoardId,
		slotIndex value_ability_board.AbilityBoardSlotIndex,
		sphereItemId value_ability_sphere.AbilitySphereItemId,
	) ([]model_user.ManagedAbilityBoard, []model_user.ItemSummary, *errwrap.SparkleError)
	// Release Player Ability
	ReleasePlayerAbility(
		ctx context.Context,
		internalUserId value_user.UserId,
		itemId value_item.ItemId,
		managedCharacterId value_user.ManagedCharacterId,
	) ([]model_user.ManagedAbilityBoard, []model_user.ItemSummary, *errwrap.SparkleError)
	// Release Slot Player Ability
	ReleaseSlotPlayerAbility(
		ctx context.Context,
		internalUserId value_user.UserId,
		managedAbilityBoardId value_user.ManagedAbilityBoardId,
		slotIndex value_ability_board.AbilityBoardSlotIndex,
	) (*model_user.User, []model_user.ItemSummary, []model_user.ManagedAbilityBoard, *errwrap.SparkleError)
	// Upgrade Sphere Player Ability
	UpgradeSpherePlayerAbility(
		ctx context.Context,
		internalUserId value_user.UserId,
		managedAbilityBoardId value_user.ManagedAbilityBoardId,
		slotIndex value_ability_board.AbilityBoardSlotIndex,
		materialItemIds []value_item.ItemId,
		materialItemAmounts []int64,
		srcSphereItemId value_ability_sphere.AbilitySphereItemId,
		goldAmount int64,
	) (*model_user.User, []model_user.ItemSummary, []model_user.ManagedAbilityBoard, *errwrap.SparkleError)
}

type playerAbilityService struct {
	uu  usecase.UserUsecase
	au  usecase.AbilityBoardUsecase
	uau usecase.UserAbilityBoardUsecase
	asu usecase.AbilitySphereUsecase
}

func NewPlayerAbilityService(uu usecase.UserUsecase, au usecase.AbilityBoardUsecase, uau usecase.UserAbilityBoardUsecase, asu usecase.AbilitySphereUsecase) PlayerAbilityService {
	return &playerAbilityService{uu, au, uau, asu}
}

// Set skill tree item into skill tree slot
func (s *playerAbilityService) EquipPlayerAbility(
	ctx context.Context,
	internalUserId value_user.UserId,
	managedAbilityBoardId value_user.ManagedAbilityBoardId,
	slotIndex value_ability_board.AbilityBoardSlotIndex,
	sphereItemId value_ability_sphere.AbilitySphereItemId,
) ([]model_user.ManagedAbilityBoard, []model_user.ItemSummary, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "EquipPlayerAbility")
	defer span.End()

	// Load
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{ItemSummary: true})
	if err != nil {
		span.RecordError(err)
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_NOT_FOUND, err)
	}
	abilityBoard, err := s.uau.GetAbilityBoard(ctx, internalUserId, managedAbilityBoardId)
	if err != nil {
		span.RecordError(err)
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_PARAMETERS, err)
	}

	// Update
	removedSphereItemId, isRemovedSphereItemIdExist := abilityBoard.GetEquipItemIfExist(slotIndex)
	if isRemovedSphereItemIdExist {
		user.AddItem(removedSphereItemId.AsItem(), 1)
	}

	if sphereItemId != value_ability_sphere.AbilitySphereItemIdUnspecified {
		// Set an item to a slot
		if consumed := user.ConsumeItem(sphereItemId.AsItem(), 1); !consumed {
			err := errors.New("not enough item")
			span.RecordError(err)
			return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_ITEM_IS_SHORT, err)
		}
	}

	if err := abilityBoard.SetEquipItem(slotIndex, sphereItemId); err != nil {
		span.RecordError(err)
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	// Save
	if _, err := s.uau.UpdateAbilityBoard(ctx, internalUserId, *abilityBoard); err != nil {
		span.RecordError(err)
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{ItemSummary: true})
	if err != nil {
		span.RecordError(err)
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	// Get result
	abilityBoards, err := s.uau.GetAbilityBoards(ctx, internalUserId)
	if err != nil {
		span.RecordError(err)
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return abilityBoards, user.ItemSummary, nil
}

// Open skill tree
func (s *playerAbilityService) ReleasePlayerAbility(
	ctx context.Context,
	internalUserId value_user.UserId,
	itemId value_item.ItemId,
	managedCharacterId value_user.ManagedCharacterId,
) ([]model_user.ManagedAbilityBoard, []model_user.ItemSummary, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "ReleasePlayerAbility")
	defer span.End()

	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{ItemSummary: true, ManagedCharacters: true})
	if err != nil {
		span.RecordError(err)
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_NOT_FOUND, err)
	}

	// Validate managed character
	if _, err := user.GetManagedCharacter(managedCharacterId); err != nil {
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_PARAMETERS, err)
	}
	// Validate abilityBoard
	abilityBoard, err := s.au.GetAbilityBoardByItemId(ctx, itemId)
	if err != nil {
		span.RecordError(err)
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_PARAMETERS, err)
	}
	// Consume item from user
	if consumed := user.ConsumeItem(itemId, 1); !consumed {
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_ITEM_IS_SHORT, errors.New("item is not enough"))
	}

	if _, serr := s.uau.AddAbilityBoard(ctx, internalUserId, abilityBoard.AbilityBoardId, managedCharacterId); serr != nil {
		span.RecordError(serr.GetRawError())
		return nil, nil, serr
	}

	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{ItemSummary: true})
	if err != nil {
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	managedAbilityBoards, err := s.uau.GetAbilityBoards(ctx, internalUserId)
	if err != nil {
		span.RecordError(err)
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return managedAbilityBoards, user.ItemSummary, nil
}

// Open skill tree slot
func (s *playerAbilityService) ReleaseSlotPlayerAbility(
	ctx context.Context,
	internalUserId value_user.UserId,
	managedAbilityBoardId value_user.ManagedAbilityBoardId,
	slotIndex value_ability_board.AbilityBoardSlotIndex,
) (*model_user.User, []model_user.ItemSummary, []model_user.ManagedAbilityBoard, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "ReleaseSlotPlayerAbility")
	defer span.End()

	abilityBoard, err := s.uau.GetAbilityBoard(ctx, internalUserId, managedAbilityBoardId)
	if err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_PARAMETERS, err)
	}
	slotCount := abilityBoard.GetEquipItemSlotCount()
	if uint8(slotIndex) < slotCount || uint8(slotIndex) > slotCount+1 {
		err := errors.New("invalid slot index")
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_PARAMETERS, err)
	}

	abilityBoardSlot, err := s.au.GetAbilityBoardSlot(ctx, abilityBoard.AbilityBoardId, slotIndex)
	if err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	abilityBoardSlotRecipe, err := s.au.GetAbilityBoardSlotRecipe(ctx, abilityBoardSlot.AbilityBoardRecipeId)
	if err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{ItemSummary: true})
	if err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_NOT_FOUND, err)
	}

	if consumed := user.ConsumeGold(uint64(abilityBoardSlotRecipe.RequiredCoins)); !consumed {
		err := errors.New("not enough gold")
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_GOLD_IS_SHORT, err)
	}
	if consumed := user.ConsumeItem(abilityBoardSlotRecipe.ItemId, abilityBoardSlotRecipe.ItemAmount); !consumed {
		err := errors.New("not enough item")
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_ITEM_IS_SHORT, err)
	}

	if err := abilityBoard.AddEquipItemSlot(); err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{ItemSummary: true})
	if err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	if _, err := s.uau.UpdateAbilityBoard(ctx, internalUserId, *abilityBoard); err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	managedAbilityBoards, err := s.uau.GetAbilityBoards(ctx, internalUserId)
	if err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return user, user.ItemSummary, managedAbilityBoards, nil
}

func (s *playerAbilityService) UpgradeSpherePlayerAbility(
	ctx context.Context,
	internalUserId value_user.UserId,
	managedAbilityBoardId value_user.ManagedAbilityBoardId,
	slotIndex value_ability_board.AbilityBoardSlotIndex,
	// unused parameter
	materialItemIds []value_item.ItemId,
	// unused parameter
	materialItemAmounts []int64,
	// unused parameter
	srcSphereItemId value_ability_sphere.AbilitySphereItemId,
	// unused parameter (why does the client calculate it?)
	goldAmount int64,
) (*model_user.User, []model_user.ItemSummary, []model_user.ManagedAbilityBoard, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "UpgradeSpherePlayerAbility")
	defer span.End()

	// Load
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{ItemSummary: true})
	if err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_NOT_FOUND, err)
	}
	abilityBoard, err := s.uau.GetAbilityBoard(ctx, internalUserId, managedAbilityBoardId)
	if err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_PARAMETERS, err)
	}
	equipItem, err := abilityBoard.GetEquipItem(slotIndex)
	if err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_PARAMETERS, err)
	}
	if srcSphereItemId != equipItem {
		err := errors.New("invalid srcItemId")
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_PARAMETERS, err)
	}
	if isValid, err := s.asu.IsValidAbilitySphere(ctx, srcSphereItemId); !isValid || err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_PARAMETERS, err)
	}
	abilitySphereRecipe, err := s.asu.GetAbilitySphereRecipeByItemId(ctx, srcSphereItemId)
	if err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	// Item and Gold Consume
	if consumed := user.ConsumeGold(uint64(abilitySphereRecipe.GoldAmount)); !consumed {
		err := errors.New("not enough gold")
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_GOLD_IS_SHORT, err)
	}
	currentAmount := user.GetItemAmount(abilitySphereRecipe.SrcItemId.AsItem())
	if currentAmount >= abilitySphereRecipe.SrcItemAmount {
		if consumed := user.ConsumeItem(abilitySphereRecipe.SrcItemId.AsItem(), uint32(abilitySphereRecipe.SrcItemAmount)); !consumed {
			err := errors.New("not enough item")
			return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_ITEM_IS_SHORT, err)
		}
	} else {
		// FIXME: This is not same as official server logic
		// It seems the client sends the coins / item ids / item amounts.
		// But I was unsure why it is needed, so I just automatically consume the items at server.

		// Convert the rest of the needed item amounts from the lowest fruits
		shortedAmount := uint32(abilitySphereRecipe.SrcItemAmount) - currentAmount
		lowerSphereRecipe, err := s.asu.GetAbilitySphereRecipeByItemId(ctx, abilitySphereRecipe.BaseItemId)
		if err != nil {
			span.RecordError(err)
			return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		}
		if consumed := user.ConsumeGold(uint64(lowerSphereRecipe.GoldAmount * shortedAmount)); !consumed {
			err := errors.New("not enough gold")
			return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_GOLD_IS_SHORT, err)
		}
		if consumed := user.ConsumeItem(lowerSphereRecipe.SrcItemId.AsItem(), uint32(lowerSphereRecipe.SrcItemAmount*shortedAmount)); !consumed {
			err := errors.New("not enough item")
			return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_ITEM_IS_SHORT, err)
		}
		user.AddItem(lowerSphereRecipe.DstItemId.AsItem(), shortedAmount)
		if consumed := user.ConsumeItem(abilitySphereRecipe.SrcItemId.AsItem(), uint32(abilitySphereRecipe.SrcItemAmount)); !consumed {
			err := errors.New("not enough item")
			return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_ITEM_IS_SHORT, err)
		}
	}

	// Replace
	if err := abilityBoard.SetEquipItem(slotIndex, abilitySphereRecipe.DstItemId); err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	// Save
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{ItemSummary: true})
	if err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	if _, err := s.uau.UpdateAbilityBoard(ctx, internalUserId, *abilityBoard); err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	managedAbilityBoards, err := s.uau.GetAbilityBoards(ctx, internalUserId)
	if err != nil {
		span.RecordError(err)
		return nil, nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return user, user.ItemSummary, managedAbilityBoards, nil
}

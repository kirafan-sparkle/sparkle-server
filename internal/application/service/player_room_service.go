package service

import (
	"context"

	schema_room "gitlab.com/kirafan/sparkle/server/internal/application/schemas/room"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_room "gitlab.com/kirafan/sparkle/server/internal/domain/value/room"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type PlayerRoomService interface {
	SetPlayerRoom(
		ctx context.Context,
		internalUserId value_user.UserId,
		params schema_room.SetPlayerRoomSchema,
	) error
	GetAllPlayerRoom(
		ctx context.Context,
		internalUserId value_user.UserId,
		floorId value_room.FloorId,
	) ([]model_user.ManagedRoom, error)
}

type playerRoomService struct {
	uu usecase.UserUsecase
}

func NewPlayerRoomService(
	uu usecase.UserUsecase,
) PlayerRoomService {
	return &playerRoomService{uu}
}

func (s *playerRoomService) SetPlayerRoom(
	ctx context.Context,
	internalUserId value_user.UserId,
	params schema_room.SetPlayerRoomSchema,
) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SetPlayerRoom")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{ManagedRooms: true, ManagedRoomObjects: true})
	if err != nil {
		return err
	}

	// NOTE: Group id and floorId are ignored since they can not change with this API
	user.UpdateRoom(params.ManagedRoomId, params.ArrangeData)

	// Update user
	if _, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{ManagedRooms: true}); err != nil {
		return err
	}
	return nil
}

func (s *playerRoomService) GetAllPlayerRoom(
	ctx context.Context,
	internalUserId value_user.UserId,
	floorId value_room.FloorId,
) ([]model_user.ManagedRoom, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetAllPlayerRoom")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{ManagedRooms: true})
	if err != nil {
		return nil, err
	}
	specifiedRoom, err := user.GetManagedRoomByFloorId(floorId)
	if err != nil {
		return nil, err
	}
	return []model_user.ManagedRoom{*specifiedRoom}, nil
}

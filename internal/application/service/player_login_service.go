package service

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type PlayerLoginService interface {
	Login(ctx context.Context, deviceUUId string, sessionUUId string) (string, *errwrap.SparkleError)
}

type playerLoginService struct {
	uu usecase.UserUsecase
	su usecase.ScheduleUsecase
}

func NewPlayerLoginService(
	uu usecase.UserUsecase,
	su usecase.ScheduleUsecase,
) PlayerLoginService {
	return &playerLoginService{uu, su}
}

var ErrorAccountNotFound = errors.New("account not found")

// FIXME: This method is TOO UN-EFFECTIVE
func (s *playerLoginService) Login(ctx context.Context, deviceUUId string, sessionUUId string) (string, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "Login")
	defer span.End()
	user, err := s.uu.GetUserBySession(ctx, deviceUUId, sessionUUId)
	if err != nil {
		return "", errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_NOT_FOUND, ErrorAccountNotFound)
	}
	user, err = s.uu.GetUserByInternalId(ctx, user.Id, repository.UserRepositoryParam{
		LoginBonuses:             true,
		ManagedFieldPartyMembers: true,
		ManagedTowns:             true,
		ItemSummary:              true,
	})
	if err != nil {
		return "", errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	// Check last schedule refresh date
	lastLoginDay := user.LastLoginAt.Day()
	today := time.Now().Day()
	// Keep current schedule if the user logged in today
	if lastLoginDay == today {
		user.LastLoginBonus = nil
		if _, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{}); err != nil {
			return "", errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		}
		return user.Session, nil
	}

	// Receive login bonus (real process)
	received := user.ReceiveLoginBonuses()
	// Convert  to json strings for use at /login_bonus/get
	receivedJsonBytes, err := json.Marshal(received)
	if err != nil {
		serr := errwrap.NewSparkleError(response.RESULT_UNKNOWN_ERROR, "invalid login bonus data")
		serr.SetResponseMessage("invalid login bonus data")
		return "", serr
	}
	receivedJsonBytesString := string(receivedJsonBytes)
	user.LastLoginBonus = &receivedJsonBytesString

	// Recreate user schedule
	var characterIds []value_character.CharacterId
	for _, m := range user.ManagedFieldPartyMembers {
		characterId, err := value_character.NewCharacterId(uint32(m.CharacterId))
		if err != nil {
			serr := errwrap.NewSparkleError(response.RESULT_UNKNOWN_ERROR, "invalid character for town schedule")
			serr.SetResponseMessage("invalid character for town schedule")
			return "", serr
		}
		characterIds = append(characterIds, characterId)
	}
	if len(user.ManagedTowns) == 0 {
		serr := errwrap.NewSparkleError(response.RESULT_UNKNOWN_ERROR, "town data is missing or corrupt")
		serr.SetResponseMessage("town data is missing or corrupt")
		return "", serr
	}

	gridData := user.ManagedTowns[0].GridData
	schedules, err := s.su.CreateSchedule(ctx, characterIds, gridData)
	if err != nil {
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		serr.SetResponseMessage("failed to create schedule")
		return "", serr
	}

	// Update field party schedules
	for i, schedule := range schedules {
		user.ManagedFieldPartyMembers[i].ScheduleTable = &schedule.ScheduleTable
		// TODO: Save the item drop (it will be used at /reap_drop)
		user.ManagedFieldPartyMembers[i].PartyDropPresents = nil
	}
	if _, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ManagedFieldPartyMembers: true,
		LoginBonuses:             true,
		ItemSummary:              true,
	}); err != nil {
		return "", errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return user.Session, nil
}

package service

import (
	"context"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"github.com/google/uuid"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_mission "gitlab.com/kirafan/sparkle/server/internal/domain/value/mission"
	value_present "gitlab.com/kirafan/sparkle/server/internal/domain/value/present"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/migrate"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/seed"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

var createUserInstance = func(presents []model_user.UserPresent) *model_user.User {
	user := model_user.NewUser(uuid.New().String(), "test")
	user.Presents = presents
	user.PresentCount = uint8(len(presents))
	return &user
}

var createUserPresent = func(managedPresentId uint, presentType value_present.PresentType, objectId int64, amount int64) model_user.UserPresent {
	return model_user.UserPresent{
		ManagedPresentId: managedPresentId,
		PresentId:        26,
		CreatedAt:        time.Now(),
		DeadlineAt:       time.Now().AddDate(0, 0, int(value_mission.EXPIRE_TUTORIAL_PRESENT)),
		ReceivedAt:       nil,
		Type:             presentType,
		ObjectId:         objectId,
		Amount:           amount,
	}
}

func Test_playerPresentService_ReceivePresents(t *testing.T) {
	logger := observability.InitLogger(observability.LogTypeDebug)
	dbConf := database.GetConfig()
	db := database.InitDatabase(dbConf, logger)
	migrate.AutoMigrate(db)
	seed.AutoSeed(db)

	ur := persistence.NewUserRepositoryImpl(db)
	s := InitializePlayerPresentService(db)
	ctx := context.Background()

	ignoreFields := []string{
		"CreatedAt",
		"UpdatedAt",
		"UUId",
		"Session",
		"ContinuousDays",
		"FacilityLimit",
		"FriendLimit",
		"IpAddr",
		"KiraraLimit",
		"LastLoginAt",
		"Level",
		"MyCode",
		"Name",
		"PartyCost",
		"RecastTimeMax",
		"RoomObjectLimit",
		"Stamina",
		"StaminaMax",
		"StaminaUpdatedAt",
		"State",
		"SupportLimit",
		"UserAgent",
		"WeaponLimit",
		"SupportCharacters",
		"ManagedBattleParties",
		"ManagedFacilities",
		"ManagedFieldPartyMembers",
		"ManagedRoomObjects",
		"ManagedRooms",
		"ManagedTowns",
		"ManagedMasterOrbs",
		"FavoriteMembers",
		"OfferTitleTypes",
		"AdvIds",
		"TipIds",
		"FlagUi",
		"FlagPush",
		"FlagStamina",
		"IsCloseInfo",
		"PresentCount",
		"StepCode",
		"OrderReceiveId",
		"LastMemberAdded",
		"LastOpenedPart1ChapterId",
		"LastOpenedPart2ChapterId",
		"LastPlayedPart1ChapterQuestId",
		"LastPlayedPart2ChapterQuestId",
		"Missions",
		"Presents",
	}
	opts := []cmp.Option{
		cmpopts.IgnoreFields(model_user.User{}, ignoreFields...),
		cmpopts.IgnoreFields(model_user.ItemSummary{}, "CreatedAt", "UpdatedAt"),
	}

	tests := []struct {
		name              string
		user              *model_user.User
		managedPresentIds []int64
		wantUser          *model_user.User
		wantReceivedLen   int
		wantErr           error
	}{
		{
			name: "Receive Star5 character 7 times success",
			user: createUserInstance(
				[]model_user.UserPresent{
					createUserPresent(1, value_present.PresentTypeCharacter, 30012000, 1),
					createUserPresent(2, value_present.PresentTypeCharacter, 30012000, 1),
					createUserPresent(3, value_present.PresentTypeCharacter, 30012000, 1),
					createUserPresent(4, value_present.PresentTypeCharacter, 30012000, 1),
					createUserPresent(5, value_present.PresentTypeCharacter, 30012000, 1),
					createUserPresent(6, value_present.PresentTypeCharacter, 30012000, 1),
					createUserPresent(7, value_present.PresentTypeCharacter, 30012000, 1),
				},
			),
			managedPresentIds: []int64{1, 2, 3, 4, 5, 6, 7},
			wantUser: &model_user.User{
				Id:             2,
				ItemSummary:    []model_user.ItemSummary{},
				ManagedWeapons: []model_user.ManagedWeapon{},
				ManagedCharacters: []model_user.ManagedCharacter{{
					ManagedCharacterId: 1,
					PlayerId:           2,
					Level:              1,
					LevelLimit:         50,
					Exp:                0,
					LevelBreak:         0,
					DuplicatedCount:    6,
					ArousalLevel:       5,
					SkillLevel1:        1,
					SkillLevelLimit1:   15,
					SkillExp1:          0,
					SkillLevel2:        1,
					SkillLevelLimit2:   5,
					SkillExp2:          0,
					SkillLevel3:        1,
					SkillLevelLimit3:   5,
					SkillExp3:          0,
					Shown:              0,
					CharacterId:        30012000,
					ViewCharacterId:    30012000,
				}},
				ManagedNamedTypes: []model_user.ManagedNamedType{{
					ManagedNamedTypeId:   1,
					UserId:               2,
					NamedType:            117,
					Level:                1,
					Exp:                  0,
					TitleType:            value_character.TitleTypeGochiusa,
					FriendshipExpTableId: 0,
				}},
			},
			wantReceivedLen: 7,
			wantErr:         nil,
		},
		{
			name: "Receive tutorial items success",
			user: createUserInstance(
				[]model_user.UserPresent{
					createUserPresent(8, value_present.PresentTypeItem, 2017, 5),
					createUserPresent(9, value_present.PresentTypeItem, 2016, 5),
					createUserPresent(10, value_present.PresentTypeItem, 2015, 5),
					createUserPresent(11, value_present.PresentTypeItem, 10007, 1),
					createUserPresent(12, value_present.PresentTypeCharacter, 15001010, 1),
					createUserPresent(13, value_present.PresentTypeCharacter, 30012000, 1),
					createUserPresent(14, value_present.PresentTypeGold, -1, 100000),
					createUserPresent(15, value_present.PresentTypeLimitedGem, -1, 1200),
				},
			),
			managedPresentIds: []int64{8, 9, 10, 11, 12, 13, 14, 15},
			wantUser: &model_user.User{
				Id:         3,
				Gold:       100000,
				LimitedGem: 1200,
				ItemSummary: []model_user.ItemSummary{
					{ItemSummaryId: 1, Id: 2017, Amount: 5, UserId: 3},
					{ItemSummaryId: 2, Id: 2016, Amount: 5, UserId: 3},
					{ItemSummaryId: 3, Id: 2015, Amount: 5, UserId: 3},
					{ItemSummaryId: 4, Id: 10007, Amount: 1, UserId: 3},
				},
				ManagedWeapons: []model_user.ManagedWeapon{},
				ManagedCharacters: []model_user.ManagedCharacter{
					{
						ManagedCharacterId: 2,
						PlayerId:           3,
						Level:              1,
						LevelLimit:         40,
						Exp:                0,
						LevelBreak:         0,
						DuplicatedCount:    0,
						ArousalLevel:       0,
						SkillLevel1:        1,
						SkillLevelLimit1:   5,
						SkillExp1:          0,
						SkillLevel2:        1,
						SkillLevelLimit2:   5,
						SkillExp2:          0,
						SkillLevel3:        1,
						SkillLevelLimit3:   5,
						SkillExp3:          0,
						Shown:              0,
						CharacterId:        15001010,
						ViewCharacterId:    15001010,
					},
					{
						ManagedCharacterId: 3,
						PlayerId:           3,
						Level:              1,
						LevelLimit:         50,
						Exp:                0,
						LevelBreak:         0,
						DuplicatedCount:    0,
						ArousalLevel:       0,
						SkillLevel1:        1,
						SkillLevelLimit1:   5,
						SkillExp1:          0,
						SkillLevel2:        1,
						SkillLevelLimit2:   5,
						SkillExp2:          0,
						SkillLevel3:        1,
						SkillLevelLimit3:   5,
						SkillExp3:          0,
						Shown:              0,
						CharacterId:        30012000,
						ViewCharacterId:    30012000,
					},
				},
				ManagedNamedTypes: []model_user.ManagedNamedType{
					{
						ManagedNamedTypeId:   2,
						UserId:               3,
						NamedType:            36,
						Level:                1,
						Exp:                  0,
						TitleType:            value_character.TitleTypeNewGame,
						FriendshipExpTableId: 0,
					},
					{
						ManagedNamedTypeId:   3,
						UserId:               3,
						NamedType:            117,
						Level:                1,
						Exp:                  0,
						TitleType:            value_character.TitleTypeGochiusa,
						FriendshipExpTableId: 0,
					},
				},
			},
			wantReceivedLen: 8,
			wantErr:         nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u, err := ur.CreateUser(ctx, tt.user)
			if err != nil {
				t.Errorf("userRepo.CreateUser error = %v, wantErr nil", err)
			}
			gotUser, gotReceived, err := s.ReceivePresents(context.Background(), u.Id, tt.managedPresentIds, -1)
			if err != tt.wantErr {
				t.Errorf("userPresentService.ReceivePresents() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if cmp.Equal(*gotUser, *tt.wantUser, opts...) != true {
				t.Errorf("userPresentService.ReceivePresents() Diff = %+v", cmp.Diff(tt.wantUser, gotUser, opts...))
				return
			}
			if len(gotReceived) != tt.wantReceivedLen {
				t.Errorf("userPresentService.ReceivePresents() ReceivedLen = %v, want %v", len(gotReceived), tt.wantReceivedLen)
				return
			}
		})
	}
}

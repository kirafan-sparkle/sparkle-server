package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type PlayerAgeService interface {
	SetPlayerAge(ctx context.Context, internalUserId value_user.UserId, age value_user.Age) error
}

type playerAgeService struct {
	uu usecase.UserUsecase
}

func NewPlayerAgeService(uu usecase.UserUsecase) PlayerAgeService {
	return &playerAgeService{uu}
}

func (s *playerAgeService) SetPlayerAge(ctx context.Context, internalUserId value_user.UserId, age value_user.Age) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SetPlayerAge")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		return err
	}
	user.Age = age
	if _, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{}); err != nil {
		return err
	}
	return nil
}

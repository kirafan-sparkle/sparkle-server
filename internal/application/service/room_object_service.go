package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_room_object "gitlab.com/kirafan/sparkle/server/internal/domain/model/room_object"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type RoomObjectService interface {
	GetAllRoomObject(ctx context.Context) ([]*model_room_object.RoomObject, error)
}

type roomObjectService struct {
	ru usecase.RoomObjectUsecase
}

func NewRoomObjectService(ru usecase.RoomObjectUsecase) RoomObjectService {
	return &roomObjectService{ru: ru}
}

func (s *roomObjectService) GetAllRoomObject(ctx context.Context) ([]*model_room_object.RoomObject, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetAllRoomObject")
	defer span.End()
	roomObjects, err := s.ru.GetAll(ctx)
	return roomObjects, err
}

package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type PlayerFieldPartyService interface {
	GetAllPlayerFieldPartyMember(ctx context.Context, internalUserId value_user.UserId) ([]model_user.ManagedFieldPartyMember, error)
	RemoveAllPlayerFieldPartyMember(ctx context.Context, internalUserId value_user.UserId, managedPartyMemberIds []int64) error
}

func NewPlayerFieldPartyService(
	uu usecase.UserUsecase,
) PlayerFieldPartyService {
	return &playerFieldPartyService{uu}
}

type playerFieldPartyService struct {
	uu usecase.UserUsecase
}

func (s *playerFieldPartyService) GetAllPlayerFieldPartyMember(ctx context.Context, internalUserId value_user.UserId) ([]model_user.ManagedFieldPartyMember, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetAllPlayerFieldPartyMember")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{ManagedFieldPartyMembers: true})
	if err != nil {
		return nil, err
	}
	return user.ManagedFieldPartyMembers, err
}

func (s *playerFieldPartyService) RemoveAllPlayerFieldPartyMember(ctx context.Context, internalUserId value_user.UserId, managedPartyMemberIds []int64) error {
	_, span := observability.Tracer.StartAppServiceSpan(ctx, "RemoveAllPlayerFieldPartyMember")
	defer span.End()

	// NOTE: Maybe original server validate reset time and remove target at this endpoint...?
	// but actually we don't need the feature so return nil here.

	return nil
}

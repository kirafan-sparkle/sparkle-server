package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type PlayerMoveService interface {
	SetPlayerAccountTransfer(ctx context.Context, userId value_user.UserId, password string) (*model_user.User, string, *errwrap.SparkleError)
	UsePlayerAccountTransfer(ctx context.Context, moveCode string, movePassword string, uuid string, platform value_version.Platform) (*model_user.User, *errwrap.SparkleError)
}

type playerMoveService struct {
	uu usecase.UserUsecase
}

func NewPlayerMoveService(uu usecase.UserUsecase) PlayerMoveService {
	return &playerMoveService{uu}
}

func (s *playerMoveService) SetPlayerAccountTransfer(ctx context.Context, internalId value_user.UserId, password string) (*model_user.User, string, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SetPlayerAccountTransfer")
	defer span.End()

	user, err := s.uu.GetUserByInternalId(ctx, internalId, repository.UserRepositoryParam{})
	if err != nil {
		span.RecordError(err)
		return nil, "", errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_SESSION_EXPIRED, err)
	}

	newCode, err := user.UpdateMoveCode(password)
	if err != nil {
		span.RecordError(err)
		return nil, "", errwrap.NewSparkleErrorWithCustomError(response.RESULT_UNKNOWN_ERROR, err)
	}

	if _, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{}); err != nil {
		return nil, "", errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return user, newCode, nil
}

func (s *playerMoveService) UsePlayerAccountTransfer(ctx context.Context, moveCode string, movePassword string, uuid string, platform value_version.Platform) (*model_user.User, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "UsePlayerAccountTransfer")
	defer span.End()

	user, err := s.uu.GetUserByMoveCode(ctx, moveCode)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_MOVE_PARAMETERS, err)
	}
	if !user.IsMovePasswordValid(movePassword) {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_MOVE_PARAMETERS, err)
	}

	// NOTE: 3 is unused platform, it is for website login
	if platform != 3 {
		user.UUId = uuid
	}

	if _, err := user.UpdateMoveCode(calc.GetSparkleRandomString()); err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_UNKNOWN_ERROR, err)
	}
	if _, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{}); err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return user, nil
}

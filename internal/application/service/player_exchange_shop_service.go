package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_exchange_shop "gitlab.com/kirafan/sparkle/server/internal/domain/model/exchange_shop"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type PlayerExchangeShopService interface {
	ShownPlayerExchangeShop(ctx context.Context, internalUserId value_user.UserId, ExchangeShopIds []int64) ([]model_exchange_shop.ExchangeShop, error)
}

type playerExchangeShopService struct {
	uu usecase.UserUsecase
}

func NewPlayerExchangeShopService(uu usecase.UserUsecase) PlayerExchangeShopService {
	return &playerExchangeShopService{uu}
}

func (s *playerExchangeShopService) ShownPlayerExchangeShop(ctx context.Context, internalUserId value_user.UserId, ExchangeShopIds []int64) ([]model_exchange_shop.ExchangeShop, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "ShownPlayerExchangeShop")
	defer span.End()
	// TODO: This endpoint should change `/api/player/store/get_all` endpoint's exchangeShops field
	// STUB
	resp := []model_exchange_shop.ExchangeShop{}
	return resp, nil
}

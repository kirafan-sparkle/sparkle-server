package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type PlayerPushTokenService interface {
	SetPlayerPushToken(ctx context.Context, internalUserId value_user.UserId, token string) error
}

type playerPushTokenService struct {
	uu usecase.UserUsecase
}

func NewPlayerPushTokenService(uu usecase.UserUsecase) PlayerPushTokenService {
	return &playerPushTokenService{uu}
}

func (s *playerPushTokenService) SetPlayerPushToken(ctx context.Context, internalUserId value_user.UserId, token string) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SetPlayerPushToken")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		return err
	}

	user.PushToken = token

	if _, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{}); err != nil {
		return err
	}
	return nil
}

package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_event_banner "gitlab.com/kirafan/sparkle/server/internal/domain/model/event_banner"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type EventBannerService interface {
	GetAllEventBanner(ctx context.Context) ([]*model_event_banner.EventBanner, *errwrap.SparkleError)
}

type eventBannerService struct {
	uc usecase.EventBannerUsecase
}

func NewEventBannerService(uc usecase.EventBannerUsecase) EventBannerService {
	return &eventBannerService{uc}
}

func (s *eventBannerService) GetAllEventBanner(ctx context.Context) ([]*model_event_banner.EventBanner, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetAllEventBanner")
	defer span.End()
	resp, err := s.uc.GetAllEventBanners(ctx)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return resp, nil
}

package service

import (
	"context"
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_trade "gitlab.com/kirafan/sparkle/server/internal/domain/model/trade"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_present "gitlab.com/kirafan/sparkle/server/internal/domain/value/present"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx/logger"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
	"go.uber.org/zap"
)

type PlayerItemService interface {
	GetAllPlayerItemTradeRecipe(ctx context.Context, internalUserId value_user.UserId) ([]model_user.UserTradeRecipe, *errwrap.SparkleError)
	TradePlayerItem(ctx context.Context, internalUserId value_user.UserId, recipeId uint, tradeMethod uint8, count uint16) (*model_user.User, uint16, uint16, *errwrap.SparkleError)
	SalePlayerItem(ctx context.Context, internalUserId value_user.UserId, itemId value_item.ItemId, count uint16) (*model_user.User, *errwrap.SparkleError)
}

type playerItemService struct {
	uu usecase.UserUsecase
	iu usecase.ItemUsecase
	cu usecase.CharacterUsecase
	nu usecase.NamedTypeUsecase
	tu usecase.TradeRecipeUsecase
	pu usecase.PackageItemUsecase
}

func NewPlayerItemService(
	uu usecase.UserUsecase,
	iu usecase.ItemUsecase,
	cu usecase.CharacterUsecase,
	nu usecase.NamedTypeUsecase,
	tu usecase.TradeRecipeUsecase,
	pu usecase.PackageItemUsecase,
) PlayerItemService {
	return &playerItemService{uu, iu, cu, nu, tu, pu}
}

func (s *playerItemService) GetAllPlayerItemTradeRecipe(ctx context.Context, internalUserId value_user.UserId) ([]model_user.UserTradeRecipe, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetAllPlayerItemTradeRecipe")
	defer span.End()

	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		Trades: true,
	})
	if err != nil {
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		span.RecordError(serr)
		return []model_user.UserTradeRecipe{}, serr
	}
	tradeRecipes, err := s.tu.GetTradeRecipes(ctx)
	if err != nil {
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		span.RecordError(err)
		return []model_user.UserTradeRecipe{}, serr
	}

	// Delete expired recipes and reset monthly recipes
	user.RefreshTradeRecipes(time.Now())

	// Add new trade recipes to user
	now := time.Now()
	for i := range tradeRecipes {
		if available := tradeRecipes[i].IsAvailable(now); !available {
			continue
		}
		// NOTE: Ignore duplication error
		user.AddTradeRecipe(tradeRecipes[i].Id)
	}

	// Update user
	_, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		Trades: true,
	})
	if err != nil {
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		span.RecordError(serr)
		return []model_user.UserTradeRecipe{}, serr
	}

	// Re-get all trade recipes since relations are dead
	user, err = s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		Trades: true,
	})
	if err != nil {
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		span.RecordError(serr)
		return []model_user.UserTradeRecipe{}, serr
	}

	return user.Trades, nil
}

// Validate the trade amount is not over the trade limit
func (s *playerItemService) validateTradeAmount(ctx context.Context, user *model_user.User, userTrade *model_user.UserTradeRecipe, tradeAmount uint16) *errwrap.SparkleError {
	_, span := observability.Tracer.StartAppServiceSpan(ctx, "validateTradeAmount")
	defer span.End()

	// Validate max trade amount per a request
	maxTradeAmountPerTrade := userTrade.Recipe.TradeMax
	if tradeAmount > uint16(maxTradeAmountPerTrade) {
		err := errwrap.NewSparkleError(response.RESULT_INVALID_PARAMETERS, "trade amount is over trade max value")
		span.RecordError(err)
		return err
	}
	// Validate max trade amount per a month or user
	maxTradeAmountPerUserOrMonth := userTrade.Recipe.LimitCount
	isUnlimitedTrade := maxTradeAmountPerUserOrMonth == -1
	isMonthlyTrade := userTrade.Recipe.ResetFlag == value.BoolLikeUIntTrue
	isMonthlyTradeCountOver := isMonthlyTrade && userTrade.MonthlyTradeCount+tradeAmount > uint16(maxTradeAmountPerUserOrMonth)
	isTotalTradeCountOver := !isMonthlyTrade && userTrade.TotalTradeCount+tradeAmount > uint16(maxTradeAmountPerUserOrMonth)
	if !isUnlimitedTrade && (isTotalTradeCountOver || isMonthlyTradeCountOver) {
		err := errwrap.NewSparkleError(response.RESULT_INVALID_PARAMETERS, "trade count is over trade limit count value")
		span.RecordError(err)
		return err
	}
	return nil
}

// Consume trade source objects from user
func (s *playerItemService) consumeSources(ctx context.Context, user *model_user.User, recipe model_trade.TradeRecipe, tradeMethod uint8, tradeAmount uint16) *errwrap.SparkleError {
	_, span := observability.Tracer.StartAppServiceSpan(ctx, "consumeSources")
	defer span.End()

	srcType := recipe.SrcType1
	srcId := recipe.SrcId1
	srcAmount := recipe.SrcAmount1
	if tradeMethod == 1 {
		srcType = recipe.SrcType2
		srcId = recipe.SrcId2
		srcAmount = recipe.SrcAmount2
	}

	logger := logger.FromContext(ctx)
	logger.Ctx(ctx).Debug(
		"trade source",
		zap.Uint8("SrcType", uint8(srcType)),
		zap.Int32("SrcId", int32(srcId)),
		zap.Int32("SrcAmount", srcAmount),
	)

	switch srcType {
	case value_present.PresentTypeItem:
		srcItemId, err := value_item.NewItemId(srcId)
		if err != nil {
			span.RecordError(err)
			return errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		}
		if consumed := user.ConsumeItem(srcItemId, uint32(srcAmount)*uint32(tradeAmount)); !consumed {
			err := errwrap.NewSparkleError(response.RESULT_ITEM_IS_SHORT, "item is not enough")
			span.RecordError(err)
			return err
		}
	case value_present.PresentTypeKiraraPoint:
		if consumed := user.ConsumeKirara(uint32(srcAmount) * uint32(tradeAmount)); !consumed {
			err := errwrap.NewSparkleError(response.RESULT_KIRARA_IS_SHORT, "kirara point is not enough")
			span.RecordError(err)
			return err
		}
	case value_present.PresentTypeGold:
		if consumed := user.ConsumeGold(uint64(srcAmount) * uint64(tradeAmount)); !consumed {
			err := errwrap.NewSparkleError(response.RESULT_GOLD_IS_SHORT, "gold is not enough")
			span.RecordError(err)
			return err
		}
	case value_present.PresentTypeLimitedGem:
		if consumed := user.ConsumeGem(uint32(srcAmount) * uint32(tradeAmount)); !consumed {
			err := errwrap.NewSparkleError(response.RESULT_LIMITED_GEM_IS_SHORT, "gem is not enough")
			span.RecordError(err)
			return err
		}
	case value_present.PresentTypeUnlimitedGem:
		if consumed := user.ConsumeUnlimitedGem(uint32(srcAmount) * uint32(tradeAmount)); !consumed {
			err := errwrap.NewSparkleError(response.RESULT_UNLIMITED_GEM_IS_SHORT, "unlimited gem is not enough")
			span.RecordError(err)
			return err
		}
	// Below types are probably not supported at clients (the displays are broken)
	case value_present.PresentTypeNone, value_present.PresentTypeCharacter:
		fallthrough
	case value_present.PresentTypeWeapon, value_present.PresentTypeTownFacility:
		fallthrough
	case value_present.PresentTypeRoomObject, value_present.PresentTypeMasterOrb:
		fallthrough
	case value_present.PresentTypePackageItem, value_present.PresentTypeAllowRoomObject:
		fallthrough
	case value_present.PresentTypeMasterOrbLvUp, value_present.PresentTypeAchievement:
		err := errwrap.NewSparkleError(response.RESULT_DB_ERROR, "invalid source type specified at db record")
		span.RecordError(err)
		return err
	}
	return nil
}

// Add new character to user
func (s *playerItemService) obtainCharacter(ctx context.Context, user *model_user.User, recipe model_trade.TradeRecipe, tradeAmount uint16) *errwrap.SparkleError {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "obtainCharacter")
	defer span.End()

	characterId, err := value_character.NewCharacterId(uint32(recipe.DstId))
	if err != nil {
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		span.RecordError(serr)
		return serr
	}
	character, err := s.cu.GetCharacterById(ctx, characterId)
	if err != nil {
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		span.RecordError(serr)
		return serr
	}
	namedType, err := s.nu.GetNamedTypeById(ctx, uint(character.NamedType))
	if err != nil {
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		span.RecordError(serr)
		return serr
	}
	// NOTE: Ignore the duplicated error
	user.AddNamedType(character.NamedType, namedType.TitleType)
	// TODO: Replace this process with send presents to user
	for i := 0; i < int(recipe.DstAmount*int32(tradeAmount)); i++ {
		duplicated := user.AddCharacter(characterId)
		if duplicated {
			// Add alternative item
			if recipe.AltType != value_present.PresentTypeItem {
				err := errwrap.NewSparkleError(response.RESULT_DB_ERROR, "invalid alt type specified at db record")
				span.RecordError(err)
				return err
			}
			user.AddItem(recipe.AltId, uint32(recipe.AltAmount))
		}
	}
	return nil
}

// Add trade destination objects to user
func (s *playerItemService) obtainDestinations(ctx context.Context, user *model_user.User, recipe model_trade.TradeRecipe, tradeAmount uint16) *errwrap.SparkleError {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "obtainDestinations")
	defer span.End()

	dstType := recipe.DstType
	dstId := recipe.DstId
	dstAmount := recipe.DstAmount

	logger := logger.FromContext(ctx)
	logger.Ctx(ctx).Debug(
		"trade destination",
		zap.Uint8("dstType", uint8(dstType)),
		zap.Int32("dstId", int32(dstId)),
		zap.Int32("dstAmount", dstAmount),
	)

	switch dstType {
	case value_present.PresentTypeCharacter:
		if err := s.obtainCharacter(ctx, user, recipe, tradeAmount); err != nil {
			return err
		}
	case value_present.PresentTypeItem:
		dstItemId, err := value_item.NewItemId(dstId)
		if err != nil {
			span.RecordError(err)
			return errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		}
		user.AddItem(dstItemId, uint32(dstAmount)*uint32(tradeAmount))
	case value_present.PresentTypeKiraraPoint:
		// TODO: Send overflowed kirara to user's present box (?) or ignore it
		_, err := user.AddKirara(uint32(dstAmount) * uint32(tradeAmount))
		if err != nil {
			serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_KIRARA_LIMIT, err)
			return serr
		}
	case value_present.PresentTypeGold:
		user.AddGold(uint64(dstAmount) * uint64(tradeAmount))
	case value_present.PresentTypeWeapon:
		weaponId := value_weapon.NewWeaponId(uint32(dstId))
		for i := 0; i < int(dstAmount*int32(tradeAmount)); i++ {
			if err := user.AddWeapon(weaponId, false); err != nil {
				serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_WEAPON_LIMIT, err)
				span.RecordError(serr)
				return serr
			}
		}
	case value_present.PresentTypePackageItem:
		packageItem, err := s.pu.GetPackageItemById(ctx, uint(dstId))
		if err != nil {
			serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
			span.RecordError(serr)
			return serr
		}
		for i := 0; i < int(dstAmount*int32(tradeAmount)); i++ {
			for _, item := range packageItem.Contents {
				switch item.ItemType {
				case value_present.PresentTypeGold:
					user.AddGold(uint64(item.ItemAmount))
				case value_present.PresentTypeItem:
					user.AddItem(item.ItemId, uint32(item.ItemAmount))
				default:
					serr := errwrap.NewSparkleError(response.RESULT_DB_ERROR, "invalid package item content type specified at db record")
					span.RecordError(serr)
					return serr
				}
			}
		}
	// Below types are probably not supported at clients (the displays are broken)
	case value_present.PresentTypeNone:
		fallthrough
	case value_present.PresentTypeTownFacility:
		fallthrough
	case value_present.PresentTypeRoomObject, value_present.PresentTypeMasterOrb:
		fallthrough
	case value_present.PresentTypeUnlimitedGem, value_present.PresentTypeLimitedGem:
		fallthrough
	case value_present.PresentTypeAllowRoomObject:
		fallthrough
	case value_present.PresentTypeMasterOrbLvUp, value_present.PresentTypeAchievement:
		err := errwrap.NewSparkleError(response.RESULT_DB_ERROR, "invalid destination type specified at db record")
		span.RecordError(err)
		return err
	}
	return nil
}

func (s *playerItemService) TradePlayerItem(ctx context.Context, internalUserId value_user.UserId, recipeId uint, tradeMethod uint8, tradeAmount uint16) (*model_user.User, uint16, uint16, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "TradePlayerItem")
	defer span.End()

	// Get required infos to process
	// TODO: Refactor this get user process, probably it can reduce some select queries
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		Trades:            true,
		ItemSummary:       true,
		ManagedCharacters: true,
		ManagedNamedTypes: true,
		ManagedWeapons:    true,
	})
	if err != nil {
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		span.RecordError(serr)
		return nil, 0, 0, serr
	}
	userTrade := user.GetUserTradeByRecipeId(recipeId)
	if userTrade == nil {
		serr := errwrap.NewSparkleError(response.RESULT_TRADE_OUT_OF_PERIOD, "user trade recipe not found")
		span.RecordError(serr)
		return nil, 0, 0, serr
	}
	if serr := s.validateTradeAmount(ctx, user, userTrade, tradeAmount); serr != nil {
		serr.WrapError("failed at validateTradeAmount")
		return nil, 0, 0, serr
	}
	if err := user.IncreaseUserTradeCounts(recipeId, tradeAmount); err != nil {
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_SESSION_EXPIRED, err)
		span.RecordError(serr)
		return nil, 0, 0, serr
	}

	if serr := s.consumeSources(ctx, user, userTrade.Recipe, tradeMethod, tradeAmount); serr != nil {
		serr.WrapError("failed at consume sources")
		return nil, 0, 0, serr
	}
	if serr := s.obtainDestinations(ctx, user, userTrade.Recipe, tradeAmount); serr != nil {
		serr.WrapError("failed at obtain destinations")
		return nil, 0, 0, serr
	}

	// Update user
	srcType := userTrade.Recipe.SrcType1
	if tradeMethod == 1 {
		srcType = userTrade.Recipe.SrcType2
	}
	dstType := userTrade.Recipe.DstType
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		Trades:            true,
		ItemSummary:       srcType == value_present.PresentTypeItem || dstType == value_present.PresentTypeItem || dstType == value_present.PresentTypePackageItem,
		ManagedCharacters: dstType == value_present.PresentTypeCharacter,
		ManagedNamedTypes: dstType == value_present.PresentTypeCharacter,
		ManagedWeapons:    dstType == value_present.PresentTypeWeapon,
	})
	if err != nil {
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		span.RecordError(serr)
		return nil, 0, 0, serr
	}
	return user, userTrade.MonthlyTradeCount + tradeAmount, userTrade.TotalTradeCount + tradeAmount, nil
}

func (s *playerItemService) SalePlayerItem(ctx context.Context, internalUserId value_user.UserId, itemId value_item.ItemId, count uint16) (*model_user.User, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SalePlayerItem")
	defer span.End()

	// Get required infos to process
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ItemSummary: true,
	})
	if err != nil {
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		span.RecordError(serr)
		return nil, serr
	}

	// Consume item from user
	if consumed := user.ConsumeItem(itemId, uint32(count)); !consumed {
		err := errwrap.NewSparkleError(response.RESULT_ITEM_IS_SHORT, "item is not enough")
		span.RecordError(err)
		return nil, err
	}

	// Add gold to user
	item, err := s.iu.GetItemById(ctx, itemId)
	if err != nil {
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		span.RecordError(serr)
		return nil, serr
	}
	user.AddGold(uint64(item.SaleAmount) * uint64(count))

	// Update user
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ItemSummary: true,
	})
	if err != nil {
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		span.RecordError(serr)
		return nil, serr
	}
	return user, nil
}

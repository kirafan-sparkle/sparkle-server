package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type PlayerTownService interface {
	GetPlayerTown(ctx context.Context, requesterPlayerId value_user.UserId, targetPlayerId value_user.UserId) (*model_user.User, *errwrap.SparkleError)
	// TODO: return managedTownId as value object
	SetPlayerTown(ctx context.Context, internalUserId value_user.UserId, managedTownId uint, gridData string) (uint, *errwrap.SparkleError)
}

type playerTownApiService struct {
	uu usecase.UserUsecase
	su usecase.ScheduleUsecase
}

func NewPlayerTownService(uu usecase.UserUsecase, su usecase.ScheduleUsecase) PlayerTownService {
	return &playerTownApiService{uu, su}
}

func (s *playerTownApiService) GetPlayerTown(ctx context.Context, requesterPlayerId value_user.UserId, targetPlayerId value_user.UserId) (*model_user.User, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "GetPlayerTown")
	defer span.End()

	user, err := s.uu.GetUserByInternalId(ctx, targetPlayerId, repository.UserRepositoryParam{ManagedTowns: true})
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return user, nil
}

func (s *playerTownApiService) initPlayerTown(ctx context.Context, internalUserId value_user.UserId, managedTownId uint, gridData string) (uint, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "initPlayerTown")
	defer span.End()

	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{ManagedFieldPartyMembers: true})
	if err != nil {
		span.RecordError(err)
		return 0, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	// Init town
	user.UpdateTown(gridData)

	// Generate user schedule
	characterIds := make([]value_character.CharacterId, len(user.ManagedFieldPartyMembers))
	for i, m := range user.ManagedFieldPartyMembers {
		characterId, err := value_character.NewCharacterId(uint32(m.CharacterId))
		if err != nil {
			span.RecordError(err)
			return 0, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		}
		characterIds[i] = characterId
	}

	// Update field party schedules
	schedules, err := s.su.CreateSchedule(ctx, characterIds, gridData)
	if err != nil {
		span.RecordError(err)
		return 0, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	for i, schedule := range schedules {
		user.ManagedFieldPartyMembers[i].ScheduleTable = &schedule.ScheduleTable
		// TODO: Save the item drop (it will be used at /reap_drop)
		user.ManagedFieldPartyMembers[i].PartyDropPresents = nil
	}

	if _, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{ManagedTowns: true, ManagedFieldPartyMembers: true}); err != nil {
		span.RecordError(err)
		return 0, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return user.ManagedTowns[0].ManagedTownId, nil
}

func (s *playerTownApiService) updatePlayerTown(ctx context.Context, internalUserId value_user.UserId, managedTownId uint, gridData string) (uint, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "updatePlayerTown")
	defer span.End()

	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{ManagedTowns: true})
	if err != nil {
		span.RecordError(err)
		return 0, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	user.UpdateTown(gridData)

	if _, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{ManagedTowns: true}); err != nil {
		span.RecordError(err)
		return 0, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return user.ManagedTowns[0].ManagedTownId, nil
}

func (s *playerTownApiService) SetPlayerTown(ctx context.Context, internalUserId value_user.UserId, managedTownId uint, gridData string) (uint, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "SetPlayerTown")
	defer span.End()

	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		span.RecordError(err)
		return 0, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	if user.StepCode != value_user.StepCodeAdvIntro3Done {
		return s.updatePlayerTown(ctx, internalUserId, managedTownId, gridData)
	}
	return s.initPlayerTown(ctx, internalUserId, managedTownId, gridData)
}

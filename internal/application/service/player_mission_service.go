package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type playerMissionService struct {
	uu usecase.UserUsecase
}

type PlayerMissionService interface {
	GetPlayerMissions(ctx context.Context, internalUserId value_user.UserId) ([]model_user.UserMission, *errwrap.SparkleError)
	SetPlayerMission(ctx context.Context, internalUserId value_user.UserId, managedMissionId uint, rate uint) *errwrap.SparkleError
	// TODO: define the value model
	CompletePlayerMission(ctx context.Context, internalUserId value_user.UserId, managedMissionId uint) (*model_user.User, *errwrap.SparkleError)
}

func NewPlayerMissionService(uu usecase.UserUsecase) PlayerMissionService {
	return &playerMissionService{uu}
}

func (s *playerMissionService) GetPlayerMissions(ctx context.Context, internalUserId value_user.UserId) ([]model_user.UserMission, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetPlayerMissions")
	defer span.End()

	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{Missions: true})
	if err != nil || user.Missions == nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return user.Missions, nil
}

func (s *playerMissionService) SetPlayerMission(ctx context.Context, internalUserId value_user.UserId, managedMissionId uint, rate uint) *errwrap.SparkleError {
	_, span := observability.Tracer.StartAppServiceSpan(ctx, "SetPlayerMission")
	defer span.End()

	// TODO: Implement me
	// NOTE: This one is called at the tutorials so just ignore the request and returns success
	return nil
}

func (s *playerMissionService) CompletePlayerMission(ctx context.Context, internalUserId value_user.UserId, managedMissionId uint) (*model_user.User, *errwrap.SparkleError) {
	_, span := observability.Tracer.StartAppServiceSpan(ctx, "CompletePlayerMission")
	defer span.End()

	return nil, errwrap.NewSparkleError(response.RESULT_UNAVAILABLE, "not yet implemented")
}

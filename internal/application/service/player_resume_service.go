package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type PlayerResumeService interface {
	RefreshPlayerStatus(ctx context.Context, internalUserId value_user.UserId) (*model_user.User, *errwrap.SparkleError)
}

type playerResumeService struct {
	uu usecase.UserUsecase
}

func NewPlayerResumeService(
	uu usecase.UserUsecase,
) PlayerResumeService {
	return &playerResumeService{uu}
}

func (s *playerResumeService) RefreshPlayerStatus(
	ctx context.Context,
	internalUserId value_user.UserId,
) (*model_user.User, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "RefreshPlayerStatus")
	defer span.End()

	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_NOT_FOUND, err)
	}

	user.RefreshStamina()

	// Update user
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{})
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return user, nil
}

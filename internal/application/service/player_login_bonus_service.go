package service

import (
	"context"
	"encoding/json"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type PlayerLoginBonusService interface {
	GetPlayerLoginBonus(ctx context.Context, internalUserId value_user.UserId) (*model_user.User, []model_user.UserLoginBonus, *errwrap.SparkleError)
}

type playerLoginBonusService struct {
	uu usecase.UserUsecase
}

func NewPlayerLoginBonusService(uu usecase.UserUsecase) PlayerLoginBonusService {
	return &playerLoginBonusService{uu}
}

func (s *playerLoginBonusService) GetPlayerLoginBonus(ctx context.Context, internalUserId value_user.UserId) (*model_user.User, []model_user.UserLoginBonus, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartInterfaceSpan(ctx, "GetPlayerLoginBonus")
	defer span.End()

	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{LoginBonuses: true})
	if err != nil {
		span.RecordError(err)
		return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	// Receive login bonus (it just reads /login endpoint result)
	loginBonuses := make([]model_user.UserLoginBonus, 0)
	if user.LastLoginBonus != nil {
		if err = json.Unmarshal([]byte(*user.LastLoginBonus), &loginBonuses); err != nil {
			return nil, nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		}
	}

	return user, loginBonuses, nil
}

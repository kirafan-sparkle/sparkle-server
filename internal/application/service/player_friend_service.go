package service

import (
	"context"
	"errors"
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_friend "gitlab.com/kirafan/sparkle/server/internal/domain/model/friend"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type PlayerFriendService interface {
	// Remove the friend relationship
	TerminatePlayerFriend(ctx context.Context, internalId value_friend.PlayerId, managedFriendId value_friend.ManagedFriendId) *errwrap.SparkleError
	// Cancel a friend request to a player
	CancelPlayerFriend(ctx context.Context, internalId value_friend.PlayerId, managedFriendId value_friend.ManagedFriendId) *errwrap.SparkleError
	// Add the friend relationship
	AcceptPlayerFriend(ctx context.Context, internalId value_friend.PlayerId, managedFriendId value_friend.ManagedFriendId) *errwrap.SparkleError
	// Deny a friend request from a player
	RefusePlayerFriend(ctx context.Context, internalId value_friend.PlayerId, managedFriendId value_friend.ManagedFriendId) *errwrap.SparkleError
	// Send a friend request to a player
	ProposePlayerFriend(ctx context.Context, internalId value_friend.PlayerId, targetPlayerId value_friend.PlayerId) (*value_friend.ManagedFriendId, *errwrap.SparkleError)
	// Find a player info by the player code
	SearchPlayerFriend(ctx context.Context, internalId value_friend.PlayerId, targetMyCode value_friend.FriendOrCheatCode) (*model_friend.FriendFull, *errwrap.SparkleError)
	// Get list of friends
	GetAllPlayerFriend(
		ctx context.Context,
		internalId value_friend.PlayerId,
		getType value_friend.FriendGetType,
		managedBattleParty uint,
		ignoreSupport value.BoolLikeUInt8,
		reload value.BoolLikeUInt8,
	) ([]model_friend.FriendFull, []model_friend.FriendFull, *errwrap.SparkleError)
}

func NewPlayerFriendService(
	uu usecase.UserUsecase,
	fu usecase.FriendUsecase,
	cs CheatService,
) PlayerFriendService {
	return &playerFriendService{uu, fu, cs}
}

type playerFriendService struct {
	uu usecase.UserUsecase
	fu usecase.FriendUsecase
	cs CheatService
}

func (s *playerFriendService) TerminatePlayerFriend(ctx context.Context, internalId value_friend.PlayerId, managedFriendId value_friend.ManagedFriendId) *errwrap.SparkleError {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "TerminatePlayerFriend")
	defer span.End()

	// GetFriend
	friend, err := s.fu.GetFriend(ctx, managedFriendId)
	if err != nil {
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	if friend == nil {
		return errwrap.NewSparkleError(response.RESULT_NOT_YET_FRIEND, "friend not found")
	}
	if !friend.IsChangeable(internalId) {
		return errwrap.NewSparkleError(response.RESULT_NOT_YET_FRIEND, "not changeable friend")
	}

	if err := s.fu.DeleteFriend(ctx, managedFriendId); err != nil {
		return err
	}

	return nil
}

func (s *playerFriendService) CancelPlayerFriend(ctx context.Context, internalId value_friend.PlayerId, managedFriendId value_friend.ManagedFriendId) *errwrap.SparkleError {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "CancelPlayerFriend")
	defer span.End()

	// GetFriendRequest
	friendReq, err := s.fu.GetFriendRequest(ctx, managedFriendId)
	if err != nil {
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	if friendReq == nil {
		return errwrap.NewSparkleError(response.RESULT_FRIEND_NOT_REQUEST, "friend request not found")
	}

	// Validate friend request
	if !friendReq.IsChangeable(internalId) || !friendReq.IsSender(internalId) {
		return errwrap.NewSparkleError(response.RESULT_INVALID_PARAMETERS, "invalid friend request")
	}

	if err := s.fu.DeleteFriendRequest(ctx, internalId, managedFriendId); err != nil {
		return err
	}

	return nil
}

func (s *playerFriendService) validateFriendLimit(ctx context.Context, requesterPlayerId value_friend.PlayerId, otherPlayerId value_friend.PlayerId) *errwrap.SparkleError {
	requesterUserId := value_user.NewUserId(requesterPlayerId)
	user, err := s.uu.GetUserByInternalId(ctx, requesterUserId, repository.UserRepositoryParam{})
	if err != nil {
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	requesterFriendCount, serr := s.fu.GetFriendCount(ctx, requesterPlayerId)
	if serr != nil {
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	if user.IsFriendLimitReached(requesterFriendCount) {
		return errwrap.NewSparkleError(response.RESULT_FRIEND_LIMIT, "friend limit over (sender)")
	}

	otherUserId := value_user.NewUserId(otherPlayerId)
	otherUser, err := s.uu.GetUserByInternalId(ctx, otherUserId, repository.UserRepositoryParam{})
	if err != nil {
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	receiverFriendCount, serr := s.fu.GetFriendCount(ctx, otherPlayerId)
	if serr != nil {
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, serr)
	}
	if otherUser.IsFriendLimitReached(receiverFriendCount) {
		return errwrap.NewSparkleError(response.RESULT_FRIEND_LIMIT_TARGET, "friend limit over (receiver)")
	}

	return nil
}

func (s *playerFriendService) AcceptPlayerFriend(ctx context.Context, internalId value_friend.PlayerId, managedFriendId value_friend.ManagedFriendId) *errwrap.SparkleError {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "AcceptPlayerFriend")
	defer span.End()

	// GetFriendRequest
	req, err := s.fu.GetFriendRequest(ctx, managedFriendId)
	if err != nil {
		return err
	}
	if req == nil {
		return errwrap.NewSparkleError(response.RESULT_ALREADY_FRIEND, "friend request not found")
	}
	if !req.IsChangeable(internalId) || req.IsSender(internalId) {
		return errwrap.NewSparkleError(response.RESULT_INVALID_PARAMETERS, "invalid friend request")
	}

	// Validate friend limit
	if serr := s.validateFriendLimit(ctx, internalId, req.GetOtherPlayerId(internalId)); serr != nil {
		return serr
	}

	// Delete the friend request
	if err := s.fu.DeleteFriendRequest(ctx, internalId, managedFriendId); err != nil {
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	// Add friend
	friend := model_friend.NewFriend(req.PlayerId1, req.PlayerId2)
	if _, err := s.fu.AddFriend(ctx, *friend); err != nil {
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return nil
}

func (s *playerFriendService) RefusePlayerFriend(ctx context.Context, internalId value_friend.PlayerId, managedFriendId value_friend.ManagedFriendId) *errwrap.SparkleError {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "RefusePlayerFriend")
	defer span.End()

	// GetFriendRequest
	friendReq, err := s.fu.GetFriendRequest(ctx, managedFriendId)
	if err != nil {
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	if friendReq == nil {
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_FRIEND_NOT_REQUEST, errors.New("friend request not found"))
	}

	// Validate friend request
	if !friendReq.IsChangeable(internalId) || friendReq.IsSender(internalId) {
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_PARAMETERS, errors.New("requester is not the friend request receiver"))
	}

	// Delete friend request
	if err := s.fu.DeleteFriendRequest(ctx, internalId, managedFriendId); err != nil {
		return err
	}
	return nil
}

func (s *playerFriendService) ProposePlayerFriend(ctx context.Context, internalId value_friend.PlayerId, targetPlayerId value_friend.PlayerId) (*value_friend.ManagedFriendId, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "ProposePlayerFriend")
	defer span.End()

	requesterUserId := value_user.NewUserId(internalId)
	user, err := s.uu.GetUserByInternalId(ctx, requesterUserId, repository.UserRepositoryParam{
		ManagedCharacters: true,
		ManagedNamedTypes: true,
		ItemSummary:       true,
	})
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	// Handle cheat codes
	dummyManagedFriendId, serr := s.cs.HandleCheatPerform(ctx, user, targetPlayerId)
	if serr != nil {
		return nil, serr
	}
	if dummyManagedFriendId != nil {
		return dummyManagedFriendId, nil
	}

	// Handle propose

	// Validate friend limit
	if serr := s.validateFriendLimit(ctx, internalId, targetPlayerId); serr != nil {
		return nil, serr
	}

	fromPlayerId, err := value_friend.NewPlayerId(user.Id, value_friend.PlayerSpecialTypeDefault)
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_INVALID_PARAMETERS, "invalid userId specified")
	}

	if exist, err := s.fu.IsAlreadyFriend(ctx, *fromPlayerId, targetPlayerId); exist || err != nil {
		if exist {
			return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_ALREADY_FRIEND, err)
		}
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	if exist, err := s.fu.IsExistFriendRequest(ctx, *fromPlayerId, targetPlayerId); exist || err != nil {
		if exist {
			return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_ALREADY_FRIEND_REQUEST, err)
		}
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	if exist, err := s.fu.IsExistFriendRequest(ctx, targetPlayerId, *fromPlayerId); exist || err != nil {
		if exist {
			return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_ALREADY_FRIEND_REQUEST_RECEIVED, err)
		}
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	friendReq, err := model_friend.NewFriendRequest(*fromPlayerId, targetPlayerId)
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_INVALID_PARAMETERS, err)
	}
	managedFriendId, serr := s.fu.AddFriendRequest(ctx, *friendReq)
	if serr != nil {
		return nil, serr
	}
	return managedFriendId, nil
}

func (s *playerFriendService) SearchPlayerFriend(ctx context.Context, internalId value_friend.PlayerId, targetMyCode value_friend.FriendOrCheatCode) (*model_friend.FriendFull, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SearchPlayerFriend")
	defer span.End()

	requesterUserId := value_user.NewUserId(internalId)
	user, err := s.uu.GetUserByInternalId(ctx, requesterUserId, repository.UserRepositoryParam{})
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	cheatType := targetMyCode.GetCheatType()

	// Validate request
	if targetMyCode.IsSameCode(user.MyCode) || cheatType == value_friend.CheatCodeTypeCrea {
		return nil, errwrap.NewSparkleError(response.RESULT_ALREADY_FRIEND, "already friend")
	}

	// Handle cheat codes
	if cheatType != value_friend.CheatCodeTypeNone {
		cheatDisplay, serr := s.cs.HandleCheatDisplay(ctx, targetMyCode)
		if serr != nil {
			span.RecordError(serr.GetRawError())
			return nil, serr
		}
		if cheatDisplay != nil {
			return cheatDisplay, nil
		}
	}

	// Handle user code
	targetUser, err := s.uu.GetUserByFriendCode(ctx, targetMyCode)
	if err != nil {
		return nil, errwrap.NewSparkleError(response.RESULT_PLAYER_NOT_FOUND, "not found")
	}

	searchResult, err := model_friend.NewSearchResultFriendFull(*targetUser)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleError(response.RESULT_DB_ERROR, "db error")
	}

	return searchResult, nil
}

func (s *playerFriendService) GetAllPlayerFriend(
	ctx context.Context,
	internalId value_friend.PlayerId,
	getType value_friend.FriendGetType,
	managedBattleParty uint,
	ignoreSupport value.BoolLikeUInt8,
	reload value.BoolLikeUInt8,
) ([]model_friend.FriendFull, []model_friend.FriendFull, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetAllPlayerFriend")
	defer span.End()

	if getType == value_friend.FriendGetTypeFriend {
		outFriendReqs, err := s.fu.GetOutGoingFriendRequests(ctx, internalId)
		if err != nil {
			return nil, nil, err
		}
		inFriendReqs, err := s.fu.GetInComingFriendRequests(ctx, internalId)
		if err != nil {
			return nil, nil, err
		}
		bothFriends, err := s.fu.GetFriends(ctx, internalId)
		if err != nil {
			return nil, nil, err
		}

		friends := make([]model_friend.FriendFull, 0, len(outFriendReqs)+len(inFriendReqs)+len(bothFriends))
		for _, req := range outFriendReqs {
			friendFull := model_friend.NewOutGoingFriendRequestFull(*req)
			friends = append(friends, *friendFull)
		}
		for _, req := range inFriendReqs {
			friendFull := model_friend.NewInComingFriendRequestFull(*req)
			friends = append(friends, *friendFull)
		}
		for _, friendFull := range bothFriends {
			friends = append(friends, *friendFull)
		}
		return friends, nil, nil
	}

	bothFriends, err := s.fu.GetFriends(ctx, internalId)
	if err != nil {
		return nil, nil, err
	}

	friends := make([]model_friend.FriendFull, len(bothFriends)+1)
	friends[0] = model_friend.FriendFull{
		ManagedFriendId:      66,
		PlayerId:             66,
		State:                value_friend.FriendStateFriend,
		Direction:            value_friend.FriendDirectionOtherToSelf,
		Name:                 "星いろどりいし",
		MyCode:               "CREA",
		Comment:              calc.ToPtr("<size=35>す、す、すごかったです！\n次回もがんばりますっ！</size>"),
		CurrentAchievementId: 133320201,
		Level:                66,
		LastLoginAt:          time.Date(2023, 2, 28, 17, 0, 0, 0, time.UTC),
		TotalExp:             0,
		SupportLimit:         8,
		NamedTypes:           nil,
		SupportName:          "きらきらふぁんたじあ",
		SupportCharacters: []model_friend.FriendSupportCharacter{
			{
				ManagedCharacterId: 1,
				CharacterId:        value_character.CharacterId(32022011),
				Level:              100,
				Exp:                0,
				LevelBreak:         4,
				SkillLevel1:        35,
				SkillLevel2:        25,
				SkillLevel3:        25,
				WeaponId:           21200,
				WeaponLevel:        20,
				WeaponSkillLevel:   30,
				WeaponSkillExp:     0,
				NamedLevel:         5,
				NamedExp:           0,
				DuplicatedCount:    5,
				ArousalLevel:       5,
				AbilityBoardId:     -1,
				EquipItemIds:       make([]value_item.ItemId, 0),
			},
		},
		FirstFavoriteMember: model_friend.FriendSupportCharacter{
			ManagedCharacterId: 1,
			CharacterId:        value_character.CharacterId(32022011),
			Level:              100,
			Exp:                0,
			LevelBreak:         4,
			SkillLevel1:        35,
			SkillLevel2:        25,
			SkillLevel3:        25,
			WeaponId:           21200,
			WeaponLevel:        20,
			WeaponSkillLevel:   30,
			WeaponSkillExp:     0,
			NamedLevel:         5,
			NamedExp:           0,
			DuplicatedCount:    5,
			ArousalLevel:       5,
			AbilityBoardId:     -1,
			EquipItemIds:       make([]value_item.ItemId, 0),
		},
	}
	for i, friendFull := range bothFriends {
		friends[i+1] = *friendFull
	}
	guests := []model_friend.FriendFull{
		{
			ManagedFriendId:      66,
			PlayerId:             66,
			State:                value_friend.FriendStateGuest,
			Direction:            value_friend.FriendDirectionNone,
			Name:                 "惜しいろどりいし",
			MyCode:               "CREA",
			Comment:              calc.ToPtr("<size=35>次回もがんばります!\nまたいつでも来てくださいね!</size>"),
			CurrentAchievementId: 131320201,
			Level:                66,
			LastLoginAt:          time.Date(2023, 2, 28, 17, 0, 0, 0, time.UTC),
			TotalExp:             0,
			SupportLimit:         8,
			NamedTypes:           nil,
			SupportName:          "きらきらふぁんたじあ",
			SupportCharacters: []model_friend.FriendSupportCharacter{
				{
					ManagedCharacterId: 1,
					CharacterId:        value_character.CharacterId(32022010),
					Level:              100,
					Exp:                0,
					LevelBreak:         4,
					SkillLevel1:        25,
					SkillLevel2:        15,
					SkillLevel3:        15,
					WeaponId:           21200,
					WeaponLevel:        20,
					WeaponSkillLevel:   30,
					WeaponSkillExp:     0,
					NamedLevel:         5,
					NamedExp:           0,
					DuplicatedCount:    5,
					ArousalLevel:       5,
					AbilityBoardId:     -1,
					EquipItemIds:       make([]value_item.ItemId, 0),
				},
			},
			FirstFavoriteMember: model_friend.FriendSupportCharacter{
				ManagedCharacterId: 1,
				CharacterId:        value_character.CharacterId(32022010),
				Level:              100,
				Exp:                0,
				LevelBreak:         4,
				SkillLevel1:        25,
				SkillLevel2:        15,
				SkillLevel3:        15,
				WeaponId:           21200,
				WeaponLevel:        20,
				WeaponSkillLevel:   30,
				WeaponSkillExp:     0,
				NamedLevel:         5,
				NamedExp:           0,
				DuplicatedCount:    5,
				ArousalLevel:       5,
				AbilityBoardId:     -1,
				EquipItemIds:       make([]value_item.ItemId, 0),
			},
		},
	}
	return friends, guests, nil
}

package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type PlayerSetService interface {
	SetPlayer(ctx context.Context, internalUserId value_user.UserId, name string, comment string, linkId *string, managedFavoriteCharacterIds []int64) error
}

type playerSetService struct {
	uu usecase.UserUsecase
}

func NewPlayerSetService(uu usecase.UserUsecase) PlayerSetService {
	return &playerSetService{uu}
}

func (s *playerSetService) SetPlayer(ctx context.Context, internalUserId value_user.UserId, name string, comment string, linkId *string, managedFavoriteCharacterIds []int64) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SetPlayer")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ManagedCharacters: true,
		FavoriteMembers:   true,
	})
	if err != nil {
		return err
	}

	user.Name = name
	user.Comment = &comment
	// LinkId is ignored since not possible to implement

	for i, id := range managedFavoriteCharacterIds {
		if id == -1 {
			user.FavoriteMembers[i].ManagedCharacterId = -1
			user.FavoriteMembers[i].CharacterId = value_character.CharacterId(0)
			user.FavoriteMembers[i].ArousalLevel = 0
			user.FavoriteMembers[i].FavoriteIndex = uint8(i)
			continue
		}
		managedCharacter, err := user.GetManagedCharacter(value_user.ManagedCharacterId(id))
		if err != nil {
			return err
		}
		user.FavoriteMembers[i].ManagedCharacterId = managedCharacter.ManagedCharacterId
		user.FavoriteMembers[i].CharacterId = managedCharacter.CharacterId
		user.FavoriteMembers[i].FavoriteIndex = uint8(i)
		user.FavoriteMembers[i].ArousalLevel = managedCharacter.ArousalLevel
	}

	if _, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		FavoriteMembers: true,
	}); err != nil {
		return err
	}
	return nil
}

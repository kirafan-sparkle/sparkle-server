package service

import (
	"context"
	"errors"

	schema_schedule "gitlab.com/kirafan/sparkle/server/internal/application/schemas/schedule"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

type PlayerScheduleService interface {
	SetScheduleTargets(ctx context.Context, internalId value_user.UserId, param schema_schedule.ScheduleTargetSchema) ([]*model_user.ManagedFieldPartyMember, error)
	GetSchedules(ctx context.Context, internalId value_user.UserId, managedFieldPartyMemberIds []uint) ([]*model_user.ManagedFieldPartyMember, error)
}

type playerScheduleService struct {
	uu usecase.UserUsecase
	su usecase.ScheduleUsecase
}

func NewPlayerScheduleService(
	uu usecase.UserUsecase,
	su usecase.ScheduleUsecase,
) PlayerScheduleService {
	return &playerScheduleService{uu, su}
}

var ErrScheduleServiceUserNotFound = errors.New("user not found")
var ErrScheduleServiceManagedCharacterNotFound = errors.New("specified managed character not found")

func (s *playerScheduleService) SetScheduleTargets(ctx context.Context, internalId value_user.UserId, param schema_schedule.ScheduleTargetSchema) ([]*model_user.ManagedFieldPartyMember, error) {
	// Validate playerId
	user, err := s.uu.GetUserByInternalId(ctx, internalId, repository.UserRepositoryParam{
		ManagedCharacters:        true,
		ManagedFieldPartyMembers: true,
	})
	if err != nil {
		return nil, err
	}

	for _, target := range param {
		for j := range user.ManagedFieldPartyMembers {
			if user.ManagedFieldPartyMembers[j].LiveIdx != uint8(target.LiveIdx) {
				continue
			}
			if user.ManagedFieldPartyMembers[j].RoomId != uint8(target.RoomId) {
				continue
			}
			user.ManagedFieldPartyMembers[j].ManagedCharacterId = target.ManagedCharacterId
			character, err := user.GetManagedCharacter(target.ManagedCharacterId)
			if err != nil {
				return nil, err
			}
			user.ManagedFieldPartyMembers[j].CharacterId = uint64(character.CharacterId)
		}
	}

	_, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{ManagedFieldPartyMembers: true})
	if err != nil {
		return nil, err
	}

	managedCharacterIds := calc.Map(
		param,
		func(target schema_schedule.ScheduleTargetObject) uint { return uint(target.ManagedCharacterId) },
	)
	targetManagedCharacters, err := s.GetSchedules(ctx, internalId, managedCharacterIds)
	if err != nil {
		return nil, err
	}

	return targetManagedCharacters, nil
}

func (s *playerScheduleService) GetSchedules(ctx context.Context, internalId value_user.UserId, managedCharacterIds []uint) ([]*model_user.ManagedFieldPartyMember, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetSchedules")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx, internalId, repository.UserRepositoryParam{ManagedFieldPartyMembers: true})
	if err != nil {
		return nil, ErrScheduleServiceUserNotFound
	}

	// Get managed characters specified at req
	targetManagedFieldPartyMembers := []*model_user.ManagedFieldPartyMember{}
	for _, managedCharacterId := range managedCharacterIds {
		var managedCharacter *model_user.ManagedFieldPartyMember = nil
		for _, mc := range user.ManagedFieldPartyMembers {
			// FIXME: Remove this force type assertion
			if mc.ManagedCharacterId == value_user.ManagedCharacterId(managedCharacterId) {
				managedCharacter = &mc
				break
			}
		}
		if managedCharacter == nil {
			return nil, ErrScheduleServiceManagedCharacterNotFound
		}
		targetManagedFieldPartyMembers = append(targetManagedFieldPartyMembers, managedCharacter)
	}

	// Create request param target character ids
	var targetCharacterIds []value_character.CharacterId
	for _, managedCharacter := range targetManagedFieldPartyMembers {
		characterId, err := value_character.NewCharacterId(uint32(managedCharacter.CharacterId))
		if err != nil {
			return nil, err
		}
		targetCharacterIds = append(targetCharacterIds, characterId)
	}

	// Create request param target schedules
	var targetSchedules []string
	for _, managedCharacter := range targetManagedFieldPartyMembers {
		targetSchedules = append(targetSchedules, *managedCharacter.ScheduleTable)
	}

	// Refresh user schedule
	schedules, err := s.su.RefreshSchedule(ctx, targetSchedules)
	if err != nil {
		return nil, err
	}

	// Update managed field party member schedules
	var resp []*model_user.ManagedFieldPartyMember
	for _, managedCharacterId := range managedCharacterIds {
		for i, mc := range user.ManagedFieldPartyMembers {
			// FIXME: Remove this force type assertion
			if mc.ManagedCharacterId == value_user.ManagedCharacterId(managedCharacterId) {
				var targetCharacterIndex int
				for j, c := range targetCharacterIds {
					if int(c) == int(mc.CharacterId) {
						targetCharacterIndex = j
						break
					}
				}
				user.ManagedFieldPartyMembers[i].ScheduleTable = &schedules[targetCharacterIndex]
				resp = append(resp, &user.ManagedFieldPartyMembers[i])
			}
		}
	}

	// Update user
	_, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{ManagedFieldPartyMembers: true})
	if err != nil {
		return nil, err
	}

	return resp, nil
}

//go:build wireinject
// +build wireinject

package service

import (
	"github.com/google/wire"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	registry_providers_usecase "gitlab.com/kirafan/sparkle/server/internal/registry/providers/usecase"
	"gorm.io/gorm"
)

func InitializeUserUsecase(db *gorm.DB) usecase.UserUsecase {
	wire.Build(
		registry_providers_usecase.UserUsecaseSet,
	)
	return nil
}

func InitializePlayerPresentService(db *gorm.DB) PlayerPresentService {
	wire.Build(
		registry_providers_usecase.UserUsecaseSet,
		registry_providers_usecase.CharacterUsecaseSet,
		registry_providers_usecase.NamedTypeUsecaseSet,
		registry_providers_usecase.UserPresentUsecaseSet,
		NewPlayerPresentService,
	)
	return nil
}

func InitializePlayerGachaService(db *gorm.DB) PlayerGachaService {
	wire.Build(
		registry_providers_usecase.UserUsecaseSet,
		registry_providers_usecase.GachaUsecaseSet,
		registry_providers_usecase.NamedTypeUsecaseSet,
		registry_providers_usecase.CharacterUsecaseSet,
		registry_providers_usecase.ItemUsecaseSet,
		NewPlayerGachaService,
	)
	return nil
}

func InitializePlayerQuestService(db *gorm.DB) PlayerQuestService {
	wire.Build(
		registry_providers_usecase.UserUsecaseSet,
		registry_providers_usecase.CharacterUsecaseSet,
		registry_providers_usecase.QuestUsecaseSet,
		registry_providers_usecase.QuestWaveUsecaseSet,
		registry_providers_usecase.WeaponTableUsecaseSet,
		registry_providers_usecase.ExpTableCharacterUsecaseSet,
		registry_providers_usecase.ExpTableSkillUsecaseSet,
		registry_providers_usecase.ExpTableRankUsecaseSet,
		registry_providers_usecase.ExpTableFriendshipUsecaseSet,
		registry_providers_usecase.EventQuestUsecaseSet,
		registry_providers_usecase.CharacterQuestUsecaseSet,
		NewPlayerQuestService,
	)
	return nil
}

func InitializePlayerTownFacilityService(db *gorm.DB) PlayerTownFacilityService {
	wire.Build(
		registry_providers_usecase.UserUsecaseSet,
		registry_providers_usecase.TownFacilityUsecaseSet,
		registry_providers_usecase.LevelTableTownFacilityUsecaseSet,
		registry_providers_usecase.ItemTableTownFacilityUsecaseSet,
		NewPlayerTownFacilityService,
	)
	return nil
}

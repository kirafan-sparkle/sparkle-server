package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type playerSignupService struct {
	uu  usecase.UserUsecase
	pu  usecase.PresentUsecase
	mu  usecase.MissionUsecase
	lu  usecase.LoginBonusUsecase
	tu  usecase.TradeRecipeUsecase
	tru usecase.TrainingUsecase
}

type PlayerSignupService interface {
	SignupPlayer(ctx context.Context, name string, platform value_version.Platform, stepCode value_user.StepCode, uuid string) (*model_user.User, *errwrap.SparkleError)
}

func NewPlayerSignupService(
	uu usecase.UserUsecase,
	pu usecase.PresentUsecase,
	mu usecase.MissionUsecase,
	lu usecase.LoginBonusUsecase,
	tu usecase.TradeRecipeUsecase,
	tru usecase.TrainingUsecase,
) PlayerSignupService {
	return &playerSignupService{uu, pu, mu, lu, tu, tru}
}

func (s *playerSignupService) SignupPlayer(ctx context.Context, name string, platform value_version.Platform, stepCode value_user.StepCode, uuid string) (*model_user.User, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SignupPlayer")
	defer span.End()

	missions, err := s.mu.GetTutorialMissions(ctx)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	presents, err := s.pu.GetTutorialPresents(ctx)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	loginBonuses, err := s.lu.GetAllLoginBonuses(ctx)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	tradeRecipes, err := s.tu.GetTradeRecipes(ctx)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	// Get only default trainings
	trainings, err := s.tru.GetTrainings(ctx, 1)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	user, err := s.uu.CreateUser(
		ctx,
		uuid,
		name,
		presents,
		missions,
		loginBonuses,
		tradeRecipes,
		trainings,
	)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_ALREADY_EXISTS, err)
	}

	return user, nil
}

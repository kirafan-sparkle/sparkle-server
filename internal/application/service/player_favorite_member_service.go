package service

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type PlayerFavoriteMemberService interface {
	GetFavoriteMember(ctx context.Context, internalUserId value_user.UserId) ([]model_user.FavoriteMember, error)
}

type playerFavoriteMemberService struct {
	uu usecase.UserUsecase
}

func NewPlayerFavoriteMemberService(
	uu usecase.UserUsecase,
) PlayerFavoriteMemberService {
	return &playerFavoriteMemberService{uu}
}

func (s *playerFavoriteMemberService) GetFavoriteMember(ctx context.Context, internalUserId value_user.UserId) ([]model_user.FavoriteMember, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetFavoriteMember")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{FavoriteMembers: true})
	if err != nil {
		return nil, err
	}
	return user.FavoriteMembers, nil
}

package service

import (
	"context"
	"errors"

	schema_quest "gitlab.com/kirafan/sparkle/server/internal/application/schemas/quest"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/parser"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type PlayerQuestService interface {
	StartQuest(
		ctx context.Context,
		userId value_user.UserId, questId uint, managedBattlePartyId uint,
		supportCharacterId int64, questNpcId int64,
	) (*model_user.User, *[][]*model_quest.QuestWave, *[][][]model_quest.QuestWaveDrop, error)
	SaveQuest(ctx context.Context, userId value_user.UserId, orderReceiveId int, questData string) error
	CompleteQuest(
		ctx context.Context,
		userId value_user.UserId, orderReceiveId uint,
		state uint8, clearRank value_quest.ClearRank,
		skillExps string, weaponSkillExps string,
		friendUseNum uint8, masterSkillUseNum uint8, uniqueSkillUseNum uint8,
		stepCode value_user.StepCode,
	) (*model_user.User, bool, *errwrap.SparkleError)
	ListQuest(
		ctx context.Context,
		userId value_user.UserId,
	) (*schema_quest.AllQuestInfoWithClearRanksSchema, error)
	ResetQuest(
		ctx context.Context,
		userId value_user.UserId,
	) *errwrap.SparkleError
	RetryQuest(
		ctx context.Context,
		internalUserId value_user.UserId,
	) (*model_user.User, *errwrap.SparkleError)
}

type playerQuestService struct {
	uu  usecase.UserUsecase
	qu  usecase.QuestUsecase
	equ usecase.EventQuestUsecase
	cqu usecase.CharacterQuestUsecase
	cu  usecase.CharacterUsecase
	wu  usecase.WeaponUsecase
	ecu usecase.ExpTableCharacterUsecase
	efu usecase.ExpTableFriendshipUsecase
	eru usecase.ExpTableRankUsecase
	esu usecase.ExpTableSkillUsecase
	qwu usecase.QuestWaveUsecase
}

func NewPlayerQuestService(
	uu usecase.UserUsecase,
	qu usecase.QuestUsecase,
	equ usecase.EventQuestUsecase,
	cqu usecase.CharacterQuestUsecase,
	cu usecase.CharacterUsecase,
	wu usecase.WeaponUsecase,
	ecu usecase.ExpTableCharacterUsecase,
	efu usecase.ExpTableFriendshipUsecase,
	eru usecase.ExpTableRankUsecase,
	esu usecase.ExpTableSkillUsecase,
	qwu usecase.QuestWaveUsecase,
) PlayerQuestService {
	return &playerQuestService{uu, qu, equ, cqu, cu, wu, ecu, efu, eru, esu, qwu}
}

var ErrPlayerQuestNotEnoughStamina = errors.New("not enough stamina")
var ErrPlayerQuestNotEnoughItem = errors.New("not enough item")
var ErrPlayerQuestInvalidManagedBattlePartyId = errors.New("not valid managed battle party id")
var ErrPlayerQuestOrderReceiveIdMismatch = errors.New("orderReceiveId mismatch")
var ErrPlayerQuestLogNotFound = errors.New("quest log not found")
var ErrPlayerQuestQuestsNotFound = errors.New("quests weren't found")

func (s *playerQuestService) ListQuest(
	ctx context.Context,
	userId value_user.UserId,
) (*schema_quest.AllQuestInfoWithClearRanksSchema, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "ListQuest")
	defer span.End()

	questPart1s, err := s.qu.GetPart1Quests(ctx, userId)
	if err != nil {
		span.RecordError(err)
		return nil, ErrPlayerQuestQuestsNotFound
	}
	questPart2s, err := s.qu.GetPart2Quests(ctx, userId)
	if err != nil {
		span.RecordError(err)
		return nil, ErrPlayerQuestQuestsNotFound
	}
	characterQuests, err := s.cqu.GetCharacterQuests(ctx, userId)
	if err != nil {
		span.RecordError(err)
		return nil, ErrPlayerQuestQuestsNotFound
	}
	eventQuests, err := s.equ.GetEventQuests(ctx, userId)
	if err != nil {
		span.RecordError(err)
		return nil, ErrPlayerQuestQuestsNotFound
	}
	eventQuestPeriods, err := s.equ.GetEventQuestPeriods(ctx, userId)
	if err != nil {
		span.RecordError(err)
		return nil, ErrPlayerQuestQuestsNotFound
	}

	userClearRanks, err := s.uu.GetUserClearRanks(ctx, userId)
	if err != nil {
		span.RecordError(err)
		return nil, ErrPlayerQuestLogNotFound
	}
	user, err := s.uu.GetUserByInternalId(ctx, userId, repository.UserRepositoryParam{})
	if err != nil {
		span.RecordError(err)
		return nil, ErrPlayerQuestLogNotFound
	}

	return &schema_quest.AllQuestInfoWithClearRanksSchema{
		QuestPart1s:                   questPart1s,
		QuestPart2s:                   questPart2s,
		CharacterQuests:               characterQuests,
		EventQuests:                   eventQuests,
		EventQuestPeriods:             eventQuestPeriods,
		ClearRanks:                    userClearRanks,
		LastPlayedChapterQuestIdPart1: user.LastPlayedPart1ChapterQuestId,
		LastPlayedChapterQuestIdPart2: user.LastPlayedPart2ChapterQuestId,
		PlayedOpenChapterIdPart1:      user.LastOpenedPart1ChapterId,
		PlayedOpenChapterIdPart2:      user.LastOpenedPart2ChapterId,
	}, nil
}

func (s *playerQuestService) StartQuest(
	ctx context.Context,
	// required ids
	userId value_user.UserId,
	questIdOrEventQuestId uint,
	// optional ids for make a quest-log data
	managedBattlePartyId uint,
	supportCharacterId int64,
	questNpcId int64,
) (*model_user.User, *[][]*model_quest.QuestWave, *[][][]model_quest.QuestWaveDrop, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "StartQuest")
	defer span.End()

	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, userId, repository.UserRepositoryParam{
		ManagedBattleParties: true,
		ItemSummary:          true,
	})
	if err != nil {
		span.RecordError(err)
		return nil, nil, nil, err
	}
	user.RefreshStamina()

	// Get quest category
	questCategory, err := s.qu.GetQuestCategory(ctx, questIdOrEventQuestId)
	if err != nil {
		span.RecordError(err)
		return nil, nil, nil, err
	}

	// Get quest info
	var quest *model_quest.Quest
	var eventQuest *model_quest.EventQuest
	switch *questCategory {
	case value_quest.QuestCategoryTypeMainPart1:
		quest, err = s.qu.GetQuest(ctx, questIdOrEventQuestId)
		if err != nil {
			span.RecordError(err)
			return nil, nil, nil, err
		}
		user.LastPlayedPart1ChapterQuestId = int32(questIdOrEventQuestId)
	case value_quest.QuestCategoryTypeMainPart2:
		quest, err = s.qu.GetQuest(ctx, questIdOrEventQuestId)
		if err != nil {
			span.RecordError(err)
			return nil, nil, nil, err
		}
		user.LastPlayedPart2ChapterQuestId = int32(questIdOrEventQuestId)
	// TODO: Implement more quests
	case value_quest.QuestCategoryTypeMemorialCharacter:
		fallthrough
	case value_quest.QuestCategoryTypeCraftQuest:
		quest, err = s.qu.GetQuest(ctx, questIdOrEventQuestId)
		if err != nil {
			span.RecordError(err)
			return nil, nil, nil, err
		}
	case value_quest.QuestCategoryTypeEventAuthorDay:
		eventQuest, err = s.equ.GetEventQuest(ctx, userId, questIdOrEventQuestId)
		if err != nil {
			span.RecordError(err)
			return nil, nil, nil, err
		}
		quest = &eventQuest.Quest
	default:
		ret := errors.New("unsupported category")
		span.RecordError(ret)
		return nil, nil, nil, ret
	}

	// Get quest Id
	questId := questIdOrEventQuestId
	if *questCategory == value_quest.QuestCategoryTypeEventAuthorDay {
		questId = eventQuest.QuestId
	}

	// If the quest is only story, return without generate quest waves
	if quest.AdvOnly == value.BoolLikeUIntTrue {
		// TODO: Maybe original server stored the full questLog into different table to avoid cheating
		// Our server doesn't care about cheating so just make random number and empty questData
		orderReceiveId := uint(calc.GetSparkleRandomNumber())
		serverSideQuestLog := model_user.NewUserOnGoingQuest(orderReceiveId, questId, "", nil, nil)
		// Update user latest quest log id
		user.LatestQuestLog = serverSideQuestLog.AsJsonString()
		user.LatestQuestLogID = int(orderReceiveId)

		// Save the user
		user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{ItemSummary: true})
		if err != nil {
			span.RecordError(err)
			return nil, nil, nil, err
		}

		newAdvClear := model_user.NewUserAdvQuestClearRank(userId, questId)
		if err = s.uu.UpsertUserClearRank(ctx, userId, newAdvClear); err != nil {
			span.RecordError(err)
			return nil, nil, nil, err
		}
		return user, &[][]*model_quest.QuestWave{}, &[][][]model_quest.QuestWaveDrop{}, nil
	}

	// Validate managed battle party Id
	userManagedBattlePartyIds := make([]int64, len(user.ManagedBattleParties))
	for i := range user.ManagedBattleParties {
		userManagedBattlePartyIds[i] = int64(user.ManagedBattleParties[i].ManagedBattlePartyId)
	}
	if !calc.Contains(userManagedBattlePartyIds, int64(managedBattlePartyId)) {
		span.RecordError(ErrPlayerQuestInvalidManagedBattlePartyId)
		return nil, nil, nil, ErrPlayerQuestInvalidManagedBattlePartyId
	}
	// Validate user stamina or item and consumes
	if user.StepCode == value_user.StepCodeTutorialDone {
		if quest.Stamina != 0 {
			if consumed := user.ConsumeStamina(uint32(quest.Stamina)); !consumed {
				span.RecordError(ErrPlayerQuestNotEnoughStamina)
				return nil, nil, nil, ErrPlayerQuestNotEnoughStamina
			}
		}
		if quest.ExId2 != -1 {
			if consumed := user.ConsumeItem(quest.ExId2, uint32(quest.Ex2Amount)); !consumed {
				span.RecordError(ErrPlayerQuestNotEnoughItem)
				return nil, nil, nil, ErrPlayerQuestNotEnoughItem
			}
		}
	}

	// Create quest waves and drop items
	var questWaves [][]*model_quest.QuestWave
	var questDropItems [][][]model_quest.QuestWaveDrop
	waveIds := []int64{quest.WaveId1, quest.WaveId2, quest.WaveId3, quest.WaveId4, quest.WaveId5}
	for _, waveId := range waveIds {
		if waveId == 0 {
			continue
		}
		questWaveInfo, dropItem, err := s.qwu.GetQuestWave(ctx, waveId)
		if err != nil {
			span.RecordError(err)
			return nil, nil, nil, err
		}
		questWaves = append(questWaves, questWaveInfo)
		questDropItems = append(questDropItems, dropItem)
	}

	// TODO: Maybe original server stored the full questLog into different table to avoid cheating
	// Our server doesn't care about cheating so just make random number and empty questData
	orderReceiveId := uint(calc.GetSparkleRandomNumber())
	serverSideQuestLog := model_user.NewUserOnGoingQuest(orderReceiveId, questId, "", questWaves, questDropItems)
	// Update user latest quest log id
	user.LatestQuestLog = serverSideQuestLog.AsJsonString()
	user.LatestQuestLogID = int(orderReceiveId)

	// Save the user
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{ItemSummary: true})
	if err != nil {
		span.RecordError(err)
		return nil, nil, nil, err
	}

	return user, &questWaves, &questDropItems, nil
}

func (s *playerQuestService) SaveQuest(
	ctx context.Context,
	// required ids
	userId value_user.UserId,
	orderReceiveId int,
	clientSideQuestData string,
) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SaveQuest")
	defer span.End()

	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, userId, repository.UserRepositoryParam{})
	if err != nil {
		span.RecordError(err)
		return err
	}

	// Validate orderReceiveId
	if user.LatestQuestLogID != orderReceiveId {
		span.RecordError(err)
		return ErrPlayerQuestOrderReceiveIdMismatch
	}

	// Load the server side quest log
	var serverSideQuestLog model_user.UserOnGoingQuest
	if err := user.LatestQuestLog.ParseToModel(&serverSideQuestLog); err != nil {
		span.RecordError(err)
		return err
	}

	// TODO: validate the client questData (it contains gzip base64 json string)
	serverSideQuestLog.QuestData = clientSideQuestData
	user.LatestQuestLog = serverSideQuestLog.AsJsonString()

	// Save the user
	if _, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{}); err != nil {
		span.RecordError(err)
		return err
	}
	return nil
}

func (s *playerQuestService) updateUserRank(ctx context.Context, user *model_user.User) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "updateUserRank")
	defer span.End()
	nextRank, err := s.eru.GetNextExpTableRank(ctx, user.TotalExp)
	if err != nil {
		return err
	}
	// LevelExp means differences between current user's exp and previous level's required exp
	// FIXME: Please refactor this line, no one can understand this
	user.LevelExp = user.TotalExp - (uint64(nextRank.TotalExp) - nextRank.NextExp)

	if user.Level == uint16(nextRank.Rank) {
		return nil
	}
	user.UpdateRankByNextRankInfo(nextRank)
	return nil
}

func (s *playerQuestService) updateManagedNamedTypeLevels(
	ctx context.Context,
	user *model_user.User,
	targetNamedTypes []uint16,
) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "updateManagedNamedTypeLevels")
	defer span.End()
	for i, managedNamedType := range user.ManagedNamedTypes {
		if !calc.Contains(targetNamedTypes, managedNamedType.NamedType) {
			continue
		}
		nextNamedTypeLevel, err := s.efu.GetNextExpTableFriendship(ctx, uint64(managedNamedType.Exp))
		if err != nil {
			return err
		}
		user.ManagedNamedTypes[i].UpdateLevel(uint8(nextNamedTypeLevel.Level))
	}
	return nil
}

func (s *playerQuestService) updateManagedCharacterLevel(
	ctx context.Context,
	character *model_user.ManagedCharacter,
) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "updateManagedCharacterLevel")
	defer span.End()
	// Adjust the exp limitations
	limitCharacterExp, err := s.ecu.GetLevelExpTableCharacter(ctx, character.LevelLimit)
	if err != nil {
		return err
	}
	character.AdjustExp(limitCharacterExp.TotalExp)
	// Update level
	if !character.IsMaxLevel() {
		nextCharacterLevel, err := s.ecu.GetNextExpTableCharacter(ctx, character.Exp)
		if err != nil {
			return err
		}
		character.UpdateLevel(nextCharacterLevel.Level)
	}
	return nil
}
func (s *playerQuestService) updateManagedCharacterSkillLevel(
	ctx context.Context,
	character *model_user.ManagedCharacter,
) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "updateManagedCharacterSkillLevel")
	defer span.End()
	// Adjust the skill exp limitations
	limitCharacterSkill1Exp, err := s.esu.GetLevelExpTableSkill(
		ctx, uint32(character.SkillLevelLimit1-1), value_exp.ExpTableSkillTypeNormal,
	)
	if err != nil {
		return err
	}
	character.AdjustSkillExp(model_user.SkillType1, limitCharacterSkill1Exp.TotalExp)
	limitCharacterSkill2Exp, err := s.esu.GetLevelExpTableSkill(
		ctx, uint32(character.SkillLevelLimit2-1), value_exp.ExpTableSkillTypeNormal,
	)
	if err != nil {
		return err
	}
	character.AdjustSkillExp(model_user.SkillType2, limitCharacterSkill2Exp.TotalExp)
	limitCharacterSkill3Exp, err := s.esu.GetLevelExpTableSkill(
		ctx, uint32(character.SkillLevelLimit3-1), value_exp.ExpTableSkillTypeNormal,
	)
	if err != nil {
		return err
	}
	character.AdjustSkillExp(model_user.SkillType3, limitCharacterSkill3Exp.TotalExp)
	// Update skill levels
	if !character.IsMaxSkillLevel(model_user.SkillType1) {
		nextCharacterSkillLevel1, err := s.esu.GetNextExpTableSkill(ctx, character.SkillExp1, value_exp.ExpTableSkillTypeNormal)
		if err != nil {
			return err
		}
		character.UpdateSkillLevel(nextCharacterSkillLevel1.Level, model_user.SkillType1)
	}
	if !character.IsMaxSkillLevel(model_user.SkillType2) {
		nextCharacterSkillLevel2, err := s.esu.GetNextExpTableSkill(ctx, character.SkillExp2, value_exp.ExpTableSkillTypeNormal)
		if err != nil {
			return err
		}
		character.UpdateSkillLevel(nextCharacterSkillLevel2.Level, model_user.SkillType2)
	}
	if !character.IsMaxSkillLevel(model_user.SkillType3) {
		nextCharacterSkillLevel3, err := s.esu.GetNextExpTableSkill(ctx, character.SkillExp3, value_exp.ExpTableSkillTypeNormal)
		if err != nil {
			return err
		}
		character.UpdateSkillLevel(nextCharacterSkillLevel3.Level, model_user.SkillType3)
	}
	return nil
}
func (s *playerQuestService) updateManagedCharacterLevels(
	ctx context.Context,
	user *model_user.User,
	targetManagedIds []value_user.ManagedCharacterId,
) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "updateManagedCharacterLevels")
	defer span.End()
	for i := range user.ManagedCharacters {
		if !calc.Contains(targetManagedIds, user.ManagedCharacters[i].ManagedCharacterId) {
			continue
		}
		if err := s.updateManagedCharacterLevel(ctx, &user.ManagedCharacters[i]); err != nil {
			return err
		}
		if err := s.updateManagedCharacterSkillLevel(ctx, &user.ManagedCharacters[i]); err != nil {
			return err
		}
	}
	return nil
}

func (s *playerQuestService) updateManagedWeaponLevels(
	ctx context.Context,
	user *model_user.User,
	targetManagedIds []value_user.ManagedWeaponId,
) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "updateManagedWeaponLevels")
	defer span.End()

	for i, managedWeapon := range user.ManagedWeapons {
		if !calc.Contains(targetManagedIds, managedWeapon.ManagedWeaponId) {
			continue
		}
		// Get weapon info
		weapon, err := s.wu.GetWeaponById(ctx, user.ManagedWeapons[i].WeaponId)
		if err != nil {
			return err
		}
		skillType := value_exp.ExpTableSkillTypeNormal
		if weapon.Rarity > 3 {
			// Special character weapon uses this skill type
			skillType = value_exp.ExpTableSkillTypeWeapon
		}
		// Adjust the exp limitations
		limitWeaponSkillExp, err := s.esu.GetLevelExpTableSkill(ctx, uint32(weapon.SkillLimitLv), skillType)
		if err != nil {
			return err
		}
		user.ManagedWeapons[i].AdjustWeaponSkillExp(limitWeaponSkillExp.TotalExp - limitWeaponSkillExp.NextExp)
		// Update levels
		nextWeaponSkillLevel, err := s.esu.GetNextExpTableSkill(ctx, uint32(managedWeapon.SkillExp), skillType)
		if err != nil {
			return err
		}
		user.ManagedWeapons[i].UpdateWeaponSkillLevel(nextWeaponSkillLevel.Level)
	}
	return nil
}

func (s *playerQuestService) receiveDropItems(ctx context.Context, user *model_user.User, questDropItems [][][]model_quest.QuestWaveDrop) error {
	_, span := observability.Tracer.StartAppServiceSpan(ctx, "receiveDropItems")
	defer span.End()
	var droppedItems []model_quest.QuestWaveDrop
	for i := 0; i < len(questDropItems); i++ {
		for j := 0; j < len(questDropItems[i]); j++ {
			for k := 0; k < len(questDropItems[i][j]); k++ {
				droppedItems = append(droppedItems, questDropItems[i][j][k])
			}
		}
	}
	// Update item summaries
	for i := range droppedItems {
		user.AddItem(droppedItems[i].DropItemId, droppedItems[i].DropItemAmount)
	}
	return nil
}

func (s *playerQuestService) receiveFirstClearRewards(ctx context.Context, user *model_user.User, advId uint64, reward *model_quest.QuestFirstClearReward) {
	_, span := observability.Tracer.StartAppServiceSpan(ctx, "receiveFirstClearRewards")
	defer span.End()
	if user.HasClearedAdv(advId) {
		return
	}
	user.AddLimitedGem(uint32(reward.Gem))
	user.AddGold(uint64(reward.Gold))
	if reward.ItemId1 != -1 {
		user.AddItem(reward.ItemId1, uint32(reward.Amount1))
	}
	if reward.ItemId2 != -1 {
		user.AddItem(reward.ItemId2, uint32(reward.Amount2))
	}
	if reward.ItemId3 != -1 {
		user.AddItem(reward.ItemId3, uint32(reward.Amount3))
	}
	if reward.WeaponId != -1 && reward.WeaponAmount != -1 {
		weaponId := value_weapon.NewWeaponId(reward.WeaponId)
		for i := 0; i < int(reward.WeaponAmount); i++ {
			user.AddWeapon(weaponId, true)
		}
	}
	if reward.RoomObjectId != -1 && reward.RoomObjectAmount != -1 {
		for i := 0; i < int(reward.RoomObjectAmount); i++ {
			user.AddRoomObject(uint32(reward.RoomObjectId))
		}
	}
}

func (s *playerQuestService) CompleteQuest(
	ctx context.Context,
	userId value_user.UserId,
	orderReceiveId uint,
	// Cleared: 2 / Failed: 1
	state uint8,
	clearRank value_quest.ClearRank,
	characterSkillExps string,
	weaponSkillExps string,
	friendUseNum uint8,
	// kirara skill
	masterSkillUseNum uint8,
	// とっておき
	uniqueSkillUseNum uint8,
	stepCode value_user.StepCode,
) (*model_user.User, bool, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "CompleteQuest")
	defer span.End()

	// Get user
	user, err := s.uu.GetUserByInternalId(ctx, userId, repository.UserRepositoryParam{
		ManagedNamedTypes: true,
		ManagedCharacters: true,
		ManagedWeapons:    true,
		ItemSummary:       true,
		AdvIds:            true,
	})
	if err != nil {
		span.RecordError(err)
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		return nil, false, serr
	}
	// Validate orderReceiveId
	if user.LatestQuestLogID != int(orderReceiveId) {
		span.RecordError(ErrPlayerQuestOrderReceiveIdMismatch)
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_ALREADY_PROCESSED, err)
		return nil, false, serr
	}
	// Refresh server side stamina
	user.RefreshStamina()

	// Update user info
	user.LatestQuestLogID = -1

	// Finalize if quest is failed
	if clearRank == value_quest.ClearRankNone {
		// Save the user
		user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{})
		if err != nil {
			span.RecordError(err)
			serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
			return nil, false, serr
		}
		return user, false, nil
	}

	// Load the server side quest log
	var serverSideQuestLog model_user.UserOnGoingQuest
	if err := user.LatestQuestLog.ParseToModel(&serverSideQuestLog); err != nil {
		span.RecordError(err)
		return nil, false, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	user.LatestQuestLog = nil
	dropDataForReceive := serverSideQuestLog.DropData
	questId := serverSideQuestLog.QuestId

	// Update clear rank
	updatedClearRank := model_user.NewUserQuestClearRank(userId, questId, clearRank)
	if err := s.uu.UpsertUserClearRank(ctx, userId, updatedClearRank); err != nil {
		span.RecordError(err)
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		return nil, false, serr
	}

	// Get quest info
	quest, err := s.qu.GetQuest(ctx, questId)
	if err != nil {
		span.RecordError(err)
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		return nil, false, serr
	}

	// Finalize if quest is story only
	if quest.AdvOnly == value.BoolLikeUIntTrue {
		s.receiveFirstClearRewards(ctx, user, uint64(quest.AdvId1), &quest.QuestFirstClearReward)
		user.AddClearedAdv(uint64(quest.Id))
		user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
			ManagedWeapons: true,
			ItemSummary:    true,
			AdvIds:         true,
		})
		if err != nil {
			span.RecordError(err)
			serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
			return nil, false, serr
		}
		return user, false, nil
	}

	if err := s.receiveDropItems(ctx, user, dropDataForReceive); err != nil {
		span.RecordError(err)
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		serr.SetResponseMessage("failed at logic (receiveDropItems)")
		return nil, false, serr
	}
	s.receiveFirstClearRewards(ctx, user, uint64(quest.AdvId2), &quest.QuestFirstClearReward)

	// Parse the exps and get target ids
	parsedCharacterSkillExps := parser.ParseCharacterExpString(characterSkillExps)
	targetManagedCharacterIds := make([]value_user.ManagedCharacterId, 0, len(parsedCharacterSkillExps))
	for managedCharacterId := range parsedCharacterSkillExps {
		targetManagedCharacterIds = append(targetManagedCharacterIds, value_user.ManagedCharacterId(managedCharacterId))
	}
	parsedManagedWeaponSkillExps := parser.ParseWeaponExpString(weaponSkillExps)
	targetManagedWeaponIds := make([]value_user.ManagedWeaponId, 0, len(parsedManagedWeaponSkillExps))
	for managedWeaponId := range parsedManagedWeaponSkillExps {
		targetManagedWeaponIds = append(targetManagedWeaponIds, value_user.ManagedWeaponId(managedWeaponId))
	}

	targetCharacterIds := user.GetCharacterIdsByManagedCharacterIds(targetManagedCharacterIds)
	targetNamedTypesNonUnique, err := s.cu.GetNamedTypesByIds(ctx, targetCharacterIds)
	if err != nil {
		span.RecordError(err)
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		return nil, false, serr
	}
	targetNamedTypes := calc.Unique(targetNamedTypesNonUnique)

	// Add all clear rewards
	// NOTE: The exps can be over the limit of level but they will be fixed in the level updates
	user.AddNamedTypeExps(uint32(quest.RewardFriendshipExp), targetNamedTypes)
	user.AddCharacterExps(quest.RewardCharacterExp, targetManagedCharacterIds)
	user.AddCharacterSkillExps(parsedCharacterSkillExps)
	user.AddWeaponSkillExps(parsedManagedWeaponSkillExps)
	user.AddRankExp(quest.RewardUserExp)
	user.AddGold(quest.RewardMoney)

	// Update all levels
	// NOTE: To prevent performance issue, only affected characters/weapons will be updated
	if err := s.updateUserRank(ctx, user); err != nil {
		span.RecordError(err)
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		serr.SetResponseMessage("failed at logic (updateUserRank)")
		return nil, false, serr
	}
	if err := s.updateManagedCharacterLevels(ctx, user, targetManagedCharacterIds); err != nil {
		span.RecordError(err)
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		serr.SetResponseMessage("failed at logic (updateCharacterLevel)")
		return nil, false, serr
	}
	if err := s.updateManagedWeaponLevels(ctx, user, targetManagedWeaponIds); err != nil {
		span.RecordError(err)
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		serr.SetResponseMessage("failed at logic (updateWeaponLevel)")
		return nil, false, serr
	}
	if err := s.updateManagedNamedTypeLevels(ctx, user, targetNamedTypes); err != nil {
		span.RecordError(err)
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		serr.SetResponseMessage("failed at logic (updateNamedLevel)")
		return nil, false, serr
	}

	// Update stepCode
	if user.StepCode == value_user.StepCodeFirstQuestStart {
		user.StepCode = value_user.StepCodeFirstQuestDone
	}
	isFirstClear := !user.HasClearedAdv(uint64(quest.Id))
	user.AddClearedAdv(uint64(quest.Id))

	// Save the user
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ManagedNamedTypes: true,
		ManagedCharacters: true,
		ManagedWeapons:    true,
		ItemSummary:       true,
		AdvIds:            true,
	})
	if err != nil {
		span.RecordError(err)
		serr := errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
		return nil, false, serr
	}

	return user, isFirstClear, nil
}

func (s *playerQuestService) ResetQuest(
	ctx context.Context,
	internalUserId value_user.UserId,
) *errwrap.SparkleError {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "ResetQuestLog")
	defer span.End()

	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		span.RecordError(err)
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_NOT_FOUND, err)
	}

	user.LatestQuestLogID = -1

	// Update user
	if _, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{}); err != nil {
		span.RecordError(err)
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}

	return nil
}

func (s *playerQuestService) RetryQuest(
	ctx context.Context,
	internalUserId value_user.UserId,
) (*model_user.User, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "RetryQuest")
	defer span.End()

	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{})
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_PLAYER_NOT_FOUND, err)
	}

	consumed := user.ConsumeGem(uint32(value_quest.QuestRetryGemDefault))
	if !consumed {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_GEM_IS_SHORT, errors.New("not enough gems"))
	}

	// Update user
	user, err = s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{})
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return user, nil
}

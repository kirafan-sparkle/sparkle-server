package service

import (
	"context"
	"errors"

	schema_battle_party "gitlab.com/kirafan/sparkle/server/internal/application/schemas/battle_party"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

type PlayerBattlePartyService interface {
	// Get player battle party
	GetAllPlayerBattleParty(ctx context.Context, internalUserId value_user.UserId) ([]model_user.ManagedBattleParty, error)
	// Set player battle party
	SetAllPlayerBattleParty(ctx context.Context, internalUserId value_user.UserId, battleParties []schema_battle_party.BattlePartyMembers) error
	// Set player battle party name
	SetNamePlayerBattleParty(ctx context.Context, internalUserId value_user.UserId, managedBattlePartyId int, name string) error
}

type playerBattlePartyService struct {
	uu usecase.UserUsecase
}

func NewPlayerBattlePartyService(uu usecase.UserUsecase) PlayerBattlePartyService {
	return &playerBattlePartyService{uu}
}

func (s *playerBattlePartyService) GetAllPlayerBattleParty(ctx context.Context, internalUserId value_user.UserId) ([]model_user.ManagedBattleParty, error) {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "GetAllPlayerBattleParty")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx, internalUserId, repository.UserRepositoryParam{
		ManagedBattleParties: true,
	})
	if err != nil {
		return nil, err
	}
	return user.ManagedBattleParties, nil
}

func (s *playerBattlePartyService) SetAllPlayerBattleParty(ctx context.Context, internalUserId value_user.UserId, battlePartyMembers []schema_battle_party.BattlePartyMembers) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SetAllPlayerBattleParty")
	defer span.End()
	// Get user info
	user, err := s.uu.GetUserByInternalId(ctx,
		internalUserId,
		repository.UserRepositoryParam{
			ManagedBattleParties: true,
			ManagedCharacters:    true,
			ManagedWeapons:       true,
			ManagedMasterOrbs:    true,
		},
	)
	if err != nil {
		return err
	}

	existsManagedBattlePartyIds := make([]int, len(user.ManagedBattleParties))
	for i := 0; i < len(user.ManagedBattleParties); i++ {
		existsManagedBattlePartyIds[i] = user.ManagedBattleParties[i].ManagedBattlePartyId
	}
	existsManagedCharacterIds := make([]int64, len(user.ManagedCharacters))
	for i := 0; i < len(user.ManagedCharacters); i++ {
		existsManagedCharacterIds[i] = user.ManagedCharacters[i].ManagedCharacterId.ToInt64()
	}
	existsManagedWeaponIds := make([]int64, len(user.ManagedWeapons))
	for i := 0; i < len(user.ManagedWeapons); i++ {
		existsManagedWeaponIds[i] = int64(user.ManagedWeapons[i].ManagedWeaponId)
	}

	// Empty value be -1 if entire of the party are empty, otherwise the empty area be 0
	acceptedEmptyValues := []int64{-1, 0}
	for i := 0; i < len(battlePartyMembers); i++ {
		if !calc.Contains(existsManagedBattlePartyIds, int(battlePartyMembers[i].ManagedBattlePartyId)) {
			return errors.New("invalid parameters")
		}
		for i2 := 0; i2 < len(battlePartyMembers[i].ManagedCharacterIds); i2++ {
			refManagedCharacterId := int64(battlePartyMembers[i].ManagedCharacterIds[i2])
			if !calc.Contains(existsManagedCharacterIds, refManagedCharacterId) && !calc.Contains(acceptedEmptyValues, refManagedCharacterId) {
				return errors.New("invalid parameters")
			}
		}
		for i2 := 0; i2 < len(battlePartyMembers[i].ManagedWeaponIds); i2++ {
			refManagedWeaponId := battlePartyMembers[i].ManagedWeaponIds[i2]
			if !calc.Contains(existsManagedWeaponIds, refManagedWeaponId) && !calc.Contains(acceptedEmptyValues, refManagedWeaponId) {
				return errors.New("invalid parameters")
			}
		}
	}

	for i := 0; i < len(battlePartyMembers); i++ {
		user.ManagedBattleParties[i].ManagedCharacterId1 = battlePartyMembers[i].ManagedCharacterIds[0]
		user.ManagedBattleParties[i].ManagedCharacterId2 = battlePartyMembers[i].ManagedCharacterIds[1]
		user.ManagedBattleParties[i].ManagedCharacterId3 = battlePartyMembers[i].ManagedCharacterIds[2]
		user.ManagedBattleParties[i].ManagedCharacterId4 = battlePartyMembers[i].ManagedCharacterIds[3]
		user.ManagedBattleParties[i].ManagedCharacterId5 = battlePartyMembers[i].ManagedCharacterIds[4]
		user.ManagedBattleParties[i].ManagedWeaponId1 = int32(battlePartyMembers[i].ManagedWeaponIds[0])
		user.ManagedBattleParties[i].ManagedWeaponId2 = int32(battlePartyMembers[i].ManagedWeaponIds[1])
		user.ManagedBattleParties[i].ManagedWeaponId3 = int32(battlePartyMembers[i].ManagedWeaponIds[2])
		user.ManagedBattleParties[i].ManagedWeaponId4 = int32(battlePartyMembers[i].ManagedWeaponIds[3])
		user.ManagedBattleParties[i].ManagedWeaponId5 = int32(battlePartyMembers[i].ManagedWeaponIds[4])
		user.ManagedBattleParties[i].MasterOrbId = uint8(battlePartyMembers[i].MasterOrbId)
	}

	if _, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ManagedBattleParties: true,
	}); err != nil {
		return err
	}

	return nil
}

func (s *playerBattlePartyService) SetNamePlayerBattleParty(ctx context.Context, internalUserId value_user.UserId, managedBattlePartyId int, name string) error {
	ctx, span := observability.Tracer.StartAppServiceSpan(ctx, "SetNamePlayerBattleParty")
	defer span.End()
	user, err := s.uu.GetUserByInternalId(ctx,
		internalUserId,
		repository.UserRepositoryParam{
			ManagedBattleParties: true,
		},
	)
	if err != nil {
		return nil
	}
	// TODO: Refactor this process as user's method
	for i := 0; i < len(user.ManagedBattleParties); i++ {
		if user.ManagedBattleParties[i].ManagedBattlePartyId == managedBattlePartyId {
			user.ManagedBattleParties[i].Name = name
			break
		}
	}
	if _, err := s.uu.UpdateUser(ctx, user, repository.UserRepositoryParam{
		ManagedBattleParties: true,
	}); err != nil {
		return err
	}
	return nil
}

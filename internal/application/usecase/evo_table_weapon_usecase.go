package usecase

import (
	"context"

	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type EvoTableWeaponUsecase interface {
	GetEvolutionRecipe(ctx context.Context, weaponId value_weapon.WeaponId) (*model_evo_table.EvoTableWeapon, error)
}

type evoTableWeaponUsecase struct {
	rp repository.EvoTableWeaponRepository
}

func NewEvoTableWeaponUsecase(rp repository.EvoTableWeaponRepository) EvoTableWeaponUsecase {
	return &evoTableWeaponUsecase{rp}
}

func (u *evoTableWeaponUsecase) GetEvolutionRecipe(ctx context.Context, weaponId value_weapon.WeaponId) (*model_evo_table.EvoTableWeapon, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetEvolutionRecipe")
	defer span.End()
	res, err := u.rp.FindByWeaponId(ctx, weaponId)
	if err != nil {
		return nil, err
	}
	return res, nil
}

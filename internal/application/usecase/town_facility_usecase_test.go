package usecase

import (
	"context"
	"reflect"
	"testing"

	model_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/model/town_facility"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/value/town_facility"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

func Test_townFacilityUsecase_GetTownFacilityById(t *testing.T) {
	townFacilityRepository := persistence.NewTownFacilityRepositoryImpl(db)
	townFacilityUsecase := NewTownFacilityUsecase(townFacilityRepository)
	ctx := context.Background()

	tests := []struct {
		name       string
		facilityId uint32
		err        error
		want       *model_town_facility.TownFacility
	}{
		{
			name:       "GetTownFacilityById_success",
			facilityId: 1,
			err:        nil,
			want: &model_town_facility.TownFacility{
				FacilityId:       calc.ToPtr(uint32(1)),
				FacilityCategory: value_town_facility.FacilityCategoryGeneral,
				Name:             "汎用エリア",
				Detail:           "効果",
				MaxLevel:         0,
				MaxNum:           20,
				TitleType:        value_character.TitleTypeNone,
				ClassType:        value_character.ClassTypeNone,
				BuildTimeType:    value_town_facility.FacilityBuildTimeTypeNone,
				LevelUpListId:    value_town_facility.LevelUpListIdTitle,
				IsTutorial:       value.BoolLikeUIntFalse,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := townFacilityUsecase.GetTownFacilityById(ctx, tt.facilityId)
			if err != tt.err {
				t.Errorf("townFacilityUsecase.GetTownFacilityById() error = %v, wantErr %v", err, tt.err)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("townFacilityUsecase.GetTownFacilityById() = %v, want %v", got, tt.want)
			}
		})
	}
}

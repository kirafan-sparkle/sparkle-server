package usecase

import (
	"context"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_ability_board "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_board"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type UserAbilityBoardUsecase interface {
	AddAbilityBoard(ctx context.Context, userId value_user.UserId, abilityBoardId value_ability_board.AbilityBoardId, managedCharacterId value_user.ManagedCharacterId) (*model_user.ManagedAbilityBoard, *errwrap.SparkleError)
	UpdateAbilityBoard(ctx context.Context, userId value_user.UserId, managedAbilityBoard model_user.ManagedAbilityBoard) (*model_user.ManagedAbilityBoard, error)
	GetAbilityBoard(ctx context.Context, userId value_user.UserId, managedAbilityBoardId value_user.ManagedAbilityBoardId) (*model_user.ManagedAbilityBoard, error)
	GetAbilityBoards(ctx context.Context, userId value_user.UserId) ([]model_user.ManagedAbilityBoard, error)
}

type userAbilityBoardUsecase struct {
	rp repository.ManagedAbilityBoardRepository
}

func NewUserAbilityBoardUsecase(rp repository.ManagedAbilityBoardRepository) UserAbilityBoardUsecase {
	return &userAbilityBoardUsecase{rp}
}

func (u *userAbilityBoardUsecase) AddAbilityBoard(ctx context.Context, userId value_user.UserId, abilityBoardId value_ability_board.AbilityBoardId, managedCharacterId value_user.ManagedCharacterId) (*model_user.ManagedAbilityBoard, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "AddNewAbilityBoard")
	defer span.End()

	isExist, err := u.rp.IsAbilityBoardExist(ctx, userId, abilityBoardId)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	if isExist {
		return nil, errwrap.NewSparkleError(response.RESULT_INVALID_PARAMETERS, "ability board already exist")
	}

	abilityBoard := model_user.NewManagedAbilityBoard(userId, abilityBoardId, managedCharacterId)
	abilityBoard.Initialize()
	board, err := u.rp.InsertManagedAbilityBoard(ctx, userId, &abilityBoard)
	if err != nil {
		span.RecordError(err)
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return board, nil
}

func (u *userAbilityBoardUsecase) UpdateAbilityBoard(ctx context.Context, userId value_user.UserId, managedAbilityBoard model_user.ManagedAbilityBoard) (*model_user.ManagedAbilityBoard, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "UpdateAbilityBoard")
	defer span.End()

	abilityBoard, err := u.rp.UpdateManagedAbilityBoard(ctx, userId, &managedAbilityBoard)
	if err != nil {
		return nil, err
	}
	return abilityBoard, nil
}

func (u *userAbilityBoardUsecase) GetAbilityBoard(ctx context.Context, userId value_user.UserId, managedAbilityBoardId value_user.ManagedAbilityBoardId) (*model_user.ManagedAbilityBoard, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetAbilityBoard")
	defer span.End()

	abilityBoard, err := u.rp.FindManagedAbilityBoard(ctx, userId, managedAbilityBoardId)
	if err != nil {
		return nil, err
	}
	return abilityBoard, nil
}

func (u *userAbilityBoardUsecase) GetAbilityBoards(ctx context.Context, userId value_user.UserId) ([]model_user.ManagedAbilityBoard, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetAbilityBoards")
	defer span.End()

	abilityBoards, err := u.rp.FindManagedAbilityBoards(ctx, userId)
	if err != nil {
		return nil, err
	}
	return abilityBoards, nil
}

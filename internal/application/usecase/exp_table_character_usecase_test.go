package usecase

import (
	"context"
	"reflect"
	"testing"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

func Test_expTableCharacterUsecase_GetNextExpTableCharacter(t *testing.T) {
	p := persistence.NewExpTableCharacterRepositoryImpl(db)
	u := NewExpTableCharacterUsecase(p)
	ctx := context.Background()

	tests := []struct {
		name       string
		currentExp uint64
		want       *model_exp_table.ExpTableCharacter
		wantErr    bool
	}{
		{
			name:       "get next exp table character returns level 1 at exp 0",
			currentExp: 0,
			want: &model_exp_table.ExpTableCharacter{
				Level:               1,
				NextExp:             100,
				TotalExp:            100,
				RequiredCoinPerItem: 100,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table character returns level 2 at exp 101",
			currentExp: 101,
			want: &model_exp_table.ExpTableCharacter{
				Level:               2,
				NextExp:             200,
				TotalExp:            300,
				RequiredCoinPerItem: 120,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table character returns level 100 at exp 16779101",
			currentExp: 16779101,
			want: &model_exp_table.ExpTableCharacter{
				Level:               100,
				NextExp:             9999999,
				TotalExp:            26779099,
				RequiredCoinPerItem: 0,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := u.GetNextExpTableCharacter(ctx, tt.currentExp)
			if (err != nil) != tt.wantErr {
				t.Errorf("expTableCharacterUsecase.GetNextExpTableCharacter() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("expTableCharacterUsecase.GetNextExpTableCharacter() = %v, want %v", got, tt.want)
			}
		})
	}
}

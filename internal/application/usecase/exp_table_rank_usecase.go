package usecase

import (
	"context"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type ExpTableRankUsecase interface {
	GetNextExpTableRank(ctx context.Context, currentExp uint64) (*model_exp_table.ExpTableRank, error)
}

type expTableRankUsecase struct {
	rp repository.ExpTableRankRepository
}

func NewExpTableRankUsecase(rp repository.ExpTableRankRepository) ExpTableRankUsecase {
	return &expTableRankUsecase{rp}
}

func (uc *expTableRankUsecase) GetNextExpTableRank(ctx context.Context, currentExp uint64) (*model_exp_table.ExpTableRank, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetNextExpTableRank")
	defer span.End()
	criteria := map[string]interface{}{
		"total_exp > ?": currentExp,
	}
	expTableRank, err := uc.rp.FindExpTableRank(ctx, nil, criteria)
	if err != nil {
		return nil, err
	}
	return expTableRank, nil
}

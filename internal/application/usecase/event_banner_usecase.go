package usecase

import (
	"context"

	model_event_banner "gitlab.com/kirafan/sparkle/server/internal/domain/model/event_banner"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type EventBannerUsecase interface {
	GetAllEventBanners(ctx context.Context) ([]*model_event_banner.EventBanner, error)
}

type eventBannerUsecase struct {
	rp repository.EventBannerRepository
}

func NewEventBannerUsecase(rp repository.EventBannerRepository) EventBannerUsecase {
	return &eventBannerUsecase{rp: rp}
}

func (uc *eventBannerUsecase) GetAllEventBanners(ctx context.Context) ([]*model_event_banner.EventBanner, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetAllEventBanners")
	defer span.End()
	eventBanners, err := uc.rp.GetEventBanners(ctx)
	if err != nil {
		return nil, err
	}
	return eventBanners, nil
}

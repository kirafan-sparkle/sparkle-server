package usecase

import (
	"context"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type CharacterQuestUsecase interface {
	GetCharacterQuest(ctx context.Context, internalUserId value_user.UserId, questId uint) (*model_quest.CharacterQuest, error)
	GetCharacterQuests(ctx context.Context, internalUserId value_user.UserId) ([]model_quest.CharacterQuest, error)
}

type characterQuestUsecase struct {
	eqr repository.CharacterQuestRepository
}

func NewCharacterQuestUsecase(eqr repository.CharacterQuestRepository) CharacterQuestUsecase {
	return &characterQuestUsecase{eqr}
}

func (uc *characterQuestUsecase) GetCharacterQuest(ctx context.Context, internalUserId value_user.UserId, characterQuestId uint) (*model_quest.CharacterQuest, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetCharacterQuest")
	defer span.End()
	characterQuest, err := uc.eqr.GetCharacterQuest(ctx, internalUserId, characterQuestId)
	if err != nil {
		return nil, err
	}
	return characterQuest, nil
}

func (uc *characterQuestUsecase) GetCharacterQuests(ctx context.Context, internalUserId value_user.UserId) ([]model_quest.CharacterQuest, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetCharacterQuests")
	defer span.End()
	characterQuests, err := uc.eqr.GetCharacterQuests(ctx, internalUserId)
	if err != nil {
		return nil, err
	}
	return characterQuests, nil
}

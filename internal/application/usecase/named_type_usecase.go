package usecase

import (
	"context"

	model_named_type "gitlab.com/kirafan/sparkle/server/internal/domain/model/named_type"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type NamedTypeUsecase interface {
	GetNamedTypeById(ctx context.Context, id uint) (*model_named_type.NamedType, error)
}

type namedTypeUsecase struct {
	rp repository.NamedTypeRepository
}

func NewNamedTypeUsecase(rp repository.NamedTypeRepository) NamedTypeUsecase {
	return &namedTypeUsecase{rp}
}

func (uc *namedTypeUsecase) GetNamedTypeById(ctx context.Context, id uint) (*model_named_type.NamedType, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetNamedTypeById")
	defer span.End()
	namedType, err := uc.rp.FindNamedType(ctx, &model_named_type.NamedType{NamedType: uint(id)}, nil, nil)
	if err != nil {
		return nil, err
	}
	return namedType, nil
}

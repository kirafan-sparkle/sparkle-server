package usecase

import (
	"context"
	"errors"
	"time"

	model_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/model/login_bonus"
	model_mission "gitlab.com/kirafan/sparkle/server/internal/domain/model/mission"
	model_present "gitlab.com/kirafan/sparkle/server/internal/domain/model/present"
	model_trade "gitlab.com/kirafan/sparkle/server/internal/domain/model/trade"
	model_training "gitlab.com/kirafan/sparkle/server/internal/domain/model/training"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
	value_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/value/login_bonus"
	value_mission "gitlab.com/kirafan/sparkle/server/internal/domain/value/mission"
	value_training "gitlab.com/kirafan/sparkle/server/internal/domain/value/training"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type UserUsecase interface {
	CreateUser(
		ctx context.Context,
		uuid string,
		username string,
		presents []*model_present.Present,
		missions []*model_mission.Mission,
		loginBonuses []*model_login_bonus.LoginBonus,
		tradeRecipes []model_trade.TradeRecipe,
		trainings []*model_training.Training,
	) (*model_user.User, error)
	UpdateUser(ctx context.Context, user *model_user.User, param repository.UserRepositoryParam) (*model_user.User, error)
	SetupUser(ctx context.Context, userId value_user.UserId) (*model_user.User, error)
	GetUserByInternalId(ctx context.Context, internalId value_user.UserId, param repository.UserRepositoryParam) (*model_user.User, error)
	GetUserBySession(ctx context.Context, deviceUUId string, sessionUUId string) (*model_user.User, error)
	GetUserByFriendCode(ctx context.Context, code value_friend.FriendOrCheatCode) (*model_user.User, error)
	GetUserByMoveCode(ctx context.Context, code string) (*model_user.User, error)
	UpsertUserClearRank(ctx context.Context, internalId value_user.UserId, clearRank model_user.UserQuestClearRank) error
	GetUserClearRanks(ctx context.Context, internalId value_user.UserId) (model_user.UserQuestClearRanks, error)
}

var ErrUserSetupAlreadyDone = errors.New("the user setup is already done")
var ErrUserSetupNotEnoughCharacter = errors.New("the user does not have enough character")
var ErrUserSetupNotDesiredStep = errors.New("the user stepCode is not desired step")

type userUsecase struct {
	ur  repository.UserRepository
	ucr repository.UserClearRankRepository
}

func NewUserUsecase(ur repository.UserRepository, ucr repository.UserClearRankRepository) UserUsecase {
	return &userUsecase{ur, ucr}
}

func (uu *userUsecase) CreateUser(ctx context.Context, uuid string, name string, presents []*model_present.Present, missions []*model_mission.Mission, loginBonuses []*model_login_bonus.LoginBonus, tradeRecipes []model_trade.TradeRecipe, trainings []*model_training.Training) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "CreateUser")
	defer span.End()
	if u, err := uu.ur.FindUserByUUID(ctx, uuid, repository.UserRepositoryParam{}); u != nil {
		return nil, err
	}
	user := model_user.NewUser(uuid, name)
	// Inject initial missions
	var userMissions []model_user.UserMission
	for _, mission := range missions {
		userMissions = append(userMissions,
			model_user.UserMission{
				UserId:    user.Id,
				LimitTime: time.Now().AddDate(0, 0, int(value_mission.EXPIRE_TUTORIAL_MISSION)),
				Mission:   *mission,
				Rate:      0,
				State:     value_mission.MissionStateNotCleared,
			},
		)
	}
	user.Missions = userMissions
	// Inject initial presents
	var userPresents []model_user.UserPresent
	for _, present := range presents {
		userPresents = append(userPresents,
			model_user.UserPresent{
				UserId:     user.Id,
				Present:    *present,
				ReceivedAt: nil,
				DeadlineAt: time.Now().AddDate(0, 0, int(value_mission.EXPIRE_TUTORIAL_PRESENT)),
				Type:       present.Type,
				Amount:     present.Amount,
				ObjectId:   present.ObjectId,
			},
		)
	}
	user.Presents = userPresents
	// Inject initial trainings
	userTrainings := make([]*model_user.UserTraining, len(trainings))
	for i, training := range trainings {
		userTrainings[i] = model_user.NewUserTraining(user.Id, training.TrainingId)
	}
	user.Trainings = userTrainings
	// Inject initial training slots
	slotIds := []value_training.SlotId{value_training.SlotId0, value_training.SlotId1, value_training.SlotId2, value_training.SlotId3}
	userTrainingSlots := make([]*model_user.UserTrainingSlot, len(slotIds))
	for i, slotId := range slotIds {
		// NOTE: the slot should be opened depending the user rank but the client ignores openState???
		// TODO: server side slotOpen validation requires correct value of isOpen state, please implement me later.
		userTrainingSlots[i] = model_user.NewUserTrainingSlot(user.Id, slotId, value.BoolLikeUIntTrue)
		// if i <= 1 {
		// 	userTrainingSlots[i] = model_user.NewUserTrainingSlot(user.Id, slotId, value.BoolLikeUIntTrue)
		// } else {
		// 	userTrainingSlots[i] = model_user.NewUserTrainingSlot(user.Id, slotId, value.BoolLikeUIntFalse)
		// }
	}
	user.TrainingSlots = userTrainingSlots

	// Inject initial gacha
	user.Gachas = []model_user.UserGacha{
		model_user.NewUserGacha(user.Id, 1),
	}
	// Inject initial login bonuses
	var userLoginBonuses []model_user.UserLoginBonus
	for _, loginBonus := range loginBonuses {
		userLoginBonuses = append(userLoginBonuses,
			model_user.UserLoginBonus{
				CreatedAt:          time.Now(),
				UpdatedAt:          time.Now().AddDate(0, 0, -1),
				DeletedAt:          nil,
				LoginBonusDayIndex: value_login_bonus.LoginBonusDayIndexDay1,
				LoginBonusId:       loginBonus.BonusId,
				UserId:             user.Id,
			},
		)
	}
	user.LoginBonuses = userLoginBonuses
	// Inject initial trade recipes
	userTradeRecipes := make([]model_user.UserTradeRecipe, len(tradeRecipes))
	for i, tradeRecipe := range tradeRecipes {
		userTradeRecipes[i] = model_user.UserTradeRecipe{
			CreatedAt:         time.Now(),
			UpdatedAt:         time.Now(),
			TotalTradeCount:   0,
			MonthlyTradeCount: 0,
			RecipeId:          tradeRecipe.Id,
			UserId:            user.Id,
		}
	}
	user.Trades = userTradeRecipes

	createdUser, err := uu.ur.CreateUser(ctx, &user)
	if err != nil {
		return nil, err
	}

	newManagedRooms := model_user.NewInitialManagedRooms(user.Id, createdUser.ManagedRoomObjects)
	user.ManagedRooms = newManagedRooms

	initializedUser, err := uu.ur.UpdateUser(ctx, &user, repository.UserRepositoryParam{ManagedRooms: true})
	if err != nil {
		return nil, err
	}
	return initializedUser, nil
}

func (uu *userUsecase) SetupUser(ctx context.Context, userId value_user.UserId) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "SetupUser")
	defer span.End()
	u, err := uu.ur.FindUserById(ctx, userId, repository.UserRepositoryParam{Entire: true})
	if err != nil {
		return nil, err
	}
	if u.StepCode != value_user.StepCodeAdvCreaDone {
		return u, ErrUserSetupNotDesiredStep
	}
	if len(u.ManagedCharacters) < 5 {
		return u, ErrUserSetupNotEnoughCharacter
	}
	if u.ManagedBattleParties[0].ManagedCharacterId1 != -1 {
		return u, ErrUserSetupAlreadyDone
	}
	if len(u.ManagedFieldPartyMembers) > 0 {
		return u, ErrUserSetupAlreadyDone
	}
	if len(u.FavoriteMembers) > 0 {
		return u, ErrUserSetupAlreadyDone
	}

	// Update battle party
	managedCharacterLen := len(u.ManagedCharacters)
	newManagedBattleParty := model_user.ManagedBattleParty{
		ManagedBattlePartyId: u.ManagedBattleParties[0].ManagedBattlePartyId,
		UserId:               u.Id,
		Name:                 "パーティー1",
		CostLimit:            0,
		ManagedCharacterId1:  int64(u.ManagedCharacters[managedCharacterLen-1].ManagedCharacterId),
		ManagedCharacterId2:  int64(u.ManagedCharacters[managedCharacterLen-2].ManagedCharacterId),
		ManagedCharacterId3:  int64(u.ManagedCharacters[managedCharacterLen-3].ManagedCharacterId),
		ManagedCharacterId4:  int64(u.ManagedCharacters[managedCharacterLen-4].ManagedCharacterId),
		ManagedCharacterId5:  int64(u.ManagedCharacters[managedCharacterLen-5].ManagedCharacterId),
		ManagedWeaponId1:     0,
		ManagedWeaponId2:     0,
		ManagedWeaponId3:     0,
		ManagedWeaponId4:     0,
		ManagedWeaponId5:     0,
		MasterOrbId:          1,
	}
	u.ManagedBattleParties[0] = newManagedBattleParty

	// Update field members
	var newManagedFieldPartyMembers []model_user.ManagedFieldPartyMember
	for i := 0; i < 5; i++ {
		newManagedFieldPartyMembers = append(newManagedFieldPartyMembers, model_user.ManagedFieldPartyMember{
			ManagedCharacterId: u.ManagedCharacters[managedCharacterLen-1-i].ManagedCharacterId,
			CharacterId:        uint64(u.ManagedCharacters[managedCharacterLen-1-i].CharacterId),
			UserId:             u.Id,
			LiveIdx:            uint8(i),
			ManagedFacilityId:  0,
			RoomId:             1,
			ScheduleTable:      nil,
			ScheduleId:         -1,
			ScheduleTag:        -1,
			TouchItemResultNo:  -1,
			Flag:               0,
			PartyDropPresents:  nil,
		})
	}
	u.ManagedFieldPartyMembers = newManagedFieldPartyMembers

	// Update favorite members
	var newManagedFavoriteMembers []model_user.FavoriteMember
	for i := 0; i < 5; i++ {
		newManagedFavoriteMembers = append(newManagedFavoriteMembers, model_user.FavoriteMember{
			ManagedCharacterId: u.ManagedCharacters[managedCharacterLen-1-i].ManagedCharacterId,
			CharacterId:        u.ManagedCharacters[managedCharacterLen-1-i].CharacterId,
			FavoriteIndex:      uint8(i),
			ArousalLevel:       u.ManagedCharacters[managedCharacterLen-1-i].ArousalLevel,
			UserId:             u.Id,
		})
	}
	u.FavoriteMembers = newManagedFavoriteMembers

	// Update user stepCode
	u.StepCode = value_user.StepCodeGachaDrawDone

	newUser, err := uu.ur.UpdateUser(ctx, u, repository.UserRepositoryParam{
		ManagedCharacters:        true,
		ManagedBattleParties:     true,
		ManagedFieldPartyMembers: true,
		FavoriteMembers:          true,
	})
	if err != nil {
		return nil, err
	}
	return newUser, nil
}

func (uu *userUsecase) UpdateUser(ctx context.Context, user *model_user.User, param repository.UserRepositoryParam) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "UpdateUser")
	defer span.End()
	newUser, err := uu.ur.UpdateUser(ctx, user, param)
	if err != nil {
		return nil, err
	}
	return newUser, nil
}

func (uu *userUsecase) GetUserByInternalId(ctx context.Context, internalId value_user.UserId, param repository.UserRepositoryParam) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetUserByInternalId")
	defer span.End()
	foundUser, err := uu.ur.FindUserById(ctx, internalId, param)
	if err != nil {
		return nil, err
	}
	foundUser.RefreshStamina()
	return foundUser, nil
}

func (uu *userUsecase) GetUserBySession(ctx context.Context, deviceUUId string, sessionUUId string) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetUserBySession")
	defer span.End()
	foundUser, err := uu.ur.FindUserBySession(ctx, deviceUUId, sessionUUId)
	if err != nil {
		return nil, err
	}
	foundUser.RefreshStamina()
	return foundUser, nil
}

func (uu *userUsecase) GetUserByFriendCode(ctx context.Context, code value_friend.FriendOrCheatCode) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetUserByFriendCode")
	defer span.End()
	foundUser, err := uu.ur.FindUserByFriendCode(ctx, code)
	if err != nil {
		return nil, err
	}
	return foundUser, nil
}

func (uu *userUsecase) GetUserByMoveCode(ctx context.Context, code string) (*model_user.User, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetUserByMoveCode")
	defer span.End()
	foundUser, err := uu.ur.FindUserByMoveCode(ctx, code)
	if err != nil {
		return nil, err
	}
	return foundUser, nil
}

func (uu *userUsecase) GetUserClearRanks(ctx context.Context,
	internalId value_user.UserId,
) (model_user.UserQuestClearRanks, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetUserClearRanks")
	defer span.End()

	userQuestClearRanks, err := uu.ucr.GetClearRanks(ctx, internalId)
	if err != nil {
		return nil, err
	}
	return userQuestClearRanks, nil
}

func (uu *userUsecase) UpsertUserClearRank(
	ctx context.Context,
	internalId value_user.UserId,
	clearRank model_user.UserQuestClearRank,
) error {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "UpsertUserClearRank")
	defer span.End()

	if err := uu.ucr.UpsertClearRank(ctx, internalId, clearRank); err != nil {
		return err
	}
	return nil
}

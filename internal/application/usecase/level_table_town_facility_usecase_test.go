package usecase

import (
	"context"
	"reflect"
	"testing"

	model_level_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/level_table"
	value_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/value/town_facility"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

func Test_levelTableTownFacilityUsecase_GetCurrentLevelTableTownFacility(t *testing.T) {
	p := persistence.NewLevelTableTownFacilityRepositoryImpl(db)
	uc := NewLevelTableTownFacilityUsecase(p)
	ctx := context.Background()

	type args struct {
		levelUpListId value_town_facility.LevelUpListId
		currentLevel  uint8
	}
	tests := []struct {
		name    string
		uc      LevelTableTownFacilityUsecase
		args    args
		want    *model_level_table.LevelTableTownFacility
		wantErr bool
	}{
		{
			name: "GetCurrentLevelTableTownFacility success with levelUpListIdGold and currentLevel 1",
			uc:   uc,
			args: args{
				levelUpListId: value_town_facility.LevelUpListIdGold,
				currentLevel:  1,
			},
			want: &model_level_table.LevelTableTownFacility{
				LevelTableFacilityId:  calc.ToPtr(uint(24)),
				LevelUpListId:         3,
				TargetLevel:           1,
				GoldAmountBuy:         1000,
				KiraraPointAmountBuy:  0,
				GoldAmountSell:        250,
				KiraraPointAmountSell: 0,
				ReqUserLv:             0,
				BuildTimeSeconds:      0,
				IntervalTimeSeconds:   60,
				MaxGenMinutesOrItems:  30,
				MaxKiraraPoint:        0,
				IsRoomFunc:            0,
				IsSubRoomFunc:         0,
				IsSubRoomOpen:         0,
				IsTownFunc:            0,
				FreeStepCode:          0,
				FieldItemDropProbabilities: []model_level_table.LevelTableTownFacilityFieldItemDropProbability{
					{
						LevelTableFacilityId:       calc.ToPtr(uint(24)),
						FieldItemDropProbabilityId: 21,
						FieldItemDropProbability:   100,
					},
				},
				FieldItemDropIds: []model_level_table.LevelTableTownFacilityFieldItemDropId{
					{
						LevelTableFacilityId: calc.ToPtr(uint(24)),
						FieldItemDropListId:  21,
						FieldItemDropId:      130101,
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.uc.GetCurrentLevelTableTownFacility(ctx, tt.args.levelUpListId, tt.args.currentLevel)
			if (err != nil) != tt.wantErr {
				t.Errorf("levelTableTownFacilityUsecase.GetCurrentLevelTableTownFacility() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("levelTableTownFacilityUsecase.GetCurrentLevelTableTownFacility() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_levelTableTownFacilityUsecase_GetNextLevelTableTownFacility(t *testing.T) {
	p := persistence.NewLevelTableTownFacilityRepositoryImpl(db)
	uc := NewLevelTableTownFacilityUsecase(p)
	ctx := context.Background()

	type args struct {
		levelUpListId value_town_facility.LevelUpListId
		currentLevel  uint8
	}
	tests := []struct {
		name    string
		uc      LevelTableTownFacilityUsecase
		args    args
		want    *model_level_table.LevelTableTownFacility
		wantErr bool
	}{
		{
			name: "GetNextLevelTableTownFacility success with levelUpListIdGold and currentLevel 1",
			uc:   uc,
			args: args{
				levelUpListId: value_town_facility.LevelUpListIdGold,
				currentLevel:  1,
			},
			want: &model_level_table.LevelTableTownFacility{
				LevelTableFacilityId:  calc.ToPtr(uint(25)),
				LevelUpListId:         3,
				TargetLevel:           2,
				GoldAmountBuy:         2000,
				KiraraPointAmountBuy:  750,
				GoldAmountSell:        500,
				KiraraPointAmountSell: 0,
				ReqUserLv:             0,
				BuildTimeSeconds:      10,
				IntervalTimeSeconds:   60,
				MaxGenMinutesOrItems:  50,
				IsRoomFunc:            0,
				MaxKiraraPoint:        0,
				IsSubRoomFunc:         0,
				IsSubRoomOpen:         0,
				IsTownFunc:            0,
				FreeStepCode:          0,
				FieldItemDropProbabilities: []model_level_table.LevelTableTownFacilityFieldItemDropProbability{
					{
						LevelTableFacilityId:       calc.ToPtr(uint(25)),
						FieldItemDropProbabilityId: 22,
						FieldItemDropProbability:   100,
					},
				},
				FieldItemDropIds: []model_level_table.LevelTableTownFacilityFieldItemDropId{
					{
						LevelTableFacilityId: calc.ToPtr(uint(25)),
						FieldItemDropListId:  22,
						FieldItemDropId:      130102,
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.uc.GetNextLevelTableTownFacility(ctx, tt.args.levelUpListId, tt.args.currentLevel)
			if (err != nil) != tt.wantErr {
				t.Errorf("levelTableTownFacilityUsecase.GetNextLevelTableTownFacility() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("levelTableTownFacilityUsecase.GetNextLevelTableTownFacility() = %v, want %v", got, tt.want)
			}
		})
	}
}

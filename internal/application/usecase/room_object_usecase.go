package usecase

import (
	"context"

	model_room_object "gitlab.com/kirafan/sparkle/server/internal/domain/model/room_object"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type RoomObjectUsecase interface {
	GetAll(ctx context.Context) ([]*model_room_object.RoomObject, error)
	GetRoomObject(ctx context.Context, id uint32) (*model_room_object.RoomObject, error)
}

type roomObjectUsecase struct {
	ro repository.RoomObjectRepository
}

func NewRoomObjectUsecase(ro repository.RoomObjectRepository) RoomObjectUsecase {
	return &roomObjectUsecase{ro}
}

func (cu *roomObjectUsecase) GetAll(ctx context.Context) ([]*model_room_object.RoomObject, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetAll")
	defer span.End()
	roomObjects, err := cu.ro.FindRoomObjects(ctx, nil, nil, nil)
	if err != nil {
		return nil, err
	}
	return roomObjects, nil
}

func (cu *roomObjectUsecase) GetRoomObject(ctx context.Context, id uint32) (*model_room_object.RoomObject, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetRoomObject")
	defer span.End()
	roomObject, err := cu.ro.FindRoomObject(ctx, uint(id))
	if err != nil {
		return nil, err
	}
	return roomObject, nil
}

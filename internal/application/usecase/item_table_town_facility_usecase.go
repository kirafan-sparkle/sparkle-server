package usecase

import (
	"context"

	model_item_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/item_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type ItemTableTownFacilityUsecase interface {
	GetItemTableTownFacility(ctx context.Context, itemNo uint32) ([]*model_item_table.ItemTableTownFacility, error)
}

type itemTableTownFacilityUsecase struct {
	rp repository.ItemTableTownFacilityRepository
}

func NewItemTableTownFacilityUsecase(rp repository.ItemTableTownFacilityRepository) ItemTableTownFacilityUsecase {
	return &itemTableTownFacilityUsecase{rp}
}

func (uc *itemTableTownFacilityUsecase) GetItemTableTownFacility(ctx context.Context, itemNo uint32) ([]*model_item_table.ItemTableTownFacility, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetItemTableTownFacility")
	defer span.End()
	itemTableTownFacilities, err := uc.rp.FindItemTableTownFacility(ctx, itemNo)
	if err != nil {
		return nil, err
	}
	return itemTableTownFacilities, nil
}

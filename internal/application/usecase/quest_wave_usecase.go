package usecase

import (
	"context"
	"math/rand"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

type QuestWaveUsecase interface {
	GetQuestWave(ctx context.Context, waveId int64) ([]*model_quest.QuestWave, [][]model_quest.QuestWaveDrop, error)
}

type questWaveUsecase struct {
	rp repository.QuestWaveRepository
}

func NewQuestWaveUsecase(rp repository.QuestWaveRepository) QuestWaveUsecase {
	return &questWaveUsecase{rp}
}

func (uc *questWaveUsecase) GetQuestWave(ctx context.Context, waveId int64) ([]*model_quest.QuestWave, [][]model_quest.QuestWaveDrop, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetQuestWave")
	defer span.End()
	questWaves, err := uc.rp.FindQuestWaves(ctx, &model_quest.QuestWave{WaveId: waveId}, nil, nil)
	if err != nil {
		return nil, nil, err
	}
	var questItems [][]model_quest.QuestWaveDrop
	for _, questWave := range questWaves {
		// Randomize the enemy
		if questWave.EnemyRandomId != -1 {
			enemyRandoms, err := uc.rp.FindQuestWaveRandoms(
				ctx,
				&model_quest.QuestWaveRandom{EnemyRandomId: questWave.EnemyRandomId}, nil, nil,
			)
			if err != nil {
				return nil, nil, err
			}
			var weights []int64
			for _, enemyRandom := range enemyRandoms {
				weights = append(weights, int64(enemyRandom.EnemyRandomProbability))
			}
			choiceIndex := calc.CumulativeRandomChoice(weights, nil)
			questWave.EnemyId = int64(enemyRandoms[choiceIndex].EnemyId)
			questWave.EnemyDropId = int64(enemyRandoms[choiceIndex].EnemyDropId)
			questWave.EnemyDisplayScale = enemyRandoms[choiceIndex].EnemyDisplayScale
			questWave.EnemyLv = enemyRandoms[choiceIndex].EnemyLv
		}
		// Calculate the enemy drop
		enemyDropItems := make([]model_quest.QuestWaveDrop, 0)
		if questWave.EnemyDropId != -1 {
			enemyDrops, err := uc.rp.FindQuestWaveDrops(
				ctx,
				&model_quest.QuestWaveDrop{EnemyDropId: questWave.EnemyDropId}, nil, nil,
			)
			if err != nil {
				return nil, nil, err
			}
			for _, enemyDrop := range enemyDrops {
				value := rand.Intn(100)
				if value <= int(enemyDrop.DropItemProbability) {
					enemyDropItems = append(enemyDropItems, model_quest.QuestWaveDrop{
						EnemyDropId:         questWave.EnemyDropId,
						DropItemProbability: enemyDrop.DropItemProbability,
						DropItemId:          enemyDrop.DropItemId,
						DropItemAmount:      enemyDrop.DropItemAmount,
					})
				}
			}
		}
		questItems = append(questItems, enemyDropItems)
	}

	return questWaves, questItems, nil
}

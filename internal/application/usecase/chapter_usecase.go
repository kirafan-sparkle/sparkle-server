package usecase

import (
	"context"
	"time"

	model_chapter "gitlab.com/kirafan/sparkle/server/internal/domain/model/chapter"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type ChapterUsecase interface {
	GetAll(ctx context.Context) ([]*model_chapter.Chapter, *time.Time, error)
}

type chapterUsecase struct {
	cr repository.ChapterRepository
}

func NewChapterUsecase(qr repository.ChapterRepository) ChapterUsecase {
	return &chapterUsecase{cr: qr}
}

func (cu *chapterUsecase) GetAll(ctx context.Context) ([]*model_chapter.Chapter, *time.Time, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetAll")
	defer span.End()
	chapters, err := cu.cr.GetAll(ctx)
	if err != nil {
		return nil, nil, err
	}
	var latestTime time.Time
	for _, chapter := range chapters {
		if chapter.NewDispEndAt != nil && chapter.NewDispEndAt.After(latestTime) {
			latestTime = *chapter.NewDispEndAt
		}
	}
	return chapters, &latestTime, nil
}

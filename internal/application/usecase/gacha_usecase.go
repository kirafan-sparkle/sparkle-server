package usecase

import (
	"context"
	"errors"
	"time"

	schema_gacha "gitlab.com/kirafan/sparkle/server/internal/application/schemas/gacha"
	model_gacha "gitlab.com/kirafan/sparkle/server/internal/domain/model/gacha"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_gacha "gitlab.com/kirafan/sparkle/server/internal/domain/value/gacha"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/gacha"
)

type GachaUsecase interface {
	GetAvailableGachas(ctx context.Context) ([]*model_gacha.Gacha, error)
	GetInitialGacha(ctx context.Context) (*model_gacha.Gacha, error)
	GetSpecificGacha(ctx context.Context, gachaId uint) (*model_gacha.Gacha, error)
	IsGachaAvailable(ctx context.Context, gachaId uint) (bool, error)
	DrawGacha(ctx context.Context, gachaId uint, param schema_gacha.GachaDrawParamSchema) ([]value_character.CharacterId, error)
}

type gachaUsecase struct {
	rp           repository.GachaRepository
	gachaHandler gacha.GachaHandler
}

func NewGachaUsecase(rp repository.GachaRepository) GachaUsecase {
	gachaHandler := gacha.NewGachaHandler(nil)
	return &gachaUsecase{rp, gachaHandler}
}

func (uc *gachaUsecase) GetAvailableGachas(ctx context.Context) ([]*model_gacha.Gacha, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetAvailableGachas")
	defer span.End()
	now := time.Now()
	criteria := map[string]interface{}{
		"disp_start_at <= ?": now,
		"disp_end_at >= ?":   now,
	}
	gachas, err := uc.rp.FindGachas(ctx, nil, criteria, nil)
	if err != nil {
		return nil, err
	}
	return gachas, nil
}

func (uc *gachaUsecase) GetInitialGacha(ctx context.Context) (*model_gacha.Gacha, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetInitialGacha")
	defer span.End()
	gacha, err := uc.rp.FindGacha(ctx, &model_gacha.Gacha{GachaId: 1}, nil, nil)
	if err != nil {
		return nil, err
	}
	return gacha, nil
}

func (uc *gachaUsecase) GetSpecificGacha(ctx context.Context, gachaId uint) (*model_gacha.Gacha, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetSpecificGacha")
	defer span.End()
	criteria := map[string]interface{}{
		"gacha_id": gachaId,
	}
	gacha, err := uc.rp.FindGacha(ctx, nil, criteria, &[]string{"GachaTables"})
	if err != nil {
		return nil, err
	}
	return gacha, nil
}

func (uc *gachaUsecase) IsGachaAvailable(ctx context.Context, gachaId uint) (bool, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "IsGachaAvailable")
	defer span.End()
	_, err := uc.GetSpecificGacha(ctx, gachaId)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (uc *gachaUsecase) formatGachaTableForRoll(ctx context.Context, tables []model_gacha.GachaTable) (gacha.GachaCharacterDrops, gacha.GachaCharacterDrops) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "formatGachaTableForRoll")
	defer span.End()
	drops := gacha.GachaCharacterDrops{
		gacha.Rarity5: {},
		gacha.Rarity4: {},
		gacha.Rarity3: {},
	}
	pickUps := gacha.GachaCharacterDrops{
		gacha.Rarity5: {},
		gacha.Rarity4: {},
		gacha.Rarity3: {},
	}
	for _, drop := range tables {
		if !drop.Pickup {
			switch drop.Rarity {
			case value_character.CharacterRarityStar5:
				drops[gacha.Rarity5] = append(drops[gacha.Rarity5], uint32(drop.CharacterId))
			case value_character.CharacterRarityStar4:
				drops[gacha.Rarity4] = append(drops[gacha.Rarity4], uint32(drop.CharacterId))
			case value_character.CharacterRarityStar3:
				drops[gacha.Rarity3] = append(drops[gacha.Rarity3], uint32(drop.CharacterId))
			}
		} else {
			switch drop.Rarity {
			case value_character.CharacterRarityStar5:
				pickUps[gacha.Rarity5] = append(pickUps[gacha.Rarity5], uint32(drop.CharacterId))
			case value_character.CharacterRarityStar4:
				pickUps[gacha.Rarity4] = append(pickUps[gacha.Rarity4], uint32(drop.CharacterId))
			case value_character.CharacterRarityStar3:
				pickUps[gacha.Rarity3] = append(pickUps[gacha.Rarity3], uint32(drop.CharacterId))
			}
		}
	}
	return drops, pickUps
}

func (uc *gachaUsecase) DrawGacha(ctx context.Context, gachaId uint, param schema_gacha.GachaDrawParamSchema) ([]value_character.CharacterId, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "DrawGacha")
	defer span.End()
	gacha, err := uc.GetSpecificGacha(ctx, gachaId)
	if err != nil {
		return nil, err
	}
	var characterIds []value_character.CharacterId
	switch gacha.Type {
	case value_gacha.GachaTypeSelectable:
		characterId, err := value_character.NewCharacterId(uint32(param.SelectedCharacterId))
		if err != nil {
			return nil, err
		}
		characterIds = append(characterIds, characterId)
	case value_gacha.GachaTypeStepup:
		fallthrough
	case value_gacha.GachaTypePickup:
		fallthrough
	case value_gacha.GachaTypeNormal:
		// Format request
		times := 10
		if !param.Is10Roll {
			times = 1
		}
		drops, pickUps := uc.formatGachaTableForRoll(ctx, gacha.GachaTables)
		// Roll the gacha
		results, err := uc.gachaHandler.Roll(times, param.IsChanceUp, drops, pickUps)
		if err != nil {
			return nil, err
		}
		// Format result
		characterIdResults := make([]value_character.CharacterId, len(results))
		for i, result := range results {
			characterId, err := value_character.NewCharacterId(uint32(result))
			if err != nil {
				return nil, err
			}
			characterIdResults[i] = characterId
		}
		return characterIdResults, nil
	default:
		return nil, errors.New("not implemented gacha type")
	}
	return characterIds, nil
}

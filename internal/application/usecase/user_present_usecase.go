package usecase

import (
	"context"

	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type UserPresentUsecase interface {
	GetReceivableUserPresents(ctx context.Context, userId value_user.UserId) ([]*model_user.UserPresent, error)
	GetReceivedUserPresents(ctx context.Context, userId value_user.UserId) ([]*model_user.UserPresent, error)
	ReceiveUserPresents(ctx context.Context, userId value_user.UserId, managedPresentIds []int64) error
	DeleteExpiredUserPresents(ctx context.Context, userId value_user.UserId) error
}

type userPresentUsecase struct {
	rp repository.UserPresentRepository
}

func NewUserPresentUsecase(rp repository.UserPresentRepository) UserPresentUsecase {
	return &userPresentUsecase{rp}
}

func (pu *userPresentUsecase) GetReceivableUserPresents(ctx context.Context, userId value_user.UserId) ([]*model_user.UserPresent, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetReceivableUserPresents")
	defer span.End()

	presents, err := pu.rp.GetReceivableUserPresents(ctx, userId)
	if err != nil {
		return nil, err
	}
	return presents, nil
}

func (pu *userPresentUsecase) GetReceivedUserPresents(ctx context.Context, userId value_user.UserId) ([]*model_user.UserPresent, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetReceivedUserPresents")
	defer span.End()

	presents, err := pu.rp.GetReceivedUserPresents(ctx, userId)
	if err != nil {
		return nil, err
	}
	return presents, nil
}

func (pu *userPresentUsecase) ReceiveUserPresents(ctx context.Context, userId value_user.UserId, managedPresentIds []int64) error {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "ReceiveUserPresents")
	defer span.End()

	if err := pu.rp.ReceiveUserPresents(ctx, userId, managedPresentIds); err != nil {
		return err
	}
	return nil
}

func (pu *userPresentUsecase) DeleteExpiredUserPresents(ctx context.Context, userId value_user.UserId) error {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "DeleteExpiredUserPresents")
	defer span.End()

	if err := pu.rp.DeleteExpiredUserPresents(ctx, userId); err != nil {
		return err
	}
	return nil
}

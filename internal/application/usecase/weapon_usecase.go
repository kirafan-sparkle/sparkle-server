package usecase

import (
	"context"

	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type WeaponUsecase interface {
	GetWeaponById(ctx context.Context, weaponId value_weapon.WeaponId) (*model_weapon.Weapon, error)
}

type weaponUsecase struct {
	rp repository.WeaponRepository
}

func NewWeaponUsecase(rp repository.WeaponRepository) WeaponUsecase {
	return &weaponUsecase{rp}
}

func (uc *weaponUsecase) GetWeaponById(ctx context.Context, weaponId value_weapon.WeaponId) (*model_weapon.Weapon, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetWeaponById")
	defer span.End()
	weapon, err := uc.rp.FindByWeaponId(ctx, weaponId)
	if err != nil {
		return nil, err
	}
	return weapon, nil
}

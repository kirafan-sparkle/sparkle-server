package usecase

import (
	"context"
	"reflect"
	"testing"

	model_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/model/login_bonus"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

func Test_loginBonusUsecase_GetAllLoginBonuses(t *testing.T) {
	p := persistence.NewLoginBonusRepositoryImpl(db)
	u := NewLoginBonusUsecase(p)
	ctx := context.Background()

	tests := []struct {
		name    string
		want    []*model_login_bonus.LoginBonus
		wantErr bool
	}{
		{
			name:    "getAllLoginBonuses success",
			want:    []*model_login_bonus.LoginBonus{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := u.GetAllLoginBonuses(ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("loginBonusUsecase.GetAllLoginBonuses() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("loginBonusUsecase.GetAllLoginBonuses() = %v, want %v", got, tt.want)
			}
		})
	}
}

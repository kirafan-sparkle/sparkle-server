package usecase

import (
	"context"

	model_information "gitlab.com/kirafan/sparkle/server/internal/domain/model/information"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type InformationUsecase interface {
	GetAllInformations(ctx context.Context, platform value_version.Platform) ([]*model_information.Information, error)
}

type informationUsecase struct {
	rp repository.InformationRepository
}

func NewInformationUsecase(rp repository.InformationRepository) InformationUsecase {
	return &informationUsecase{rp: rp}
}

func (uc *informationUsecase) GetAllInformations(ctx context.Context, platform value_version.Platform) ([]*model_information.Information, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetAll")
	defer span.End()
	informations, err := uc.rp.GetInformations(ctx, platform)
	if err != nil {
		return nil, err
	}
	return informations, nil
}

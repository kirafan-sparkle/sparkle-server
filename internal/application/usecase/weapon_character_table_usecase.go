package usecase

import (
	"context"

	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/value/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type WeaponCharacterTableUsecase interface {
	GetWeaponCharacterTableById(ctx context.Context, characterId value_character.CharacterId) (*value_weapon.WeaponId, error)
}

type weaponCharacterTableUsecase struct {
	rp repository.WeaponCharacterTableRepository
}

func NewWeaponCharacterTableUsecase(rp repository.WeaponCharacterTableRepository) WeaponCharacterTableUsecase {
	return &weaponCharacterTableUsecase{rp}
}

func (uc *weaponCharacterTableUsecase) GetWeaponCharacterTableById(ctx context.Context, characterId value_character.CharacterId) (*value_weapon.WeaponId, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetWeaponCharacterTableById")
	defer span.End()
	character, err := uc.rp.FindByCharacterId(ctx, characterId)
	if err != nil {
		return nil, err
	}
	return &character.WeaponId, nil
}

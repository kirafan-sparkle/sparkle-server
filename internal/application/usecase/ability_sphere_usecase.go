package usecase

import (
	"context"
	"fmt"

	model_ability_sphere "gitlab.com/kirafan/sparkle/server/internal/domain/model/ability_sphere"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_ability_sphere "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_sphere"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type AbilitySphereUsecase interface {
	IsValidAbilitySphere(ctx context.Context, itemId value_ability_sphere.AbilitySphereItemId) (bool, error)
	GetAbilitySphereByItemId(ctx context.Context, itemId value_ability_sphere.AbilitySphereItemId) (*model_ability_sphere.AbilitySphere, error)
	GetAbilitySphereRecipeByItemId(ctx context.Context, itemId value_ability_sphere.AbilitySphereItemId) (*model_ability_sphere.AbilitySphereRecipe, error)
}

type abilitySphereUsecase struct {
	rp repository.AbilitySphereRepository
}

func NewAbilitySphereUsecase(rp repository.AbilitySphereRepository) AbilitySphereUsecase {
	return &abilitySphereUsecase{rp}
}

func (u *abilitySphereUsecase) IsValidAbilitySphere(ctx context.Context, itemId value_ability_sphere.AbilitySphereItemId) (bool, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "IsValidAbilitySphere")
	defer span.End()

	abilitySphere, err := u.rp.FindAbilitySphereBySphereItemId(ctx, itemId)
	if err != nil {
		return false, err
	}

	return abilitySphere != nil, nil
}

func (u *abilitySphereUsecase) GetAbilitySphereByItemId(ctx context.Context, itemId value_ability_sphere.AbilitySphereItemId) (*model_ability_sphere.AbilitySphere, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetAbilitySphereByItemId")
	defer span.End()

	abilitySphere, err := u.rp.FindAbilitySphereBySphereItemId(ctx, itemId)
	if err != nil {
		return nil, err
	}
	if abilitySphere == nil {
		return nil, fmt.Errorf("specified abilitySphere %d was not found", itemId)
	}

	return abilitySphere, nil
}

func (u *abilitySphereUsecase) GetAbilitySphereRecipeByItemId(ctx context.Context, itemId value_ability_sphere.AbilitySphereItemId) (*model_ability_sphere.AbilitySphereRecipe, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetAbilitySphereRecipeByItemId")
	defer span.End()

	abilitySphereRecipe, err := u.rp.FindAbilitySphereRecipeBySphereItemId(ctx, itemId)
	if err != nil {
		return nil, err
	}
	if abilitySphereRecipe == nil {
		return nil, fmt.Errorf("specified abilitySphereRecipe %d was not found", itemId)
	}

	return abilitySphereRecipe, nil
}

package usecase

import (
	"context"
	"testing"

	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

func Test_chapterUsecase_GetAll(t *testing.T) {
	chapterRepository := persistence.NewChapterRepositoryImpl(db)
	chapterUsecase := NewChapterUsecase(chapterRepository)
	ctx := context.Background()

	tests := []struct {
		name string
		err  error
	}{
		{
			name: "GetAll success",
			err:  nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, _, err := chapterUsecase.GetAll(ctx)
			if err != nil {
				t.Errorf("chapterUsecase.GetAll() error = %v, wantErr nil", err)
				return
			}
			t.Logf("chapterUsecase.GetAll() = %+v", got[0])
		})
	}
}

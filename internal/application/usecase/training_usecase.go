package usecase

import (
	"context"

	model_training "gitlab.com/kirafan/sparkle/server/internal/domain/model/training"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type TrainingUsecase interface {
	GetTraining(ctx context.Context, trainingId uint) (*model_training.Training, error)
	GetTrainings(ctx context.Context, categoryId uint8) ([]*model_training.Training, error)
}

type trainingUsecase struct {
	eqr repository.TrainingRepository
}

func NewTrainingUsecase(eqr repository.TrainingRepository) TrainingUsecase {
	return &trainingUsecase{eqr}
}

func (uc *trainingUsecase) GetTraining(ctx context.Context, trainingId uint) (*model_training.Training, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetTraining")
	defer span.End()
	training, err := uc.eqr.GetTraining(ctx, trainingId)
	if err != nil {
		return nil, err
	}
	return training, nil
}

func (uc *trainingUsecase) GetTrainings(ctx context.Context, categoryId uint8) ([]*model_training.Training, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetTrainings")
	defer span.End()
	trainings, err := uc.eqr.GetTrainings(ctx, categoryId)
	if err != nil {
		return nil, err
	}
	return trainings, nil
}

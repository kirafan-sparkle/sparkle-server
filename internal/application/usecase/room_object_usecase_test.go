package usecase

import (
	"context"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	model_room_object "gitlab.com/kirafan/sparkle/server/internal/domain/model/room_object"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	value_room "gitlab.com/kirafan/sparkle/server/internal/domain/value/room"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

func Test_roomObjectUsecase_GetAll(t *testing.T) {
	roomObjectRepository := persistence.NewRoomObjectRepositoryImpl(db)
	roomObjectUsecase := NewRoomObjectUsecase(roomObjectRepository)
	ctx := context.Background()

	tokyoTimeZone := time.FixedZone("Asia/Tokyo", 9*60*60)
	tests := []struct {
		name         string
		err          error
		expectIndex0 model_room_object.RoomObject
	}{
		{
			name: "GetAll_success",
			err:  nil,
			expectIndex0: model_room_object.RoomObject{
				RoomObjectId:     1001,
				Id:               1001,
				Name:             "四角テーブル/ホワイト",
				Type:             value_room.RoomObjectTypeDesk,
				BuyAmount:        1000,
				SaleAmount:       250,
				ObjectLimit:      10,
				ShopFlag:         value.BoolLikeUIntTrue,
				DispStartAt:      time.Date(2017, 1, 1, 0, 0, 0, 0, tokyoTimeZone),
				DispEndAt:        time.Date(2099, 12, 31, 0, 0, 0, 0, tokyoTimeZone),
				BargainFlag:      value.BoolLikeUIntFalse,
				BargainStartAt:   time.Date(1, 1, 1, 0, 0, 0, 0, tokyoTimeZone),
				BargainEndAt:     time.Date(1, 1, 1, 0, 0, 0, 0, tokyoTimeZone),
				BargainBuyAmount: -1,
				IsSaleable:       true,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := roomObjectUsecase.GetAll(ctx)
			if err != nil {
				t.Errorf("roomObjectUsecase.GetAll() error = %v, wantErr nil", err)
				return
			}
			if cmp.Equal(&tt.expectIndex0, got[0]) != true {
				t.Errorf("roomObjectUsecase.GetAll() Diff = %+v", cmp.Diff(tt.expectIndex0, got[0]))
				return
			}
		})
	}
}

package usecase

import (
	"os"
	"testing"

	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/migrate"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/seed"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gorm.io/gorm"
)

var db *gorm.DB

func TestMain(m *testing.M) {
	logger := observability.InitLogger(observability.LogTypeDebug)
	dbConf := database.GetConfig()
	db = database.InitDatabase(dbConf, logger)
	migrate.AutoMigrate(db)
	seed.AutoSeed(db)

	exitVal := m.Run()
	os.Exit(exitVal)
}

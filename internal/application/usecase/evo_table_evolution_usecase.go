package usecase

import (
	"context"

	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type EvoTableEvolutionUsecase interface {
	GetEvolutionRecipe(ctx context.Context, characterId value_character.CharacterId) (*model_evo_table.EvoTableEvolution, error)
}

type evoTableEvolutionUsecase struct {
	rp repository.EvoTableEvolutionRepository
}

func NewEvoTableEvolutionUsecase(rp repository.EvoTableEvolutionRepository) EvoTableEvolutionUsecase {
	return &evoTableEvolutionUsecase{rp}
}

func (u *evoTableEvolutionUsecase) GetEvolutionRecipe(ctx context.Context, characterId value_character.CharacterId) (*model_evo_table.EvoTableEvolution, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetEvolutionRecipe")
	defer span.End()
	res, err := u.rp.FindByCharacterId(ctx, characterId)
	if err != nil {
		return nil, err
	}
	return res, nil
}

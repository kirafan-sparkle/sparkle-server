package usecase

import (
	"context"

	model_mission "gitlab.com/kirafan/sparkle/server/internal/domain/model/mission"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_mission "gitlab.com/kirafan/sparkle/server/internal/domain/value/mission"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type MissionUsecase interface {
	GetTutorialMissions(ctx context.Context) ([]*model_mission.Mission, error)
	GetDailyMissions(ctx context.Context) ([]*model_mission.Mission, error)
	GetWeeklyMissions(ctx context.Context) ([]*model_mission.Mission, error)
	GetEventMissions(ctx context.Context) ([]*model_mission.Mission, error)
	GetNextRankUpMission(ctx context.Context, currentCount uint) (*model_mission.Mission, error)
	GetNextCharacterRelationshipMission(ctx context.Context, currentCount uint) (*model_mission.Mission, error)
	GetNextCharacterEvolutionMission(ctx context.Context, currentCount uint) (*model_mission.Mission, error)
}

type missionUsecase struct {
	mr repository.MissionRepository
}

func NewMissionUsecase(mr repository.MissionRepository) MissionUsecase {
	return &missionUsecase{mr}
}

func (mu *missionUsecase) GetTutorialMissions(ctx context.Context) ([]*model_mission.Mission, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetTutorialMissions")
	defer span.End()
	criteria := map[string]interface{}{
		"mission_insert_type": value_mission.MissionInsertTypeTutorial,
	}
	missions, err := mu.mr.FindMissions(ctx, nil, criteria, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return missions, nil
}

func (mu *missionUsecase) GetDailyMissions(ctx context.Context) ([]*model_mission.Mission, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetDailyMissions")
	defer span.End()
	missions, err := mu.mr.FindMissions(ctx, &model_mission.Mission{
		MissionInsertType: value_mission.MissionInsertTypeDaily,
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return missions, nil
}

func (mu *missionUsecase) GetWeeklyMissions(ctx context.Context) ([]*model_mission.Mission, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetWeeklyMissions")
	defer span.End()
	missions, err := mu.mr.FindMissions(ctx, &model_mission.Mission{
		MissionInsertType: value_mission.MissionInsertTypeWeekly,
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return missions, nil
}

func (mu *missionUsecase) GetEventMissions(ctx context.Context) ([]*model_mission.Mission, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetEventMissions")
	defer span.End()
	missions, err := mu.mr.FindMissions(ctx, &model_mission.Mission{
		MissionInsertType: value_mission.MissionInsertTypeEvent,
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return missions, nil
}

func (mu *missionUsecase) GetNextRankUpMission(ctx context.Context, currentCount uint) (*model_mission.Mission, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetNextRankUpMission")
	defer span.End()
	mission, err := mu.mr.FindMission(ctx, &model_mission.Mission{
		MissionInsertType: value_mission.MissionInsertTypeRankUp,
		Rate:              uint32(currentCount),
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return mission, nil
}

func (mu *missionUsecase) GetNextCharacterRelationshipMission(ctx context.Context, currentCount uint) (*model_mission.Mission, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetNextCharacterRelationshipMission")
	defer span.End()
	mission, err := mu.mr.FindMission(ctx, &model_mission.Mission{
		MissionInsertType: value_mission.MissionInsertTypeCharacterRelationship,
		Rate:              uint32(currentCount),
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return mission, nil
}

func (mu *missionUsecase) GetNextCharacterEvolutionMission(ctx context.Context, currentCount uint) (*model_mission.Mission, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetNextCharacterEvolutionMission")
	defer span.End()
	mission, err := mu.mr.FindMission(ctx, &model_mission.Mission{
		MissionInsertType: value_mission.MissionInsertTypeCharacterEvolution,
		Rate:              uint32(currentCount),
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return mission, nil
}

package usecase

import (
	"context"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type ExpTableFriendshipUsecase interface {
	GetNextExpTableFriendship(ctx context.Context, currentExp uint64) (*model_exp_table.ExpTableFriendship, error)
}

type expTableFriendshipUsecase struct {
	rp repository.ExpTableFriendshipRepository
}

func NewExpTableFriendshipUsecase(rp repository.ExpTableFriendshipRepository) ExpTableFriendshipUsecase {
	return &expTableFriendshipUsecase{rp}
}

func (uc *expTableFriendshipUsecase) GetNextExpTableFriendship(ctx context.Context, currentExp uint64) (*model_exp_table.ExpTableFriendship, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetNextExpTableFriendship")
	defer span.End()
	criteria := map[string]interface{}{
		"total_exp > ?": currentExp,
	}
	expTableRank, err := uc.rp.FindExpTableFriendship(ctx, nil, criteria)
	if err != nil {
		return nil, err
	}
	return expTableRank, nil
}

package usecase

import (
	"context"
	"testing"

	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

func Test_namedTypeUsecase_GetAll(t *testing.T) {
	namedTypeRepository := persistence.NewNamedTypeRepositoryImpl(db)
	namedTypeUsecase := NewNamedTypeUsecase(namedTypeRepository)
	ctx := context.Background()

	tests := []struct {
		name      string
		namedType uint
		nickName  string
		err       error
	}{
		{
			name:      "GetKfcn info success",
			namedType: 117,
			nickName:  "チノ",
			err:       nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := namedTypeUsecase.GetNamedTypeById(ctx, tt.namedType)
			if err != nil {
				t.Errorf("namedTypeUsecase.GetNamedTypeById(namedTypeId) error = %v, wantErr nil", err)
				return
			}
			if got.NickName != tt.nickName {
				t.Errorf("namedTypeUsecase.GetNamedTypeById(namedTypeId) nickName = %v, wantNickName = %v", got.NickName, tt.nickName)
				return
			}
			// t.Logf("namedTypeUsecase.GetNamedTypeById(namedTypeId) = %v", got)
		})
	}
}

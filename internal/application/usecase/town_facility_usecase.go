package usecase

import (
	"context"

	model_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/model/town_facility"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type TownFacilityUsecase interface {
	GetTownFacilityById(ctx context.Context, facilityId uint32) (*model_town_facility.TownFacility, error)
}

type townFacilityUsecase struct {
	rp repository.TownFacilityRepository
}

func NewTownFacilityUsecase(rp repository.TownFacilityRepository) TownFacilityUsecase {
	return &townFacilityUsecase{rp}
}

func (uc *townFacilityUsecase) GetTownFacilityById(ctx context.Context, facilityId uint32) (*model_town_facility.TownFacility, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetTownFacilityById")
	defer span.End()
	townFacility, err := uc.rp.FindTownFacility(ctx, facilityId)
	if err != nil {
		return nil, err
	}
	return townFacility, nil
}

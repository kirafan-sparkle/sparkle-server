package usecase

import (
	"context"
	"reflect"
	"testing"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

func Test_expTableRankUsecase_GetNextExpTableRank(t *testing.T) {
	p := persistence.NewExpTableRankRepositoryImpl(db)
	u := NewExpTableRankUsecase(p)
	ctx := context.Background()

	tests := []struct {
		name       string
		currentExp uint64
		want       *model_exp_table.ExpTableRank
		wantErr    bool
	}{
		{
			name:       "get next exp table rank returns level 1 at exp 0",
			currentExp: 0,
			want: &model_exp_table.ExpTableRank{
				Rank:            1,
				NextExp:         30,
				TotalExp:        30,
				Stamina:         10,
				FriendLimit:     20,
				SupportLimit:    6,
				BattlePartyCost: 50,
				WeaponLimit:     20,
				TrainingSlotNum: 2,
				StoreReview:     0,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table rank returns level 2 at exp 31",
			currentExp: 31,
			want: &model_exp_table.ExpTableRank{
				Rank:            2,
				NextExp:         60,
				TotalExp:        90,
				Stamina:         12,
				FriendLimit:     21,
				SupportLimit:    6,
				BattlePartyCost: 52,
				WeaponLimit:     20,
				TrainingSlotNum: 2,
				StoreReview:     0,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table rank returns level 140 at exp 1511137371",
			currentExp: 1511137371,
			want: &model_exp_table.ExpTableRank{
				Rank:            140,
				NextExp:         999999999,
				TotalExp:        2511137369,
				Stamina:         165,
				FriendLimit:     130,
				SupportLimit:    8,
				BattlePartyCost: 135,
				WeaponLimit:     36,
				TrainingSlotNum: 4,
				StoreReview:     0,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := u.GetNextExpTableRank(ctx, tt.currentExp)
			if (err != nil) != tt.wantErr {
				t.Errorf("expTableRankUsecase.GetNextExpTableRank() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("expTableRankUsecase.GetNextExpTableRank() = %v, want %v", got, tt.want)
			}
		})
	}
}

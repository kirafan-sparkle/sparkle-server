package usecase

import (
	"context"
	"errors"

	model_schedule "gitlab.com/kirafan/sparkle/server/internal/domain/model/schedule"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type ScheduleUsecase interface {
	CreateSchedule(ctx context.Context, characterIds []value_character.CharacterId, gridData string) ([]*model_schedule.Schedule, error)
	RefreshSchedule(ctx context.Context, characterSchedules []string) ([]string, error)
}

var ErrRefreshScheduleInvalidParamLength = errors.New("characterIds and schedules must have same length")

type scheduleUsecase struct {
	sr repository.ScheduleRepository
}

func NewScheduleUsecase(
	sr repository.ScheduleRepository,
) ScheduleUsecase {
	return &scheduleUsecase{sr}
}

func (u *scheduleUsecase) CreateSchedule(ctx context.Context, characterIds []value_character.CharacterId, gridData string) ([]*model_schedule.Schedule, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "CreateSchedule")
	defer span.End()
	schedules, err := u.sr.Create(ctx, characterIds, gridData)
	if err != nil {
		return nil, err
	}
	return schedules, nil
}

func (u *scheduleUsecase) RefreshSchedule(ctx context.Context, characterSchedules []string) ([]string, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "RefreshSchedule")
	defer span.End()
	refreshedSchedules, err := u.sr.Refresh(ctx, characterSchedules)
	if err != nil {
		return nil, err
	}
	return refreshedSchedules, nil
}

package usecase

import (
	"context"
	"errors"

	model_item "gitlab.com/kirafan/sparkle/server/internal/domain/model/item"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type ItemUsecase interface {
	GetItemById(ctx context.Context, itemId value_item.ItemId) (*model_item.Item, error)
	GetAllItems(ctx context.Context) ([]*model_item.Item, error)
	GetItemsByCategory(ctx context.Context, category value_item.ItemCategory) ([]*model_item.Item, error)
	GetWeaponUpgradeAmount(ctx context.Context, itemId value_item.ItemId) (value_exp.WeaponExp, error)
	GetCharacterUpgradeAmount(ctx context.Context, itemId value_item.ItemId) (value_character.ElementType, value_exp.CharacterExp, error)
}

type itemUsecase struct {
	rp repository.ItemRepository
}

func NewItemUsecase(rp repository.ItemRepository) ItemUsecase {
	return &itemUsecase{rp}
}

func (uc *itemUsecase) GetAllItems(ctx context.Context) ([]*model_item.Item, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetAllItems")
	defer span.End()
	items, err := uc.rp.FindItems(ctx, nil, nil, nil)
	if err != nil {
		return nil, err
	}
	return items, nil
}

func (uc *itemUsecase) GetItemById(ctx context.Context, itemId value_item.ItemId) (*model_item.Item, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetItemById")
	defer span.End()
	item, err := uc.rp.FindItem(ctx, &model_item.Item{ItemId: itemId}, nil, nil)
	if err != nil {
		return nil, err
	}
	return item, nil
}

func (uc *itemUsecase) GetItemsByCategory(ctx context.Context, category value_item.ItemCategory) ([]*model_item.Item, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetItemsByCategory")
	defer span.End()
	items, err := uc.rp.FindItems(ctx, &model_item.Item{Category: category}, nil, nil)
	if err != nil {
		return nil, err
	}
	return items, nil
}

func (uc *itemUsecase) GetWeaponUpgradeAmount(ctx context.Context, itemId value_item.ItemId) (value_exp.WeaponExp, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetWeaponUpgradeAmount")
	defer span.End()
	amount, err := uc.rp.GetWeaponUpgradeAmount(ctx, itemId)
	if err != nil {
		return 0, err
	}
	exp, err := value_exp.NewWeaponExp(uint32(amount))
	if err != nil {
		return 0, errors.New("invalid weapon exp from db")
	}
	return exp, nil
}

func (uc *itemUsecase) GetCharacterUpgradeAmount(ctx context.Context, itemId value_item.ItemId) (value_character.ElementType, value_exp.CharacterExp, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetCharacterUpgradeAmount")
	defer span.End()
	classRaw, expRaw, err := uc.rp.GetCharacterUpgradeAmount(ctx, itemId)
	if err != nil {
		return 0, 0, err
	}
	elementType, err := value_character.NewElementType(uint8(classRaw))
	if err != nil {
		return 0, 0, errors.New("invalid element type from db")
	}
	exp, err := value_exp.NewCharacterExp(uint32(expRaw))
	if err != nil {
		return 0, 0, errors.New("invalid character exp from db")
	}
	return elementType, exp, nil
}

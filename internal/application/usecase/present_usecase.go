package usecase

import (
	"context"

	model_present "gitlab.com/kirafan/sparkle/server/internal/domain/model/present"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_present "gitlab.com/kirafan/sparkle/server/internal/domain/value/present"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type PresentUsecase interface {
	GetTutorialPresents(ctx context.Context) ([]*model_present.Present, error)
}

type presentUsecase struct {
	rp repository.PresentRepository
}

func NewPresentUsecase(rp repository.PresentRepository) PresentUsecase {
	return &presentUsecase{rp}
}

func (pu *presentUsecase) GetTutorialPresents(ctx context.Context) ([]*model_present.Present, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetTutorialPresents")
	defer span.End()
	criteria := map[string]interface{}{
		"present_insert_type": value_present.PresentInsertTypeTutorial,
	}
	presents, err := pu.rp.FindPresents(ctx, nil, criteria, nil)
	if err != nil {
		return nil, err
	}
	return presents, nil
}

package usecase

import (
	"context"

	model_character "gitlab.com/kirafan/sparkle/server/internal/domain/model/character"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type CharacterUsecase interface {
	GetCharacterById(ctx context.Context, characterId value_character.CharacterId) (*model_character.Character, error)
	GetNamedTypesByIds(ctx context.Context, characterIds []value_character.CharacterId) ([]uint16, error)
}

type characterUsecase struct {
	rp repository.CharacterRepository
}

func NewCharacterUsecase(rp repository.CharacterRepository) CharacterUsecase {
	return &characterUsecase{rp}
}

func (uc *characterUsecase) GetCharacterById(ctx context.Context, characterId value_character.CharacterId) (*model_character.Character, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetCharacterById")
	defer span.End()
	character, err := uc.rp.FindCharacter(ctx, &model_character.Character{CharacterId: uint64(characterId)}, nil, nil)
	if err != nil {
		return nil, err
	}
	return character, nil
}

func (uc *characterUsecase) GetNamedTypesByIds(ctx context.Context, characterIds []value_character.CharacterId) ([]uint16, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetNamedTypesByIds")
	defer span.End()

	namedTypes, err := uc.rp.GetNamedTypesByIds(ctx, characterIds)
	if err != nil {
		return []uint16{}, err
	}
	return namedTypes, nil
}

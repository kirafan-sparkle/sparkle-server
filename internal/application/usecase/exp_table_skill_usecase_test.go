package usecase

import (
	"context"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

func Test_expTableSkillUsecase_GetNextExpTableSkill(t *testing.T) {
	p := persistence.NewExpTableSkillRepositoryImpl(db)
	u := NewExpTableSkillUsecase(p)
	ctx := context.Background()

	tests := []struct {
		name       string
		currentExp uint32
		skillType  value_exp.ExpTableSkillType
		want       *model_exp_table.ExpTableSkill
		wantErr    bool
	}{
		{
			name:       "get next exp table skill type special returns level 1 at exp 0",
			currentExp: 0,
			skillType:  value_exp.ExpTableSkillTypeSpecial,
			want: &model_exp_table.ExpTableSkill{
				SkillType: value_exp.ExpTableSkillTypeSpecial,
				Level:     1,
				NextExp:   5,
				TotalExp:  5,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table skill type special returns level 2 at exp 6",
			currentExp: 6,
			skillType:  value_exp.ExpTableSkillTypeSpecial,
			want: &model_exp_table.ExpTableSkill{
				SkillType: value_exp.ExpTableSkillTypeSpecial,
				Level:     2,
				NextExp:   6,
				TotalExp:  11,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table skill type special returns level 35 at exp 816",
			currentExp: 816,
			skillType:  value_exp.ExpTableSkillTypeSpecial,
			want: &model_exp_table.ExpTableSkill{
				SkillType: value_exp.ExpTableSkillTypeSpecial,
				Level:     35,
				NextExp:   999999,
				TotalExp:  1000814,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table skill type normal returns level 1 at exp 0",
			currentExp: 0,
			skillType:  value_exp.ExpTableSkillTypeNormal,
			want: &model_exp_table.ExpTableSkill{
				SkillType: value_exp.ExpTableSkillTypeNormal,
				Level:     1,
				NextExp:   15,
				TotalExp:  15,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table skill type normal returns level 2 at exp 16",
			currentExp: 16,
			skillType:  value_exp.ExpTableSkillTypeNormal,
			want: &model_exp_table.ExpTableSkill{
				SkillType: value_exp.ExpTableSkillTypeNormal,
				Level:     2,
				NextExp:   16,
				TotalExp:  31,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table skill type normal returns level 25 at exp 637",
			currentExp: 637,
			skillType:  value_exp.ExpTableSkillTypeNormal,
			want: &model_exp_table.ExpTableSkill{
				SkillType: value_exp.ExpTableSkillTypeNormal,
				Level:     25,
				NextExp:   999999,
				TotalExp:  1000635,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table skill type weapon returns level 1 at exp 0",
			currentExp: 0,
			skillType:  value_exp.ExpTableSkillTypeWeapon,
			want: &model_exp_table.ExpTableSkill{
				SkillType: value_exp.ExpTableSkillTypeWeapon,
				Level:     1,
				NextExp:   2,
				TotalExp:  2,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table skill type weapon returns level 2 at exp 3",
			currentExp: 3,
			skillType:  value_exp.ExpTableSkillTypeWeapon,
			want: &model_exp_table.ExpTableSkill{
				SkillType: value_exp.ExpTableSkillTypeWeapon,
				Level:     2,
				NextExp:   3,
				TotalExp:  5,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table skill type weapon returns level 50 at exp 1530",
			currentExp: 1530,
			skillType:  value_exp.ExpTableSkillTypeWeapon,
			want: &model_exp_table.ExpTableSkill{
				SkillType: value_exp.ExpTableSkillTypeWeapon,
				Level:     50,
				NextExp:   999999,
				TotalExp:  1001528,
			},
			wantErr: false,
		},
	}

	opts := []cmp.Option{
		cmpopts.IgnoreFields(model_exp_table.ExpTableSkill{}, "ExpTableSkillId"),
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := u.GetNextExpTableSkill(ctx, tt.currentExp, tt.skillType)
			if (err != nil) != tt.wantErr {
				t.Errorf("expTableSkillUsecase.GetNextExpTableSkill() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if cmp.Equal(got, tt.want, opts...) != true {
				t.Errorf("expTableSkillUsecase.GetNextExpTableSkill()  Diff = %+v", cmp.Diff(got, tt.want, opts...))
			}
		})
	}
}

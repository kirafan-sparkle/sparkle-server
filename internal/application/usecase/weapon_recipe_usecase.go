package usecase

import (
	"context"

	model_weapon "gitlab.com/kirafan/sparkle/server/internal/domain/model/weapon"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type WeaponRecipeUsecase interface {
	GetWeaponRecipeById(ctx context.Context, recipeId uint32) (*model_weapon.WeaponRecipe, error)
}

type weaponRecipeUsecase struct {
	rp repository.WeaponRecipeRepository
}

func NewWeaponRecipeUsecase(rp repository.WeaponRecipeRepository) WeaponRecipeUsecase {
	return &weaponRecipeUsecase{rp}
}

func (uc *weaponRecipeUsecase) GetWeaponRecipeById(ctx context.Context, recipeId uint32) (*model_weapon.WeaponRecipe, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetWeaponRecipeById")
	defer span.End()
	recipe, err := uc.rp.FindByRecipeId(ctx, recipeId)
	if err != nil {
		return nil, err
	}
	return recipe, nil
}

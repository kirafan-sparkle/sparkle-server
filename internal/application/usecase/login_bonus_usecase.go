package usecase

import (
	"context"

	model_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/model/login_bonus"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type LoginBonusUsecase interface {
	GetAllLoginBonuses(ctx context.Context) ([]*model_login_bonus.LoginBonus, error)
}

type loginBonusUsecase struct {
	rp repository.LoginBonusRepository
}

func NewLoginBonusUsecase(rp repository.LoginBonusRepository) LoginBonusUsecase {
	return &loginBonusUsecase{rp}
}

func (uc *loginBonusUsecase) GetAllLoginBonuses(ctx context.Context) ([]*model_login_bonus.LoginBonus, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetAllLoginBonuses")
	defer span.End()
	loginBonuses, err := uc.rp.FindAvailableLoginBonuses(ctx)
	if err != nil {
		return nil, err
	}
	return loginBonuses, nil
}

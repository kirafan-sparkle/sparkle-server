package usecase

import (
	"context"

	model_friend "gitlab.com/kirafan/sparkle/server/internal/domain/model/friend"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_friend "gitlab.com/kirafan/sparkle/server/internal/domain/value/friend"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
	"gitlab.com/kirafan/sparkle/server/pkg/errwrap"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type FriendUsecase interface {
	IsExistFriendRequest(ctx context.Context, internalUserId value_friend.PlayerId, targetUserId value_friend.PlayerId) (bool, *errwrap.SparkleError)
	AddFriendRequest(ctx context.Context, friendRequest model_friend.FriendRequest) (*value_friend.ManagedFriendId, *errwrap.SparkleError)
	DeleteFriendRequest(ctx context.Context, internalUserId value_friend.PlayerId, managedFriendId value_friend.ManagedFriendId) *errwrap.SparkleError
	GetInComingFriendRequests(ctx context.Context, internalUserId value_friend.PlayerId) ([]*model_friend.FriendRequest, *errwrap.SparkleError)
	GetOutGoingFriendRequests(ctx context.Context, internalUserId value_friend.PlayerId) ([]*model_friend.FriendRequest, *errwrap.SparkleError)
	GetFriendRequest(ctx context.Context, managedFriendId value_friend.ManagedFriendId) (*model_friend.FriendRequest, *errwrap.SparkleError)
	GetFriendCount(ctx context.Context, internalUserId value_friend.PlayerId) (uint16, *errwrap.SparkleError)
	IsAlreadyFriend(ctx context.Context, internalUserId value_friend.PlayerId, targetUserId value_friend.PlayerId) (bool, *errwrap.SparkleError)
	AddFriend(ctx context.Context, friend model_friend.Friend) (*value_friend.ManagedFriendId, *errwrap.SparkleError)
	DeleteFriend(ctx context.Context, managedFriendId value_friend.ManagedFriendId) *errwrap.SparkleError
	GetFriend(ctx context.Context, friendId value_friend.ManagedFriendId) (*model_friend.Friend, *errwrap.SparkleError)
	GetFriends(ctx context.Context, internalUserId value_friend.PlayerId) ([]*model_friend.FriendFull, *errwrap.SparkleError)
}

type friendUsecase struct {
	fr  repository.FriendRepository
	frr repository.FriendRequestRepository
}

func NewFriendUsecase(fr repository.FriendRepository, frr repository.FriendRequestRepository) FriendUsecase {
	return &friendUsecase{fr, frr}
}

func (uc *friendUsecase) IsExistFriendRequest(ctx context.Context, internalUserId value_friend.PlayerId, targetUserId value_friend.PlayerId) (bool, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "IsExistFriendRequest")
	defer span.End()

	isExist, err := uc.frr.IsExistFriendRequest(ctx, internalUserId, targetUserId)
	if err != nil {
		return false, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return isExist, nil
}

func (uc *friendUsecase) AddFriendRequest(ctx context.Context, friendRequest model_friend.FriendRequest) (*value_friend.ManagedFriendId, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "AddFriendRequest")
	defer span.End()

	managedFriendId, err := uc.frr.AddFriendRequest(ctx, friendRequest)
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return managedFriendId, nil
}

func (uc *friendUsecase) DeleteFriendRequest(ctx context.Context, internalUserId value_friend.PlayerId, managedFriendId value_friend.ManagedFriendId) *errwrap.SparkleError {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "DeleteFriendRequest")
	defer span.End()

	if err := uc.frr.DeleteFriendRequest(ctx, managedFriendId); err != nil {
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return nil
}

func (uc *friendUsecase) GetOutGoingFriendRequests(ctx context.Context, userId value_friend.PlayerId) ([]*model_friend.FriendRequest, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetOutGoingFriendRequests")
	defer span.End()

	requests, err := uc.frr.GetOutGoingFriendRequests(ctx, userId)
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return requests, nil
}

func (uc *friendUsecase) GetInComingFriendRequests(ctx context.Context, userId value_friend.PlayerId) ([]*model_friend.FriendRequest, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetInComingFriendRequests")
	defer span.End()

	requests, err := uc.frr.GetInComingFriendRequests(ctx, userId)
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return requests, nil
}

func (uc *friendUsecase) GetFriendRequest(ctx context.Context, managedFriendId value_friend.ManagedFriendId) (*model_friend.FriendRequest, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetFriendRequest")
	defer span.End()
	friendReq, err := uc.frr.GetFriendRequest(ctx, managedFriendId)
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return friendReq, nil
}

func (uc *friendUsecase) GetFriendCount(ctx context.Context, userId value_friend.PlayerId) (uint16, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetFriendCount")
	defer span.End()

	count, err := uc.fr.GetFriendCount(ctx, userId)
	if err != nil {
		return 0, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return uint16(count), nil
}

func (uc *friendUsecase) IsAlreadyFriend(ctx context.Context, internalUserId value_friend.PlayerId, targetUserId value_friend.PlayerId) (bool, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "IsAlreadyFriend")
	defer span.End()
	isFriend, err := uc.fr.IsAlreadyFriend(ctx, internalUserId, targetUserId)
	if err != nil {
		return false, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return isFriend, nil
}

func (uc *friendUsecase) AddFriend(ctx context.Context, friend model_friend.Friend) (*value_friend.ManagedFriendId, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "AddFriend")
	defer span.End()
	managedFriendId, err := uc.fr.AddFriend(ctx, friend)
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return managedFriendId, nil
}

func (uc *friendUsecase) DeleteFriend(ctx context.Context, managedFriendId value_friend.ManagedFriendId) *errwrap.SparkleError {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "DeleteFriend")
	defer span.End()
	if err := uc.fr.DeleteFriend(ctx, managedFriendId); err != nil {
		return errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return nil
}

func (uc *friendUsecase) GetFriend(ctx context.Context, friendId value_friend.ManagedFriendId) (*model_friend.Friend, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetFriend")
	defer span.End()
	friend, err := uc.fr.GetFriend(ctx, friendId)
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return friend, nil
}

func (uc *friendUsecase) GetFriends(ctx context.Context, internalUserId value_friend.PlayerId) ([]*model_friend.FriendFull, *errwrap.SparkleError) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetFriends")
	defer span.End()
	friends, err := uc.fr.GetFriends(ctx, internalUserId)
	if err != nil {
		return nil, errwrap.NewSparkleErrorWithCustomError(response.RESULT_DB_ERROR, err)
	}
	return friends, nil
}

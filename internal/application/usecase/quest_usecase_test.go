package usecase

import (
	"context"
	"testing"

	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

func Test_questUsecase_GetAll(t *testing.T) {
	questRepository := persistence.NewQuestRepositoryImpl(db)
	questUsecase := NewQuestUsecase(questRepository)
	ctx := context.Background()

	type args struct {
		internalUserId value_user.UserId
	}
	tests := []struct {
		name string
		args args
		err  error
	}{
		{
			name: "GetAll success",
			args: args{
				internalUserId: 1,
			},
			err: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := questUsecase.GetPart1Quests(ctx, tt.args.internalUserId)
			if err != nil {
				t.Errorf("questUsecase.GetAll() error = %v, wantErr nil", err)
				return
			}
			t.Logf("questUsecase.GetAll().Quests[0] = %+v", got[0])
			// t.Logf("questUsecase.GetAll().QuestPart2s[0] = %+v", got.QuestPart2s[0])
		})
	}
}

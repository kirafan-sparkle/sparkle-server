package usecase

import (
	"context"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type EventQuestUsecase interface {
	GetEventQuest(ctx context.Context, internalUserId value_user.UserId, questId uint) (*model_quest.EventQuest, error)
	GetEventQuests(ctx context.Context, internalUserId value_user.UserId) ([]*model_quest.EventQuest, error)
	GetEventQuestPeriods(ctx context.Context, internalUserId value_user.UserId) ([]*model_quest.EventQuestPeriod, error)
}

type eventQuestUsecase struct {
	eqr repository.EventQuestRepository
}

func NewEventQuestUsecase(eqr repository.EventQuestRepository) EventQuestUsecase {
	return &eventQuestUsecase{eqr}
}

func (uc *eventQuestUsecase) GetEventQuest(ctx context.Context, internalUserId value_user.UserId, eventQuestId uint) (*model_quest.EventQuest, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetEventQuest")
	defer span.End()
	eventQuest, err := uc.eqr.GetEventQuest(ctx, internalUserId, eventQuestId)
	if err != nil {
		return nil, err
	}
	return eventQuest, nil
}

func (uc *eventQuestUsecase) GetEventQuests(ctx context.Context, internalUserId value_user.UserId) ([]*model_quest.EventQuest, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetEventQuests")
	defer span.End()
	eventQuests, err := uc.eqr.GetEventQuests(ctx, internalUserId)
	if err != nil {
		return nil, err
	}
	return eventQuests, nil
}

func (uc *eventQuestUsecase) GetEventQuestPeriods(ctx context.Context, internalUserId value_user.UserId) ([]*model_quest.EventQuestPeriod, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetEventQuestPeriods")
	defer span.End()
	eventQuestPeriods, err := uc.eqr.GetEventQuestPeriods(ctx, internalUserId)
	if err != nil {
		return nil, err
	}
	return eventQuestPeriods, nil
}

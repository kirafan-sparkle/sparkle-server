package usecase

import (
	"context"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_quest "gitlab.com/kirafan/sparkle/server/internal/domain/value/quest"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type QuestUsecase interface {
	GetPart1Quests(ctx context.Context, internalUserId value_user.UserId) ([]*model_quest.Quest, error)
	GetPart2Quests(ctx context.Context, internalUserId value_user.UserId) ([]*model_quest.Quest, error)
	GetQuestCategory(ctx context.Context, questId uint) (*value_quest.QuestCategoryType, error)
	GetQuest(ctx context.Context, questId uint) (*model_quest.Quest, error)
}

type questUsecase struct {
	qr repository.QuestRepository
}

func NewQuestUsecase(qr repository.QuestRepository) QuestUsecase {
	return &questUsecase{qr: qr}
}

func (uc *questUsecase) GetQuest(ctx context.Context, questId uint) (*model_quest.Quest, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetQuest")
	defer span.End()
	foundQuest, err := uc.qr.FindQuest(ctx, &model_quest.Quest{Id: questId}, nil, []string{"QuestFirstClearReward"})
	if err != nil {
		return nil, err
	}
	return foundQuest, nil
}

func (uc *questUsecase) GetPart1Quests(ctx context.Context, internalUserId value_user.UserId) ([]*model_quest.Quest, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetPart1Quests")
	defer span.End()
	// NOTE: Since GORM ignores zero value, we need to use map instead of struct
	criteria := map[string]interface{}{
		"category": value_quest.QuestCategoryTypeMainPart1,
	}
	foundQuests, err := uc.qr.FindQuests(ctx, nil, criteria, []string{"QuestFirstClearReward"})
	if err != nil {
		return nil, err
	}
	return foundQuests, nil
}

func (uc *questUsecase) GetPart2Quests(ctx context.Context, internalUserId value_user.UserId) ([]*model_quest.Quest, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetPart2Quests")
	defer span.End()
	query := &model_quest.Quest{
		Category: value_quest.QuestCategoryTypeMainPart2,
	}
	foundQuests, err := uc.qr.FindQuests(ctx, query, nil, []string{"QuestFirstClearReward"})
	if err != nil {
		return nil, err
	}
	return foundQuests, nil
}

func (uc *questUsecase) GetQuestCategory(ctx context.Context, questId uint) (*value_quest.QuestCategoryType, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetQuestCategory")
	defer span.End()
	category, err := uc.qr.GetQuestCategory(ctx, questId)
	return category, err
}

package usecase

import (
	"context"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type ExpTableSkillUsecase interface {
	GetNextExpTableSkill(ctx context.Context, currentExp uint32, skillType value_exp.ExpTableSkillType) (*model_exp_table.ExpTableSkill, error)
	GetLevelExpTableSkill(ctx context.Context, level uint32, skillType value_exp.ExpTableSkillType) (*model_exp_table.ExpTableSkill, error)
}

type expTableSkillUsecase struct {
	rp repository.ExpTableSkillRepository
}

func NewExpTableSkillUsecase(rp repository.ExpTableSkillRepository) ExpTableSkillUsecase {
	return &expTableSkillUsecase{rp}
}

func (uc *expTableSkillUsecase) GetNextExpTableSkill(ctx context.Context, currentExp uint32, skillType value_exp.ExpTableSkillType) (*model_exp_table.ExpTableSkill, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetNextExpTableSkill")
	defer span.End()
	criteria := map[string]interface{}{
		"total_exp > ?":  currentExp,
		"skill_type = ?": skillType,
	}
	expTableSkill, err := uc.rp.FindExpTableSkill(ctx, nil, criteria)
	if err != nil {
		return nil, err
	}
	return expTableSkill, nil
}

func (uc *expTableSkillUsecase) GetLevelExpTableSkill(ctx context.Context, level uint32, skillType value_exp.ExpTableSkillType) (*model_exp_table.ExpTableSkill, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetLevelExpTableSkill")
	defer span.End()
	criteria := map[string]interface{}{
		"level = ?":      level,
		"skill_type = ?": skillType,
	}
	expTableSkill, err := uc.rp.FindExpTableSkill(ctx, nil, criteria)
	if err != nil {
		return nil, err
	}
	return expTableSkill, nil
}

package usecase

import (
	"context"
	"reflect"
	"testing"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

func Test_expTableFriendshipUsecase_GetNextExpTableFriendship(t *testing.T) {
	p := persistence.NewExpTableFriendshipRepositoryImpl(db)
	u := NewExpTableFriendshipUsecase(p)
	ctx := context.Background()

	tests := []struct {
		name       string
		currentExp uint64
		want       *model_exp_table.ExpTableFriendship
		wantErr    bool
	}{
		{
			name:       "get next exp table friendship returns level 1 at exp 0",
			currentExp: 0,
			want: &model_exp_table.ExpTableFriendship{
				Level:            1,
				NextExp:          300,
				TotalExp:         300,
				BoostPercentHp:   1,
				BoostPercentAtk:  1,
				BoostPercentMgc:  1,
				BoostPercentDef:  1,
				BoostPercentMDef: 1,
				BoostPercentSpd:  1,
				BoostPercentLuck: 1,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table friendship returns level 2 at exp 301",
			currentExp: 301,
			want: &model_exp_table.ExpTableFriendship{
				Level:            2,
				NextExp:          2500,
				TotalExp:         2800,
				BoostPercentHp:   2,
				BoostPercentAtk:  2,
				BoostPercentMgc:  2,
				BoostPercentDef:  2,
				BoostPercentMDef: 2,
				BoostPercentSpd:  2,
				BoostPercentLuck: 1,
			},
			wantErr: false,
		},
		{
			name:       "get next exp table friendship returns level 7 at exp 257801",
			currentExp: 257801,
			want: &model_exp_table.ExpTableFriendship{
				Level:            7,
				NextExp:          999999,
				TotalExp:         1257799,
				BoostPercentHp:   15,
				BoostPercentAtk:  15,
				BoostPercentMgc:  15,
				BoostPercentDef:  10,
				BoostPercentMDef: 10,
				BoostPercentSpd:  4,
				BoostPercentLuck: 1,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := u.GetNextExpTableFriendship(ctx, tt.currentExp)
			if (err != nil) != tt.wantErr {
				t.Errorf("expTableFriendshipUsecase.GetNextExpTableFriendship() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("expTableFriendshipUsecase.GetNextExpTableFriendship() = %v, want %v", got, tt.want)
			}
		})
	}
}

package usecase

import (
	"context"

	model_trade "gitlab.com/kirafan/sparkle/server/internal/domain/model/trade"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type TradeRecipeUsecase interface {
	GetTradeRecipes(ctx context.Context) ([]model_trade.TradeRecipe, error)
}

type tradeRecipeUsecase struct {
	tradeRecipeRepo repository.TradeRecipeRepository
}

func NewTradeRecipeUsecase(tradeRecipeRepo repository.TradeRecipeRepository) TradeRecipeUsecase {
	return &tradeRecipeUsecase{tradeRecipeRepo}
}

func (tu *tradeRecipeUsecase) GetTradeRecipes(ctx context.Context) ([]model_trade.TradeRecipe, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetTradeRecipes")
	defer span.End()

	trades, err := tu.tradeRecipeRepo.GetTradeRecipes(ctx)
	if err != nil {
		return nil, err
	}
	return trades, nil
}

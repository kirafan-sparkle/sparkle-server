package usecase

import (
	"context"

	model_evo_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/evo_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type EvoTableLimitBreakUsecase interface {
	GetLimitBreakRecipe(ctx context.Context, recipeId uint) (*model_evo_table.EvoTableLimitBreak, error)
}

type evoTableLimitBreakUsecase struct {
	rp repository.EvoTableLimitBreakRepository
}

func NewEvoTableLimitBreakUsecase(rp repository.EvoTableLimitBreakRepository) EvoTableLimitBreakUsecase {
	return &evoTableLimitBreakUsecase{rp}
}

func (u *evoTableLimitBreakUsecase) GetLimitBreakRecipe(ctx context.Context, recipeId uint) (*model_evo_table.EvoTableLimitBreak, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetLimitBreakRecipe")
	defer span.End()
	res, err := u.rp.FindByRecipeId(ctx, recipeId)
	if err != nil {
		return nil, err
	}
	return res, nil
}

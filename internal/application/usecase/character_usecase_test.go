package usecase

import (
	"context"
	"testing"

	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

func Test_characterUsecase_GetAll(t *testing.T) {
	characterRepository := persistence.NewCharacterRepositoryImpl(db)
	characterUsecase := NewCharacterUsecase(characterRepository)
	characterKfcn5Id, _ := value_character.NewCharacterId(30012000)
	characterKfcn4Id, _ := value_character.NewCharacterId(30011000)
	ctx := context.Background()

	tests := []struct {
		name        string
		characterId value_character.CharacterId
		namedType   uint16
		err         error
	}{
		{
			name:        "GetKfcn5 namedType success",
			characterId: characterKfcn5Id,
			namedType:   117,
			err:         nil,
		},
		{
			name:        "GetKfcn4 namedType success",
			characterId: characterKfcn4Id,
			namedType:   117,
			err:         nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := characterUsecase.GetCharacterById(ctx, tt.characterId)
			if err != nil {
				t.Errorf("characterUsecase.GetCharacterById(characterId) error = %v, wantErr nil", err)
				return
			}
			if got.NamedType != tt.namedType {
				t.Errorf("characterUsecase.GetCharacterById(characterId) namedType = %v, wantNamedType = %v", got.NamedType, tt.namedType)
				return
			}
		})
	}
}

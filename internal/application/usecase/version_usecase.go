package usecase

import (
	"context"
	"errors"

	model_version "gitlab.com/kirafan/sparkle/server/internal/domain/model/version"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_version "gitlab.com/kirafan/sparkle/server/internal/domain/value/version"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type VersionUsecase interface {
	FindByPlatformAndVersion(ctx context.Context, platform value_version.Platform, version value_version.Version) (*model_version.Version, error)
}

type versionUsecase struct {
	versionRepo repository.VersionRepository
}

func NewVersionUsecase(versionRepo repository.VersionRepository) VersionUsecase {
	return &versionUsecase{versionRepo: versionRepo}
}

func (vu *versionUsecase) FindByPlatformAndVersion(ctx context.Context, platform value_version.Platform, version value_version.Version) (*model_version.Version, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "FindByPlatformAndVersion")
	defer span.End()

	existedVersion, err := vu.versionRepo.FindByPlatformAndVersion(ctx, platform, version)
	if err != nil {
		return nil, errors.New("version not supported")
	}
	return existedVersion, nil
}

package usecase

import (
	"context"

	model_ability_board "gitlab.com/kirafan/sparkle/server/internal/domain/model/ability_board"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_ability_board "gitlab.com/kirafan/sparkle/server/internal/domain/value/ability_board"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type AbilityBoardUsecase interface {
	GetAbilityBoardByItemId(ctx context.Context, itemId value_item.ItemId) (*model_ability_board.AbilityBoard, error)
	GetAbilityBoard(ctx context.Context, abilityBoardId value_ability_board.AbilityBoardId) (*model_ability_board.AbilityBoard, error)
	GetAbilityBoardSlot(
		ctx context.Context,
		abilityBoardId value_ability_board.AbilityBoardId,
		slotIndex value_ability_board.AbilityBoardSlotIndex,
	) (*model_ability_board.AbilityBoardSlot, error)
	GetAbilityBoardSlotRecipe(ctx context.Context, abilityBoardSlotRecipeId value_ability_board.AbilityBoardSlotRecipeId) (*model_ability_board.AbilityBoardSlotRecipe, error)
}

type abilityBoardUsecase struct {
	rp repository.AbilityBoardRepository
}

func NewAbilityBoardUsecase(rp repository.AbilityBoardRepository) AbilityBoardUsecase {
	return &abilityBoardUsecase{rp}
}

func (u *abilityBoardUsecase) GetAbilityBoardByItemId(ctx context.Context, itemId value_item.ItemId) (*model_ability_board.AbilityBoard, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetAbilityBoardByItemId")
	defer span.End()

	abilityBoard, err := u.rp.FindAbilityBoardByItemId(ctx, itemId)
	if err != nil {
		return nil, err
	}
	return abilityBoard, nil
}

func (u *abilityBoardUsecase) GetAbilityBoard(ctx context.Context, abilityBoardId value_ability_board.AbilityBoardId) (*model_ability_board.AbilityBoard, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetAbilityBoard")
	defer span.End()

	abilityBoard, err := u.rp.FindAbilityBoard(ctx, abilityBoardId)
	if err != nil {
		return nil, err
	}
	return abilityBoard, nil
}

func (u *abilityBoardUsecase) GetAbilityBoardSlot(
	ctx context.Context,
	abilityBoardId value_ability_board.AbilityBoardId,
	slotIndex value_ability_board.AbilityBoardSlotIndex,
) (*model_ability_board.AbilityBoardSlot, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetAbilityBoardSlot")
	defer span.End()

	abilityBoardSlot, err := u.rp.FindAbilityBoardSlot(ctx, abilityBoardId, slotIndex)
	if err != nil {
		return nil, err
	}
	return abilityBoardSlot, nil
}

func (u *abilityBoardUsecase) GetAbilityBoardSlotRecipe(ctx context.Context, abilityBoardSlotRecipeId value_ability_board.AbilityBoardSlotRecipeId) (*model_ability_board.AbilityBoardSlotRecipe, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetAbilityBoardSlotRecipe")
	defer span.End()

	abilityBoardSlotRecipe, err := u.rp.FindAbilityBoardSlotRecipe(ctx, abilityBoardSlotRecipeId)
	if err != nil {
		return nil, err
	}
	return abilityBoardSlotRecipe, nil
}

package usecase

import (
	"context"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type ExpTableCharacterUsecase interface {
	GetNextExpTableCharacter(ctx context.Context, currentExp uint64) (*model_exp_table.ExpTableCharacter, error)
	GetRequiredCoinsForUpgrade(ctx context.Context, currentLevel value_character.CharacterLevel) (value_user.Gold, error)
	GetLevelExpTableCharacter(ctx context.Context, level value_character.CharacterLevel) (*model_exp_table.ExpTableCharacter, error)
}

type expTableCharacterUsecase struct {
	rp repository.ExpTableCharacterRepository
}

func NewExpTableCharacterUsecase(rp repository.ExpTableCharacterRepository) ExpTableCharacterUsecase {
	return &expTableCharacterUsecase{rp}
}

func (uc *expTableCharacterUsecase) GetNextExpTableCharacter(ctx context.Context, currentExp uint64) (*model_exp_table.ExpTableCharacter, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetNextExpTableCharacter")
	defer span.End()
	criteria := map[string]interface{}{
		"total_exp > ?": currentExp,
	}
	expTableRank, err := uc.rp.FindExpTableCharacter(ctx, nil, criteria)
	if err != nil {
		return nil, err
	}
	return expTableRank, nil
}

func (uc *expTableCharacterUsecase) GetRequiredCoinsForUpgrade(ctx context.Context, currentLevel value_character.CharacterLevel) (value_user.Gold, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetRequiredCoinsForUpgrade")
	defer span.End()
	coins, err := uc.rp.GetRequiredCoinsForUpgrade(ctx, currentLevel)
	if err != nil {
		return 0, err
	}
	golds := value_user.NewGold(uint64(coins))
	return golds, nil
}

func (uc *expTableCharacterUsecase) GetLevelExpTableCharacter(ctx context.Context, level value_character.CharacterLevel) (*model_exp_table.ExpTableCharacter, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetLevelExpTableCharacter")
	defer span.End()
	criteria := map[string]interface{}{
		"level = ?": level,
	}
	expTableRank, err := uc.rp.FindExpTableCharacter(ctx, nil, criteria)
	if err != nil {
		return nil, err
	}
	return expTableRank, nil
}

package usecase

import (
	"context"

	model_level_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/level_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_town_facility "gitlab.com/kirafan/sparkle/server/internal/domain/value/town_facility"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type LevelTableTownFacilityUsecase interface {
	GetCurrentLevelTableTownFacility(ctx context.Context, levelUpListId value_town_facility.LevelUpListId, currentLevel uint8) (*model_level_table.LevelTableTownFacility, error)
	GetNextLevelTableTownFacility(ctx context.Context, levelUpListId value_town_facility.LevelUpListId, currentLevel uint8) (*model_level_table.LevelTableTownFacility, error)
}

type levelTableTownFacilityUsecase struct {
	rp repository.LevelTableTownFacilityRepository
}

func NewLevelTableTownFacilityUsecase(rp repository.LevelTableTownFacilityRepository) LevelTableTownFacilityUsecase {
	return &levelTableTownFacilityUsecase{rp}
}

func (uc *levelTableTownFacilityUsecase) GetCurrentLevelTableTownFacility(ctx context.Context, levelUpListId value_town_facility.LevelUpListId, currentLevel uint8) (*model_level_table.LevelTableTownFacility, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetCurrentLevelTableTownFacility")
	defer span.End()
	levelTableTownFacility, err := uc.rp.FindLevelTableTownFacility(ctx, &model_level_table.LevelTableTownFacility{
		LevelUpListId: levelUpListId,
		TargetLevel:   currentLevel,
	})
	if err != nil {
		return nil, err
	}
	return levelTableTownFacility, nil
}

func (uc *levelTableTownFacilityUsecase) GetNextLevelTableTownFacility(ctx context.Context, levelUpListId value_town_facility.LevelUpListId, currentLevel uint8) (*model_level_table.LevelTableTownFacility, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetNextLevelTableTownFacility")
	defer span.End()
	levelTableTownFacility, err := uc.rp.FindLevelTableTownFacility(ctx, &model_level_table.LevelTableTownFacility{
		LevelUpListId: levelUpListId,
		TargetLevel:   currentLevel + 1,
	})
	if err != nil {
		return nil, err
	}
	return levelTableTownFacility, nil
}

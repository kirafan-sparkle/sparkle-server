package usecase

import (
	"context"

	model_trade "gitlab.com/kirafan/sparkle/server/internal/domain/model/trade"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type PackageItemUsecase interface {
	GetPackageItemById(ctx context.Context, packageItemId uint) (*model_trade.PackageItem, error)
	GetPackageItems(ctx context.Context) ([]*model_trade.PackageItem, error)
}

type packageItemUsecase struct {
	rp repository.PackageItemRepository
}

func NewPackageItemUsecase(rp repository.PackageItemRepository) PackageItemUsecase {
	return &packageItemUsecase{rp}
}

func (uc *packageItemUsecase) GetPackageItemById(ctx context.Context, packageItemId uint) (*model_trade.PackageItem, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetPackageItemById")
	defer span.End()
	packageItem, err := uc.rp.FindByPK(ctx, packageItemId)
	if err != nil {
		span.RecordError(err)
		return nil, err
	}
	return packageItem, nil
}

func (uc *packageItemUsecase) GetPackageItems(ctx context.Context) ([]*model_trade.PackageItem, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetPackageItems")
	defer span.End()
	packageItems, err := uc.rp.GetAll(ctx)
	if err != nil {
		span.RecordError(err)
		return nil, err
	}
	return packageItems, nil
}

package usecase

import (
	"context"

	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	value_exp "gitlab.com/kirafan/sparkle/server/internal/domain/value/exp"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/observability"
)

type ExpTableWeaponUsecase interface {
	GetNextExpTableWeapon(ctx context.Context, currentExp value_exp.WeaponExp, weaponType uint8) (*model_exp_table.ExpTableWeapon, error)
	GetRequiredCoinsForUpgrade(ctx context.Context, currentLevel uint8) (value_user.Gold, error)
	GetLevelExpTableWeapon(ctx context.Context, level uint8, weaponType uint8) (*model_exp_table.ExpTableWeapon, error)
}

type expTableWeaponUsecase struct {
	rp repository.ExpTableWeaponRepository
}

func NewExpTableWeaponUsecase(rp repository.ExpTableWeaponRepository) ExpTableWeaponUsecase {
	return &expTableWeaponUsecase{rp}
}

func (uc *expTableWeaponUsecase) GetNextExpTableWeapon(ctx context.Context, currentExp value_exp.WeaponExp, weaponType uint8) (*model_exp_table.ExpTableWeapon, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetNextExpTableWeapon")
	defer span.End()
	criteria := map[string]interface{}{
		"total_exp > ?":   currentExp,
		"weapon_type = ?": weaponType,
	}
	expTableWeapon, err := uc.rp.FindExpTableWeapon(ctx, nil, criteria)
	if err != nil {
		return nil, err
	}
	return expTableWeapon, nil
}

func (uc *expTableWeaponUsecase) GetRequiredCoinsForUpgrade(ctx context.Context, currentLevel uint8) (value_user.Gold, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetRequiredCoinsForUpgrade")
	defer span.End()
	coins, err := uc.rp.GetRequiredCoinsForUpgrade(ctx, currentLevel)
	if err != nil {
		return 0, err
	}
	golds := value_user.NewGold(uint64(coins))
	return golds, nil
}

func (uc *expTableWeaponUsecase) GetLevelExpTableWeapon(ctx context.Context, level uint8, weaponType uint8) (*model_exp_table.ExpTableWeapon, error) {
	ctx, span := observability.Tracer.StartUsecaseSpan(ctx, "GetLevelExpTableWeapon")
	defer span.End()
	criteria := map[string]interface{}{
		"level = ?":       level,
		"weapon_type = ?": weaponType,
	}
	expTableWeapon, err := uc.rp.FindExpTableWeapon(ctx, nil, criteria)
	if err != nil {
		return nil, err
	}
	return expTableWeapon, nil
}

package schema_weapon

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
)

type SkillUpWeaponResponseSchema struct {
	ManagedWeapon model_user.ManagedWeapon
	ItemSummary   []model_user.ItemSummary
}

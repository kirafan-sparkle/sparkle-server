package schema_weapon

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type LimitBreakWeaponResponseSchema struct {
	ManagedWeapon model_user.ManagedWeapon
	ItemSummary   []model_user.ItemSummary
	Gold          value_user.Gold
}

package schema_training

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
)

type CancelTrainingResponseSchema struct {
	BasePlayer   model_user.User
	SlotInfo     []*model_user.UserTrainingSlot
	TrainingInfo []*model_user.UserTraining
}

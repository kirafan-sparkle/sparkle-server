package schema_training

import "gitlab.com/kirafan/sparkle/server/internal/domain/value"

type CompletesTrainingRequestPartSchema struct {
	OrderId uint
	SkipGem value.BoolLikeUInt8
}

type CompletesTrainingRequestSchema []CompletesTrainingRequestPartSchema

package schema_training

import (
	value_training "gitlab.com/kirafan/sparkle/server/internal/domain/value/training"
	value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"
)

type OrdersTrainingRequestPartSchema struct {
	ManagedCharacterIds []value_user.ManagedCharacterId
	SlotId              value_training.SlotId
	TrainingId          uint
}

type OrdersTrainingRequestSchema []OrdersTrainingRequestPartSchema

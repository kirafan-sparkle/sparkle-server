package schema_training

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
	value_training "gitlab.com/kirafan/sparkle/server/internal/domain/value/training"
)

type CompletesTrainingResponseRewardItemSchema struct {
	ItemId     value_item.ItemId
	ItemAmount int64
	RareDisp   int64
}

type CompletesTrainingResponseRewardSchema struct {
	SlotId              value_training.SlotId
	FirstGem            uint32
	RewardCharaExp      uint64
	RewardFriendship    uint64
	RewardGold          uint64
	RewardKRRPoint      uint64
	TrainingRewardItems []CompletesTrainingResponseRewardItemSchema
}

type CompletesTrainingResponseSchema struct {
	BasePlayer        model_user.User
	SlotInfo          []*model_user.UserTrainingSlot
	TrainingInfo      []*model_user.UserTraining
	ItemSummary       []model_user.ItemSummary
	ManagedCharacters []model_user.ManagedCharacter
	ManagedNamedTypes []model_user.ManagedNamedType
	Rewards           []CompletesTrainingResponseRewardSchema
}

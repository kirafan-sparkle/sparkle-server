package schema_battle_party

type BattlePartyMembers struct {
	ManagedBattlePartyId int64
	ManagedCharacterIds  []int64
	ManagedWeaponIds     []int64
	MasterOrbId          int64
}

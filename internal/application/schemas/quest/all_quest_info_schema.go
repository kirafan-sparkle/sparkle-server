package schema_quest

import (
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
)

type AllQuestInfoWithClearRanksSchema struct {
	QuestPart1s       []*model_quest.Quest
	QuestPart2s       []*model_quest.Quest
	EventQuests       []*model_quest.EventQuest
	EventQuestPeriods []*model_quest.EventQuestPeriod
	CharacterQuests   []model_quest.CharacterQuest
	// TODO: Implement craft quest model
	CraftQuests []*interface{}
	// TODO: Implement player offer quest model
	PlayerOfferQuests             []*interface{}
	ClearRanks                    model_user.UserQuestClearRanks
	LastPlayedChapterQuestIdPart1 int32
	LastPlayedChapterQuestIdPart2 int32
	PlayedOpenChapterIdPart1      int8
	PlayedOpenChapterIdPart2      int8
}

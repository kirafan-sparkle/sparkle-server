package schema_gacha

import (
	value_character "gitlab.com/kirafan/sparkle/server/internal/domain/value/character"
	value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"
)

type GachaResult struct {
	CharacterId value_character.CharacterId
	ItemId      value_item.ItemId
	ItemAmount  uint32
}

package schema_schedule

import value_user "gitlab.com/kirafan/sparkle/server/internal/domain/value/user"

type ScheduleTargetObject struct {
	RoomId uint8

	LiveIdx uint8

	ManagedCharacterId value_user.ManagedCharacterId

	ManagedPartyMemberId uint
}

type ScheduleTargetSchema []ScheduleTargetObject

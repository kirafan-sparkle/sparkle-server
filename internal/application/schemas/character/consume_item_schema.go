package schema_character

import value_item "gitlab.com/kirafan/sparkle/server/internal/domain/value/item"

type ConsumeItem struct {
	ItemId value_item.ItemId
	Count  uint16
}

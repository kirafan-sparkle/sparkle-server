import os

import glob
import re


template = """# Database seeding
This api supports database seeding.
We heavily recommend to use this feature before initial api startup.
(It is almost impossible to play with write them by hands.)

## How to use
- 1 Make a `assets` folder at correct place.
  - For build from source (`go run cmd/main/main.go`)
    - `cmd/main/assets`
      - same folder as `main.go`
  - For prebuilt binaries
    - `assets`
      - same folder as binary
- 2 Put seed files into `assets` folder.
- 3 Change environment values (`.env` file) as below.
  - `SEED_ENABLED: true`
  - `SEED_SOURCE: local`
- 4 Startup this api, it automatically runs database seeding at launch.

## Supported asset files
Currently, below seed files are supported.

"""


# 指定したパターンに一致するファイル名のリストを取得します。
def get_file_list(directory: str, extension: str) -> list[str]:
    return glob.glob(os.path.join(directory, extension))


# 指定したパターンに一致するテキストを抜き出します。
def extract_matched_texts(lines: list[str], pattern: re.Pattern) -> list[str]:
    matched_texts: list[str] = []
    for line in lines:
        match = pattern.search(line)
        if not match:
            continue
        matched_texts.append(match.group(1))
    return matched_texts


# 指定したパターンに一致するファイル名のリストを取得します。
def extract_matched_texts_from_files(
    file_list: list[str],
    pattern: re.Pattern,
) -> list[str]:
    matched_texts: list[str] = []
    for file in file_list:
        with open(file, 'r', encoding="utf8") as f:
            lines = f.readlines()
            matched_texts += extract_matched_texts(lines, pattern)
    return matched_texts


def main():
    # ファイルを読み込むディレクトリを指定します。
    in_directory = '../../internal/infrastructure/database/seed'
    out_directory = '../../docs/DATABASE_SEEDING.md'

    file_list = get_file_list(in_directory, '*.go')
    pattern = re.compile(r'Read\("(.+)", conf\)')
    matched_texts = extract_matched_texts_from_files(file_list, pattern)
    matched_texts = list(set(matched_texts))
    matched_texts.sort()

    with open(out_directory, 'w', encoding="utf8") as f:
        f.write(template)
        for text in matched_texts:
            f.write(f'- {text}.json\n')


if __name__ == '__main__':
    main()
